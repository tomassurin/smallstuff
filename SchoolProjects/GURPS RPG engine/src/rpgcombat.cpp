#include "gameobjects.h"
#include "rpgcombat.h"
#include "rpgcampaign.h"

using namespace RPG;

/* CombatCharacter */
CombatCharacter::CombatCharacter()
{
	character = NULL;
	maneuver = DO_NOTHING;
	lastAttacker = NULL;
	target = NULL;
}

bool CombatCharacter::isValid()
{
	return character != NULL;
}

void CombatCharacter::clear()
{
	if (character != NULL)
		character->setInCombat(false);
	character = NULL;
	lastAttacker = NULL;
	maneuver = DO_NOTHING;
	target = NULL;
}

void CombatCharacter::clearManeuver()
{
	maneuver = DO_NOTHING;
	target = NULL;
}

void CombatCharacter::setCharacter(Character *ch)
{ 
	character = ch; 
	if (character != NULL)
		character->setInCombat(true);
	maneuver = DO_NOTHING;
}

QPoint CombatCharacter::getPos() const
{
	if (character == NULL) return QPoint(-1,-1);
	return character->location()->getPos();
}

void CombatCharacter::setManeuver(CombatManeuver man, CombatCharacter *enemy)
{
	setManeuver(man);
	setTarget(enemy);
}

void CombatCharacter::setTarget(CombatCharacter *enemy)
{
	target = enemy;
}

bool CombatCharacter::isAlive() const
{
	return character->isAlive();
}

/* class RPGCombat */
RPGCombat::RPGCombat() : QObject()
{
	player = NULL;
}

RPGCombat::~RPGCombat()
{
	turnSequence.clear();
}

void RPGCombat::addCharacter(CombatCharacter *character)
{
	if (character== NULL) return;
	Character *ch = character->getCharacter();
	if (ch == NULL) return;
	if (typeid(*ch) == typeid(PlayerNode)) {
		player = character;
	}
	if (typeid(*ch) == typeid(Monster)) {
		ch->replenishMovePt();
		ch->processTurnEvents();
	}
	/* zaradenie do usporiadania */

	double chspeed = ch->getBasicSpeed();
	turnSequenceIterator it;
	for(it = turnSequence.begin();it != turnSequence.end();++it) {
		if ((*it)->character->getBasicSpeed() < chspeed) continue;
		if ((*it)->character->getBasicSpeed() > chspeed) break;
		if ((*it)->character->getBasicSpeed() == chspeed) {
			while(it != turnSequence.end() && (*it)->character->getBasicSpeed() == chspeed) {
				Die react;
				react.setDie(1,2,0);
				if (react.throwDie()==1) {
					// hod sa podaril (vzhladom k hracovi ch)
					break;
				}
				else {
					++it;
					continue;
				}
			}
			break;
		}
	}
	ch->setInCombat(true);
	turnSequence.insert(it,character);
}

int RPGCombat::turnSequenceSize()
{
	return turnSequence.size();
}

Character *RPGCombat::turnSequenceAt(int ind)
{
	if (ind < turnSequence.size()) {
		return turnSequence.at(ind)->getCharacter();
	}
	else 
		return NULL;
}

CombatCharacter *RPGCombat::getCharacter(Character *ch)
{
	turnSequenceIterator it;
	for(it=turnSequence.begin();it != turnSequence.end();++it) {
		if ((*it)->character== ch) return *it;
	}
	return NULL;
}

bool RPGCombat::isInCombat(Character *ch)
{
	turnSequenceIterator it;
	for(it=turnSequence.begin();it != turnSequence.end();++it) {
		if ((*it)->getCharacter() == ch) return true;
	}
	return false;
}

bool RPGCombat::isRunning() const 
{
	return turnSequence.size() >= 2;
}

void RPGCombat::tryEndCombat()
{
  	foreach(CombatCharacter *ch, turnSequence) {
		if (ch == player) continue;
		if (ch == NULL) continue;
		Character *character = ch->getCharacter();
		if (character == NULL) continue;
		if (character->isAlive()) continue;
		removeDeadCharacter(ch);
	}

	if (turnSequence.isEmpty()) {
		if (player != NULL) {
			player->clear();
			delete player;
		}
		player = NULL;
		return;
	}
	// zo suboja odstranime poslednych hracov
	else if (turnSequence.size() < 2) {
		foreach(CombatCharacter *ch, turnSequence) {
			if (ch == NULL)
				continue;
			ch->clear();
			delete ch;
		}
		turnSequence.clear();
		player = NULL;
	}
}

void RPGCombat::clear() 
{
	foreach(CombatCharacter *ch, turnSequence) {
		ch->clear();
		delete ch;
	}
	turnSequence.clear();
	player = NULL;
}

void RPGCombat::processTurnBeforePlayer()
{
	foreach(CombatCharacter *ch, turnSequence) {
		if (ch == player) return;
		processTurn(ch);
	}
	tryEndCombat();
}

void RPGCombat::processTurnAfterPlayer()
{
	turnSequenceIterator it;
	for(it = turnSequence.begin();it != turnSequence.end(); ++it) {
		if (*it == player) break;
	}
	
	if (it != turnSequence.end()) ++it;
	else return;

	for(;it != turnSequence.end();++it) {
		processTurn(*it);
	}
	tryEndCombat();
}

void RPGCombat::processTurn(CombatCharacter *ch) 
{
	Monster *monster;
	CombatCharacter *tmp = NULL;
	CombatManeuver man;
	if (ch == NULL) return;
	monster = dynamic_cast<Monster*>(ch->getCharacter());
	if (monster == NULL) return;
	if (monster->getMovePt() == 0) {
		monster->replenishMovePt();
		monster->processTurnEvents();
	}
	if (!monster->isAlive()) {
		if (ch->lastAttacker != NULL) {
			winnerAction(ch->lastAttacker,ch);
		}
		return;
	}
	bool moved = false;
	while (monster->getMovePt() > 0) {
		ch->clearManeuver();
		// ziskame manever od postavy
		man = monster->decideCombatManeuver();
		if (man == MOVE) {
			if (moved)
				break;
			moved = true;
			continue;
		}
		ch->setManeuver(man);
		// ak je utocny zistime na koho chce utocit postava
		if (man == ATTACK || man == ATTACK_STRONG || 
			man == ATTACK_DETERMINED || man == ATTACK_DOUBLE) {
			tmp = getCharacter(monster->decideEnemyToAttack());
			if (tmp == NULL || !tmp->isAlive()) {
				man = DO_NOTHING;
				ch->setManeuver(DO_NOTHING);
				ch->setTarget(NULL);
			}
			else 
				ch->setTarget(tmp);
		}
		if (man == OTHERMAN)
			break;
		// vykoname manever
		executeManeuver(ch);
		// minieme move pt postavy ktore trebalo na vykonanie manevra
		int cost = rules.getActionCost(man);
		if (monster->getMovePt() <= cost) {
			monster->spendMovePt(INT_MAX);
			break;
		}
		monster->spendMovePt(cost);
	}
}

void RPGCombat::removeDeadCharacter(CombatCharacter *ch)
{
	Character *character = ch->getCharacter();
	if (character == NULL) return;
	campaign->writeStatus(tr("<b>%1</b> died").arg(character->getName()));
	turnSequence.removeOne(ch);
	if (ch == player) {
		campaign->clearPlayer();
	}
	else {
		character->release();
	}	
	delete ch;
}

void RPGCombat::executeManeuver(CombatCharacter *ch)
{
	if (ch == NULL) return;
	CombatResult result;
	CombatCharacter *enemy;
	switch(ch->getManeuver()) {
		case ATTACK_DOUBLE:
			enemy = ch->getTarget();
			if (enemy == NULL) {
				return;
			}
			result = attackSequence(ch, enemy);
			if (result == WINNER) {
				winnerAction(ch,enemy);
				return;
			}
			result = attackSequence(ch, enemy);
			if (result == WINNER) {
				winnerAction(ch,enemy);
				return;
			}
			break;
		case ATTACK:
		case ATTACK_DETERMINED:
		case ATTACK_STRONG:
			enemy = ch->getTarget();
			if (enemy == NULL) {
				return;
			}
			result = attackSequence(ch, enemy);
			if (result == WINNER) {
				winnerAction(ch,enemy);
				return;
			}
			break;
		default: {}
	}
	ch->clearManeuver();
}

void RPGCombat::winnerAction(CombatCharacter *winner, CombatCharacter *looser)
{
	if ( !turnSequence.contains(winner) || !turnSequence.contains(looser))  {
		return;
	}
	Monster *mon = dynamic_cast<Monster*>(looser->getCharacter());
	if (mon != NULL) {
		winner->getCharacter()->earnXP(mon->getXPGain());
		mon->killedBy(winner->getCharacter());
		return;
	}
	PlayerNode *pl = dynamic_cast<PlayerNode*>(looser->getCharacter());
	if (pl != NULL) {
		campaign->writeStatus("GAME OVER, You died");
	}
}

CombatResult RPGCombat::attackSequence(CombatCharacter *attacker, CombatCharacter *defender)
{
	Character *att = attacker->getCharacter();
	Character *def = defender->getCharacter();
	if (att == NULL || def == NULL) return ERROR_COMBAT_RESULT;
	campaign->writeStatus(tr("------------------"));
	campaign->writeStatus(tr("<b>%1</b>: Attacking <b>%2</b>").arg(att->getName()).arg(def->getName()));
	// hod na utok
	switch (attackRoll(attacker,defender)) {
			// utocnik netrafil
			case FAILURE:
			case CRITICALFAILURE:	
				campaign->writeStatus(tr("<b>%1</b>: Missed").arg(att->getName()));
				break;
			// utocnik trafil
			case SUCCESS:
				campaign->writeStatus(tr("<b>%1</b>: Hit").arg(att->getName()));
				// hod na obranu obrancu
				if (defenseRoll(defender)) {
					// obranca sa ubranil
					campaign->writeStatus(tr("<b>%1</b>: Defended").arg(def->getName()));
					break;
				}
				// obranca sa neubranil => utocnik hadze na zranenie obrancu
				damageRoll(attacker,defender);
				// nastavime ktora posledna postava zasiahla obrancu
				defender->lastAttacker = attacker;
				break;
			// utocnik zasiahol obrancu kriticky
			case CRITICALSUCCESS: 
				campaign->writeStatus(tr("<b>%1</b>: Critical Hit").arg(att->getName()));
				// utocnik nehadze na zranenie namiesto toho pouzije maximalne zranenie
				damageRoll(attacker,defender,true);
				defender->lastAttacker = attacker;
				break;
			default: return ERROR_COMBAT_RESULT;
	}
	if (def->isAlive()) return CONTINUE;
	else return WINNER;
}

SuccessRollResult RPGCombat::attackRoll(CombatCharacter *attacker, CombatCharacter *defender)
{
	Character *ch = attacker->getCharacter();
	Weapon *wp = ch->getActiveWeapon();
	if (wp == NULL) return ERROR_RESULT;
	Skill *skill;
	skill=ch->getActiveWeapon()->getEffectiveSkill();
	if (skill == NULL) return ERROR_RESULT;
	SuccessRoll roll;
	// modifikatory hodu na zasah
	int modif = skill->otherMod + defender->getCharacter()->getSizeMod();
	
	if (attacker->getManeuver() == ATTACK_DETERMINED)
		modif +=rules.determinedAttackMod;

	roll.setRoll(ch,skill,modif);
	return roll.roll();
}

bool RPGCombat::defenseRoll(CombatCharacter *defender)
{
	Character *def = defender->getCharacter();
	int defenseScore = def->getDefenseDodge();
	if (defender->getManeuver() == DEFENSE_STRONG)
		defenseScore += rules.strongDefenseMod;

	SuccessRoll roll;
	roll.setRoll(defenseScore);

	if (defender->getManeuver() == DEFENSE_DOUBLE) {	
		switch (roll.roll()) {
			case FAILURE:
			case CRITICALFAILURE:	
				switch (roll.roll()) {
					case FAILURE:
					case CRITICALFAILURE:	
						return false;
						break;
					case SUCCESS:
					case CRITICALSUCCESS: 
						return true;
						break;
					default: return false;
				}		
				break;
			case SUCCESS:
			case CRITICALSUCCESS: 
				return true;
				break;
			default: return false;
		}
		return false;
	}

	switch (roll.roll()) {
			case FAILURE:
			case CRITICALFAILURE:	
				return false;
				break;
			case SUCCESS:
			case CRITICALSUCCESS: 
				return true;
				break;
			default: return false;
	}
	return false;
}

void RPGCombat::damageRoll(CombatCharacter *attacker, CombatCharacter *defender, bool maximum)
{
	Character *att = attacker->getCharacter();
	Character *def = defender->getCharacter();
	Weapon *wp = att->getActiveWeapon();
	if (wp == NULL) return;

	int damage;
	Die dmg=wp->getDamageDie();
	if (maximum) 
		damage=dmg.getMaxVal();
	else 
		damage=dmg.throwDie();
	
	damage = damage - def->getDR();
	if (damage > 0) {
		if (dmg.dmgType < 8) 
			damage = damage*rules.dmgMods[dmg.dmgType];
	}
	else damage = 0;

	if (attacker->getManeuver() == ATTACK_STRONG) 
		damage += rules.strongAttackMod;

	if (damage<0) damage=0;
	def->inflictDmg(damage);
	campaign->writeStatus(tr("<b>%1</b>: Inflicted %2 dmg").arg(att->getName()).arg(damage));
}