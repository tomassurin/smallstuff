/******************************************************************************
 * Implementacia suboja
******************************************************************************/
#ifndef RPGCOMBAT_H
#define RPGCOMBAT_H

#include <QGraphicsItem>
#include <QGraphicsScene> 

#include "gameobjects.h"

namespace RPG {

	// dopredne deklaracie 
	class Campaign;

	/** Trieda reprezentujuca ucastnika suboja */
	class CombatCharacter {
	public:
		CombatCharacter();

		/** Vymaze triedu */
		void clear();
		/** Vymaze nastaveny manever */
		void clearManeuver();
		/** Zisti ci je objekt spravny (tj. obsahuje vsetky povinne polozky */
		bool isValid();
		/** Nastavi postavu reprezentovanu tymto objektom */
		void setCharacter(Character *ch);
		/** Ziska postavu */
		inline Character *getCharacter() { return character; }	
		/** Nastavi manever */
		void setManeuver(CombatManeuver man) { maneuver = man; target = NULL; }
		/** Nastavi manever a nepriatela na ktoreho utocit */
		void setManeuver(CombatManeuver man, CombatCharacter *enemy);
		/** Ziska manever */
		inline CombatManeuver getManeuver() const { return maneuver; }
		/** Nastavi nepriatela na ktoreho utocit */
		void setTarget(CombatCharacter *ch);
		/** Ziska nepriatela na ktoreho utocit */
		CombatCharacter *getTarget() { return target; }
		/** Ziska poziciu postavy */
		QPoint getPos() const;
		/** Zisti ci je postava nazive */
		bool isAlive() const;

		// udava kto na postavu posledny utocil
		CombatCharacter *lastAttacker;

		friend class RPGCombat;
	public:
		// hrac reprezentovany tymto objektom
		Character *character;
		// manever
		CombatManeuver maneuver;
		// nepriatel na zautocenie
		CombatCharacter *target;
	};

	/** Trieda reprezentujuca boj */
	class RPGCombat : public QObject
	{
		Q_OBJECT
	public:
		RPGCombat();
		~RPGCombat();
		
		/** Vymazanie dat o boji */
		void clear();
		/** Vyskusame skoncit boj - vymazeme mrtve postavy ... */
		void tryEndCombat();
		
		/** Pridame do boja postavu */
		void addCharacter(CombatCharacter*);
		/** Ziskame postavu hraca ch */
		CombatCharacter *getCharacter(Character *ch);
		/** Ziskame postavu hraca */
		CombatCharacter *getPlayer() { return player; }
		/** Zistime ci hrac je v suboji */
		bool isInCombat(Character *ch);
		/** Zistime ci suboj bezi */
		bool isRunning() const;
		/** Vykoname manever postavy */
		void executeManeuver(CombatCharacter *ch);
		/** Vyhodnotime tahovu sekvenciu pred tahom hraca */
		void processTurnBeforePlayer();
		/** Vyhodnotime tahovu sekvenciu po tahu hraca */
		void processTurnAfterPlayer();
		/** Vyhodnotenie vitaza */
		void winnerAction(CombatCharacter *winner, CombatCharacter *looser);
		/** Vrati pocet ucastnikov suboja */
		int turnSequenceSize();
		/** Ziska ucastnika suboja na indexe ind */
		Character *turnSequenceAt(int ind);

	private:
		/** Vyhodnoti tah nepriatela */
		void processTurn(CombatCharacter *ch);
		/** Vymaze mrtveho protivnikov */
		void removeDeadCharacter(CombatCharacter *ch);
		
		/** Utocna sekvencia */
		CombatResult attackSequence(CombatCharacter *attacker, CombatCharacter *defender);
		/** Hod na utok */
		SuccessRollResult attackRoll(CombatCharacter *attacker, CombatCharacter *defender);
		/** Hod na obranu */
		bool defenseRoll(CombatCharacter *defender);
		/** Hod na zranenie */
		void damageRoll(CombatCharacter *attacker, CombatCharacter *defender, bool maximum = false);				

		// hrac
		CombatCharacter *player;
		// tahova sekvencia 
		QList<CombatCharacter*> turnSequence;
		typedef QList<CombatCharacter*>::iterator turnSequenceIterator;
	};
}

#endif