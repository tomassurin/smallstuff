#include <algorithm>
#include <QRegExp>

#include "rpgcharacter.h"
#include "triggerevent.h"
#include "rpgcampaign.h"

using namespace RPG;

/* class BodyLocation  */
BodyLocation::BodyLocation()
{
	hitMod=0;
	dmgMod=0;
	object=NULL;
	blockLocation = NULL;
	canAttack = false;
	blocking.clear();
}

void BodyLocation::setObject(InventoryObject *object)
{
	this->object=object;
}

InventoryObject *BodyLocation::getObject()
{
	return object;
}

void BodyLocation::setBlockLocations(const QStringList &block)
{
	blocking = block;
}

/* class Body */
Body::Body()
{
	id.clear();
	body.clear();
	activeWeaponLoc.clear();
}

void Body::addRPGBodyPart(BodyLocation loc)
{
	body[loc.getID()]=loc;
}

const QMap<QString,BodyLocation>& Body::getBodyParts() const
{
	return body;
}

QStringList Body::getAttackableParts() const
{
	QStringList out;
	foreach(BodyLocation loc, body) {
		if (loc.isAttackable())
			out.append(loc.getID());
	}
	return out;
}

int Body::getDamageResistance() const 
{
	int dr = 0;
	Armor *obj;
	foreach(BodyLocation loc, body) {
		obj = dynamic_cast<Armor*>(loc.getObject());
		if (obj == NULL) continue;
		dr += obj->getDR();
	}
	return dr;
}

BodyLocation *Body::getLocation(const QString &loc)
{
	if (body.contains(loc))
		return &body[loc];
	else return NULL;
}

void Body::setActiveWeaponLoc(const QString &loc) 
{
	if (getAttackableParts().contains(loc.toLower()))
		activeWeaponLoc = loc.toLower();
}

bool Body::equipObject(const QString &location, InventoryObject *object)
{
	BodyLocation *loc = getLocation(location);
	if (loc == NULL) return false;

	BodyLocation *tmp;
	if (loc->isBlocked()) {
		BodyLocation *block = loc->getBlockingLocation();
		InventoryObject *obj = block->getObject();
		// objekt nastavime do inventara
		if (obj!=NULL)
			obj->bodyLocation = "inventory";			
		block->setObject(NULL);
		foreach(QString str, block->getBlockLocations()) {
			tmp = getLocation(str);
			if (tmp == NULL) continue;
			tmp->setBlockingLocation(NULL);
		}
	}

	InventoryObject *obj = loc->getObject();
	if (obj!=NULL)
		obj->bodyLocation = "inventory";
	loc->setObject(object);
	foreach(QString str, loc->getBlockLocations()) {
		tmp = getLocation(str);
		if (tmp == NULL) continue;
		tmp->setBlockingLocation(loc);
		obj = tmp->getObject();
		if (obj != NULL) 
			obj->bodyLocation = "inventory";
		tmp->setObject(NULL);
	}
	return true;
}

/* class Character */
Character::Character() : RPGObject()
{
	name.clear();
	inventory=new Inventory();
	inventory->setOwner(this);
	body=rules.getRace("basic");
	connect(this,SIGNAL(statsInfoChanged()),this,SIGNAL(changed()));
	setInCombat(false);
	state = HEALTHY;
	BasicSpeedBonus = 0;
	BasicMoveBonus = 0;
	movePt = 0;
	basicWeapon = NULL;
	scoutRadius = 0;
	lastDeadThrow = 0;
	ST = 0;
	DX = 0;
	IQ = 0;
	HT = 0;
	HP = 1;
	cHP = 1;
	STbonus = 0;
	DXbonus = 0;
	IQbonus = 0;
	HTbonus = 0;
	HPbonus = 0;
	encumbrance = 0;
	unusedXP = 0;
	totalXP = 0;
	size = 2;
	setST(0);
	setDX(0);
	setIQ(0);
	setHT(0);
	setcHP(1);
	setHP(1);
	
	addBasicRules();
}
Character::Character(const Character& obj) : RPGObject(obj)
{
	connect(this,SIGNAL(statsInfoChanged()),this,SIGNAL(changed()));
	operator=(obj);
}

Character& Character::operator=(const Character& obj)
{
	name=obj.name;
	info=obj.info;
	body=obj.body;
	state=obj.state;
	inCombat = false;

	ST=obj.ST;
	DX=obj.DX;
	IQ=obj.IQ;
	HT=obj.HT;
	HP=obj.HP;
	cHP=obj.cHP;
	encumbrance = obj.encumbrance;
	size = obj.size;
	unusedXP = obj.unusedXP;
	totalXP = obj.totalXP;

	inventory=NULL;
	if (obj.inventory!=NULL) {
		inventory=new Inventory(*obj.inventory);
		inventory->setOwner(this);
	}

	BasicSpeed=obj.BasicSpeed;
	scoutRadius = obj.scoutRadius;
	inCombat = obj.inCombat;
	movePt = obj.movePt;
	lastDeadThrow = obj.lastDeadThrow;

	STbonus = obj.STbonus;
	DXbonus = obj.DXbonus;
	IQbonus = obj.IQbonus;
	HTbonus = obj.HTbonus;
	HPbonus = obj.HPbonus;
	BasicSpeedBonus = obj.BasicSpeedBonus;
	BasicMoveBonus = obj.BasicSpeedBonus;
		
	if (obj.basicWeapon != NULL) 
		basicWeapon  = new Weapon(*obj.basicWeapon);

	dmgThr=obj.dmgThr;
	dmgSw=obj.dmgSw;
	
	for(skills_iterator it=obj.skills.begin();it!=obj.skills.end();++it) 
		addSkill((*it)->getID(),(*it)->getLevel());

	updateSecStats();

	return *this;
}

void Character::addBasicRules()
{
	addSkill(tr("ST"),0);
	addSkill(tr("DX"),0);
	addSkill(tr("IQ"),0);
	addSkill(tr("HT"),0);
	Weapon *wp=new Weapon();
	wp->setName(tr("Punch"));
	wp->setDamage(tr("thr-1 cr"));
	wp->addSkill(tr("Brawling,Karate,DX"));
	wp->setOwner(this);
	basicWeapon=wp;
}

void Character::setRace(const QString& race)
{
	/* TODO*/
}

Weapon *Character::getActiveWeapon()
{
	Weapon *wp = NULL;
	BodyLocation *loc = body.getLocation(body.getActiveWeaponLoc());
	if (loc != NULL) {
		wp = dynamic_cast<Weapon*>(loc->getObject());
	}

	if (wp == NULL) {
		wp = basicWeapon;
	}
	return wp;
}

int Character::getDefenseDodge()
{
	return (int)getBasicSpeed()+3;
}

void Character::updateSecStats()
{
	HP=(int)HT+0.5*ST;
	cHP=getHP();
	encumbrance = 5*ST;
	BasicSpeed=((double)getHT()+(double)getDX())/4;
	scoutRadius = (int) IQ/2;
	emit movePointsChanged();
	dmgSw=Die(rules.getDmgSwingType(getST()));
	dmgThr=Die(rules.getDmgThrustType(getST()));

	updateStatsInfo();
	emit statsChanged();
}

void Character::updateStatsInfo()
{
	QRegExp skillRegExp(tr("(st|dx|iq|ht)"));
	info="";
	QString tmp;
	/* zakladne */
	info+=tr("<table width=\"100%\">");
	info+=tr("<tr><td><b>Name:</b> %1</td></tr>")
			.arg(name);
	info+=tr("<tr><td><b>Race:</b> %1</td></tr>").arg(body.name);
	info+=tr("<tr><td><b>Size:</b> %1</td></tr>").arg(size, 0,'f', 2);

	if (state == HEALTHY) 
		tmp = "alive";
	else if (state == INJURED)
		tmp = "injured";
	else if (state == DYING)
		tmp = "dying";
	else if (state == UNCONSCIOUS)
		tmp = "unconscious";
	else 
		tmp = "dead";
	info+=tr("<tr><td><b>State:</b> %1</td></tr>").arg(tmp);

	info+=tr("<tr><td><b>ST:</b> %1(<font color=\"blue\">%2</font>)</td><td><b>HP:</b> %3/%4(<font color=\"blue\">%5</font>)</td></tr>")
		.arg(getST()).arg(getBonus(STRENGTH)).arg(getcHP()).arg(getHP()).arg(getBonus("hp"));
	info+=tr("<tr><td><b>DX:</b> %1(<font color=\"blue\">%2</font>)</td></tr>")
			.arg(getDX()).arg(getBonus(DEXTERITY));
	info+=tr("<tr><td><b>IQ:</b> %1(<font color=\"blue\">%2</font>)</td></tr>")
			.arg(getIQ()).arg(getBonus(INTELLIGENCE));
	info+=tr("<tr><td><b>HT:</b> %1(<font color=\"blue\">%2</font>)</td></tr>")
			.arg(getHT()).arg(getBonus(HEALTH));
	info+=tr("<tr><td><b>TotalXP:</b> %1</td><td><b>UnusedXP:</b> %2</td></tr>").arg(getTotalXP()).arg(getUnusedXP());
	info+=tr("<tr><td><hr><b>Basic Speed:</b> %1(<font color=\"blue\">%2</font>)</td> <td><hr><b>Basic Move:</b> %3(<font color=\"blue\">%4</font>)</td></tr>")
			.arg(getBasicSpeed(),0,'f',2).arg(getBonus("basicspeed")).arg(getBasicMove()).arg(getBonus("basicmove"));
	if (inventory != NULL)
		info+=tr("<tr><td><b>Encumbrance:</b> %1/%2</td></tr>").arg(inventory->getInventoryWeight(),0,'f',2).arg(encumbrance,0,'f',2);
	info+=tr("<tr><td><hr><b>Thrust DMG:</b> %1</td> <td><hr><b>Swing DMG:</b> %2</td></tr>")
			.arg(dmgThr.getDie()).arg(dmgSw.getDie());
	info+=tr("<tr><td><b>DR:</b> %1</td><td><b>Dodge:</b> %2</td></tr>").arg(getDR()).arg(getDefenseDodge());
	info+=tr("</table>");

	/* dovednosti */
	info+=tr("<hr><b>Skills:</b>");
	info+=tr("<table width=\"100%\">");
	info+=tr("<tr><td><b>Name</b></td><td><b>Info</b></td><td><b>Level</b></td></tr>");
	for(QMap<QString,Skill*>::iterator it=skills.begin();it!=skills.end();++it) {
		if (skillRegExp.indexIn((*it)->getID())>=0) continue;
		int fin = (*it)->getFinalLevel(this);
		int lev = (*it)->getLevel();
		if (fin == NONE ) {
			info+=tr("<tr><td>%1</td><td>%2</td><td>none(<font color=\"blue\">%5</font>)</td></tr>").arg((*it)->name)
				.arg((*it)->getShortInfo()).arg((*it)->getBonus());				
		}
		else {
			info+=tr("<tr><td>%1</td><td>%2</td><td>%3/%4(<font color=\"blue\">%5</font>)</td></tr>").arg((*it)->name)
				.arg((*it)->getShortInfo()).arg((*it)->getFinalLevel(this)).arg((*it)->getLevel()).arg((*it)->getBonus());				
		}
	}
	info+=tr("</table>");

	/* zbran */
	/*
	if (weapon != NULL) {
		info+=tr("<hr><b>Weapon:</b>");
		info+=tr("<table width=\"100%\">");
		info+=tr("<tr><td>%1</td><td>%2</td><td>%3</td></tr>")
			.arg(weapon->getName()).arg(weapon->getSkillInfo()).arg(weapon->getDamageString());
		info+=tr("</table>");
	}
	*/

	/* equipment */
	info+=tr("<hr><b>Equipment:</b>");
	info+=tr("<table width=\"100%\">");
	info+=tr("<tr><td><b>Location</b></td><td><b>Object</b></td><td></tr>");
	QMap<QString,BodyLocation> RPGBodyparts = body.getBodyParts();
	QString activeid = body.getActiveWeaponLoc();
	foreach(BodyLocation loc, RPGBodyparts) {
		info+=tr("<tr><td>%1</td>").arg(loc.name);
		if (loc.getObject()==NULL) info+=tr("<td>Empty</td>");
		else { 
			info+=tr("<td>%1").arg(loc.getObject()->getName());
			if (loc.getID() == activeid) info+=tr(" *");
			info+=tr("</td>");
		}
	}

	info+=tr("</table>");

	emit statsInfoChanged();
}

void Character::setSize(double sz) 
{
	this->size = sz;
	updateSecStats();
}

int Character::getSizeMod()
{
	return rules.getSizeMod(size);
}

void Character::setST(int ST) 
{ 
	this->ST=ST; 
	updateSecStats();
}

void Character::setDX(int DX) 
{ 
	this->DX=DX;	
	updateSecStats();
};

void Character::setIQ(int IQ) 
{ 
	this->IQ=IQ;	
	updateSecStats();
};

void Character::setHT(int HT) 
{ 
	this->HT=HT;	
	updateSecStats();
};

void Character::setHP(int HP)
{
	this->HP=HP;
	processHealth();
	updateStatsInfo();
}

void Character::setcHP(int cHP)
{
	this->cHP=cHP;
	processHealth();
	updateStatsInfo();
}

void Character::setTotalXP(int xp)
{
	if (xp < 0)
		xp = 0;
	this->totalXP = xp;
	updateStatsInfo();
}

void Character::setUnusedXP(int xp)
{
	this->unusedXP = xp;
	updateStatsInfo();
}

void Character::spendUnusedXP(int xp)
{
	this->unusedXP -=xp;
	updateStatsInfo();
}

void Character::earnXP(int xp)
{
	this->unusedXP +=xp;
	this->totalXP +=xp;
	updateStatsInfo();
}

void Character::setBonus(Attribute attr, int bonus)
{
	switch (attr) {
		case STRENGTH :
			STbonus = bonus;
			break;
		case DEXTERITY:
			DXbonus = bonus;
			break;
		case INTELLIGENCE:
			IQbonus = bonus;
			break;
		case HEALTH:
			HTbonus = bonus;
			break;
		default: break;
	};
	updateSecStats();
}

void Character::setBonus(const QString &attr, int bonus)
{
	QString tmp = attr.toLower();
	if (attr == "hp") 
		HPbonus = bonus;
	if (attr == "basicmove") 
		BasicMoveBonus = bonus;
	else if (attr == "basicspeed") 
		BasicSpeedBonus = bonus;

	updateStatsInfo();
}

int Character::getBonus(Attribute attr)
{
	switch (attr) {
		case STRENGTH :
			return STbonus;
		case DEXTERITY:
			return DXbonus;
		case INTELLIGENCE:
			return IQbonus;
		case HEALTH:
			return HTbonus;
		default: return 0;
	};
}

int Character::getBonus(const QString &attr)
{
	QString tmp = attr.toLower();
	if (attr == "hp") 
		return HPbonus;
	if (attr == "basicmove") 
		return BasicMoveBonus;
	else if (attr == "basicspeed") 
		return BasicSpeedBonus;
	return 0;
}

double Character::getBasicSpeed() const 
{ 
	if (state != HEALTHY) {
		return (BasicSpeed+BasicSpeedBonus)*rules.lowHPMod;
	}
	return BasicSpeed+BasicSpeedBonus; 
}

int Character::getBasicMove() const 
{ 
	return (int)getBasicSpeed()+BasicMoveBonus; 
}

int Character::getTotalMovePt() const
{
	if (state == UNCONSCIOUS || state == DEAD) 
		return 0;
	return getBasicMove()*rules.movePtScaleFactor;
}

void Character::inflictDmg(int dmg)
{
	setcHP(getcHP()-dmg);
}

void Character::spendMovePt(int pt)
{
	if (movePt >= pt) {
		movePt-=pt;
		emit movePointsChanged();
		if (movePt == 0) {
			emit movePointsReset();
		}
	}
	else {
		movePt = 0;
		emit movePointsChanged();
		emit movePointsReset();
	}
}

void Character::replenishMovePt()
{
	movePt = getTotalMovePt();
	emit movePointsChanged();
}

void Character::setMovePt(int pt)
{
	if (pt < 0)
		pt = 0;
	movePt = pt;
	emit movePointsChanged();
}

void Character::processHealth()
{
	int currHP = getcHP();
	int totalHP = getHP();
	if (currHP*3 > totalHP) {
		setState(HEALTHY);
		return;
	}
	if ((currHP+totalHP*5) <=0) {
		setState(DEAD);
		return;
	}
	if (currHP <= 0) {
		if (state != UNCONSCIOUS)
			setState(DYING);
		return;
	}
	if (currHP*3 <= totalHP) {
		setState(INJURED);
		return;
	}
	
}

void Character::setState(CharacterState st) 
{
	state = st;
	updateStatsInfo();
	if (state == UNCONSCIOUS) {
		spendMovePt(INT_MAX);
	}
}

void Character::processTurnEvents()
{
	if (state == DYING || state == UNCONSCIOUS) {
		int currHP = getcHP();
		int totalHP = getHP();
		int multiple = (int) abs(currHP) / totalHP;
		SuccessRoll roll;
		if (state != UNCONSCIOUS) {
			roll.setRoll(this,"HT",(-1)*multiple);
			switch (roll.roll()) {
				case FAILURE:
				case CRITICALFAILURE:	
					setState(UNCONSCIOUS);
					break;
				case SUCCESS:
				case CRITICALSUCCESS: 
					break;
				default: {}
			}
		}

		if (multiple > lastDeadThrow) {
			roll.setRoll(this,"HT",0);
			switch (roll.roll()) {
				case FAILURE:
				case CRITICALFAILURE:
					setState(DEAD);
					return;
				case SUCCESS:
				case CRITICALSUCCESS: 
					break;
				default: {}
			}
		}
	}
}

int Character::getDR() const 
{
	return body.getDamageResistance();
}


Skill *Character::addSkill(const QString &id, int level)
{
	if (skills.contains(id.toLower())) {
		Skill *skill = skills[id.toLower()];
		skill->setLevel(level);
		updateStatsInfo();
		return skill;
	}
	Skill *skill = rules.getSkill(id.toLower());
	if (skill == NULL)
		return NULL;
	skill->setLevel(level);
	skills[skill->getID()]=skill;
	updateStatsInfo();
	return skill;
}

Skill *Character::getSkill(const QString &name) const
{
	if (skills.contains(name.toLower()))
		return skills[name.toLower()];
	return NULL;
}

bool Character::addInventoryObject(InventoryObject *obj)
{
	if (obj->getWeight()+inventory->getInventoryWeight() > encumbrance)
		return false;
	inventory->addItem(obj);
	updateStatsInfo();
	return true;
}

void Character::clear()
{
	for(skills_iterator it=skills.begin();it!=skills.end();++it)
		delete *it;
	skills.clear();

	if (inventory!=NULL) { 
		delete inventory;
		inventory=NULL;
	}

	if (basicWeapon != NULL) {
		basicWeapon->release();
		basicWeapon = NULL;
	}
}

Character::~Character()
{
	clear();
}

/* class Monster */
Monster::Monster() : Character()
{
	onTalk = NULL;
	combatManeuver = NULL;
	targetEnemy = NULL;
	onKill = NULL;
	xpgain = 0;
	lastman = DO_NOTHING;
	target = NULL;
}

Monster::Monster(const Monster &obj) : Character(obj)
{
	lastman = DO_NOTHING;
	operator=(obj);
}

Monster &Monster::operator =(const Monster &obj)
{
	onTalk = NULL;
	combatManeuver = NULL;
	targetEnemy = NULL;
	onKill = NULL;
	target = NULL;
	xpgain = obj.xpgain;
	if (obj.onTalk != NULL)	{
		onTalk = new Trigger(*obj.onTalk);
		onTalk->setMonster(this);
	}
	if (obj.combatManeuver != NULL) {
		combatManeuver = new Trigger(*obj.combatManeuver);
		combatManeuver->setMonster(this);
	}
	if (obj.targetEnemy != NULL) {
		targetEnemy = new Trigger(*obj.targetEnemy);
		targetEnemy->setMonster(this);
	}
	if (obj.onKill != NULL) {
		onKill = new Trigger(*obj.onKill);
		onKill->setMonster(this);
	}
	return *this;
}

Character *Monster::decideEnemyToAttack()
{	
	if (target!= NULL)
		return target;

	if (targetEnemy == NULL) {
		PlayerNode *pl = campaign->getPlayer();
		return pl;
	}

	Value ret = targetEnemy->execute();
		
	Character *ch = dynamic_cast<Character*>(ret.getObject());
	return ch;
}

CombatManeuver Monster::decideCombatManeuver()
{
	CombatManeuver man = DO_NOTHING;
	if (combatManeuver == NULL) {
		// defaultne spravanie je zautocit ak moze (ak nie je uvedeny ziadny trigger)
		int movept = getMovePt();
		if (movept >= rules.actionCosts.attack)
			man = ATTACK;
		else 
			man = DO_NOTHING;
	}
	else {
		// z triggeru ziskame pouzity utocny manever
		Value ret = combatManeuver->execute();
		int m = ret.getInt();
		if (m < 0 || m > 8) {
			man = DO_NOTHING;
		}

		int movept = getMovePt();
		switch (m) {
			case ATTACK:
				if (movept >= rules.actionCosts.attack)
					man = ATTACK;
				break;
			case ATTACK_DETERMINED:
				if (movept >= rules.actionCosts.determinedAttack) {
					man = ATTACK_DETERMINED;
				}
				break;
			case ATTACK_STRONG:
				if (movept >= rules.actionCosts.strongAttack)
					man = ATTACK_STRONG;
				break;
			case ATTACK_DOUBLE:
				if (movept >= rules.actionCosts.doubleAttack)
					man = ATTACK_DOUBLE;
				break;
			case DEFENSE_DOUBLE:
				if (movept >= rules.actionCosts.doubleDefense)
					man = DEFENSE_DOUBLE;
				break;
			case DEFENSE_STRONG:
				if(movept >= rules.actionCosts.strongDefense)
					man = DEFENSE_STRONG;
				break;
			default:
				man = DO_NOTHING;
		}
	}

	if (man == ATTACK || man == ATTACK_STRONG || man == ATTACK_DETERMINED || man == ATTACK_DOUBLE) {
		target = NULL;
		target = decideEnemyToAttack();

		if (target == NULL)	{
			lastman = DO_NOTHING;
			return DO_NOTHING;
		}

		int x = target->location()->getX();
		int y = target->location()->getY();
		int thisx = location()->getX();
		int thisy = location()->getY();

		if (abs(thisx-x) < 2 && abs(thisy-y) < 2) {
			return man;
		}
		else {
			moveTo(target->location()->getX(),target->location()->getY());
			lastman = MOVE;
			return lastman;
		}
	}

	lastman = man;
	return man;
}

void Monster::moveTo(int tox, int toy)
{
	MapFindPath pathFinder(campaign->getCurrentMap());
	pathFinder.setIgnoreObjects(true);
	MapFindPath::PathList path = pathFinder.find(location()->getX(),location()->getY(),tox,toy);
	if (path.empty()) return;
	campaign->getMapWidget()->setObjectPath(path,this);
}

void Monster::setCombatManeuverTrigger(Trigger *trig)
{
	if (combatManeuver != NULL)	{
		campaign->addTriggerCache(combatManeuver);
		combatManeuver = NULL;
	}
	if (trig == NULL)
		combatManeuver = NULL;
	else {
		combatManeuver = new Trigger(*trig);
		combatManeuver->setMonster(this);
	}
}

void Monster::setTargetEnemyTrigger(Trigger *trig)
{
	if (targetEnemy != NULL) {
		campaign->addTriggerCache(targetEnemy);
		targetEnemy = NULL;
	}
	if (trig == NULL)
		targetEnemy = NULL;
	else {
		targetEnemy = new Trigger(*trig);
		targetEnemy->setMonster(this);
	}
}

void Monster::setOnTalkTrigger(Trigger *trig)
{
	if (onTalk != NULL) {
		campaign->addTriggerCache(onTalk);
		onTalk = NULL;
	}
	if (trig == NULL)
		onTalk = NULL;
	else {
		onTalk = new Trigger(*trig);
		onTalk->setMonster(this);
	}
}

void Monster::setOnKillTrigger(Trigger *trig)
{
	if (onKill != NULL) {
		campaign->addTriggerCache(onKill);
		onKill = NULL;
	}
	if (trig == NULL)
		onKill = NULL;
	else {
		onKill = new Trigger(*trig);
		onKill->setMonster(this);
	}
}

void Monster::removeEmbeddedObject(RPGObject *obj)
 {
	if (onKill == obj) {
		onKill = NULL;
		return;
	}
	if (onTalk == obj) {
		onTalk = NULL;
		return;
	}
	if (combatManeuver == obj) {
		combatManeuver = NULL;
		return;
	}
	if (targetEnemy == obj) {
		targetEnemy = NULL;
		return;
	}
}

void Monster::talkTo(Character *ch) 
{
	onTalk->setObject(ch);
	onTalk->execute();
}

bool Monster::isTalkable()
{
	return onTalk != NULL;
}

void Monster::killedBy(Character *ch)
{
	if (onKill != NULL) {
		onKill->setObject(ch);
		onKill->execute();
	}
}

void Monster::release() 
{
	if (onTalk != NULL) {
		campaign->addTriggerCache(onTalk);
		onTalk = NULL;
	}
	if (combatManeuver != NULL) {
		campaign->addTriggerCache(combatManeuver);
		combatManeuver = NULL;
	}
	if (targetEnemy != NULL) {
		campaign->addTriggerCache(targetEnemy);
		targetEnemy = NULL;
	}
	if (onKill != NULL) {
		campaign->addTriggerCache(onKill);
		onKill = NULL;
	}
	delete this;
}

Monster::~Monster()
{

}

/* class Player */
Player::Player() : Character()
{
	this->setID(QString("player"));
}

Player::~Player()
{
}