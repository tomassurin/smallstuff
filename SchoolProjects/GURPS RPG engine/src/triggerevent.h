/******************************************************************************
 * Implementacia systemu udalosti
******************************************************************************/
#ifndef TRIGGEREVENT_H
#define TRIGGEREVENT_H

#include <QString>
#include <QMap>

#include "gameobjects.h"
#include "rpgcampaign.h"

namespace RPG {

		// dopredne deklaracie
		class Campaign;
		class Trigger;

		// konstanty typu polozky vo vyraze
		enum EntryType { NOTYPE, VALUE, SIGN, BRACKET };
		// konstanty typov znamienok
		enum SignType { SIGN_NO=0, BRAC_OP, BRAC_CL, SIGN_PLUS, SIGN_MINUS, SIGN_MUL, SIGN_DIV, SIGN_MOD,
			SIGN_AND, SIGN_OR, SIGN_LT, SIGN_GT, SIGN_LE, SIGN_GE, SIGN_EQ, SIGN_NE, SIGN_NOT };
		// konstanty typov premennych
		enum ValueType {VALUE_ERROR=0, BOOL, INTEGER, FLOATING, STRING, RPGOBJECT, VOID};
		// konstanty typov triggerov
		enum TriggerType { NOTRIGGER=0, MAPTRIGGER=1, ITEMTRIGGER=2, MONSTERTRIGGER = 3};
		// konstanty chyby z vyhodnocovania funkcii
		enum Errno { FUNCT, FUNCT_ARG_TYPE, FUNCT_ARG_NUM, FUNCT_NUM, FUNCT_ARG };

		/** Trieda reprezentujuca hodnotu datovej polozky vo vyrazoch */
		class Value
		{
		public:
			Value();
			Value(const Value &obj);
			Value& operator=(const Value &obj);
			~Value();

			/** pretypovanie **/
			Value(bool val);
			Value(int val);
			Value(double val);
			Value(const QString &val);
			Value(RPGObject *obj);
			operator int ();
			operator bool ();
			operator double (); 
			operator QString ();

			/** Vytvori objekt Value typu VALUE_ERROR a chybovym retazcom err */
			static Value createError(const QString &err);
			/** Vytvori objekt Value pomocou konstanty chyb vyhodnocovania funkcii */
			static Value createError(int err);
			/** Nastavi hodnotu ako chyba */
			void error(const QString &err);
			/** Vytvori void hodnotu */
			static Value createVoid();
			/** Ziska chybu */
			QString getError();

			/** Nastavenie hodnot **/
			inline void setBool(bool val);
			inline void setInt(int val);
			inline void setDouble(double val);
			inline void setString(const QString &val);
			inline void setObject(RPGObject *obj);

			/** Ziskanie hodnot */
			inline bool getBool();
			inline int getInt();
			inline double getDouble();
			inline QString getString();
			RPGObject* getObject();
			
			/** Konverzia typov hodnot na rovnaky */
			static ValueType convert(Value &a, Value &b);

			/** Vymazanie dat */
			inline void clear();

			// typ hodnoty
			ValueType type;
			// retazec
			QString str;
				  
			union {
				// logicka hodnota
				bool logical;
				// prirodzene cislo
				int integer;
				// realne cislo
				double floating;
				// objekt
				RPGObject *obj;
			};
		};
	
		// pomocna struktura pre ulozenie rozdelenych casti volania funkcie
		struct FunctionDef
		{
			QString name;
			QString args;
		};

		// typedef pouzivany ako typ poolov premennych triggerov
		typedef QMap<QString,Value*> TriggerVariables;
		// typedef pouzivany ako vyparsovana definicia funkcii
		typedef QVector<FunctionDef> FunctionDefinition;

		/** Trieda pouzivana pre vyhodnotenie funkcii a premennych */
		class FunctionEval
		{
		public:
			FunctionEval(Trigger *trig);
			~FunctionEval();

			/** Nastavi trigger ktoremu patri tento objekt */
			void setTrigger(Trigger *t) { trig = t; }

			/** Nastavi premenne - vsetky */
			void setVariables(const TriggerVariables &vars);
			/** Ziska pool premennych */
			const TriggerVariables &getVariables();
			/** Nastavi premennu name na hodnotu val */
			void setVariable(const QString &name, Value *val);
			/** Ziska premennu s hodnotou name */
			Value *getVariable(const QString &name);
			/** Vymaze premenne */
			void clearVariables();
			
			/** Vyhodnoti funkciu s definiciou funct */
			Value eval(FunctionDefinition &funct);
			/** Vyhodnoti funkciu s definiciou funct na premennej val */
			Value eval(FunctionDefinition &funct, Value &val);

		private:
			/** Vyhodnotenie funkcii na objektoch typu character */
			Value charactereval(FunctionDefinition &funct, Character *character);
			/** Vyhodnotenie funkcii na objektoch */
			Value objecteval(FunctionDefinition &funct, RPGObject *obj);
			/** Vyhodnotenie funkcii na inventarnych objektoch */
			Value itemeval(FunctionDefinition &funct, InventoryObject *obj);
			/** Vyhodnotenie funkcii na objektoch typu trigger */
			Value triggereval(FunctionDefinition &funct, Trigger *trig);
			/** Vyodnotenie argumentov funkcie */
			QVector<Value> evaluatearguments(const QString &args);
			
			// udalost, ktorej patri tento objekt 
			Trigger *trig;
			// margin of success posledneho vyrazu												
			int lastmos;					
			// asociativne pole pre ukladanie a dotazovanie premennych (pool premennych)
			TriggerVariables variables;		
		};

		/** Trieda reprezentujuca datovu polozku pouzivanu vo vyrazoch */
		class Entry
		{
		public:

			/** Vrati typ polozky */
			ValueType getValueType() const;
			/** Vymaze data */
			void clear();

			//typ polozky
			EntryType type;				
			//znamienko
			SignType sign;				
			//priorita znamienok - mensie cislo znaci vacsiu prioritu - priority podla operatorov v c++            
			uint priority;				
			//hodnota
			Value value;				
		};

		/** Trieda pouzivana na vyhodnocovanie vyrazov */
		class Expression
		{
		public:
			Expression(FunctionEval *functeval);

			/** Nastavi vyhodnocovac funkcii */
			void setFunctEval(FunctionEval *funct) { functeval = funct; }

			/** Nastavi vyraz */
			void setExpression(const QString &str);
			
			/** Vyhodnoti vyraz */
			Value eval();

		private:
			/** Vyparsovanie vyrazu na zoznam Entry poloziek */
			void parse();
			/** Prevod zoznamu poloziek Entry na postfix */
			void infixtopostfix();
			/** Vyhodnotenie postfixu */
			Value evaluatepostfix();

			// ulozena chyba
			QString error;				
			// originalny vstup
			QString original;			
			// ulozeny vyraz - uz vyparsovany
			QVector<Entry> expr;        
			//pointer na triedu pre vyhodnocovanie funkcii
			FunctionEval *functeval;	
		};

		/** Zakladny objekt reprezentujuci trigger */
		class Trigger : public RPGObject
		{
		public:
			 Trigger();
			 Trigger(const Trigger &);
			 Trigger &operator=(const Trigger&);
			 ~Trigger();
			
			 /** Vrati informacie o triggere */
			 QString getInfo() const;
			 /** Nastavi skript */
			 void setScript(const QString &script);
			 /** Vrati skript */
			 const QString &getScript() const { return script; }
			 /** Nastavi ci je trigger aktivovany - ci sa vykona */
			 void setEnabled(bool val) { enabled = val;}
			
			 /** Vykona trigger vrati vysledok (return hodnotu) */
			 Value execute();

			 /** Vrati typ triggeru */
			 TriggerType getType() const { return type; }
			 /** Nastavi objekt triggeru */
			 void setObject(RPGObject *obj);
			 /** Vrati objekt triggeru */
			 RPGObject *getObject() { return object; }
			 /** Nastavi item na ktorom sa trigger nachadza a trigger nastavi na typ ITEMTRIGGER */
			 void setItem(InventoryObject *obj);
			 /** Vrati item ak je typu ITEMTRIGGER */
			 InventoryObject *getItem() { return item; }
			 /** Nastavi bunku na ktorej sa trigger nachadza a trigger nastavi na typ MAPTRIGGER */
			 void setNode(MapNode *node);
			 /** Vrati bunku na ktorej sa trigger nachadza ak je typu MAPTRIGGER */
			 MapNode *getNode() { return node; }
			 /** Nastavi NPC postavu na ktorej sa trigger nachadza a trigger nastavi na typ MONSTERTRIGGER */
			 void setMonster(Monster *mon);
			 /** Vrati NPC posavu na ktorej sa trigger nachadza */
			 Monster *getMonster() { return monster;}
				
			 /** Nastavi premenne triggeru */
			 void setVariables(const TriggerVariables &vars);
			 /** Ziska premenne triggeru */
			 const TriggerVariables &getVariables();
			
			 /** Zisti ci trigger prave bezi */
			 bool isRunning();
			 /** Nastavi priznak na vymazanie triggeru */
			 void setRemove(bool r) { remove = r;}

			 friend class FunctionEval;

		protected:
			/** Zakladne definicie konstruktoru */
			void constructorDef();
			/** Vyhodnoti blok triggeru (start;end) (riadky vyparsovaneho skriptu) */
			Value evalBlock(int start, int end);
			/** Najde blok */
			void findBlock(int &start, int &end);
			
			// typ triggeru
			TriggerType type;
			// originalny script 
			QString script;
			// rozparsovany script (na riadky)
			QStringList lines;
			// ulozena chyba
			QString error;
			
			// priznak ci je triger aktivovany
			bool enabled;
			// vyhodnocovac funkcii prisluchajuci k triggeru
			FunctionEval *eval;
			// vyhodnocovac vyrazov prisluchajuci k triggeru
			Expression *expr;

			// predmet na ktorom sa nachadza trigger
			InventoryObject *item;
			// bunka na mape na ktorej sa nachadza trigger
			MapNode *node;
			// objekt na ktory trigger posobi
			RPGObject *object;
			// NPC postava na ktorej sa nachadza trigger
			Monster *monster;

			// priznak ci trigger bezi
			bool running;
			// priznak ci sa ma trigger vymazat
			bool remove;
		};
}	

#endif