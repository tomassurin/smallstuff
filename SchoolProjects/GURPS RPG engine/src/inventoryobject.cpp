#include <cctype>
#include <algorithm>
#include <QRegExp>

#include "inventoryobject.h"
#include "triggerevent.h"
#include "rpgcampaign.h"
#include "rpgcharacter.h"

using namespace RPG;

/* class InventoryObject */
InventoryObject::InventoryObject() : RPGObject()
{
	name="";
	bodyLocation="";
	uses=0;
	weight=0;
	owner=NULL;
	onEquip = NULL;
	onUnEquip = NULL;
	onInventory = NULL;
	onUnInventory = NULL;
	onUse = NULL;
	minimumST = 0;

	equipLoc.clear();
}

InventoryObject::~InventoryObject() {
	clear();
}

void InventoryObject::clear() 
{
	if (onEquip != NULL)
		delete onEquip;
	if (onUnEquip != NULL)
		delete onUnEquip;
	if (onInventory != NULL)
		delete onInventory;
	if (onUnInventory != NULL)
		delete onUnInventory;
	if (onUse != NULL)
		delete onUse;
}

void InventoryObject::removeEmbeddedObject(RPGObject *obj)
{
	if (onEquip == obj)	{
		onEquip = NULL;
		return;
	}
	if (onUnEquip == obj) {
		onUnEquip = NULL;
		return;
	}
	if (onInventory == obj) {
		onInventory = NULL;
		return;
	}
	if (onUnInventory == obj) {
		onUnInventory = NULL;
		return;
	}
	if (onUse == obj) {
		onUse = NULL;
		return;
	}
}

void InventoryObject::release() 
{
	if (onEquip != NULL) {
		campaign->addTriggerCache(onEquip);
		onEquip = NULL;
	}
	if (onUnEquip != NULL) {
		campaign->addTriggerCache(onUnEquip);
		onUnEquip = NULL;
	}
	if (onInventory != NULL) {
		campaign->addTriggerCache(onInventory);
		onInventory = NULL;
	}
	if (onUnInventory != NULL) {
		campaign->addTriggerCache(onUnInventory);
		onUnInventory = NULL;
	}
	if (onUse != NULL) {
		campaign->addTriggerCache(onUse);
		onUse = NULL;
	}
	delete this;
}

void InventoryObject::equip(const QString &location)
{
	if (owner == NULL) 
		return;

	QString loc = location.toLower();
	Body *body = owner->getBody();
	if (body == NULL) return;
	
	if (body->getLocation(loc) == NULL && !loc.isEmpty())
		loc = "inventory";


	if (loc != "inventory" && !loc.isEmpty()) {
		if (owner->getST() < getMinimumST()) {
			if (typeid(*owner) == typeid(PlayerNode)) {
				campaign->writeStatus("Can't equip item - not enought ST");
			}
			return;
		}
	}

	body->setActiveWeaponLoc("");
	QString curloc=getBodyLocation();
	// otestujem ci loc je v povolenych equip lokaciach pre tento predmet
	//if (!equipLoc.contains(loc) && loc != "inventory" && !curloc.isEmpty() && !loc.isEmpty()) return;
	if (!equipLoc.contains(loc) && loc != "inventory" && !loc.isEmpty()) return;
	
	//if (curloc.isEmpty()) 
	//	loc = "inventory";
	// nemenime lokaciu ak sucasna aj nova sa rovnaju
	if (curloc==loc) {
		body->setActiveWeaponLoc(curloc);
		return;
	}
	// vymazeme sucasny predmet na lokacii loc
	if (!curloc.isEmpty() && curloc!="inventory") {
		body->equipObject(curloc,NULL);
		// unequip trigger
		if (onUnEquip != NULL) 
			onUnEquip->execute();
	}
	this->bodyLocation = loc;
	// nastavime predmet na lokacii na this
	if (loc!="inventory" && !loc.isEmpty()) { 
		body->equipObject(loc,this);
		body->setActiveWeaponLoc(loc);
		// equip trigger
		if (onEquip != NULL) 
			onEquip->execute();
	}
}

void InventoryObject::setEquipTrigger(Trigger *trig) 
{ 
	if (onEquip != NULL) {
		campaign->addTriggerCache(onEquip);
		onEquip = NULL;
	}
	if (trig == NULL)
		onEquip = NULL;
	else {
		onEquip = new Trigger(*trig); 
		onEquip->setItem(this);
	}
}

void InventoryObject::setUnEquipTrigger(Trigger *trig) 
{ 
	if (onUnEquip != NULL) {
		campaign->addTriggerCache(onUnEquip);
		onUnEquip = NULL;
	}
	if (trig == NULL)
		onUnEquip = NULL;
	else {
		onUnEquip = new Trigger(*trig); 
		onUnEquip->setItem(this);
	}
}

void InventoryObject::setInventoryTrigger(Trigger *trig) 
{ 
	if (onInventory != NULL) {
		campaign->addTriggerCache(onInventory);
		onInventory = NULL;
	}
	if (trig == NULL)
		onInventory = NULL;
	else {
		onInventory = new Trigger(*trig); 
		onInventory->setItem(this);
	}
}

void InventoryObject::setUnInventoryTrigger(Trigger *trig) 
{
	if (onUnInventory != NULL) {
		campaign->addTriggerCache(onUnInventory);
		onUnInventory = NULL;
	}
	if (trig == NULL)
		onUnInventory = NULL;
	else {
		onUnInventory = new Trigger(*trig); 
		onUnInventory->setItem(this);
	}
}
void InventoryObject::setUseTrigger(Trigger *trig) 
{ 
	if (onUse != NULL) {
		campaign->addTriggerCache(onUse);
		onUse = NULL;
	}
	if (trig == NULL)
		onUse = NULL;
	else {
		onUse = new Trigger(*trig); 
		onUse->setItem(this);
	}
}

const QString& InventoryObject::getBodyLocation() const
{
	return bodyLocation;
}

InventoryObject::InventoryObject(const InventoryObject& obj) : RPGObject(obj)
{
	minimumST = 0;
	this->operator =(obj);
}

InventoryObject& InventoryObject::operator=(const InventoryObject& obj)
{
	minimumST = obj.minimumST;
	this->name=obj.name;
	this->bodyLocation=obj.bodyLocation;
	this->weight=obj.weight;
	this->owner=obj.owner;
	this->equipLoc=obj.equipLoc;
	this->uses = obj.uses;
	if (obj.onEquip != NULL) {
		this->onEquip = new Trigger(*obj.onEquip);
		onEquip->setItem(this);
	}
	else 
		this->onEquip = NULL;
	if (obj.onUnEquip != NULL) {
		this->onUnEquip = new Trigger(*obj.onUnEquip);
		onUnEquip->setItem(this);
	}
	else 
		this->onUnEquip = NULL;
	if (obj.onInventory !=NULL) {
		this->onInventory = new Trigger(*obj.onInventory);
		onInventory->setItem(this);
	}
	else 
		this->onInventory = NULL;
	if (obj.onUnInventory !=NULL) {
		this->onUnInventory = new Trigger(*obj.onUnInventory);
		onUnInventory->setItem(this);
	}
	else 
		this->onUnInventory = NULL;
	if (obj.onUse !=NULL) {
		this->onUse = new Trigger(*obj.onUse);
		onUse->setItem(this);
	}
	else 
		this->onUse = NULL;
	return *this;
}

void InventoryObject::addEquipLocation(const QString &loc)
{
	equipLoc.append(loc.toLower());
}

QStringList InventoryObject::getEquipLocations() const
{
	return equipLoc;
}

void InventoryObject::setOwner(Character *ch)
{
	this->owner=ch;
}

Character *InventoryObject::getOwner()
{
	return owner;
}

void InventoryObject::use()
{
	if (owner == NULL)
		return;

	if (owner->getMovePt() < rules.actionCosts.useItem) {
		if (typeid(*owner) == typeid(PlayerNode))
			campaign->writeStatus("Not enought move pt to use item");
		return;
	}

	owner->spendMovePt(rules.actionCosts.useItem);

	if (uses > 0 || uses == -1) {
		if (uses > 0)
			--uses;

		if (onUse != NULL)
			onUse->execute();
	}
	if (uses == 0) {
		this->removeFromLocation();
		this->release();
	}
}

/* class Inventory */
Inventory::Inventory() : QAbstractTableModel()
{
	totalweight=0;
	items.clear();
	this->owner=NULL;
}

Inventory::Inventory(const Inventory& obj) : QAbstractTableModel()
{
	this->owner=NULL;
	this->operator=(obj);
}

Inventory& Inventory::operator=(const Inventory& obj)
{
	InventoryObject* item;
	foreach(item, obj.items)
		addItem(new InventoryObject(*item));

	totalweight=obj.totalweight;
	return *this;
}

Inventory::~Inventory()
{
	InventoryObject* item;
	foreach(item,items) {
		item->location()->clear();
		item->release();
	}
	items.clear();	
}

void Inventory::addItem(InventoryObject *object, const QString &loc)
{
	if (object == NULL) return;
	int n=items.count();
	beginInsertRows(QModelIndex(),n,n);
		object->setOwner(owner);
		object->location()->setInventory(this, n);
		items.append(object);
		totalweight+=object->getWeight();
	endInsertRows();

	setItemBodyLocation(n,"inventory");
	if (owner != NULL && !loc.isEmpty())
		setItemBodyLocation(n,loc);

	// inventory trigger
	if (object->onInventory != NULL) 
		object->onInventory->execute();
}

InventoryObject* Inventory::removeItem(int index)
{
	InventoryObject *obj=items[index];
	
	//uninventory trigger
	if (obj->onUnInventory !=NULL)
		obj->onUnInventory->execute();

	beginRemoveRows(QModelIndex(),index,index);
		totalweight-=items[index]->getWeight();
		items.remove(index);
	endRemoveRows();
	obj->equip("");
	obj->setOwner(NULL);
	obj->location()->clear();	
	if (owner != NULL)
		owner->updateStatsInfo();
	return obj;
}

void Inventory::setItemBodyLocation(int index, const QString &loc)
{
	InventoryObject *obj;
	obj=items.at(index);
	obj->equip(loc);
	dataChanged(createIndex(0,0),createIndex(this->rowCount(),3));
	if (owner!= NULL)
		owner->updateStatsInfo();
}

InventoryObject *Inventory::getItem(int index)
{
	return items.at(index);
}

InventoryObject *Inventory::getItem(const QString &id)
{
	foreach(InventoryObject *obj, items) {
		if (obj->getID() == id) 
			return obj;
	}
	return NULL;
}

const QMap<QString,BodyLocation>& Inventory::getBodyParts() const
{
	return owner->getBody()->getBodyParts();
}

void Inventory::setOwner(Character *character)
{
	this->owner = character;
}

Character *Inventory::getOwner()
{
	return owner;
}

int Inventory::rowCount(const QModelIndex &parent) const
{
	return items.size();
}

int Inventory::columnCount(const QModelIndex &parent) const
{
    return 4;
}

QVariant Inventory::headerData ( int section, Qt::Orientation orientation, int role) const 
{
	if (role != Qt::DisplayRole)
         return QVariant();

	if (orientation==Qt::Horizontal) 
		switch (section) {
			case 0: return tr("Img");
				break;
			case 1: return tr("Name");
				break;
			case 2: return tr("Location");
				break;
			case 3: return tr("Weight");
				break;
			default: return QVariant();
	}

	return QVariant();
}

QVariant Inventory::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() >= items.size())
        return QVariant();

	if (role == Qt::DisplayRole) {
		QString str;
		switch (index.column()) {
			case 1: return items.at(index.row())->getName();
				break;
			case 2: 
					str=items.at(index.row())->getBodyLocation();
					if (str=="inventory") return tr("Inventory");
					else return owner->getBody()->getLocation(str)->name;
				break;
			case 3: return items.at(index.row())->getWeight();
				break;
			default: return QVariant();
		}
	}
	else if (role == Qt::DecorationRole && index.column() == 0) 
		return items.at(index.row())->getPixmap();
	else if (role == Qt::TextAlignmentRole) 
		return QVariant(Qt::AlignCenter | Qt::AlignVCenter);
	else if (role == Qt::SizeHintRole) 
		return QVariant(QSize(sidesize,sidesize));
	else if (role == Qt::ToolTipRole)
		return items.at(index.row())->getInfo();
    else
		return QVariant();
}

/* Weapon */
Weapon::Weapon()
{
}

Weapon::Weapon(const Weapon& obj) : InventoryObject(obj)
{

	this->operator=(obj);
}


Weapon& Weapon::operator=(const Weapon& obj)
{
	damage=obj.damage;
	skillInfo=obj.skillInfo;
	skills=obj.skills;
	return *this;
}


bool NumberStart(QChar c)
{
	return c.isDigit() || c=='-' || c=='+';
}
	
bool SkillEnd(QChar c)
{
	return c==',';
}

void Weapon::addSkill(QString& skill)
{
	QString::iterator itbegin,itend,ittmp;
	SkillInfo info;
	skillInfo=skill;
	
	removeWhitespaces(skill);
	skill.toLower();
	info.mod=0;
	info.id.clear();

	itbegin=skill.begin();
	while (itbegin!=skill.end()) {
		//najdeme ciarku tj. tam kde konci prave spracovany skill
		itend=std::find_if(itbegin,skill.end(),SkillEnd);
		//najdeme v spracovavanom skille cislo
		ittmp=std::find_if(itbegin,itend,NumberStart);
		//ulozime nazov
		info.id=QString(itbegin,ittmp-itbegin);
		//cislo si ulozime 
		info.mod=QString(ittmp,itend-ittmp).toInt();
		//vysledok ulozime
		skills.push_back(info);
		//zacneme spracovavat dalsiu oblast
		if (itend!=skill.end()) ++itend;
		itbegin=itend;
	}
}

const QString& Weapon::getSkillInfo() const
{
	return skillInfo;
}

Skill* Weapon::getEffectiveSkill(const Character *ch) const
{
	int max=0;
	int mod=0;
	int current;
	Skill *maxSkill=NULL;
	Skill *skill;
	//najdeme si s ktorym skillom mame najvyssi level
	for(QVector<SkillInfo>::const_iterator it=skills.begin();it!=skills.end();++it) {
		skill=ch->getSkill(it->id);
		if (skill==NULL) continue;
		current=skill->getFinalLevel(ch);
		if (current==NONE) continue;
		current+=it->mod;
		if (max<current) {
			max=current;
			maxSkill=skill;
			mod=it->mod;
		}
	}

	if (maxSkill==NULL) return NULL;

	maxSkill->otherMod=mod;
	return maxSkill;
}

Skill *Weapon::getEffectiveSkill() const
{
	if (owner!=NULL) return getEffectiveSkill(owner);
	else return NULL;
}


void Weapon::setDamage(QString &dmg)
{
	damage.setDamage(dmg);
}

const QString& Weapon::getDamageString() const
{
	return damage.getDamageString();
}

Die Weapon::getDamageDie(Character *ch) const
{
	return damage.getDamageDie(ch);
}

Die Weapon::getDamageDie() const
{
	if (owner!=NULL) return damage.getDamageDie(owner);
	else return Die();
}

QString Weapon::getInfo() const
{
	QString info="";
	info+=tr("<b>Dmg:</b> %1<br>").arg(damage.getDamageString());
	info+=tr("<b>Skill:</b> %1").arg(skillInfo);
	info+=tr("<hr>");
	info+=about;
	return info;
}

/* RPGArmor */
Armor::Armor()
{
	dr = 0;
}

Armor::Armor(const Armor& obj) : InventoryObject(obj)
{
	this->operator=(obj);
}

Armor& Armor::operator=(const Armor& obj)
{
	this->dr = obj.getDR();
	return *this;
}

QString Armor::getInfo() const
{
	QString info;
	info.clear();
	info+=tr("<b>Dr:</b> %1").arg(dr);
	info+=tr("<hr>");
	info+=about;
	return info;
}