/******************************************************************************
 * Implementacia XML parsera. Vsetky jeho triedy su uzavrete do namespace XMLReader
 * XML Parser sluzi na nacitanie datovych suborov.
******************************************************************************/
#ifndef XMLREADER_H
#define XMLREADER_H

#include <QXmlStreamReader>

#include "rpgcampaign.h"

namespace XMLReader {
	/** Funkcie sluziaca pre citanie neznamych elementov */
	void readUnknownElement(QXmlStreamReader *reader);

	/** Funkcia na nacitanie grafiky objektu object, aktualny adresar je v retazci cwd */
	void readGraphics(QXmlStreamReader *reader, const QString& cwd, RPG::RPGObject *object);

	/** Funkcia na nacitanie zakladnych atributov postavy character */
	void readStats(QXmlStreamReader *reader, RPG::Character *character);

	/** Funkcia na nacitanie dovednosti skill */
	void readSkill(QXmlStreamReader *reader, RPG::Skill *skill);

	/** Funkcia na nacitanie postavy ch, aktualny adresar je v retazci cwd. Premenna objdb udava ukazatel 
	  *	do definicie objektov (bud ukazatel na RPG::Rules alebo RPG::Map) */
	template<class T>
	void readRPGCharacter(QXmlStreamReader *reader, const QString& cwd, RPG::Character *ch, T *objdb);

	/**	Funkcia na nacitanie definicie objektov objdb, aktualny adresar je v retazci cwd. Typ premennej objdb
	  * moze byt RPG::Rules alebo RPG::Map) */
	template<class T>
	void readObjectTypedef(QXmlStreamReader *reader, const QString& cwd, T *objdb);

	/** Funkcia na nacitanie zbrane weapon. Premenna objdb udava ukazatel do definicie objektov 
	  * (bud ukazatel na RPG::Rules alebo RPG::Map). cwd je aktualny adresar **/
	template <class T, class R>
	void readItem(QXmlStreamReader *reader, const QString& cwd, T *weapon, R *objdb);	

	/** Funkcia na nacitanie inventara, aktualny adresar je v retazci cwd. */
	template <class T> 
	void readInventory(QXmlStreamReader *reader, const QString& cwd,  T *inventory);

	/** Funkcia na nacitanie mapy, cwd je aktualny adresar. */
	void readMap(QXmlStreamReader *reader, const QString& cwd,RPG::Map *map);

	/** Funkcia na nacitanie bunky mapy node, cwd je aktualny adresar, map je mapa kam patri bunka. */ 
	void readMapNode(QXmlStreamReader *reader, const QString& cwd, RPG::Map *map, RPG::MapNode* node);

	/** Funkcia na nacitanie definicie terenu mapy map, cwd je aktualny adresar. */
	void readTerrainTypedef(QXmlStreamReader *reader, const QString& cwd, RPG::Map *map);

	/** Funkcia na nacitanie terenu terrain, cwd je aktualny adresar */
	void readTerrain(QXmlStreamReader *reader, const QString& cwd, RPG::RPGTerrain *terrain);

	/** Trieda pre nacitanie kampane */
	class CampaignXMLReader : public QXmlStreamReader
	{
	public:
		/** Nastavenie IO zariadenia odkial citat a aktualneho adresara cwd */
		CampaignXMLReader(QIODevice *device, QString& cwd);
		
		/** Precitanie suboru, vrati false ak nastala chyba */
		bool read();

	private:
		void readCampaign();
		void readRules(RPG::RPGRules &rules);

		QString cwd;
	};

	/** Trieda pre nacitanie mapy */
	class MapXMLReader : public QXmlStreamReader
	{
	public:
		/** Nastavenie IO zariadenia odkial citat, aktualneho adresara cwd a objektu mapy do ktoreho ulozit data */
		MapXMLReader(QIODevice *device, QString& cwd, RPG::Map *map);

		/** Precitanie suboru, vrati false ak nastala chyba */
		bool read();
	private:
		RPG::Map *map;
		QString cwd;
	};

	/** Trieda pre nacitanie hraca */
	class PlayerXMLRead : public QXmlStreamReader
	{
	public:
		/** Nastavenie IO zariadenia odkial citat, aktualneho adresara cwd a objektu hraca do ktoreho ulozit data */
		PlayerXMLRead(QIODevice *device, QString& cwd, RPG::PlayerNode *RPGCharacter);

		/** Precitanie suboru, vrati false ak nastala chyba */		
		bool read();
	private:

		RPG::PlayerNode *ch;
		QString cwd;
	};
}
#endif