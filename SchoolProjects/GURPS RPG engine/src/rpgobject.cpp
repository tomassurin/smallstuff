#include <cctype>
#include <QTime>
#include <algorithm>
#include <QRegExp>

#include "gameobjects.h"
#include "triggerevent.h"
#include "rpgcampaign.h"

RPG::RPGRules rules;
RPG::Campaign *campaign=NULL;

using namespace RPG;

bool isWhitespace(QChar c)
{
	return c.isSpace();
}

void removeWhitespaces(QString &str)
{
	QString::iterator it;
	it=std::remove_if(str.begin(),str.end(),isWhitespace);
	str=QString(str.begin(),it-str.begin());
}

/* class ObjectLocation */
ObjectLocation::ObjectLocation()
{
	clear();
}

void ObjectLocation::setMap(Map *map, int x, int y)
{
	clear();
	type = MAP_LOC;
	this->map = map;
	mapX = x;
	mapY = y;
}

void ObjectLocation::setMonster(Monster *mon)
{
	clear();
	type = MONSTER_LOC;
	monster = mon;
}

Monster* ObjectLocation::getMonster()
{
	if (type == MONSTER_LOC)
		return monster;
	return NULL;
}

InventoryObject *ObjectLocation::getItem()
{
	if (type == ITEM_LOC)
		return item;
	return NULL;
}

void ObjectLocation::setItem(InventoryObject *it)
{
	clear();
	type = ITEM_LOC;
	item = it;
}
Map *ObjectLocation::getMap()
{
	if (type == MAP_LOC) 
		return map;
	return NULL;
}

int ObjectLocation::getX() const
{
	if (type == MAP_LOC)
		return mapX;
	return NULL;
}
int ObjectLocation::getY() const
{
	if (type == MAP_LOC)
		return mapY;
	return NULL;
}

QPoint ObjectLocation::getPos() const
{
	if (type == MAP_LOC)
		return QPoint(mapX,mapY);
	return QPoint(-1,-1);
}

void ObjectLocation::setX(int x)
{
	if (type == MAP_LOC)
		mapX = x;
}

void ObjectLocation::setY(int y)
{
	if (type == MAP_LOC)
		mapY = y;
}

void ObjectLocation::setInventory(Inventory *inv, int index)
{
	clear();
	type = INVENTORY_LOC;
	inventory = inv;
	inventoryIndex = index;
}

Inventory *ObjectLocation::getInventory() 
{
	if (type == INVENTORY_LOC)
		return inventory;
	return NULL;
}

int ObjectLocation::getIndex() const
{
	if (type == INVENTORY_LOC)
		return inventoryIndex;
	return NULL;
}

void ObjectLocation::clear()
{
	type = UNDEFINED_LOC;
	mapX = -1;
	mapY = -1;
	map = NULL;
	
	inventory = NULL;
	inventoryIndex = -1;

	monster = NULL;
	item = NULL;
}

/* class Skill */
Skill::Skill()
{
	this->attr=DEFAULTATTR;
	this->def=NONE;
	this->difficulty=DEFAULTDIFF;
	this->level=NONE;
	this->bonus = 0;
}

void Skill::setAttribute(Attribute data)
{
	if (data==DEFAULTATTR) this->attr=DEXTERITY;
	else this->attr=data;
}	

void Skill::setDifficulty(Difficulty data)
{
	if (data==DEFAULTDIFF) this->difficulty=HARD;
	else this->difficulty=data;
}

void Skill::setLevel(int level)
{
	this->level=level;
}

void Skill::setDefault(int def)
{
	this->def=def;
}

int Skill::getFinalLevel(const Character *ch)
{
	if (level==NONE) {
		if (def != NONE) {
			return getDefaultLevel(ch);
		}
		else return NONE;
	}
	int final=level;

	switch (attr) {
		case STRENGTH : final+=ch->getST();
			break;
		case DEXTERITY : final+=ch->getDX();
			break;
		case INTELLIGENCE : final+=ch->getIQ();
			break;
		case HEALTH : final+=ch->getHT();
			break;
	}
	return final+bonus;
}

int Skill::getDefaultLevel(const Character *ch)
{
	if (def==NONE) return NONE;

	int final=def;

	switch (attr) {
		case STRENGTH : final+=ch->getST();
			break;
		case DEXTERITY : final+=ch->getDX();
			break;
		case INTELLIGENCE : final+=ch->getIQ();
			break;
		case HEALTH : final+=ch->getHT();
			break;
	}
	return final;
}

QString Skill::getShortInfo()
{
	QString tmp="";
	switch (attr) {
		case STRENGTH : tmp+="ST";
			break;
		case DEXTERITY : tmp+="DX";
			break;
		case INTELLIGENCE : tmp+="IQ";
			break;
		case HEALTH : tmp+="HT";
			break;
	}
	switch (difficulty) {
		case EASY: tmp+="/E";
			break;
		case AVERAGE: tmp+="/A";
			break;
		case HARD:	tmp+="/H";
			break;
	}
	return tmp;
}

int Skill::getFirstLevel()
{
	return rules.getSkillFirstLevel(difficulty);
}

int Skill::getNextLevelCost()
{
	if (level == NONE) {
		return rules.getSkillCost(getFirstLevel(),difficulty);
	}
	return rules.getSkillCost(level+1,difficulty);
}

void Skill::processNextLevel()
{
	if (level == NONE) {
		setLevel(getFirstLevel());
		return;
	}
	setLevel(level+1);
}

/* class RPGRules */
RPGRules::RPGRules() 
{
	addBasicRules();
}

void RPGRules::addDmgThrustType(int ST, QString &str)
{
	DmgThrustTable[ST]=str;
}

QString RPGRules::getDmgThrustType(int ST)
{
	if (!isValid()) return "";
	if (DmgThrustTable.contains(ST))
		return DmgThrustTable[ST];
	if (ST > DmgThrustTable.keys().last()) {
		int mul = (int) ((ST - 20) / 5 + 1);
		if (mul < 1)
			mul = 1;
		return mul+"d";
	}
	return "1d";
}

void RPGRules::addDmgSwingType(int ST, QString &str)
{
	DmgSwingTable[ST]=str;
}

QString RPGRules::getDmgSwingType(int ST)
{
	if (!isValid()) return "";
	if (DmgSwingTable.contains(ST))
		return DmgSwingTable[ST];
	if (ST > DmgSwingTable.keys().last()) {
		int mul = (int) ((ST - 20) / 5 + 3);
		if (mul < 1)
			mul = 1;
		return mul+"d";
	}
	return "2d";	
}

void RPGRules::addSkill(const Skill &skill)
{
	QString tmp=skill.getID().toLower();
	SkillTable[tmp]=skill;
}
Skill *RPGRules::getSkill(const QString &name) const
{
	QString nm = name.toLower();
	if (SkillTable.contains(nm))
		return new Skill(SkillTable[nm]);
	return NULL;
}

void RPGRules::addRace(const Body &race)
{
	RaceTable[race.getID().toLower()]=race;
}

Body RPGRules::getRace(const QString &name) const
{
	return RaceTable[name.toLower()];
}

void RPGRules::clear()
{
	DmgSwingTable.clear();
	RaceTable.clear();
	DmgThrustTable.clear();
	SkillTable.clear();
	sizeModTable.clear();
	addBasicRules();
}

QList<QString> RPGRules::getSkillsNames()
{
	QList<QString> out;
	QList<QString> names = SkillTable.keys();
	QList<QString>::ConstIterator it = names.begin();
	for(;it!=names.end();++it) {
		if (*it == "st" || *it == "dx" || *it == "iq" || *it == "ht")
			continue;
		out.append(*it);
	}
	return out;
}

void RPGRules::addBasicRules()
{	
	Skill tmp;
	tmp.setID("st");
	tmp.setAttribute(STRENGTH);
	tmp.setDefault(0);
	tmp.setDifficulty(ATTRIBUTE);
	addSkill(tmp);
	tmp.setID("dx");
	tmp.setAttribute(DEXTERITY);
	tmp.setDefault(0);
	tmp.setDifficulty(ATTRIBUTE);
	addSkill(tmp);
	tmp.setID("iq");
	tmp.setAttribute(INTELLIGENCE);
	tmp.setDefault(0);
	tmp.setDifficulty(ATTRIBUTE);
	addSkill(tmp);
	tmp.setID("ht");
	tmp.setAttribute(HEALTH);
	tmp.setDefault(0);
	tmp.setDifficulty(ATTRIBUTE);
	addSkill(tmp);

	Body race;
	BodyLocation loc;

	loc.name="Head";
	loc.setID(QString("head"));
	race.addRPGBodyPart(loc);
	loc.name="Torso";
	loc.setID(QString("torso"));
	race.addRPGBodyPart(loc);
	loc.name="Legs";
	loc.setID(QString("legs"));
	race.addRPGBodyPart(loc);
	loc.name="Right Hand";
	loc.setAttackable(true);
	loc.setID(QString("rhand"));
	race.addRPGBodyPart(loc);
	loc.name="Left Hand";
	loc.setAttackable(true);
	loc.setID(QString("lhand"));
	race.addRPGBodyPart(loc);
	loc.name="Both Hands";
	loc.setAttackable(true);
	loc.setID(QString("bhand"));
	QStringList blocklist;
	blocklist.append("lhand");
	blocklist.append("rhand");
	loc.setBlockLocations(blocklist);
	race.addRPGBodyPart(loc);

	race.setID("basic");
	race.name="Basic";
	addRace(race);

	actionCosts.attack = 200;
	actionCosts.determinedAttack = 400;
	actionCosts.doubleAttack = 400;
	actionCosts.strongAttack = 400;
	actionCosts.talk = 0;
	actionCosts.useItem = 200;
	actionCosts.doubleDefense = 400;
	actionCosts.strongDefense = 400;
	actionCosts.pickItem = 200;
	
	reactionLimit.reactionBad = 9;
	reactionLimit.reactionNeutral = 13;
	reactionLimit.reactionGood = 100;

	addSizeMod(0.3,-5);
	addSizeMod(1,-2);
	addSizeMod(2,0);
	addSizeMod(3,1);
	addSizeMod(5,2);
	addSizeMod(10,4);
	addSizeMod(15,5);
	addSizeMod(20,6);
	addSizeMod(30,7);
	addSizeMod(50,8);
	addSizeMod(70,9);
	addSizeMod(100,10);

	addDmgThrustType(1,QString("1d-6"));
	addDmgThrustType(2,QString("1d-6"));
	addDmgThrustType(3,QString("1d-5"));
	addDmgThrustType(4,QString("1d-5"));
	addDmgThrustType(5,QString("1d-4"));
	addDmgThrustType(6,QString("1d-4"));
	addDmgThrustType(7,QString("1d-3"));
	addDmgThrustType(8,QString("1d-3"));
	addDmgThrustType(9,QString("1d-2"));
	addDmgThrustType(10,QString("1d-2"));
	addDmgThrustType(11,QString("1d-1"));
	addDmgThrustType(12,QString("1d-1"));
	addDmgThrustType(13,QString("1d"));
	addDmgThrustType(14,QString("1d"));
	addDmgThrustType(15,QString("1d+1"));
	addDmgThrustType(16,QString("1d+1"));
	addDmgThrustType(17,QString("1d+2"));
	addDmgThrustType(18,QString("1d+2"));
	addDmgThrustType(19,QString("2d-1"));
	addDmgThrustType(19,QString("2d-1"));

	addDmgSwingType(1,QString("1d-5"));
	addDmgSwingType(2,QString("1d-5"));
	addDmgSwingType(3,QString("1d-4"));
	addDmgSwingType(4,QString("1d-4"));
	addDmgSwingType(5,QString("1d-3"));
	addDmgSwingType(6,QString("1d-3"));
	addDmgSwingType(7,QString("1d-2"));
	addDmgSwingType(8,QString("1d-2"));
	addDmgSwingType(9,QString("1d-1"));
	addDmgSwingType(10,QString("1d"));
	addDmgSwingType(11,QString("1d+1"));
	addDmgSwingType(12,QString("1d+2"));
	addDmgSwingType(13,QString("2d-1"));
	addDmgSwingType(14,QString("2d"));
	addDmgSwingType(15,QString("2d+1"));
	addDmgSwingType(16,QString("2d+2"));
	addDmgSwingType(17,QString("3d-1"));
	addDmgSwingType(18,QString("3d"));
	addDmgSwingType(19,QString("3d+1"));
	addDmgSwingType(20,QString("3d+1"));

	strongAttackMod = 2;
	determinedAttackMod = 4;
	lowHPMod = 0.5;
	strongDefenseMod = 2;

	addSkillCost(0,100,EASY);
	addSkillCost(1,200,EASY);
	addSkillCost(2,400,EASY);
	addSkillCost(3,800,EASY);
	addSkillStep(400,EASY);
	addSkillCost(-1,100,AVERAGE);
	addSkillCost(0,200,AVERAGE);
	addSkillCost(1,400,AVERAGE);
	addSkillCost(2,800,AVERAGE);
	addSkillCost(3,1200,AVERAGE);
	addSkillStep(400,AVERAGE);
	addSkillCost(-2,100,HARD);
	addSkillCost(-1,200,HARD);
	addSkillCost(0,400,HARD);
	addSkillCost(1,800,HARD);
	addSkillStep(400,HARD);

	
	dmgMods[0] = 1;
	dmgMods[1] = 1.5;
	dmgMods[2] = 1.5;
	dmgMods[3] = 1.5;
	dmgMods[4] = 2;
	dmgMods[5] = 0.5;
	dmgMods[6] = 1;
	dmgMods[7] = 1.5;
}

void RPGRules::addSkillCost(int level, int cost, Difficulty dif)
{
	if (dif == EASY) 
		skillCosts.easyCosts[level] = cost;
	else if (dif == AVERAGE)
		skillCosts.averageCosts[level] = cost;
	else if (dif == HARD)
		skillCosts.hardCosts[level] = cost;
}

void RPGRules::addSkillStep( int step, Difficulty dif) 
{
	if (dif == EASY) 
		skillCosts.easyStep = step;
	else if (dif == AVERAGE)
		skillCosts.averageStep = step;
	else if (dif == HARD)
		skillCosts.hardStep = step;
}

int RPGRules::getSkillCost(int level, Difficulty dif)
{
	if (level < getSkillFirstLevel(dif))
		return INT_MAX;

	if (dif == EASY) {
		if (skillCosts.easyCosts.contains(level)) {
			return skillCosts.easyCosts[level];
		}
		int last = skillCosts.easyCosts.keys().last();
		if (level < last )
			return INT_MAX;
		return skillCosts.easyStep*(level-last)+skillCosts.easyCosts[last];
	}
	else if (dif == AVERAGE) {
		if (skillCosts.averageCosts.contains(level)) {
			return skillCosts.averageCosts[level];
		}
		int last = skillCosts.averageCosts.keys().last();
		if (level < last )
			return INT_MAX;
		return skillCosts.averageStep*(level-last)+skillCosts.averageCosts[last];
	}
	else if (dif == HARD) {
		if (skillCosts.hardCosts.contains(level)) {
			return skillCosts.hardCosts[level];
		}
		int last = skillCosts.hardCosts.keys().last();
		if (level < last )
			return INT_MAX;
		return skillCosts.hardStep*(level-last)+skillCosts.hardCosts[last];
	}
	return INT_MAX;
}

int RPGRules::getSkillFirstLevel(Difficulty dif)
{
	if (dif == EASY)
		return skillCosts.easyCosts.keys().first();
	else if (dif == AVERAGE)
		return skillCosts.averageCosts.keys().first();
	else if (dif == HARD)
		return skillCosts.hardCosts.keys().first();
	return INT_MAX;
}

void RPGRules::addSizeMod(double sz, int mod) 
{
	sizeModTable[sz] = mod;
}
double RPGRules::getSizeMod(double sz)
{
	if (sizeModTable.contains(sz))
		return sizeModTable[sz];

	// najdeme prvy vacsi kluc
	QMap<double,int>::ConstIterator it = sizeModTable.upperBound(sz);
	if (it == sizeModTable.end())
		return sizeModTable.values().last();
	else 
		return *it;
}

bool RPGRules::isValid()
{
	return !(DmgSwingTable.isEmpty() || DmgThrustTable.isEmpty());
}


void RPGRules::addObjectType(const QString &id, RPGObject *obj)
{
	if (obj == NULL)
		return;
	if (objects.contains(id)) {
		delete objects[id];
	}
	objects[id] = obj;
}

RPGObject *RPGRules::getObjectType(const QString &id) const
{
	if (objects.contains(id)) {
		RPGObject *obj = objects[id];
		if (typeid(*obj) == typeid(RPGObject))
			return new RPGObject(*(RPGObject*)obj);
		if (typeid(*obj) == typeid(Trigger)) 
			return new Trigger(*(Trigger*)obj);
		if (typeid(*obj) == typeid(Monster)) 
			return new Monster(*(Monster*)obj);
		if (typeid(*obj) == typeid(Weapon)) 
			return new Weapon(*(Weapon*)obj);
		if (typeid(*obj) == typeid(Armor))
			return new Armor(*(Armor*)obj);
		if (typeid(*obj) == typeid(InventoryObject)) 
			return new InventoryObject(*(InventoryObject*)obj);
	}
	return NULL;
}


int RPGRules::getActionCost(CombatManeuver man) 
{
	switch (man) {
		case ATTACK:
			return rules.actionCosts.attack;
			break;
		case ATTACK_DETERMINED:
			return rules.actionCosts.determinedAttack;
			break;
		case ATTACK_STRONG:
			return rules.actionCosts.strongAttack;
			break;
		case ATTACK_DOUBLE:
			return rules.actionCosts.doubleAttack;
			break;
		case DEFENSE_DOUBLE:
			return rules.actionCosts.doubleDefense;
			break;
		case DEFENSE_STRONG:
			return rules.actionCosts.strongAttack;
			break;
		case OTHERMAN:
			return 0;
		default:
			return INT_MAX;
	}
}

/* class Die */
Die::Die()
{
	clear();
}

Die::Die(QString &str)
{
	clear();
	setDie(str);
}

bool isNotDigit(QChar c)
{
	return !c.isDigit();
}

bool isDigit(QChar c)
{
	return c.isDigit();
}

void Die::setDie(QString &str)
{
	int c=0;
	bool negative=false;

	removeWhitespaces(str);
	str.toLower();
	name=str;

	QString::iterator it=str.begin();

	while (it!=str.end() && it->isDigit()) {
		c=c*10+it->digitValue();
		it++;
	}
	count=c;
	
	if (it==str.end()) return;

	if (*it=='d') { 
		type=6;
		++it; 
	}
	else if (*it=='k') {
		++it;
		c=0;
		while (it!=str.end() && it->isDigit()) {
			c=c*10+it->digitValue();
			it++;
		}
	type=c;
	}
	
	if (it==str.end()) return;
	if (*it=='-') negative=true;
	
	c=0;
	++it;
	while (it!=str.end() && it->isDigit()) {
			c=c*10+it->digitValue();
			it++;
	}
	if (negative) c*=-1;
	modifier=c;
}

int Die::throwDie() const
{
	int value=0;
	value+=modifier;
	for(int i=0;i<count;++i)
		value+=1+qrand() % type;
	return value;
}

int Die::getMaxVal() const
{
	return count*type+modifier;
}

void Die::setDie(int count, int type, int modifier)
{
	this->count=count;
	this->type=type;
	this->modifier=modifier;
	this->name=getDie();
}

QString Die::getDie() const
{	
	QString str;
	str=QString("%1").arg(count);
	str+=type==6?QString("d"):QString("k%1").arg(type);
	if (modifier!=0) {
		str+=modifier>0?QString("+"):QString("");
		str+=QString("%1").arg(modifier);
	}
	return str;
}

void Die::addMod(int mod) 
{
	this->modifier+=mod;
}

void Die::clear()
{
	count=0;
	type=0;
	modifier=0;
	dmgType=OTHERDMG;
	name.clear();
}

/* class SuccessRoll */
SuccessRoll::SuccessRoll()
{
	die.setDie(3,6,0);
	mos=0;
	effectiveSkill=NONE;
}

void SuccessRoll::setRoll(RPG::Character *ch, const QString &skillname, int modifiers)
{
	if (ch!=NULL) {
		Skill *skill=ch->getSkill(skillname);
		if (skill!=NULL)
			effectiveSkill=skill->getFinalLevel(ch)+modifiers;
	}
	mos=0;
}

void SuccessRoll::setRoll(Character *ch, Skill *skill, int modifiers)
{
	if (skill!=NULL && ch!=NULL) 
		effectiveSkill=skill->getFinalLevel(ch)+modifiers;
	mos=0;
}

void SuccessRoll::setRoll(int effective)
{
	effectiveSkill = effective;
	mos = 0;
}

SuccessRollResult SuccessRoll::roll()
{
	lastRoll=die.throwDie();
	mos=effectiveSkill-lastRoll;
	if (lastRoll==3 || lastRoll==4 || mos>=10) return CRITICALSUCCESS;
	if (lastRoll==18 || (lastRoll==17 && effectiveSkill<=15) || mos<=-10 ) return CRITICALFAILURE;
	if (mos>=0) return SUCCESS;
	else return FAILURE;
}

int SuccessRoll::getMarginOfSuccess() const
{
	return mos;
}

/* Damage */
Damage::Damage()
{
	this->DieType=UNDEFINED_DMG_DIE;
	this->DmgThrMod=0;
	this->DmgThrType=OTHERDMG;
	this->DmgSwMod=0;
	this->DmgSwType=OTHERDMG;
	this->DmgDie.clear();
}

DamageType Damage::parseDamageType(QString &dmg) const
{
	if (dmg=="burn") return BURN;
	if (dmg=="cr") return CRUSHING;
	if (dmg=="cut") return CUTTING;
	if (dmg=="imp") return IMPALING;
	if (dmg=="pi-") return SMALLPIERCING;
	if (dmg=="pi") return PIERCING;
	if (dmg=="pi+") return LARGEPIERCING;
	else return OTHERDMG;
}

void Damage::setDamage(QString &dmg)
{
	this->damage=dmg;
	this->DieType=UNDEFINED_DMG_DIE;
	this->DmgThrMod=0;
	this->DmgThrType=OTHERDMG;
	this->DmgSwMod=0;
	this->DmgSwType=OTHERDMG;
	this->DmgDie.clear();

	removeWhitespaces(dmg);
	dmg.toLower();

	QRegExp dieRegExp(QString("(\\d+[dk]\\d*[\\-\\+]?\\d*)(burn|cr|cut|imp|pi\\-|pi|pi\\+)?"));
	QRegExp STDmgExp(QString("(thr|sw)([\\-\\+]\\d+)?(burn|cr|cut|imp|pi\\-|pi|pi\\+)?(thr|sw)?([\\-\\+]\\d+)?(burn|cr|cut|imp|pi\\-|pi|pi\\+)?"));

	if (dieRegExp.indexIn(dmg)>=0) {	//ak je v tvare hodu kockou
		DieType=DIE;
		DmgDie.setDie(dieRegExp.cap(1));
		if (!dieRegExp.cap(2).isEmpty()) 
			DmgDie.dmgType=parseDamageType(dieRegExp.cap(2));
	}
	else {
		STDmgExp.indexIn(dmg);
		DieType=STDmgExp.cap(1)=="thr"?THRUST:SWING;
		
		if (DieType==THRUST) {
			DmgThrMod=STDmgExp.cap(2).toInt();
			if (!STDmgExp.cap(3).isEmpty()) 
				DmgThrType=parseDamageType(STDmgExp.cap(3));
		}
		else { 
			DmgSwMod=STDmgExp.cap(2).toInt();
			if (!STDmgExp.cap(3).isEmpty()) 
				DmgSwType=parseDamageType(STDmgExp.cap(3));
		}		

		if (!STDmgExp.cap(4).isEmpty()) {
			DieType=BOTH;
			if (STDmgExp.cap(4)=="thr") {
				DmgThrMod=STDmgExp.cap(5).toInt();
				if (!STDmgExp.cap(6).isEmpty()) 
				DmgThrType=parseDamageType(STDmgExp.cap(6));
			}
			else {
				DmgSwMod=STDmgExp.cap(5).toInt();
				if (!STDmgExp.cap(6).isEmpty()) 
				DmgSwType=parseDamageType(STDmgExp.cap(6));
			}
		}
	}
}

DamageDieType Damage::getDamageDieType() const
{
	return DieType;
}

Die Damage::getDamageDie(Character *ch) const
{
	if (DieType==DIE) {	//tu budeme pokracovat hodom kockou
		return DmgDie;
	}
	else {					//tu vytvorime hod kocky podla parametru ch
		Die tmp;
		if (DieType==THRUST) {
			tmp=ch->getDmgThr();
			tmp.addMod(DmgThrMod);
			tmp.dmgType=DmgThrType;
		}
		else if (DieType==SWING) {
			tmp=ch->getDmgSw();
			tmp.addMod(DmgSwMod);
			tmp.dmgType=DmgSwType;
		}
		else return Die();
		return tmp;
	}
}

Die Damage::getDamageDieThr(Character *ch) const
{
	if (DieType==THRUST) {
		Die tmp;
		tmp=ch->getDmgThr();
		tmp.addMod(DmgThrMod);
		tmp.dmgType=DmgThrType;
		return tmp;
	}
	else return Die();
}

Die Damage::getDamageDieSw(Character *ch) const
{
	if (DieType==SWING) {
		Die tmp;
		tmp=ch->getDmgSw();
		tmp.addMod(DmgSwMod);
		tmp.dmgType=DmgSwType;
		return tmp;
	}
	else return Die();
}

const QString& Damage::getDamageString() const
{
	return damage;
}

/* class RPGObject */
RPGObject::RPGObject() 
{
	// generovanie identifikatoru objektu
	id.clear();
	id += "$";
	for(int i=0; i<10; ++i)
		id += char (26*qrand() / RAND_MAX +65);

	// vytvorenie prazdnej pixmapy a nastavenie priesvitnosti
	pixmap = new QPixmap(sidesize,sidesize);
	QPixmap alpha(sidesize,sidesize);
	alpha.fill(Qt::transparent);
	pixmap->setAlphaChannel(alpha);
	
	// vytvorenie painteru
	painter = new QPainter(pixmap);
	painter->setRenderHint(QPainter::HighQualityAntialiasing,true);
	pixmapRect.setRect(0,0,sidesize,sidesize);

	objectLocation.clear();
}

RPGObject::RPGObject(const RPGObject &obj)
{
	//this->pixmap=new QPixmap(*obj.pixmap);
	this->pixmapRect=obj.pixmapRect;
	//this->painter=new QPainter(pixmap);
	operator =(obj);
}

RPGObject& RPGObject::operator =(const RPGObject& obj)
{
	//delete pixmap;
	//delete painter;
	this->pixmap=new QPixmap(*obj.pixmap);
	this->pixmapRect=obj.pixmapRect;
	this->painter=new QPainter(pixmap);
	painter->setRenderHint(QPainter::HighQualityAntialiasing,true);
	this->id = obj.id;
	objectLocation = obj.objectLocation;
	return *this;
}

void RPGObject::drawBackground(const QColor &bgcolor)
{
	painter->begin(pixmap);
	painter->fillRect(pixmapRect, QBrush(bgcolor));
	painter->end();
}

void RPGObject::drawText(const QString &str)
{
	painter->begin(pixmap);
	painter->setFont(QFont("Arial",sidesize/2,QFont::Bold));
	painter->setPen(Qt::black);
	painter->drawText(pixmapRect,Qt::AlignCenter , str);
	painter->end();
}

void RPGObject::drawCircle(const QColor &color)
{
	painter->begin(pixmap);
	painter->setBrush(QBrush(color));
	painter->setPen(color);
	painter->drawEllipse(pixmapRect);
	painter->end();
}

/* Automaticky detekuje typ obrazku.  Nakresli texture z obrazku zacinajucu na suradniciach (x,y) 
velkosti (sx,sy). Tuto potom nascaluje na sidesize. (ak nie je uvedene x, y, sx a sy tak nacita 
texturu z obrazku zacinajucu na suradnici (0,0) a konciacu na (sidesize,sidesize)).*/
void RPGObject::drawTexture(const QImage &image, int x, int y, int sx, int sy, bool fit)
{
	painter->begin(pixmap);
	if (x==0 && y==0 && sx==0 && sy==0) {
		x=y=0;
		if (fit) {
			sx = image.width();
			sy = image.height();
		}
		else sx=sy=sidesize;
	}
	QImage texture=image.copy(x,y,sx,sy);
	//texture=texture.scaled(sidesize,sidesize);
	if (fit) 
		texture=texture.scaled(sidesize,sidesize);
	painter->drawImage(0,0,texture);
	painter->end();
}

const QPixmap& RPGObject::getPixmap() const
{
	return (const QPixmap&)*pixmap;
}

void RPGObject::removeFromLocation()
{
	ObjectLocationType loctype = objectLocation.getLocationType();
	if (loctype == MAP_LOC) {
		Map *map = objectLocation.getMap();
		if (map == NULL) {
			objectLocation.clear();
			return;
		}
		int x = objectLocation.getX();
		int y = objectLocation.getY();
		map->removeObject(x,y,this);
	}
	else if (loctype == INVENTORY_LOC) {
		Inventory *inv = objectLocation.getInventory();
		if (inv == NULL) {
			objectLocation.clear();
			return;
		}
		InventoryObject *obj = inv->removeItem(objectLocation.getIndex());
	}
	else if (loctype == ITEM_LOC) {
		InventoryObject *obj = objectLocation.getItem();
		if (obj == NULL) {
			objectLocation.clear();
			return;
		}
		obj->removeEmbeddedObject(this);
	}
	else if (loctype == MONSTER_LOC) {
		Monster *mon = objectLocation.getMonster();
		if (mon == NULL) {
			objectLocation.clear();
			return;
		}
		mon->removeEmbeddedObject(this);
	}
	objectLocation.clear();
}

RPGObject::~RPGObject()
{
	removeFromLocation();
	delete painter;
	delete pixmap;
}

bool RPGObject::moveOnMap(int tox, int toy, bool spendmove)
{
	if (objectLocation.getLocationType() != MAP_LOC) return false;
	Map *map = objectLocation.getMap();
	if (map == NULL) return false;
	int x = objectLocation.getX();
	int y = objectLocation.getY();
	return map->moveObject(x,y,id,tox,toy,spendmove);
}