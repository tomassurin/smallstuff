/******************************************************************************
 * Implementacia tried mapy a kampane
******************************************************************************/
#ifndef RPGCAMPAIGN_H
#define RPGCAMPAIGN_H

#include <QMap>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QtGui>
#include <QWidget>
#include <QVector>
#include <QGraphicsView>

#include "gameobjects.h"
#include "characterwidget.h"
#include "rpgcombat.h"

class QString;

namespace RPG {
	
	// dopredne deklaracie
	class Campaign;
	class Map;
	class Trigger;
	class Value;
	class MapWidget;
	
	// zaciatocna velkost mapoveho widgetu
	static int mapWidgetSize=500;
	// typ pre reprezentaciu suradnice na mape
	typedef QPair<int,int> MapPoint; 

	/** Trieda reprezentujuca teren - info o priechodnosti a dalsich parametrov prostredia */
	class RPGTerrain : public RPGObject
	{
	public:
		RPGTerrain(bool walkable = false);
		/** Nastavi ci je teren priechodny */
		void setWalkable(bool value) { walkable=value;}
		/** Zisti ci je teren priechodny */
		bool isWalkable() const { return walkable;}
	    /** Nastavi cenu (v movePt) za pohyb po tomto terene */
		void setCost(int value) { cost = value; }
		/** Zisti cenu terenu */
		int getCost() const { return cost; }
		
	private:
		// priechodnost terenu
		bool walkable;
		// cena terenu
		int cost;
	};
	
	/** Trieda reprezentuje zakladnu bunku mapy */
	class BasicMapNode : public QGraphicsItem
	{
	public:
		BasicMapNode();
		QRectF boundingRect() const;
		/** Vrati suradnicu x bunky (v hernych suradniciach) */
		int getX() const { return x;}
		/** Vrati suradnicu y bunky (v hernych suradniciach) */
		int getY() const { return y;}
		/** Nastavi herene suradnice bunky */
		void setCoord(int x, int y);
		/** Nastavi mapu do ktorej patri bunka */
		virtual void setMap(Map *map) { 
			this->map = map; 
		}
		/** Update tooltipu */
		virtual void updateTooltip() {};

		friend class Map;
	protected:
		// mapa do ktorej patri bunka
		Map *map;
		// suradnice bunky
		int x,y;
	};

	/* Trieda reprezentuje jednu bunku na mape - su tu ulozene informacie na jej 
	 * vykreslenie a informacie o objektoch nachadzajucej sa na nej */
	class MapNode : public QObject, public BasicMapNode
	{
		Q_OBJECT
	public:
		MapNode();
		~MapNode();
		
		/** Nastavi typ terenu na bunke s identifikatorom id - z definicie terenu mapy */
		void setTerrain(const QString &id);
		/** Vrati teren bunky */
		RPGTerrain *getTerrain();

		/** Prida na bunku objekt */
		void addObject(RPGObject *object);
		/** Ziska prvy objekt z bunky */
		RPGObject* getFirstObject();
		/** Ziska objekt z bunky s identifikatorom id */
		RPGObject* getObject(const QString &id);
		/** Ziska objekty z bunky typu T */
		template<class T> QList<T*> getObjects();
		/** Ziska viditelne objekty z bunky (tj. Character, InventoryObject, Weapon, Armor) */
		QList<RPGObject*> getVisibleObjects();
		/** Zisti ci bunka obsahuje objekt typu T */
		template <class T> bool containsObject();
		/** Odstrani objekt z bunky a vymaze ho */
		void deleteObject(RPGObject *object);
		/** Odstrani objekt z bunky a vrati ho */
		RPGObject* removeObject(RPGObject *object);		
		/** Odstrani objekt s identifikatorom id z bunky a vrati ho */
		RPGObject* removeObject(const QString &id);
		/** Zisti ci je teren na bunke priechodny */
		bool isWalkable() const;
		/** Nastavi mapu do ktorej bunka patri */
		virtual void setMap(Map *map);

	public slots:
		virtual void updateTooltip();

	protected:
		/** Prekreslenie bunky - vyziadava si to mapwidget */
		virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);

	protected:
		// teren bunky
		RPGTerrain *terrain;
		// objekty na bunke
		QList<RPGObject *> objects;
	};

	/** Bunka mapy reprezentujuca hraca - moze prekryvat ostatne bunky */
	class PlayerNode : public BasicMapNode, public Player
	{
	public:
		PlayerNode();
		~PlayerNode();

		/** Pohyb na bunku so suradnicami (tox;toy). Spendmove udava ci ma minat movePt hraca */
		bool moveTo(int tox, int toy, bool spendmove = false);
		/** Pohyb o (byx;byy) buniek. Spendmove udava ci ma minat movePt hraca */
		void move(int byx, int byy, bool spendmove = false);
		/** Nastavi mapu do ktorej bunka patri */
		virtual void setMap(Map *map);
		/** Pohyb po mape na suradnicu (tox;toy) */
		virtual bool moveOnMap(int tox, int toy, bool spendmove = false);

	protected:
		/** Prekreslenie bunky / vyziadava si to mapwidget */
		virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget=0);
	};

	/* Trieda reprezentujuca mapu */
	class Map : public RPGObject
	{
	public:
		Map();
		~Map();
		/** Vymazanie mapy */
		void clear();

		/** Nastavi velkost mapy (hernu) */
		void setSize(int width, int height);
		/** Ziska velkost mapy (hernu) */
		QSize getSize() const { return QSize(width,height);}

		/** Nastavi bunku na ktorej sa zacina */
		void setStartPos(int x, int y) { startx = x; starty = y; }
		/** Ziska bunku na ktorej sa zacina */
		QPoint getStartPos() const { return QPoint(startx, starty); }

		/** Ziska bunku na suradniciach (x;y) (hernych) */
		MapNode *getNode(int x, int y);
		/** Prida do mapy bunku */
		void addNode(MapNode *node);
		/** Vymaze bunku na suradniciach (x;y) (hernych) */
		void removeNode(int x, int y);
		/** Posunie objekt na suradniciach (x;y) (hernych) s identifikatorom id na bunku so 
		  * suradnicami (tox;toy) (herne). spendmove udava ci ma minat movePt postavy */
		bool moveObject(int x, int y, const QString &id, int toX, int toY, bool spendmove = false);
		/** Odstrani objekt z bunky so suradnicami (x;y) s identifikatorom id a vrati ho */
		RPGObject *removeObject(int x, int y, const QString &id);
		/** Odstrani objekt z bunky so suradnicami (x;y) a vrati ho */
		RPGObject *removeObject(int x, int y, RPGObject *obj);
		/** Odstrani a vymaze objekt z bunky so suradnicami (x;y) a identifikatorom id */ 
		bool deleteObject(int x, int y, const QString &id);

		/** Prida typ terenu s identifikatorom id do definicie terenov */
		void addTerrainType(const QString& id, RPGTerrain *terraintype);
		/** Vrati typ terenu s identifikatorom id */
		RPGTerrain *getTerrainType(const QString& id);
		/** Prida typ objektu s identifikatorom id do definicie objektov */
		void addObjectType(const QString& id, RPGObject *object);
		/** Vrati novy objekt s identifikatorom id z definicie objektov */
		RPGObject *getObjectType(const QString& id);
		
		/** Nastavi aktualny adresar */
		void setCwd(const QString &cwd) { this->cwd = cwd; }
		/** Vrati aktualny adresar */
		const QString &getCwd() { return cwd; }

		/** Ziska MapWidget */		
		MapWidget *getMapWidget() { return mapWidget;}
		/** Nastavi MapWidget mapy */
		void setMapWidget(MapWidget *w) { mapWidget = w; }
		
		/** Prida bunku hraca do mapy */
		void addPlayer(PlayerNode *player);
		/** Ziska bunku hraca */
		PlayerNode *getPlayer() { return player; }
		/** Vymaze bunku hraca */
		void removePlayer();

		// not implemented
		bool isFogOfWar(int x, int y);
		void setFogOfWar(int x, int y, int radius);
		void unsetFogOfWar();

		friend class MapWidget;
	protected:
		// graficka scena
		QGraphicsScene *scene;
		// bunka hraca
		PlayerNode *player;
		// definicia terenu
		QMap<QString,RPGTerrain*> terrainTypedef;
		// definicia objektov
		QMap<QString,RPGObject*> objectTypedef;
		// pociatocne suradnice na mape (herne)
		int startx, starty;
		// sirka a vyska mapy (herne suradnice)
		int width, height;
		// aktualny adresar
		QString cwd;
		// widget mapy
		MapWidget *mapWidget;

		// not implemented
		QSet<MapPoint> noFogNodes;
	};

	/** Trieda sluziaca na vyhladanie najkratsej cesty na mape - dijkstrov algoritmus */
	class MapFindPath
	{
	public:
		/** Konstruktor nastavi mapu */
		MapFindPath(Map *map);
		~MapFindPath();

		// body na ceste
		typedef QPair<int,int> VertexPos;
		// v tomto type sa uklada zoznam cesty
		typedef QList<VertexPos> PathList;
		// struktura pouzivana pri behu algoritmu
		struct Vertex {
			VertexPos prev;
			VertexPos pos;
			bool final;
			bool walkable;
			int dist;
			int cost;
		};
		/** Vrati cestu (ak existuje) medzi (x;y) a (tox;toy) na mape (v hernych suradniciach) */
		PathList find(int x, int y, int tox, int toy, bool reset = true);
		/** Inicializuje strukturu bodu (x;y) */
		Vertex *reinitVertex(int x, int y);
		/** Nastavi ci ma pocas vyhodnocovania cesty ignorovat objekty  alebo ich brat ako nepriechodne */
		void setIgnoreObjects(bool val) { ignoreObjects = val;}
		/** Vrati manhatansku vzdialenost medzi (x;y) a (tox;toy) na mape (v hernych suradniciach) */
		static int distance(int x, int y, int tox, int toy);

	private:
		/** Inicializacia */
		void init();
		/** Reinicializacia */
		void reinit(int x, int y,int tox, int toy);
		/** Inicializacia vrcholu */
		Vertex *initVertex(int x, int y);
		/** Hlavny algoritmus */
		void algorithm();
		/**	Vrati minimalny bod z progress zoznamu */
		VertexPos minimumVertex();
		/** Spatny beh algoritmu */
		void findPath(Vertex *);
		/** Vrati vrchol reprezentujuci bunku (x;y) */
		Vertex *getVertex(int x, int y);

		// vrcholy do, kt. som sa uz dostal a nie su este konecne
		QList<Vertex*> progress;			
		// buffer na vrcholy ktore sme uz nacitali z mapy
		QHash<VertexPos,Vertex*> edges;		
		// vysledna cesta
		PathList path;
		// pociatocny vrchol
		Vertex *source;
		// mapa
		Map *map;
		// velkost mapy
		QSize mapSize;
		// koncovy vrchol
		int tox, toy;
		// nastavenie ci mame alebo nemame ignorovat objekty na mape
		bool ignoreObjects;
	};
	
	/** Vlakno sluziace pre animaciu pohybu NPC postavy po mape */
	class PathAnimation : public QThread
	{
		Q_OBJECT
	public:
		PathAnimation(Map *map);
		/** Nastavi cestu p objektu obj */
		void setObjectPath(const MapFindPath::PathList &p, RPGObject *obj);
	public slots:
		/** Zaciatok behu casovaca */
		void startTimer();
	private slots:
		/** Jeden pohyb postavy */
		void movePathStep();
	protected:
		/** Inicialzacia vlakna */
		void run();
	private:
		// casovac
		QTimer *timer;
		// cesta
		MapFindPath::PathList path;
		// hladac cesty
		MapFindPath *pathFinder;
		// objekt
		RPGObject *object;
	};

	/* Widget pre vykreslenie mapy */
	class MapWidget : public QGraphicsView
	{
			Q_OBJECT
	public:
		MapWidget(QWidget* parent=0);
		
		/** Vracia potrebnu velkost widgetu */
		virtual QSize sizeHint() const;

		/** Nastavi mapu */
		void setMap(Map *map);
		/** Ziska mapu */
		Map *getMap() { return map; }

		/** Vymaze widget */
		void clear();
		/** Nastavi delay v animacii pohybu postavy */
		void setSpeed(int sp) { speed = sp; }
		/** Vrati delay v animacii pohybu postavy */
		int getSpeed() const { return speed; }
		
		friend class Map;
		friend class Campaign;
		
	protected:
		/** Spracovanie udalosti klavesnice */
		void keyPressEvent(QKeyEvent *event);
		/** Spracovanie kolecka mysi */
		void wheelEvent(QWheelEvent *event);
		/** Spracovanie udalosti mysi */
		void mousePressEvent(QMouseEvent *event);

	public slots:
		/** Vymaze hraca */
		void removePlayer();
		/** Nastavi cestu path hraca */
		void setPlayerPath(const MapFindPath::PathList &path);
		/** Nastavi cestu objektu */
		void setObjectPath(const MapFindPath::PathList &path, RPGObject *mon);

	private slots:
		/** Pohne hraca o jeden krok */
		void movePlayerPathStep();

	signals:
		/** Reprezentuje kliknutie mysou na bunku so suradnicami (x;y) (herene suradnice) */
		void mapWidgetClicked(int x, int y);
		/** Reprezentuje start casovaca v animovacom objekte */
		void animTimerStart();

	private:
		// casovac na pohyb postavy
		QTimer *timer;
		// cesta postavy pri animacii
		MapFindPath::PathList path;
		// vyhladavac cesty
		MapFindPath *pathFinder;
		// aktualna mapa
		Map *map;		
		// aktualny hrac
		PlayerNode *player;	
		// objekt, kt. sa pohybuje
		RPGObject *object;
		// rychlost pohybu
		int speed;
		// indikator ci widget pracuje alebo nie
		bool processing;
		// objekt animatora pohybu objektov mapy
		PathAnimation* anim;
	};

	/* Widget na zobrazenie textovych hlasok pocas behu */
	class StatusWidget : public QTextEdit
	{
		Q_OBJECT
	public:
		StatusWidget(QWidget *parent = 0);
		virtual QSize sizeHint() const;
	};

	/* Hlavny widget a taktiez objekt reprezentujuci danu kampan
	 * Su tu ulozene udaje o kampani, globalne objekty kampane
	 * dalej sa tu nachadza widget mapy, postavy a statusu */
	class Campaign : public QWidget
	{
			Q_OBJECT
	public:
		Campaign(QWidget *parent=0);
		~Campaign();

		enum EventType { STOP, CONTINUE };

		/** Ziska nazov kampane */
		QString getName() const { return name;}
		/** Nastavi nazov kampane */
		void setName(const QString &name);
		
		/** Prida hraca */
		void addPlayer(PlayerNode *player);
		/** Vymaze hraca */
		void clearPlayer();
		/** Ziska hraca */
		PlayerNode *getPlayer() { return player; }

		/** Vypis hlasky do status widgetu */
		void writeStatus(const QString &msg);
		/** Vymazanie aktualnej kampane */
		void clear();

		/** Vrati cislo tahu */
		uint getTurn() const { return turn; }
		/** Nastavi cislo tahu */
		void setTurn(uint t);

		// praca s mapou - zmena a ziskanie aktualnej mapy
		/** Vrati aktualnu mapu */
		Map *getCurrentMap();
		/** Nacita aktualnu mapu zo suboru s adresou filename a trigger ulozi do trigger cache */
		bool setCurrentMap(const QString &filename, Trigger *trigger);
		/** Nastavi aktualnu mapu */
		bool setCurrentMap(Map *map);
		
		/** Nastavi globalnu premennu triggerov s nazvom name na hodnotu val */
		void setTriggerVar(const QString &name, Value val);
		/** Vrati hodnotu globalnej premennej triggerov s nazvom name */
		Value getTriggerVar(const QString &name) const;
		/** Vymaze globalnu premennu triggerov s nazvom name */
		void removeTriggerVar(const QString &name);
		/** Naplanuje vykonanie triggeru na tah t */
		void schedule(uint t, Trigger *trig);
		/** Prida trigger do trigger cache - ak vymazavame objekty alebo mapu nech ma trigger cas na ukoncenie */
		void addTriggerCache(Trigger *trig) ;
		
		/** Zistime ci prave prebieha nejake spracuvavanie udalosti */
		bool isProcessing() {
			return mapWidget->processing;
		}
		/** Ziska widget mapy */
		MapWidget *getMapWidget() { return mapWidget;} 

		friend class FunctionEval;

	public slots:
		/** Funkcia volana pri premiestneni hraca */
		EventType processMapEvents(int x, int y, Character *ch);
		/** Funkcia volana pri zmene tahu */
		void processTurnEvents();
		/** Funkcia volana pri kliknuti na bunku (x;y) (herne suradnice) */
		void processMapWidgetMouse(int x, int y);
		
		/** Prida hraca do boja */
		void addPlayerToCombat();

		/** Zobrazi dialog postavy */
		void showSelfMenu();

	signals:
		/** Reprezentuje koniec zivota hraca */
		void playerEnded();
		/** Emitnuty pri zmene mena kampane */
		void campaignNameChanged();
		/** Emitnuty pri zmene mapy */
		void currentMapChanged();
		/** Emitnuty pri zmene tahu */
		void turnChanged();
		/** Emitnuty pri kliknuti na mapu */
		void mapWidgetClicked(int x, int y);

	private slots:
		/** Pohyb hraca na bunku (tox;toy) (herne suradnice) */
		void movePlayerPath(int tox, int toy);
		
		/** Prida nepriatelov zo zoznamu do suboja */
		void addMonstersToCombat(QList<Monster*> &monsters);

	protected:
		// widget mapy
		MapWidget *mapWidget;
		// aktualna mapa
		Map *map;
		// nazov kampane
		QString name;
		// widget informacii o hracovi 
		CharacterWidget *character;
		// status widget
		StatusWidget *status;
		// objekt suboja
		RPGCombat *combat;
		// objekt hraca
		PlayerNode *player;
		// indikuje ci prebieha vykonavanie udalosti
		bool processing;
		// cache triggerov 
		QList<Trigger *> triggercache;
		// globalne (v ramci kampane) premenne triggerov
		QMap<QString, Value> TriggerVars;
		// pole naplanovanych triggerov
		QMultiMap<uint,Trigger *> schedtriggers;
		// cislo tahu
		uint turn;

		// dialog akcii postavy s priserkou
		RPGDialog *monsterMsgBox;
		QPushButton *talkButton;
		QPushButton *attackButton;
		QPushButton *abortButton;
	
		// dialog vyberu manevru pocas suboja
		RPGDialog *attackMsgBox;
		QPushButton *ordinaryAttackButton;
		QPushButton *determinedAttackButton;
		QPushButton *doubleAttackButton;
		QPushButton *strongAttackButton;
		QPushButton *waitButton;

		// dialog vyberu dalsich akcii hraca
		RPGDialog *selfMsgBox;	
		QPushButton *selfWaitButton;
		QPushButton *selfAbortButton;
		QPushButton *doubleDefenseButton;
		QPushButton *strongDefenseButton;
		QPushButton *skillLevelButton;

		// dialog zlepsenia dovednosti
		SkillLevelDialog *skillLevDial;
	};

	/** Event filter na znemoznenie vstupu ked sa vykonava nejaka udalost */
	class BusyAppFilter : public QObject
	{
	protected:
		bool eventFilter( QObject *obj, QEvent *event );
	};
}

#endif