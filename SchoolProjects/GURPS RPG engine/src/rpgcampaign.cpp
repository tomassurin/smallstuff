#include <QPainter>
#include <QtGui>

#include "rpgcampaign.h"
#include "xmlreader.h"
#include "triggerevent.h"

using namespace RPG;

bool BusyAppFilter::eventFilter(QObject *obj, QEvent *event)
{
	switch ( event->type() ) {
		case QEvent::KeyPress:
		case QEvent::KeyRelease:
		case QEvent::MouseButtonPress:
		case QEvent::MouseButtonDblClick:
		case QEvent::MouseMove:
		case QEvent::HoverEnter:
		case QEvent::HoverLeave:
		case QEvent::HoverMove:
		case QEvent::DragEnter:
		case QEvent::DragLeave:
		case QEvent::DragMove:
		case QEvent::Drop:
			return true;
		default:
			return QObject::eventFilter( obj, event );
	}
}


// konverzia suradnic zo sceny do hernych
QPoint sceneToGame(QPointF pt)
{
	int x = (int) pt.x() / sidesize;
	int y = (int) pt.y() / sidesize;
	return QPoint(x,y);
}

// konverzia suradnic z hernych do scenovych
QPointF gameToScene(QPoint pt)
{
	return QPointF(sidesize*pt.x()+sidesize/2,sidesize*pt.y()+sidesize/2);
}

/* class RPGTerrain */
RPGTerrain::RPGTerrain(bool walkable) : RPGObject()
{
	this->walkable=walkable;
	this->cost=100;
	drawBackground(Qt::transparent);
}

/* class BasicMapNode */
BasicMapNode::BasicMapNode() : QGraphicsItem()
{
	map = NULL;
	x = -1;
	y = -1;
}

QRectF BasicMapNode::boundingRect() const
{
	return QRectF(-sidesize/2,-sidesize/2,sidesize,sidesize);
}

void BasicMapNode::setCoord(int x, int y)
{
	this->setPos(x*sidesize+sidesize/2,y*sidesize+sidesize/2);
	this->x=x;
	this->y=y;
	updateTooltip();
}

/* class MapNode */ 
MapNode::MapNode() : BasicMapNode()
{
	this->terrain=NULL;
}

MapNode::~MapNode()
{
	QQueue<RPGObject *>::iterator it;
	for(it=objects.begin();it!=objects.end();++it)
		(*it)->release();
	this->objects.clear();
}

void MapNode::setTerrain(const QString & id)
{
	terrain=map->getTerrainType(id);
	update(sidesize*x,sidesize*y,sidesize,sidesize);
}

RPGTerrain *MapNode::getTerrain()
{ 
	return terrain;
}

void MapNode::addObject(RPGObject *object)
{
	if (object == NULL) return;
	object->location()->setMap(map,x,y);
	if (typeid(*object)==typeid(Monster)) {
		objects.push_back(dynamic_cast<Monster*>(object));
		connect(dynamic_cast<const Monster*>(objects.first()),SIGNAL(statsInfoChanged()),this,SLOT(updateTooltip()));
	}
	else {
		objects.push_back(object);
	}
	update(sidesize*x,sidesize*y,sidesize,sidesize);
	updateTooltip();
}

RPGObject* MapNode::getFirstObject()
{
	if (!objects.isEmpty())	return objects.first();
	else return NULL;
}

RPGObject* MapNode::getObject(const QString &id)
{
	foreach( RPGObject *obj, objects) {
		if (obj->getID() == id) return obj;		
	}
	return NULL;
}

template <class T>
QList<T*> MapNode::getObjects()
{
	QList<T*> list;
	T *temp;
	foreach( RPGObject *obj, objects) {
		temp = dynamic_cast<T*>(obj);
		if (temp == NULL) continue;
		list.append(temp);
	}
	return list;
}

QList<RPGObject*> MapNode::getVisibleObjects()
{
	QList<RPGObject *> list;
	foreach( RPGObject *obj, objects) {
		if (typeid(*obj) == typeid(Trigger)) continue;
		list.append(obj);
	}
	return list;
}

template <class T>
bool MapNode::containsObject()
{
	T *temp;
	foreach( RPGObject *obj, objects) {
		temp = dynamic_cast<T*>(obj);
		if (temp != NULL) return true;
	}
	return false;
}

void MapNode::deleteObject(RPGObject *object)
{
	objects.removeOne(object);
	object->release();
	updateTooltip();
	update(sidesize*x,sidesize*y,sidesize,sidesize);
}

RPGObject* MapNode::removeObject(RPGObject *object)
{
	if (!objects.removeOne(object)) return NULL;
	object->location()->clear();
	updateTooltip();
	update(sidesize*x,sidesize*y,sidesize,sidesize);
	return object;
}

RPGObject* MapNode::removeObject(const QString &id)
{
	foreach( RPGObject *obj, objects) {
		if (obj->getID() == id) {
			objects.removeOne(obj);
			obj->location()->clear();
			update(sidesize*x,sidesize*y,sidesize,sidesize);
			updateTooltip();
			return obj;
		}
	}
	return NULL;
}

void MapNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	if (map == NULL) return;
	/*//if (map->isFogOfWar(x,y)) {
	PlayerNode* pl = map->getPlayer();
	if (pl == NULL) return;
	if (MapFindPath::distance(x,y,pl->getX(),pl->getY())>pl->getScoutingRadius()) {
		return;
	}
	*/
	// vykreslenie terenu
	if (terrain!=NULL) {
		painter->drawPixmap(-sidesize/2,-sidesize/2,terrain->getPixmap());
	}
	else {
		QPixmap pixmap(sidesize,sidesize);
		pixmap.fill(Qt::transparent);
		painter->drawPixmap(-sidesize/2,-sidesize/2,pixmap);
	}

	// vykreslenie objektov
	if (!objects.isEmpty()) {  
		// najskor sa pokusime o nakreslenie nepriatelov
		QList<Monster*> vismon = getObjects<Monster>();
		if (vismon.isEmpty()) {
			// ak nie su nepriatelia pokusime sa o nakreslenie objektu z bunky
			QList<RPGObject*> visobj = getVisibleObjects();
			if (visobj.isEmpty()) return;
			painter->drawPixmap(-sidesize/2,-sidesize/2, visobj.last()->getPixmap());
		}
		else {
			painter->drawPixmap(-sidesize/2,-sidesize/2, vismon.last()->getPixmap());
		}
	}
}

void MapNode::updateTooltip()
{
	QString str;
	str=QString("x=%1 y=%2").arg(x).arg(y);
	str+=QString("<hr>");
	str +=QString("<table width=\"100%\">");
	foreach(RPGObject *obj, objects) {
		if (obj == NULL) continue;
		if (typeid(*obj) == typeid(Monster)) {
			Monster *mon = dynamic_cast<Monster*>(obj);
			str+=QString("<tr><td>%1</td><td>%2/%3</td>").arg(mon->getName()).arg(mon->getcHP()).arg(mon->getHP());
		}
		else if (typeid(*obj) == typeid(Trigger)) {
			str+=QString("<tr><td>%1</td></tr>").arg(obj->getInfo());
		}
		else str+=QString("<tr><td>%1</td></tr>").arg(obj->getName());

	}
	str +=QString("</table>");

	setToolTip(str);
}

void MapNode::setMap(Map *map)
{
	BasicMapNode::setMap(map);
	foreach(RPGObject *obj, objects)
		obj->location()->setMap(map, x, y);
}

bool MapNode::isWalkable() const
{
	return terrain->isWalkable();
}

/* class PlayerNode */
PlayerNode::PlayerNode() 
	: BasicMapNode(), Player()
{
	objectLocation.setMap(map, x, y);
}

void PlayerNode::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	painter->drawPixmap(-sidesize/2,-sidesize/2,getPixmap());
}


void PlayerNode::move(int byx, int byy, bool spendmove)
{
	this->moveTo(getX()+byx, getY()+byy, spendmove);
}

bool PlayerNode::moveTo(int tox, int toy, bool spendmove)
{
	QString mapidold = map->getID();
	if (map == NULL) return false;
	//map->unsetFogOfWar();
	MapNode *node;
	node=map->getNode(tox,toy);
	if (node==NULL) return false;
	if (!node->isWalkable()) return false;
	int terraincost = node->getTerrain()->getCost();
	uint turnno = campaign->getTurn();
	int moveptold = getMovePt();

	Q_ASSERT( campaign != NULL);
	Campaign::EventType campevent = campaign->processMapEvents(tox, toy, this);
	if (campevent == Campaign::STOP) return false;

	if (this != campaign->getPlayer()) return false;
	if (map == NULL || mapidold != map->getID()) return false;
	if (campaign->getTurn() != turnno || moveptold != getMovePt()) return false;
		
	if (spendmove && this->getTotalMovePt() < terraincost) {
		return false;
	}

	if (spendmove)
		this->spendMovePt(terraincost);

	if (this != campaign->getPlayer()) return false;
	if (map == NULL || mapidold != map->getID()) return false;
		
	this->x=tox;
	objectLocation.setX(tox);
	this->y=toy;
	objectLocation.setY(toy);
	setPos(tox*sidesize+sidesize/2,toy*sidesize+sidesize/2);

	// map->setFogOfWar(x,y, getScoutingRadius());
	MapWidget *widget = map->getMapWidget();
	if (widget == NULL) return false;
	widget->centerOn(this);
	update(sidesize*x,sidesize*y,sidesize,sidesize);
	update(sidesize*tox,sidesize*toy, sidesize, sidesize);
	updateTooltip();

	//spracovanie triggerov na sucasnej bunke mapy
	QList<Trigger*> triggers=node->getObjects<Trigger>();
	if (!triggers.isEmpty()) {
		foreach(Trigger *trigg, triggers) {
			trigg->setObject(this);
			trigg->execute();
		}
	}
	return true;
}

void PlayerNode::setMap(Map *map)
{
	BasicMapNode::setMap(map);
	objectLocation.setMap(map,x,y);
	QPoint p = map->getStartPos();
	setCoord(p.x(),p.y());
}

PlayerNode::~PlayerNode()
{
}

bool PlayerNode::moveOnMap(int tox, int toy, bool spendmove)
{
	return moveTo(tox,toy, spendmove);
}

/* class Map */
Map::Map() : RPGObject()
{
	startx = 0;
	starty = 0;
	cwd.clear();

	scene=new QGraphicsScene(this);
	scene->setBackgroundBrush(QBrush(Qt::black));
	setSize(1,1);

	mapWidget = NULL;
	player = NULL;
	noFogNodes.clear();
}

void Map::setSize(int width, int height)
{
	this->width=width;
	this->height=height;
	scene->setSceneRect(0,0,width*sidesize,height*sidesize);
}

MapNode* Map::getNode(int x, int y) 
{
	if (sidesize*x+sidesize/2 >= scene->width() || sidesize*y+sidesize >= scene->height()) return NULL;
	QList<QGraphicsItem *> list=scene->items(gameToScene(QPoint(x,y)));
	QList<QGraphicsItem *>::iterator it=list.begin();
	while (it != list.end() && *it == player ) 
		++it;

	if (it!=list.end()) 
		return dynamic_cast<MapNode*>(*it);
	else return NULL;
}

void Map::addNode(MapNode *node)
{   
	int x = node->getX();
	int y = node->getY();
	if (x > width-1) {
		setSize(x+1,height);
	}
	if (y > height-1)
		setSize(width,y+1);
	node->setMap(this);
	scene->addItem(node);
}

void Map::removeNode(int x, int y)
{
	BasicMapNode *node = this->getNode(x,y);
	scene->removeItem(node);
	delete node;
}

//objekt na bunke (x,y) s identifikatorom id premiestnime do bunky (tox,toy)
bool Map::moveObject(int x, int y, const QString &id, int tox, int toy, bool spendmove)
{
	MapNode *srcnode = this->getNode(x,y);
	if (srcnode == NULL) return false;
	RPGObject *obj = srcnode->getObject(id);
	if (obj == NULL) return false;
	MapNode *dstnode = getNode(tox,toy);
	if (dstnode == NULL) return false;

	Monster *mon = dynamic_cast<Monster*>(obj);
	// pohyb NPC postavy
	if (mon != NULL) {
		if (!dstnode->getObjects<Monster>().isEmpty())
			return false;
		if (tox == player->getX() && toy == player->getY())
			return false;
		if (spendmove) {
			int terraincost = dstnode->getTerrain()->getCost();
			if (mon->getMovePt() < terraincost) 
				return false;
			mon->spendMovePt(terraincost);
		}
	}

	srcnode->removeObject(obj);
	dstnode->addObject(obj);
	return true;
}

RPGObject *Map::removeObject(int x, int y, const QString &id)
{
	MapNode *node = this->getNode(x,y);
	if (node == NULL) return NULL;
	return node->removeObject(id);
}

RPGObject *Map::removeObject(int x, int y, RPGObject *obj)
{
	MapNode *node = this->getNode(x,y);
	if (node == NULL) return NULL;
	return node->removeObject(obj);
}


bool Map::deleteObject(int x, int y, const QString &id)
{
	RPGObject *obj = removeObject(x,y,id);
	if (obj == NULL) return false;
	obj->release();
	return true;
}

void Map::addTerrainType(const QString& id, RPGTerrain *terraintype)
{
	terrainTypedef[id]=terraintype;
}

RPGTerrain *Map::getTerrainType(const QString& id)
{
	RPGTerrain *terrain = NULL;
	if (terrainTypedef.contains(id))
		terrain=terrainTypedef[id];
	if (terrain==NULL) {
		if (terrainTypedef.contains("default"))
			terrain=terrainTypedef["default"];
		if (terrain==NULL) { 
			terrain=new RPGTerrain();
			terrain->drawBackground(Qt::transparent);
		}
	}
	return terrain;
}

void Map::addObjectType(const QString& id, RPGObject *object)
{
	objectTypedef[id]=object;
}

RPGObject *Map::getObjectType(const QString& id)
{
	if (id.startsWith("rules/",Qt::CaseInsensitive)) {
		QStringList list = id.split("/");
		RPGObject *obj = rules.getObjectType(list[1]);
		if (obj != NULL)
			return obj;		
	}
	else if (objectTypedef.contains(id)) {
		RPGObject *obj = objectTypedef[id];
		if (typeid(*obj) == typeid(RPGObject))
			return new RPGObject(*(RPGObject*)obj);
		if (typeid(*obj) == typeid(Trigger)) 
			return new Trigger(*(Trigger*)obj);
		if (typeid(*obj) == typeid(Monster)) 
			return new Monster(*(Monster*)obj);
		if (typeid(*obj) == typeid(Weapon)) 
			return new Weapon(*(Weapon*)obj);
		if (typeid(*obj) == typeid(Armor))
			return new Armor(*(Armor*)obj);
		if (typeid(*obj) == typeid(InventoryObject)) 
			return new InventoryObject(*(InventoryObject*)obj);
	}
	else {
		RPGObject *obj = rules.getObjectType(id);
		if (obj != NULL)
			return obj;
	}
	return NULL;
}

void Map::clear()
{
	QMap<QString,RPGTerrain*>::iterator itt;
	for(itt=terrainTypedef.begin();itt!=terrainTypedef.end();++itt)
		(*itt)->release();
	terrainTypedef.clear();
	QMap<QString, RPGObject*>::iterator itm;
	for(itm=objectTypedef.begin();itm!=objectTypedef.end();++itm)
		(*itm)->release();
	objectTypedef.clear();

	player = NULL;
	mapWidget = NULL;
	scene->clear();
}

Map::~Map()
{
	this->clear();
	delete scene;
}

void Map::addPlayer(PlayerNode *pl)
{
	if (pl == NULL) return;

	if (player != NULL) {
		removePlayer();
	}

	player=pl;
	player->setMap(this);
	scene->addItem(player);
	player->setZValue(1000);
//	setFogOfWar(player->getX(),player->getY(), player->getScoutingRadius());
	scene->update();
	if (mapWidget != NULL) {
		mapWidget->player = pl;
		mapWidget->centerOn(player);
	}
}

void Map::removePlayer()
{
	if (player == NULL) return;
	//unsetFogOfWar();
	player->location()->clear();
	scene->removeItem(player);
	player=NULL;
}


void Map::setFogOfWar(int x, int y, int radius)
{
	if (radius < 0) return;
	--radius;
	noFogNodes.insert(MapPoint(x,y));
	
	MapNode*	node = getNode(x-1,y);
	if (node != NULL) 
		setFogOfWar(x-1,y,radius);
	
	node = getNode(x,y-1);
	if (node != NULL) 
		setFogOfWar(x,y-1,radius);

	node = getNode(x+1,y);
	if (node != NULL) 
		setFogOfWar(x+1,y,radius);

	node = getNode(x,y+1);
	if (node != NULL) 
		setFogOfWar(x,y+1,radius);
}

bool Map::isFogOfWar(int x, int y)
{
	return !noFogNodes.contains(MapPoint(x,y));
}

void Map::unsetFogOfWar()
{
	noFogNodes.clear();
}

/* class MapFindPath */
MapFindPath::MapFindPath(RPG::Map *map)
{
	this->map = map;
	mapSize = map->getSize();
	ignoreObjects = false;
	init();
}

MapFindPath::~MapFindPath()
{
	init();
}

// pocita manhatansku dlzku
int MapFindPath::distance(int x, int y, int tox, int toy)
{
	return abs(x-tox)+abs(y-toy);
}

MapFindPath::Vertex *MapFindPath::getVertex(int x, int y)
{
	if (x<0 || x >= mapSize.width() || y<0 || y >= mapSize.height()) 
		return NULL;
	if (edges.contains(VertexPos(x,y)))
		return edges[VertexPos(x,y)];
	return initVertex(x,y);
}

MapFindPath::PathList MapFindPath::find(int x, int y, int tox, int toy, bool reset)
{
	if (map->getNode(tox,toy) == NULL) return PathList();

	if (reset) init();
	else reinit(x,y,tox,toy);
	this->tox = tox;
	this->toy = toy;
	
	// inicializacia pociatocneho vrchola
	source = getVertex(x,y);
	if (source->final) return PathList();
	source->dist = 0;
	progress.append(source);

	// vlastny algoritmus
	algorithm();

	// vytvorenie cesty z (x,y) do (tox,toy)
	findPath(getVertex(tox,toy));
	edges.remove(VertexPos(tox,toy));
	edges.remove(VertexPos(x,y));
	return path;
}	

// inicializacia pomocnych premennych
void MapFindPath::init()
{
	progress.clear();
	path.clear();
	foreach(Vertex *vert, edges) {
		delete vert;
	}
	edges.clear();
	tox = -1;
	toy = -1;
	source = NULL;
}

void MapFindPath::reinit(int x, int y, int tox, int toy)
{
	path.clear();
	reinitVertex(x,y);
	reinitVertex(tox,toy);
	foreach(Vertex *vert, edges) {
		if (vert->walkable) {
			vert->dist = INT_MAX;
			vert->prev.first = -1;
			vert->prev.second = -1;
			vert->final = false;
		}
	}
}

MapFindPath::Vertex *MapFindPath::reinitVertex(int x, int y)
{
	edges.remove(VertexPos(x,y));
	return initVertex(tox,toy);
}

// inicializacia vrchola
MapFindPath::Vertex *MapFindPath::initVertex(int x, int y)
{
	MapNode *node = map->getNode(x,y);
	Vertex *vert = new Vertex;
	if (node == NULL || !node->isWalkable()) {
		vert->pos.first = x;
		vert->pos.second = y;
		vert->final = true;
		vert->walkable = false;
		vert->dist = INT_MAX;
		vert->cost = INT_MAX;
		vert->prev.first = -1;
		vert->prev.second = -1;
		edges[VertexPos(x,y)] = vert;
		return vert;
	}

	vert->final = false;
	vert->walkable = true;
	vert->dist = INT_MAX;
	vert->prev.first = -1;
	vert->prev.second = -1;
	vert->pos.first = x;
	vert->pos.second = y;
	if (!ignoreObjects && !node->getVisibleObjects().isEmpty())  {
		if (x == tox && y == toy) {
			vert->walkable = true;
		}
		else 
			vert->walkable = false;
	}
	vert->cost = node->getTerrain()->getCost();
	edges[VertexPos(x,y)] = vert;
	return vert;
}

// beh dijkstrovho algoritmu
void MapFindPath::algorithm()
{
	int i;
	VertexPos pt;
	int x,y;
	Vertex *source;
	Vertex *dest;
	while(true) {
	// najdeme minimalny vrchol
		pt=minimumVertex();
		x = pt.first;
		y = pt.second;
		if (x == -1 || y == -1) 
			break;
		source = getVertex(x,y);
		source->final = true;
		progress.removeOne(source);

		if (source->dist == INT_MAX) 
			break;

	// prezrieme susedov vrcholu
		// vlavo	
		dest = getVertex(x-1,y);
		if ( dest != NULL && dest->walkable) {
			if (!dest->final && dest->cost != INT_MAX) {
				if (dest->dist > source->dist + dest->cost) {
					dest->dist = source->dist + dest->cost;
					dest->prev.first = x;
					dest->prev.second = y;
					if (dest->pos.first == tox && dest->pos.second == toy)
						break;
				}
				if (!progress.contains(dest))
					progress.append(dest);
			}
		}

		// vpravo 
		dest = getVertex(x+1,y);
		if ( dest != NULL && dest->walkable) {
			if (!dest->final && dest->cost != INT_MAX) {
				if (dest->dist > source->dist + dest->cost) {
					dest->dist = source->dist + dest->cost;
					dest->prev.first = x;
					dest->prev.second = y;
					if (dest->pos.first == tox && dest->pos.second == toy)
						break;
				}
				if (!progress.contains(dest))
					progress.append(dest);
			}
		}
		// hore
		dest = getVertex(x,y-1);
		if ( dest != NULL && dest->walkable) {
			if (!dest->final && dest->cost != INT_MAX) {
				if (dest->dist > source->dist + dest->cost) {
					dest->dist = source->dist + dest->cost;
					dest->prev.first = x;
					dest->prev.second = y;
					if (dest->pos.first == tox && dest->pos.second == toy)
						break;
				}
				if (!progress.contains(dest))
					progress.append(dest);
			}
		}
		// dole
		dest = getVertex(x,y+1);
		if ( dest != NULL && dest->walkable) {
			if (!dest->final && dest->cost != INT_MAX) {
				if (dest->dist > source->dist + dest->cost) {
					dest->dist = source->dist + dest->cost;
					dest->prev.first = x;
					dest->prev.second = y;
					if (dest->pos.first == tox && dest->pos.second == toy)
						break;
				}
				if (!progress.contains(dest))
					progress.append(dest);
			}
		}
	}
}

// najde minimalny vrchol z prave spracovavanych vrcholov
MapFindPath::VertexPos MapFindPath::minimumVertex()
{
	int min=INT_MAX;

	VertexPos minPt(-1,-1);
	foreach(Vertex* vert, progress){
		if (vert != NULL && !vert->final) 
			if (min >= vert->dist) {
				min = vert->dist;
				minPt = vert->pos;
			}
	}
	return minPt;
}

// najde cestu
void MapFindPath::findPath(Vertex *vert)
{
	if (vert == NULL) return;

	if (vert == source) {
		path.prepend(vert->pos);
	}
	else if(vert->prev.first == -1 && vert->prev.second == -1)
		return;
	else {
		path.prepend(vert->pos);
		findPath(getVertex(vert->prev.first,vert->prev.second));
	}
}

/* class MapWidget */
MapWidget::MapWidget(QWidget *parent) : QGraphicsView(parent)
{
	player = NULL;
	map = NULL;
	pathFinder = NULL;
	object = NULL;
	speed = 100;
	processing = false;
	timer = new QTimer(this);
	connect(timer,SIGNAL(timeout()),this,SLOT(movePlayerPathStep()));

	setOptimizationFlag(QGraphicsView::DontClipPainter);
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	centerOn(0,0);
	setInteractive(false);
	setFocusPolicy(Qt::StrongFocus);
	setBackgroundBrush(QBrush(Qt::black));
	viewport()->setBackgroundRole(QPalette::Window);
}

void MapWidget::setMap(Map *m) 
{
	clear();
	if (m == NULL) {
		map = NULL;
		player = NULL;
		object = NULL;
		return;
	}
	this->map = m;
	this->map->setMapWidget(this);
	this->setScene(map->scene);
	pathFinder = new MapFindPath(map);
	anim = new PathAnimation(map);
	connect(this,SIGNAL(animTimerStart()),anim,SLOT(startTimer()),Qt::DirectConnection);
	player = map->getPlayer();
	if (player == NULL) return;
	centerOn(player);
}

void MapWidget::clear()
{
	processing = false;
	timer->stop();
	if (map != NULL) {
		map->removePlayer();
		map->setMapWidget(NULL);
	}
	if (pathFinder != NULL) {
		delete pathFinder;
		delete anim;
		pathFinder = NULL;
	}
	
	map = NULL;
	path.clear();
	player = NULL;
	object = NULL;
}

QSize MapWidget::sizeHint() const
{
	return QSize(mapWidgetSize,mapWidgetSize);
}

void MapWidget::removePlayer()
{
	timer->stop();
	if (map == NULL) return;
	map->removePlayer();
	player = NULL;
}

void MapWidget::keyPressEvent(QKeyEvent *event)
{	
	event->accept();
	if (processing )
		return;

	timer->stop();
	if (player==NULL) return;

	switch (event->key()) {
		case Qt::Key_Left:
				player->move(-1,0,true);
		break;
		case Qt::Key_Right:
				player->move(1,0,true);
			break;
		case Qt::Key_Up:
				player->move(0,-1,true);
			break;
		case Qt::Key_Down:
				player->move(0,1,true);	
			break;
		case Qt::Key_W:
				player->spendMovePt(INT_MAX);
			break;
		case Qt::Key_Space:
				campaign->showSelfMenu();
			break;
		default: {}
			QWidget::keyPressEvent(event);
	}
}

void MapWidget::wheelEvent(QWheelEvent *event)
{
	event->accept();
	if (processing)
		return;
}

void MapWidget::mousePressEvent(QMouseEvent *event)
{
	event->accept();
	if (processing )
		return;

	if (pathFinder == NULL || player == NULL || map == NULL) return;
	QPoint pt = sceneToGame(mapToScene(event->pos()));

	emit mapWidgetClicked(pt.x(),pt.y());
}


void MapWidget::setPlayerPath(const MapFindPath::PathList &p)
{
	path.clear();
	timer->stop();
	this->path = p;
	if (path.isEmpty()) return;
	MapFindPath::VertexPos tmp = path.takeFirst();
	if (tmp.first != player->getX() || tmp.second != player->getY()) return;
	timer->start(speed);
}

void MapWidget::setObjectPath(const MapFindPath::PathList &p, RPGObject *obj)
{
	if (obj == NULL)
		return;
	processing = true;

	BusyAppFilter filter;
	installEventFilter(&filter);
	map->installEventFilter(&filter);

	anim->setObjectPath(p,obj);
	anim->start();
	emit animTimerStart();
	while(!anim->wait(100)) {
		QCoreApplication::processEvents();
	}

	removeEventFilter(&filter);

	processing = false;
}

// tento slot sa vykona vzdy ked uplynie timer
void MapWidget::movePlayerPathStep()
{
	if (path.isEmpty()) {
		timer->stop();
		return;
	}
	MapFindPath::VertexPos tmp = path.takeFirst();
	if (!player->moveTo(tmp.first, tmp.second,true))
		timer->stop();
}

/* class PathAnimation */
PathAnimation::PathAnimation(Map *map) 
{
	pathFinder = new MapFindPath(map);
	timer = new QTimer(this);
	connect(timer,SIGNAL(timeout()),this,SLOT(movePathStep()));
}

void PathAnimation::setObjectPath(const MapFindPath::PathList &p, RPGObject *obj)
{
	path.clear();
	timer->stop();
	this->path = p;
	if (path.isEmpty()) return;
	MapFindPath::VertexPos tmp = path.takeFirst();
	if (tmp.first != obj->location()->getX() || tmp.second != obj->location()->getY()) return;
	object = obj;
}

void PathAnimation::run() 
{
	QThread::exec();
}

void PathAnimation::startTimer() 
{
	timer->start(100);
}

void PathAnimation::movePathStep()
{
	if (path.isEmpty()) {
		timer->stop();
		exit();
		return;
	}
	MapFindPath::VertexPos tmp = path.takeFirst();
	if (!object->moveOnMap(tmp.first,tmp.second,true))  {
		timer->stop();
		exit();
	}
}

/* class StatusWidget */
StatusWidget::StatusWidget(QWidget *parent) : QTextEdit(parent)
{
	setReadOnly(true);
	setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
	setMinimumSize(500,100);
	setMaximumHeight(300);
	viewport()->setBackgroundRole(QPalette::Window);
	viewport()->setFocusPolicy(Qt::NoFocus);
}

QSize StatusWidget::sizeHint() const
{
	return QSize(500,200);
}

/* class Campaign */
Campaign::Campaign(QWidget *parent) : QWidget(parent)
{
	this->name="Unnamed";
	this->player=NULL;
	this->turn = 0;
	this->combat = new RPGCombat();
	rules.clear();
	mapWidget=new MapWidget(this);
	map = mapWidget->getMap();
	connect(mapWidget,SIGNAL(mapWidgetClicked(int,int)),this,SLOT(processMapWidgetMouse(int,int)));
	character=new CharacterWidget(this);
	status=new StatusWidget(this);

	triggercache.clear();

	QGridLayout *layout=new QGridLayout(this);
	layout->addWidget(mapWidget,0,0);
	layout->addWidget(character,0,1,2,1);
	layout->addWidget(status,1,0);

	processing = false;

	this->setLayout(layout);
		
	attackMsgBox = new RPGDialog(this);
	attackMsgBox->setText("Choose your action");
	waitButton = attackMsgBox->addAnswer(tr("Wait"));
	ordinaryAttackButton = attackMsgBox->addAnswer(tr("Attack"));
	determinedAttackButton = attackMsgBox->addAnswer(tr("All-Out Attack: Determined"));
	determinedAttackButton->setToolTip(tr("Makes a single attack with +%1 to hit").arg(rules.determinedAttackMod));
	doubleAttackButton = attackMsgBox->addAnswer(tr("All-Out Attack: Double"));
	doubleAttackButton->setToolTip(tr("Makes 2 attacks against the same foe"));
	strongAttackButton = attackMsgBox->addAnswer(tr("All-Out Attack: Strong"));
	strongAttackButton->setToolTip(tr("Makes a single attack with bonus +%1 to damage").arg(rules.strongAttackMod));
	
	monsterMsgBox = new RPGDialog(this);
	monsterMsgBox->setText("Choose your action");
	// zacneme dialogh s npc postavou
	talkButton = monsterMsgBox->addAnswer(tr("Talk"));
	// zacneme suboj s postavou
	attackButton = monsterMsgBox->addAnswer(tr("Attack"));
	// navrat ziadna akcia
	abortButton = monsterMsgBox->addAnswer(tr("Abort"));

	selfMsgBox = new RPGDialog(this);
	selfMsgBox->setText("Choose your action");
	selfWaitButton = selfMsgBox->addAnswer(tr("Wait"));
	selfAbortButton = selfMsgBox->addAnswer(tr("Abort"));
	strongDefenseButton = selfMsgBox->addAnswer(tr("All-Out Defense: Strong"));
	strongDefenseButton->setToolTip(tr("Defend with +%1 bonus").arg(rules.strongDefenseMod));
	doubleDefenseButton = selfMsgBox->addAnswer(tr("All-Out Defense: Double"));
	doubleDefenseButton->setToolTip(tr("Defend 2 times against same attack"));
	skillLevelButton = selfMsgBox->addAnswer(tr("Improve skill"));

	skillLevDial = new SkillLevelDialog(this);
}

Campaign::~Campaign()
{
	clear();
}

void Campaign::setName(const QString &name) 
{
	this->name = name;
	emit campaignNameChanged();
}

void Campaign::setTurn(uint t)
{
	this->turn = t;
	emit turnChanged();
}

void Campaign::writeStatus(const QString &msg)
{
	status->append(msg);
}

void Campaign::addPlayer(PlayerNode *player)
{
	this->player=player;
	if (map != NULL) 
		map->addPlayer(player);
	character->setPlayer(player);
	player->replenishMovePt();
	character->inventory->updateRowHeight(QModelIndex(), 0, character->inventory->height());
	mapWidget->setFocus();
	turn++;
	emit turnChanged();
	writeStatus(tr("> %1 Joined the game").arg(player->getName()));
	connect(player,SIGNAL(movePointsReset()),this,SLOT(processTurnEvents()));
}

void Campaign::clearPlayer()
{
	PlayerNode *pl = player;
	player = NULL;
	character->clear();
	disconnect(pl,SIGNAL(movePointsReset()),this,SLOT(processTurnEvents()));
	combat->clear();
	mapWidget->removePlayer();
	pl->release();
	pl=NULL;
	emit playerEnded();
}

Map *Campaign::getCurrentMap()
{
	return this->map;
}

bool Campaign::setCurrentMap(const QString &filename, Trigger *trig)
{
	//QFile file(map->getCwd()+'/'+filename);
	QFile file(filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) 
		return false;

	/* ulozenie triggeru do docasnej pamati kampane (pretoze vymazavame mapu */
	if (trig != NULL && trig->getType() == MAPTRIGGER) {
		MapNode *node = trig->getNode();
		if (node != NULL) {
			RPGObject *obj = node->getObject(trig->getID());
			Trigger *t = dynamic_cast<Trigger*>(obj);
			if (t != NULL) {
				node->removeObject(t);
				addTriggerCache(t);
			}
		}
	}

	combat->clear();

	if (map != NULL) {
		mapWidget->setMap(NULL);
		map->release();
		map = NULL;
	}
	QFileInfo fileInfo(file);
	map = new Map();
	XMLReader::MapXMLReader reader(&file, fileInfo.absolutePath(), map);
	if (reader.read()) {
		map->release();
		map = NULL;
		emit currentMapChanged();
		return false;
	}

	QPoint pt = map->getStartPos();
	if (player != NULL) {
		player->setCoord(pt.x(), pt.y());
		map->addPlayer(player);
	}
	mapWidget->setMap(map);
	emit currentMapChanged();
	return true;
}

bool Campaign::setCurrentMap(Map *map)
{
	if (map == NULL) return false;

	combat->clear();

	if (this->map != NULL) {
		mapWidget->setMap(NULL);
		this->map->release();
		this->map = NULL;
	}

	this->map = map;
	QPoint pt = map->getStartPos();

	if (player != NULL) {
		player->setCoord(pt.x(), pt.y());
		map->addPlayer(player);
	}
	mapWidget->setMap(map);
	emit currentMapChanged();
	return true;
}

void Campaign::setTriggerVar(const QString &name, Value val)
{
	TriggerVars[name] = val;
}

Value Campaign::getTriggerVar(const QString &name) const
{
	if (TriggerVars.contains(name))
		return TriggerVars[name];
	return Value();
}

void Campaign::removeTriggerVar(const QString &name)
{
	TriggerVars.remove(name);
}

bool monstersInCombat(QList<Monster*> &mon) 
{
	if (mon.isEmpty()) return true;

	foreach(Monster* m, mon) {
		if (m == NULL)
			continue;
		if (!m->isInCombat())
			return false;
	}
	return true;
}

Campaign::EventType Campaign::processMapEvents(int x, int y, Character *ch)
{
	if (!triggercache.isEmpty()) {
		foreach(Trigger *t, triggercache) {
			if (t != NULL && !t->isRunning()) {
				delete t;
				triggercache.removeAll(t);
			}
		}
	}
	
	if (map == NULL) return STOP;
	MapNode *node = map->getNode(x,y);
	if (node == NULL) return STOP;
	
	/* spracovanie nepriatelov */
	// ziskame zoznam vsetkych nepriatelov na bunke
	QList<Monster*> monsters=node->getObjects<Monster>();
	if (monsters.empty()) {
		/* spracovanie inventarnych objektov na mape */
		QList<InventoryObject*> objects = node->getObjects<InventoryObject>();
		if (objects.empty()) return CONTINUE;
		RPGDialog dial;
		dial.setText("Choose items to take");
		QMap<QPushButton *,InventoryObject*> objs;
		foreach(InventoryObject* obj, objects) {
			objs.insert(dial.addAnswer(obj->getName()),obj);
		}
		QPushButton *reject = dial.addAnswer("Abort");

		int movept = player->getMovePt();
		if ( movept < rules.actionCosts.pickItem) {
			campaign->writeStatus("Not enought move pt to pick up item");
			return CONTINUE;
		}

		dial.display();
		QPushButton* but = dial.clickedButton();
		if (but != reject && but != NULL) {
			InventoryObject *t = objs[but];
			if (!player->addInventoryObject(t)) {
				campaign->writeStatus("Can't pick up item - encumbrance limit");
				return CONTINUE;
			}
			node->removeObject(t);
			player->spendMovePt(rules.actionCosts.pickItem);
		}
		return CONTINUE;
	}

	if (!monstersInCombat(monsters)) {
		if (combat->isRunning() || !monsters.first()->isTalkable()) 
			talkButton->setEnabled(false);
		else 
			talkButton->setEnabled(true);

		// hracovi ponukneme msgbox s vyberom akcie s nepriatelom
		monsterMsgBox->display();
		if (monsterMsgBox->clickedButton() == abortButton) {
			return STOP;
		}
		if (monsterMsgBox->clickedButton() == talkButton) {
			monsters.first()->talkTo(player);
			return STOP;
		}
		// tu klikol na attack button
		// ak hrac nie je v boji tak ho tam pridame
		if (!player->isInCombat()) {
			addPlayerToCombat();
		}		
		// pridame priserky ktore este nie su v boji
		addMonstersToCombat(monsters);
		return STOP;
	}
	
	if (!combat->isRunning())
		return STOP;
	// hracovi ponukneme messagebox s vyberom utocneho manevru (popripade ostatnych funkcii suvisiacich s bunkou)
	ordinaryAttackButton->setEnabled(false);
	determinedAttackButton->setEnabled(false);
	doubleAttackButton->setEnabled(false);
	strongAttackButton->setEnabled(false);

	// zistime na ktore manevre ma hrac dostatok bodov pohybu
	int movept = player->getMovePt();
	bool cont = false;
	if (movept >= rules.actionCosts.attack) {
		ordinaryAttackButton->setEnabled(true);
		cont = true;
	}
	if (movept >= rules.actionCosts.determinedAttack) {
		determinedAttackButton->setEnabled(true);
		cont = true;
	}
	if (movept >= rules.actionCosts.doubleAttack) {
		doubleAttackButton->setEnabled(true);
		cont = true;
	}
	if (movept >= rules.actionCosts.strongAttack) {
		strongAttackButton->setEnabled(true);
		cont = true;
	}

	// zobrazime msgbox s vyberom utocneho manevru
	attackMsgBox->display();

	// zoberieme prveho nepriatela na bunke - na toho bude hrac utocit
	CombatCharacter *enemy = combat->getCharacter(monsters.first());
	if (enemy == NULL) return STOP;

	CombatCharacter *pl = combat->getPlayer();
	if (pl == NULL || pl->getCharacter() != player) {
		addPlayerToCombat();
		pl = combat->getPlayer();
		if (pl == NULL) return STOP;
	}

	// nastavime hracov manever podla attackMsgBox
	if (attackMsgBox->clickedButton() == ordinaryAttackButton) {
		pl->setManeuver(ATTACK,enemy);
		combat->executeManeuver(pl);
		player->spendMovePt(rules.actionCosts.attack);
	}
	else if (attackMsgBox->clickedButton() == determinedAttackButton) {
		pl->setManeuver(ATTACK_DETERMINED, enemy);
		combat->executeManeuver(pl);
		player->spendMovePt(rules.actionCosts.determinedAttack);
	}
	else if (attackMsgBox->clickedButton() == doubleAttackButton) {
		pl->setManeuver(ATTACK_DOUBLE, enemy);
		combat->executeManeuver(pl);
		player->spendMovePt(rules.actionCosts.doubleAttack);
	}
	else if (attackMsgBox->clickedButton() == strongAttackButton) {
		pl->setManeuver(ATTACK_STRONG, enemy);
		combat->executeManeuver(pl);
		player->spendMovePt(rules.actionCosts.strongAttack);
	}
	else if (attackMsgBox->clickedButton() == waitButton) {
		player->spendMovePt(INT_MAX);
	}
	combat->tryEndCombat();
	return STOP;
}

void Campaign::addPlayerToCombat()
{
	if (!player->isInCombat()) {
		CombatCharacter *ch = new CombatCharacter();
		ch->setCharacter(player);
		combat->addCharacter(ch);	
	}
}

void Campaign::addMonstersToCombat(QList<Monster*> &monsters)
{
	if (monsters.empty()) return;

	foreach(Monster *monster, monsters) {
		if (monster == NULL) return;
		if (!monster->isInCombat()) {
			CombatCharacter *ch = new CombatCharacter();
			ch->setCharacter(monster);
			combat->addCharacter(ch);
		}
	}
}

void Campaign::processTurnEvents()
{
	if (processing)
		return;
	processing = true;
	if (player == NULL) {
		processing = false;
		return;
	}
	// spracovanie udalosti suboja po tahu hraca
	if (combat->isRunning()) {
		combat->processTurnAfterPlayer();
	}
	if (player == NULL) {
		processing = false;
		return;	
	}
	else 
		player->replenishMovePt();

	this->turn++;
	emit turnChanged();

	// spracovanie naplanovanych triggerov
	if (schedtriggers.contains(turn)) {
		QList<Trigger *> values = schedtriggers.values(turn);
		for (int i = 0; i < values.size(); ++i) {
			values[i]->execute();
			values[i]->release();
		}
		schedtriggers.remove(turn);
	}

	// spracovanie tahu hraca
	if (player == NULL) {
		processing = false;
		return;
	}
	if (!player->isAlive()) {
		clearPlayer();
		return;
	}
	else {
		player->processTurnEvents();
		if (!player->isAlive()) {
			clearPlayer();
			processing = false;
			return;
		}
	}

	// spracovanie udalosti suboja pred tahom hraca
	if (combat->isRunning()) {
		combat->processTurnBeforePlayer();
	}
	processing = false;
	return;
}

void Campaign::showSelfMenu() 
{
	doubleDefenseButton->setEnabled(false);
	strongDefenseButton->setEnabled(false);

	if (player->getUnusedXP() > 0) {
		skillLevelButton->setEnabled(true);
	}
	else 
		skillLevelButton->setEnabled(false);

	if (player->isInCombat()) {
		int movept = player->getMovePt();
		if (movept >= rules.actionCosts.strongDefense) 
			strongDefenseButton->setEnabled(true);
		if (movept >= rules.actionCosts.doubleDefense) 
			doubleDefenseButton->setEnabled(true);
		skillLevelButton->setEnabled(false);
	}

	selfMsgBox->display();
	if (selfMsgBox->clickedButton() == selfWaitButton) {
		player->spendMovePt(INT_MAX);
	}
	else if (selfMsgBox->clickedButton() == strongDefenseButton) {
		CombatCharacter *pl = combat->getPlayer();
		if (pl == NULL || pl->getCharacter() != player) {
			addPlayerToCombat();
			pl = combat->getPlayer();
			if (pl == NULL) return;
		}
		pl->setManeuver(DEFENSE_STRONG);
		player->spendMovePt(INT_MAX);
	}
	else if (selfMsgBox->clickedButton() == doubleDefenseButton) {
		CombatCharacter *pl = combat->getPlayer();
		if (pl == NULL || pl->getCharacter() != player) {
			addPlayerToCombat();
			pl = combat->getPlayer();
			if (pl == NULL) return;
		}
		pl->setManeuver(DEFENSE_DOUBLE);
		player->spendMovePt(INT_MAX);
	}
	else if (selfMsgBox->clickedButton() == skillLevelButton) {
		skillLevDial->display(player);
	}
}

void Campaign::processMapWidgetMouse(int x, int y)
{
	if (player->getX() == x && player->getY() == y) {
		showSelfMenu();
		return;
	}

	if (map == NULL) return;
	MapNode *node = map->getNode(x,y);
	if (node == NULL) return;
	
	movePlayerPath(x, y);
	return;
}

void Campaign::movePlayerPath(int tox, int toy)
{
	// najdeme cestu medzi sucasnou polohou a klikom mysou
	MapFindPath pathFinder(map);
	MapFindPath::PathList path = pathFinder.find(player->getX(),player->getY(), tox, toy);

	mapWidget->setPlayerPath(path);
}

void Campaign::schedule(uint t, Trigger *trig) {
	schedtriggers.insert(t,new Trigger(*trig));
}

void Campaign::clear()
{
	if (map != NULL) {
		mapWidget->setMap(NULL);
		map->release();
		map = NULL;
	}
	status->clear();
	rules.clear();
	character->clear();
	if (player != NULL)
		player->release();
	player=NULL;
	this->name.clear();
	turn = 0;
	foreach(Trigger *tr, schedtriggers) {
		tr->release();
	}
	foreach(Trigger *tr, triggercache) {
		tr->release();
	}
	triggercache.clear();
	schedtriggers.clear();
	TriggerVars.clear();
}

void Campaign::addTriggerCache(Trigger *trig) 
{
	trig->location()->clear();
	triggercache.append(trig);
}