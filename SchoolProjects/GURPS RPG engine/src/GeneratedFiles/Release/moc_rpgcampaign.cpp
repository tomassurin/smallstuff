/****************************************************************************
** Meta object code from reading C++ file 'rpgcampaign.h'
**
** Created: Sat 24. Apr 20:15:53 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../rpgcampaign.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'rpgcampaign.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RPG__MapNode[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__MapNode[] = {
    "RPG::MapNode\0\0updateTooltip()\0"
};

const QMetaObject RPG::MapNode::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_RPG__MapNode,
      qt_meta_data_RPG__MapNode, 0 }
};

const QMetaObject *RPG::MapNode::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::MapNode::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__MapNode))
        return static_cast<void*>(const_cast< MapNode*>(this));
    if (!strcmp(_clname, "BasicMapNode"))
        return static_cast< BasicMapNode*>(const_cast< MapNode*>(this));
    return QObject::qt_metacast(_clname);
}

int RPG::MapNode::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: updateTooltip(); break;
        }
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_RPG__PathAnimation[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      20,   19,   19,   19, 0x0a,
      33,   19,   19,   19, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_RPG__PathAnimation[] = {
    "RPG::PathAnimation\0\0startTimer()\0"
    "movePathStep()\0"
};

const QMetaObject RPG::PathAnimation::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_RPG__PathAnimation,
      qt_meta_data_RPG__PathAnimation, 0 }
};

const QMetaObject *RPG::PathAnimation::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::PathAnimation::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__PathAnimation))
        return static_cast<void*>(const_cast< PathAnimation*>(this));
    return QThread::qt_metacast(_clname);
}

int RPG::PathAnimation::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: startTimer(); break;
        case 1: movePathStep(); break;
        }
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_RPG__MapWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      20,   16,   15,   15, 0x05,
      46,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      63,   15,   15,   15, 0x0a,
      83,   78,   15,   15, 0x0a,
     129,  120,   15,   15, 0x0a,
     177,   15,   15,   15, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_RPG__MapWidget[] = {
    "RPG::MapWidget\0\0x,y\0mapWidgetClicked(int,int)\0"
    "animTimerStart()\0removePlayer()\0path\0"
    "setPlayerPath(MapFindPath::PathList)\0"
    "path,mon\0setObjectPath(MapFindPath::PathList,RPGObject*)\0"
    "movePlayerPathStep()\0"
};

const QMetaObject RPG::MapWidget::staticMetaObject = {
    { &QGraphicsView::staticMetaObject, qt_meta_stringdata_RPG__MapWidget,
      qt_meta_data_RPG__MapWidget, 0 }
};

const QMetaObject *RPG::MapWidget::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::MapWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__MapWidget))
        return static_cast<void*>(const_cast< MapWidget*>(this));
    return QGraphicsView::qt_metacast(_clname);
}

int RPG::MapWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGraphicsView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: mapWidgetClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: animTimerStart(); break;
        case 2: removePlayer(); break;
        case 3: setPlayerPath((*reinterpret_cast< const MapFindPath::PathList(*)>(_a[1]))); break;
        case 4: setObjectPath((*reinterpret_cast< const MapFindPath::PathList(*)>(_a[1])),(*reinterpret_cast< RPGObject*(*)>(_a[2]))); break;
        case 5: movePlayerPathStep(); break;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void RPG::MapWidget::mapWidgetClicked(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void RPG::MapWidget::animTimerStart()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
static const uint qt_meta_data_RPG__StatusWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets

       0        // eod
};

static const char qt_meta_stringdata_RPG__StatusWidget[] = {
    "RPG::StatusWidget\0"
};

const QMetaObject RPG::StatusWidget::staticMetaObject = {
    { &QTextEdit::staticMetaObject, qt_meta_stringdata_RPG__StatusWidget,
      qt_meta_data_RPG__StatusWidget, 0 }
};

const QMetaObject *RPG::StatusWidget::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::StatusWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__StatusWidget))
        return static_cast<void*>(const_cast< StatusWidget*>(this));
    return QTextEdit::qt_metacast(_clname);
}

int RPG::StatusWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTextEdit::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_RPG__Campaign[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      29,   14,   14,   14, 0x05,
      51,   14,   14,   14, 0x05,
      71,   14,   14,   14, 0x05,
      89,   85,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     132,  125,  115,   14, 0x0a,
     169,   14,   14,   14, 0x0a,
     189,   85,   14,   14, 0x0a,
     220,   14,   14,   14, 0x0a,
     240,   14,   14,   14, 0x0a,
     263,  255,   14,   14, 0x08,
     296,  287,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_RPG__Campaign[] = {
    "RPG::Campaign\0\0playerEnded()\0"
    "campaignNameChanged()\0currentMapChanged()\0"
    "turnChanged()\0x,y\0mapWidgetClicked(int,int)\0"
    "EventType\0x,y,ch\0processMapEvents(int,int,Character*)\0"
    "processTurnEvents()\0processMapWidgetMouse(int,int)\0"
    "addPlayerToCombat()\0showSelfMenu()\0"
    "tox,toy\0movePlayerPath(int,int)\0"
    "monsters\0addMonstersToCombat(QList<Monster*>&)\0"
};

const QMetaObject RPG::Campaign::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RPG__Campaign,
      qt_meta_data_RPG__Campaign, 0 }
};

const QMetaObject *RPG::Campaign::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::Campaign::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__Campaign))
        return static_cast<void*>(const_cast< Campaign*>(this));
    return QWidget::qt_metacast(_clname);
}

int RPG::Campaign::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: playerEnded(); break;
        case 1: campaignNameChanged(); break;
        case 2: currentMapChanged(); break;
        case 3: turnChanged(); break;
        case 4: mapWidgetClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: { EventType _r = processMapEvents((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< Character*(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< EventType*>(_a[0]) = _r; }  break;
        case 6: processTurnEvents(); break;
        case 7: processMapWidgetMouse((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 8: addPlayerToCombat(); break;
        case 9: showSelfMenu(); break;
        case 10: movePlayerPath((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 11: addMonstersToCombat((*reinterpret_cast< QList<Monster*>(*)>(_a[1]))); break;
        }
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void RPG::Campaign::playerEnded()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void RPG::Campaign::campaignNameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void RPG::Campaign::currentMapChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void RPG::Campaign::turnChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void RPG::Campaign::mapWidgetClicked(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_END_MOC_NAMESPACE
