/********************************************************************************
** Form generated from reading ui file 'NewCharacterDialog.ui'
**
** Created: Sat 24. Apr 20:15:53 2010
**      by: Qt User Interface Compiler version 4.4.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_NEWCHARACTERDIALOG_H
#define UI_NEWCHARACTERDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewCharacterDialog
{
public:
    QWidget *layoutWidget;
    QHBoxLayout *hboxLayout;
    QSpacerItem *spacerItem;
    QPushButton *okButton;
    QPushButton *cancelButton;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *nameLabel;
    QLineEdit *nameBox;
    QGroupBox *groupBox;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *nameLabel_3;
    QSpinBox *stBox;
    QWidget *horizontalLayoutWidget_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *nameLabel_4;
    QSpinBox *dxBox;
    QWidget *horizontalLayoutWidget_5;
    QHBoxLayout *horizontalLayout_5;
    QLabel *nameLabel_5;
    QSpinBox *iqBox;
    QWidget *horizontalLayoutWidget_6;
    QHBoxLayout *horizontalLayout_7;
    QLabel *nameLabel_7;
    QSpinBox *htBox;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *nameLabel_2;
    QDoubleSpinBox *sizeBox;
    QGroupBox *groupBox_2;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QComboBox *skill1Box;
    QComboBox *skill2Box;
    QComboBox *skill3Box;
    QComboBox *skill4Box;
    QComboBox *skill5Box;

    void setupUi(QDialog *NewCharacterDialog)
    {
    if (NewCharacterDialog->objectName().isEmpty())
        NewCharacterDialog->setObjectName(QString::fromUtf8("NewCharacterDialog"));
    NewCharacterDialog->resize(352, 266);
    layoutWidget = new QWidget(NewCharacterDialog);
    layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
    layoutWidget->setGeometry(QRect(140, 210, 191, 33));
    hboxLayout = new QHBoxLayout(layoutWidget);
#ifndef Q_OS_MAC
    hboxLayout->setSpacing(6);
#endif
    hboxLayout->setMargin(0);
    hboxLayout->setObjectName(QString::fromUtf8("hboxLayout"));
    hboxLayout->setContentsMargins(0, 0, 0, 0);
    spacerItem = new QSpacerItem(131, 31, QSizePolicy::Expanding, QSizePolicy::Minimum);

    hboxLayout->addItem(spacerItem);

    okButton = new QPushButton(layoutWidget);
    okButton->setObjectName(QString::fromUtf8("okButton"));

    hboxLayout->addWidget(okButton);

    cancelButton = new QPushButton(layoutWidget);
    cancelButton->setObjectName(QString::fromUtf8("cancelButton"));

    hboxLayout->addWidget(cancelButton);

    horizontalLayoutWidget = new QWidget(NewCharacterDialog);
    horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
    horizontalLayoutWidget->setGeometry(QRect(10, 10, 321, 31));
    horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
    horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
    horizontalLayout->setContentsMargins(0, 0, 0, 0);
    nameLabel = new QLabel(horizontalLayoutWidget);
    nameLabel->setObjectName(QString::fromUtf8("nameLabel"));

    horizontalLayout->addWidget(nameLabel);

    nameBox = new QLineEdit(horizontalLayoutWidget);
    nameBox->setObjectName(QString::fromUtf8("nameBox"));

    horizontalLayout->addWidget(nameBox);

    groupBox = new QGroupBox(NewCharacterDialog);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    groupBox->setGeometry(QRect(10, 50, 131, 191));
    horizontalLayoutWidget_3 = new QWidget(groupBox);
    horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
    horizontalLayoutWidget_3->setGeometry(QRect(10, 20, 71, 31));
    horizontalLayout_3 = new QHBoxLayout(horizontalLayoutWidget_3);
    horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
    horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
    nameLabel_3 = new QLabel(horizontalLayoutWidget_3);
    nameLabel_3->setObjectName(QString::fromUtf8("nameLabel_3"));
    nameLabel_3->setMaximumSize(QSize(41, 16777215));

    horizontalLayout_3->addWidget(nameLabel_3);

    stBox = new QSpinBox(horizontalLayoutWidget_3);
    stBox->setObjectName(QString::fromUtf8("stBox"));
    stBox->setValue(10);

    horizontalLayout_3->addWidget(stBox);

    horizontalLayoutWidget_4 = new QWidget(groupBox);
    horizontalLayoutWidget_4->setObjectName(QString::fromUtf8("horizontalLayoutWidget_4"));
    horizontalLayoutWidget_4->setGeometry(QRect(10, 50, 71, 31));
    horizontalLayout_4 = new QHBoxLayout(horizontalLayoutWidget_4);
    horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
    horizontalLayout_4->setContentsMargins(0, 0, 0, 0);
    nameLabel_4 = new QLabel(horizontalLayoutWidget_4);
    nameLabel_4->setObjectName(QString::fromUtf8("nameLabel_4"));
    nameLabel_4->setMaximumSize(QSize(41, 16777215));

    horizontalLayout_4->addWidget(nameLabel_4);

    dxBox = new QSpinBox(horizontalLayoutWidget_4);
    dxBox->setObjectName(QString::fromUtf8("dxBox"));
    dxBox->setValue(10);

    horizontalLayout_4->addWidget(dxBox);

    horizontalLayoutWidget_5 = new QWidget(groupBox);
    horizontalLayoutWidget_5->setObjectName(QString::fromUtf8("horizontalLayoutWidget_5"));
    horizontalLayoutWidget_5->setGeometry(QRect(10, 80, 71, 31));
    horizontalLayout_5 = new QHBoxLayout(horizontalLayoutWidget_5);
    horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
    horizontalLayout_5->setContentsMargins(0, 0, 0, 0);
    nameLabel_5 = new QLabel(horizontalLayoutWidget_5);
    nameLabel_5->setObjectName(QString::fromUtf8("nameLabel_5"));
    nameLabel_5->setMaximumSize(QSize(41, 16777215));

    horizontalLayout_5->addWidget(nameLabel_5);

    iqBox = new QSpinBox(horizontalLayoutWidget_5);
    iqBox->setObjectName(QString::fromUtf8("iqBox"));
    iqBox->setValue(10);

    horizontalLayout_5->addWidget(iqBox);

    horizontalLayoutWidget_6 = new QWidget(groupBox);
    horizontalLayoutWidget_6->setObjectName(QString::fromUtf8("horizontalLayoutWidget_6"));
    horizontalLayoutWidget_6->setGeometry(QRect(10, 110, 71, 31));
    horizontalLayout_7 = new QHBoxLayout(horizontalLayoutWidget_6);
    horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
    horizontalLayout_7->setContentsMargins(0, 0, 0, 0);
    nameLabel_7 = new QLabel(horizontalLayoutWidget_6);
    nameLabel_7->setObjectName(QString::fromUtf8("nameLabel_7"));
    nameLabel_7->setMaximumSize(QSize(41, 16777215));

    horizontalLayout_7->addWidget(nameLabel_7);

    htBox = new QSpinBox(horizontalLayoutWidget_6);
    htBox->setObjectName(QString::fromUtf8("htBox"));
    htBox->setValue(10);

    horizontalLayout_7->addWidget(htBox);

    horizontalLayoutWidget_2 = new QWidget(groupBox);
    horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
    horizontalLayoutWidget_2->setGeometry(QRect(10, 140, 91, 31));
    horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
    horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
    horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
    nameLabel_2 = new QLabel(horizontalLayoutWidget_2);
    nameLabel_2->setObjectName(QString::fromUtf8("nameLabel_2"));
    nameLabel_2->setMaximumSize(QSize(21, 16777215));

    horizontalLayout_2->addWidget(nameLabel_2);

    sizeBox = new QDoubleSpinBox(horizontalLayoutWidget_2);
    sizeBox->setObjectName(QString::fromUtf8("sizeBox"));
    sizeBox->setMaximum(500);

    horizontalLayout_2->addWidget(sizeBox);

    groupBox_2 = new QGroupBox(NewCharacterDialog);
    groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
    groupBox_2->setGeometry(QRect(140, 50, 191, 161));
    verticalLayoutWidget = new QWidget(groupBox_2);
    verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
    verticalLayoutWidget->setGeometry(QRect(20, 20, 160, 126));
    verticalLayout = new QVBoxLayout(verticalLayoutWidget);
    verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    verticalLayout->setContentsMargins(0, 0, 0, 0);
    skill1Box = new QComboBox(verticalLayoutWidget);
    skill1Box->setObjectName(QString::fromUtf8("skill1Box"));

    verticalLayout->addWidget(skill1Box);

    skill2Box = new QComboBox(verticalLayoutWidget);
    skill2Box->setObjectName(QString::fromUtf8("skill2Box"));

    verticalLayout->addWidget(skill2Box);

    skill3Box = new QComboBox(verticalLayoutWidget);
    skill3Box->setObjectName(QString::fromUtf8("skill3Box"));

    verticalLayout->addWidget(skill3Box);

    skill4Box = new QComboBox(verticalLayoutWidget);
    skill4Box->setObjectName(QString::fromUtf8("skill4Box"));

    verticalLayout->addWidget(skill4Box);

    skill5Box = new QComboBox(verticalLayoutWidget);
    skill5Box->setObjectName(QString::fromUtf8("skill5Box"));

    verticalLayout->addWidget(skill5Box);


#ifndef QT_NO_SHORTCUT
    nameLabel->setBuddy(nameBox);
    nameLabel_3->setBuddy(sizeBox);
    nameLabel_4->setBuddy(sizeBox);
    nameLabel_5->setBuddy(sizeBox);
    nameLabel_7->setBuddy(sizeBox);
    nameLabel_2->setBuddy(sizeBox);
#endif // QT_NO_SHORTCUT


    retranslateUi(NewCharacterDialog);
    QObject::connect(okButton, SIGNAL(clicked()), NewCharacterDialog, SLOT(accept()));
    QObject::connect(cancelButton, SIGNAL(clicked()), NewCharacterDialog, SLOT(reject()));

    QMetaObject::connectSlotsByName(NewCharacterDialog);
    } // setupUi

    void retranslateUi(QDialog *NewCharacterDialog)
    {
    NewCharacterDialog->setWindowTitle(QApplication::translate("NewCharacterDialog", "New Character", 0, QApplication::UnicodeUTF8));
    okButton->setText(QApplication::translate("NewCharacterDialog", "OK", 0, QApplication::UnicodeUTF8));
    cancelButton->setText(QApplication::translate("NewCharacterDialog", "Cancel", 0, QApplication::UnicodeUTF8));
    nameLabel->setText(QApplication::translate("NewCharacterDialog", "Name:", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QApplication::translate("NewCharacterDialog", "Stats", 0, QApplication::UnicodeUTF8));
    nameLabel_3->setText(QApplication::translate("NewCharacterDialog", "ST:", 0, QApplication::UnicodeUTF8));
    nameLabel_4->setText(QApplication::translate("NewCharacterDialog", "DX:", 0, QApplication::UnicodeUTF8));
    nameLabel_5->setText(QApplication::translate("NewCharacterDialog", "IQ:", 0, QApplication::UnicodeUTF8));
    nameLabel_7->setText(QApplication::translate("NewCharacterDialog", "HT:", 0, QApplication::UnicodeUTF8));
    nameLabel_2->setText(QApplication::translate("NewCharacterDialog", "Size:", 0, QApplication::UnicodeUTF8));
    groupBox_2->setTitle(QApplication::translate("NewCharacterDialog", "Skills", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(NewCharacterDialog);
    } // retranslateUi

};

namespace Ui {
    class NewCharacterDialog: public Ui_NewCharacterDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWCHARACTERDIALOG_H
