/****************************************************************************
** Meta object code from reading C++ file 'rpgcharacter.h'
**
** Created: Sat 24. Apr 19:29:08 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../rpgcharacter.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'rpgcharacter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RPG__Character[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      16,   15,   15,   15, 0x05,
      31,   15,   15,   15, 0x05,
      50,   15,   15,   15, 0x05,
      70,   15,   15,   15, 0x05,

 // slots: signature, parameters, type, tag, flags
      91,   88,   15,   15, 0x0a,
     105,  102,   15,   15, 0x0a,
     119,  116,   15,   15, 0x0a,
     133,  130,   15,   15, 0x0a,
     147,  144,   15,   15, 0x0a,
     161,  158,   15,   15, 0x0a,
     181,   15,  177,   15, 0x0a,
     198,  194,   15,   15, 0x0a,
     217,  210,   15,   15, 0x0a,
     235,  232,   15,   15, 0x0a,
     251,  232,   15,   15, 0x0a,
     268,  232,   15,   15, 0x0a,
     280,  232,   15,   15, 0x0a,
     310,  299,   15,   15, 0x0a,
     334,  299,   15,   15, 0x0a,
     360,  356,   15,   15, 0x0a,
     382,  376,   15,   15, 0x0a,
     407,   15,   15,   15, 0x0a,
     424,   15,   15,   15, 0x0a,
     442,   15,   15,   15, 0x0a,
     465,   15,   15,   15, 0x0a,
     485,   15,   15,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__Character[] = {
    "RPG::Character\0\0statsChanged()\0"
    "statsInfoChanged()\0movePointsChanged()\0"
    "movePointsReset()\0ST\0setST(int)\0DX\0"
    "setDX(int)\0IQ\0setIQ(int)\0HT\0setHT(int)\0"
    "HP\0setHP(int)\0sz\0setSize(double)\0int\0"
    "getSizeMod()\0cHP\0setcHP(int)\0movept\0"
    "setMovePt(int)\0xp\0setTotalXP(int)\0"
    "setUnusedXP(int)\0earnXP(int)\0"
    "spendUnusedXP(int)\0attr,bonus\0"
    "setBonus(Attribute,int)\0setBonus(QString,int)\0"
    "dmg\0inflictDmg(int)\0state\0"
    "setState(CharacterState)\0updateSecStats()\0"
    "updateStatsInfo()\0emitStatsInfoChanged()\0"
    "processTurnEvents()\0processHealth()\0"
};

const QMetaObject RPG::Character::staticMetaObject = {
    { &RPGObject::staticMetaObject, qt_meta_stringdata_RPG__Character,
      qt_meta_data_RPG__Character, 0 }
};

const QMetaObject *RPG::Character::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::Character::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__Character))
        return static_cast<void*>(const_cast< Character*>(this));
    return RPGObject::qt_metacast(_clname);
}

int RPG::Character::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = RPGObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: statsChanged(); break;
        case 1: statsInfoChanged(); break;
        case 2: movePointsChanged(); break;
        case 3: movePointsReset(); break;
        case 4: setST((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: setDX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: setIQ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: setHT((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: setHP((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: setSize((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 10: { int _r = getSizeMod();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 11: setcHP((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: setMovePt((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: setTotalXP((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: setUnusedXP((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: earnXP((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: spendUnusedXP((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: setBonus((*reinterpret_cast< Attribute(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 18: setBonus((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 19: inflictDmg((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 20: setState((*reinterpret_cast< CharacterState(*)>(_a[1]))); break;
        case 21: updateSecStats(); break;
        case 22: updateStatsInfo(); break;
        case 23: emitStatsInfoChanged(); break;
        case 24: processTurnEvents(); break;
        case 25: processHealth(); break;
        }
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void RPG::Character::statsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void RPG::Character::statsInfoChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void RPG::Character::movePointsChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void RPG::Character::movePointsReset()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}
static const uint qt_meta_data_RPG__Monster[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets

       0        // eod
};

static const char qt_meta_stringdata_RPG__Monster[] = {
    "RPG::Monster\0"
};

const QMetaObject RPG::Monster::staticMetaObject = {
    { &Character::staticMetaObject, qt_meta_stringdata_RPG__Monster,
      qt_meta_data_RPG__Monster, 0 }
};

const QMetaObject *RPG::Monster::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::Monster::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__Monster))
        return static_cast<void*>(const_cast< Monster*>(this));
    return Character::qt_metacast(_clname);
}

int RPG::Monster::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Character::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_RPG__Player[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets

       0        // eod
};

static const char qt_meta_stringdata_RPG__Player[] = {
    "RPG::Player\0"
};

const QMetaObject RPG::Player::staticMetaObject = {
    { &Character::staticMetaObject, qt_meta_stringdata_RPG__Player,
      qt_meta_data_RPG__Player, 0 }
};

const QMetaObject *RPG::Player::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::Player::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__Player))
        return static_cast<void*>(const_cast< Player*>(this));
    return Character::qt_metacast(_clname);
}

int RPG::Player::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = Character::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
