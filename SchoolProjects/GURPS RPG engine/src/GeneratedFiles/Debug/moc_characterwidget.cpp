/****************************************************************************
** Meta object code from reading C++ file 'characterwidget.h'
**
** Created: Sat 24. Apr 19:29:11 2010
**      by: The Qt Meta Object Compiler version 59 (Qt 4.4.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../characterwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'characterwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.4.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RPG__RPGDialog[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      36,   20,   16,   15, 0x0a,
      65,   15,   16,   15, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__RPGDialog[] = {
    "RPG::RPGDialog\0\0int\0message,answers\0"
    "display(QString,QStringList)\0display()\0"
};

const QMetaObject RPG::RPGDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_RPG__RPGDialog,
      qt_meta_data_RPG__RPGDialog, 0 }
};

const QMetaObject *RPG::RPGDialog::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::RPGDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__RPGDialog))
        return static_cast<void*>(const_cast< RPGDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int RPG::RPGDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: { int _r = display((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QStringList(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 1: { int _r = display();
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        }
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_RPG__MyLabel[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      14,   13,   13,   13, 0x05,

       0        // eod
};

static const char qt_meta_stringdata_RPG__MyLabel[] = {
    "RPG::MyLabel\0\0clicked()\0"
};

const QMetaObject RPG::MyLabel::staticMetaObject = {
    { &QLabel::staticMetaObject, qt_meta_stringdata_RPG__MyLabel,
      qt_meta_data_RPG__MyLabel, 0 }
};

const QMetaObject *RPG::MyLabel::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::MyLabel::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__MyLabel))
        return static_cast<void*>(const_cast< MyLabel*>(this));
    return QLabel::qt_metacast(_clname);
}

int RPG::MyLabel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QLabel::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: clicked(); break;
        }
        _id -= 1;
    }
    return _id;
}

// SIGNAL 0
void RPG::MyLabel::clicked()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
static const uint qt_meta_data_RPG__CampaignInfo[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      19,   18,   18,   18, 0x0a,
      40,   18,   18,   18, 0x0a,
      56,   18,   18,   18, 0x0a,
      75,   18,   18,   18, 0x0a,
      94,   18,   18,   18, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__CampaignInfo[] = {
    "RPG::CampaignInfo\0\0updateCampaignName()\0"
    "updateMapInfo()\0updateTurnNumber()\0"
    "updateMovePoints()\0establishConnections()\0"
};

const QMetaObject RPG::CampaignInfo::staticMetaObject = {
    { &QGroupBox::staticMetaObject, qt_meta_stringdata_RPG__CampaignInfo,
      qt_meta_data_RPG__CampaignInfo, 0 }
};

const QMetaObject *RPG::CampaignInfo::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::CampaignInfo::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__CampaignInfo))
        return static_cast<void*>(const_cast< CampaignInfo*>(this));
    return QGroupBox::qt_metacast(_clname);
}

int RPG::CampaignInfo::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGroupBox::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: updateCampaignName(); break;
        case 1: updateMapInfo(); break;
        case 2: updateTurnNumber(); break;
        case 3: updateMovePoints(); break;
        case 4: establishConnections(); break;
        }
        _id -= 5;
    }
    return _id;
}
static const uint qt_meta_data_RPG__RPGCharacterSheet[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets

       0        // eod
};

static const char qt_meta_stringdata_RPG__RPGCharacterSheet[] = {
    "RPG::RPGCharacterSheet\0"
};

const QMetaObject RPG::RPGCharacterSheet::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RPG__RPGCharacterSheet,
      qt_meta_data_RPG__RPGCharacterSheet, 0 }
};

const QMetaObject *RPG::RPGCharacterSheet::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::RPGCharacterSheet::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__RPGCharacterSheet))
        return static_cast<void*>(const_cast< RPGCharacterSheet*>(this));
    return QWidget::qt_metacast(_clname);
}

int RPG::RPGCharacterSheet::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
static const uint qt_meta_data_RPG__SkillLevelDialog[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      26,   23,   22,   22, 0x0a,
      49,   47,   22,   22, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__SkillLevelDialog[] = {
    "RPG::SkillLevelDialog\0\0pl\0"
    "display(PlayerNode*)\0i\0updateInfo(int)\0"
};

const QMetaObject RPG::SkillLevelDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_RPG__SkillLevelDialog,
      qt_meta_data_RPG__SkillLevelDialog, 0 }
};

const QMetaObject *RPG::SkillLevelDialog::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::SkillLevelDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__SkillLevelDialog))
        return static_cast<void*>(const_cast< SkillLevelDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int RPG::SkillLevelDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: display((*reinterpret_cast< PlayerNode*(*)>(_a[1]))); break;
        case 1: updateInfo((*reinterpret_cast< int(*)>(_a[1]))); break;
        }
        _id -= 2;
    }
    return _id;
}
static const uint qt_meta_data_RPG__ItemDialog[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      23,   17,   16,   16, 0x0a,
      44,   16,   16,   16, 0x0a,
      55,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__ItemDialog[] = {
    "RPG::ItemDialog\0\0index\0display(QModelIndex)\0"
    "dropItem()\0useItem()\0"
};

const QMetaObject RPG::ItemDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_RPG__ItemDialog,
      qt_meta_data_RPG__ItemDialog, 0 }
};

const QMetaObject *RPG::ItemDialog::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::ItemDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__ItemDialog))
        return static_cast<void*>(const_cast< ItemDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int RPG::ItemDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: display((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: dropItem(); break;
        case 2: useItem(); break;
        }
        _id -= 3;
    }
    return _id;
}
static const uint qt_meta_data_RPG__InventoryWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      39,   22,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__InventoryWidget[] = {
    "RPG::InventoryWidget\0\0parent,start,end\0"
    "updateRowHeight(QModelIndex,int,int)\0"
};

const QMetaObject RPG::InventoryWidget::staticMetaObject = {
    { &QTableView::staticMetaObject, qt_meta_stringdata_RPG__InventoryWidget,
      qt_meta_data_RPG__InventoryWidget, 0 }
};

const QMetaObject *RPG::InventoryWidget::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::InventoryWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__InventoryWidget))
        return static_cast<void*>(const_cast< InventoryWidget*>(this));
    return QTableView::qt_metacast(_clname);
}

int RPG::InventoryWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QTableView::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: updateRowHeight((*reinterpret_cast< const QModelIndex(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        }
        _id -= 1;
    }
    return _id;
}
static const uint qt_meta_data_RPG__CharacterWidget[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       1,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      22,   21,   21,   21, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RPG__CharacterWidget[] = {
    "RPG::CharacterWidget\0\0update()\0"
};

const QMetaObject RPG::CharacterWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RPG__CharacterWidget,
      qt_meta_data_RPG__CharacterWidget, 0 }
};

const QMetaObject *RPG::CharacterWidget::metaObject() const
{
    return &staticMetaObject;
}

void *RPG::CharacterWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RPG__CharacterWidget))
        return static_cast<void*>(const_cast< CharacterWidget*>(this));
    return QWidget::qt_metacast(_clname);
}

int RPG::CharacterWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: update(); break;
        }
        _id -= 1;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
