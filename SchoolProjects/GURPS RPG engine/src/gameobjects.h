/******************************************************************************
 * Include suborov obsahujucich zakladne objekty hry
******************************************************************************/
#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H

// postavy
#include "rpgcharacter.h"
// zakladne objekty
#include "rpgobject.h"
// predmety
#include "inventoryobject.h"

#endif