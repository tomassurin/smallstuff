/******************************************************************************
 * Vstupny bod aplikacie 
 * Vytvorenie instanciu triedy QApplication a tiez hlavneho okna aplikacie
******************************************************************************/

#include <QtGui/QApplication>

#include "mainwindow.h"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	QWindowsXPStyle *style=new QWindowsXPStyle;
	app.setStyle(style);
	// nastavenie nahodneho seed pre nahodny generator qrand
	qsrand(QTime::currentTime().msecsTo(QTime(0,0,0)));
	MainWindow mainWin;
	mainWin.show();

	return app.exec();
}
