/******************************************************************************
 * Implementacia zakladnych objektov hry. Taktiez sa tu nachadzaju zadefinovane zakladne konstanty
******************************************************************************/
#ifndef RPGOBJECT_H
#define RPGOBJECT_H

#include <QPainter>
#include <QPixmap>
#include <QObject>
#include <QMap>
#include <QVector>
#include <QImage>

namespace RPG {

	// konstanty atributov 
	enum Attribute {DEFAULTATTR=0, STRENGTH=1, DEXTERITY=2, INTELLIGENCE=3, HEALTH=4 };
	// konstanty zlozitosti dovednosti
	enum Difficulty { DEFAULTDIFF=0, EASY=1, AVERAGE=2, HARD=3, ATTRIBUTE=10};
	// konstanty implicitnej urovne dovednosti
	enum DefaultSkill { NONE=-1000 };
	// konstanty vysledku hodu na uspech
	enum SuccessRollResult { ERROR_RESULT =0, CRITICALSUCCESS=1, SUCCESS=2, FAILURE=4, CRITICALFAILURE=8 };
	// konstanty hodu na reakciu
	enum ReactionRollResult { REACTION_ERROR = 0, REACTION_BAD = 1, REACTION_NEUTRAL = 2, REACTION_GOOD = 3};
	// konstanty typu kocky zranenia
	enum DamageDieType { UNDEFINED_DMG_DIE=0, THRUST=1, SWING=2, BOTH=5, DIE=10};
	// konstanty typu zranenia
	enum DamageType { OTHERDMG=0, BURN=1, CRUSHING=2, CUTTING=3, IMPALING=4, SMALLPIERCING=5, PIERCING=6, LARGEPIERCING=7 };
	// konstanty typov lokacii objektu
	enum ObjectLocationType { UNDEFINED_LOC=0, MAP_LOC = 1, INVENTORY_LOC = 2, MONSTER_LOC, ITEM_LOC};
	// konstanty vysledkov suboja
	enum CombatResult { ERROR_COMBAT_RESULT=0, CONTINUE=3, WINNER };
	// konstanty manevrov suboja
	enum CombatManeuver { DO_NOTHING=0, ATTACK=1, ATTACK_DETERMINED=2, 
		ATTACK_DOUBLE=3, ATTACK_STRONG=4, DEFENSE_DOUBLE=5, DEFENSE_STRONG=6, OTHERMAN=7, MOVE=8 };
	// konstanty stavu postavy
	enum CharacterState { HEALTHY, INJURED, DYING, UNCONSCIOUS, DEAD };
	/**/

	// dopredne deklaracie 
	class Die;
	class RPGObject;
	class Character;
	class Inventory;
	class InventoryObject;
	class Weapon;
	class Body;
	class Map;
	class BodyLocation;
	class Trigger;
	class Monster;
	class Campaign;

	/** Trieda nesuca informacie o lokacii objektu */
	class ObjectLocation 
	{
	public:
		ObjectLocation();
		
		/** Nastavi lokaciu na MONSTER_LOC a nastavi jej NPC postavu */
		void setMonster(Monster *mon);
		/** Ziska NPC postavu ak je lokacia MONSTER_LOC*/
		Monster* getMonster();

		/** Nastavi lokaciu na ITEM_LOC a nastavi jej predmet */
		void setItem(InventoryObject *it);
		/** Ziska predmet ak je lokacia ITEM_LOC */
		InventoryObject* getItem();

		/** Nastavi lokaciu na MAP_LOC s mapou map a suradnicami (x;y) (herne) */
		void setMap(Map* map, int x, int y);		
		/** Vrati mapu ak je lokacia MAP_LOC */
		Map *getMap();
		/** Nastavi x-ovu suradnicu na mape ak je lokacia MAP_LOC */
		void setX(int x);
		/** Nastavi y-ovu suradnicu na mape ak je lokacia MAP_LOC */
		void setY(int y);
		/** Vrati x-ovu suradnicu na mape ak je lokacia MAP_LOC */
		int getX() const;
		/** Vrati y-ovu suradnicu na mape ak je lokacia MAP_LOC */
		int getY() const;
		/** Ziska poziciu na mape ak je lokacia MAP_LOC inak vracia (-1;-1)*/
		QPoint getPos() const;

		/** Nastavi lokaciu na INVENTORY_LOC a nastavi inventar a jeho index */
		void setInventory(Inventory *inv, int index);
		/** Ziska inventar ak je lokacia INVENTORY_LOC */
		Inventory *getInventory();
		/** Vrati index inventara ak je lokacia INVENTORY_LOC */
		int getIndex() const;

		/** Vrati typo lokacie */
		ObjectLocationType getLocationType() const { return type; }
		/** Vymaze lokaciu */
		void clear();

	private:
		// typ lokacie 
		ObjectLocationType type;
		// mapa
		Map *map;
		// suradnica x na mape
		int mapX;
		// suradnica y na mape
		int mapY;
		// inventar
		Inventory *inventory;
		// index v inventari
		int inventoryIndex;
		// predmet v inventari
		InventoryObject *item;
		// NPC postava
		Monster *monster;
	};

	/** Trieda reprezentuje dovednost postavy pouzivam aj na udrzovanie tabulky typov skillov */
	class Skill
	{
	public:
		Skill();

		/** Vrati kontrolny atribut */
		Attribute getAttribute() const { return attr;}
		/** Vrati zlozitost */
		Difficulty getDifficulty() const { return difficulty;}
		/** Vrati uroven */
		int getLevel() const	  { return level;}
		/** Vrati implicitnu uroven */
		int getDefault() const { return def;}
		
		/** Nastavi kontrolny atribut */
		void setAttribute(Attribute);
		/** Nastavi zlozitost */
		void setDifficulty(Difficulty);
		/** Nastavi uroven */
		void setLevel(int);
		/** Nastavi implicitnu uroven */
		void setDefault(int);
		/** Nastavi bonus k urovni dovednosti */
		void setBonus(int bonus) { this->bonus = bonus;}
		
		/** Vrati efektivnu uroven dovednosti postavy ch */
		int getFinalLevel(const Character *ch);
		/** Vrati implicitnu uroven dovednosti */
		int getDefaultLevel(const Character *);
		/** Vrati bonus */
		int getBonus() { return bonus;}
		/** Vrati prvu dosiahnutelnu uroven dovednosti */
		int getFirstLevel();
		/** Vrati cenu na postup na dalsiu uroven (v xp bodoch) */
		int getNextLevelCost();
		/** Postup urovne dovednosti o jednu uroven */
		void processNextLevel();
		
		/** Vrati identifikator */
		const QString &getID() const { return id;}
		/** Nastavi identifikator */
		void setID(const QString& str) { id=str.toLower();}
		
		/** Vrati informacie o dovednosti */ 
		QString getShortInfo();
		
		// Zobrazovany nazov dovednosti
		QString name;
		// informacie o dovednosti
		QString info;
		// sem sa ukladaju dalsie modifikatory
		int otherMod;			
					
	private:
		// identifikator
		QString id;
		// kontrolujuci primary stat	
		Attribute attr;			
		// zlozitost
		Difficulty difficulty;	
		// uroven dovednosti
		int level;				
		// defaultny level
		int def;		
		// bonus
		int bonus;
	};

	/** Trieda zdruzujuca tabulky pravidiel */
	class RPGRules
	{
	public:
		RPGRules();

		void clear();
		/** Zisti ci su pravidla spravne */
		bool isValid();
		/** Prida zakladne pravidla */
		void addBasicRules();

		/** Praca s tabulkou zranenia */
		void addDmgThrustType(int ST, QString &str);
		QString getDmgThrustType(int ST);
		void addDmgSwingType(int ST, QString &str);
		QString getDmgSwingType(int ST);

		/** Prida definiciu dovednosti */
		void addSkill(const Skill& skill);
		/** Vrati dovednost s identifikatorom id */
		Skill *getSkill(const QString &id) const;
		/** Vrati identifikatory dovednosti */
		QList<QString> getSkillsNames();

		/** Prida rasu - typ tela postavy */
		void addRace(const Body &race);
		/** Ziska rasu s id name */
		Body getRace(const QString &name) const;

		/** Prida objekt obj s identifikatorom id do globalnej definicii objektov */
		void addObjectType(const QString &id, RPGObject *obj);
		/** Vytvori objekt s identifikatorom id z globalnej definicie objektov */
		RPGObject *getObjectType(const QString &id) const;
		
		/** Ziska cenu za vykonanie manevru man */
		int getActionCost(CombatManeuver man);

		/** Prida modifikator velkosti */
		void addSizeMod(double sz, int mod);
		/** Vrati modifikator velkosti */
		double getSizeMod(double sz);

		/** Prida cenu za postup dovednosti */
		void addSkillCost(int level, int cost, Difficulty dif);
		/** Prida cenu za kazdy vyssi level, ktory nie je uvedeny v tabulke cien postupu dovednosti */
		void addSkillStep(int step,Difficulty dif);
		/** Vrati cenu postupu na uroven level ak je dovednost obtiaznosti dif */
		int getSkillCost(int level, Difficulty dif);
		/** Vrati prvu uroven dovednosti s obtiaznostou dif */
		int getSkillFirstLevel(Difficulty dif);

	public:
		// ceny manevrov
		struct {
			int talk;
			int attack;
			int determinedAttack;
			int strongAttack;
			int doubleAttack;
			int useItem;
			int doubleDefense;
			int strongDefense;
			int pickItem;
		} actionCosts;
		// tabulka reakcii 
		struct {
			int reactionBad;
			int reactionNeutral;
			int reactionGood;
		} reactionLimit;

		// modifikatory suboja
		int determinedAttackMod;
		int strongAttackMod; 
		int strongDefenseMod;
		double lowHPMod;
		double dmgMods[8];
		// skalovacia konstanta na body pohybu
		static const int movePtScaleFactor = 100;
	private:
		// tabulka zranenia bodnutim
		QMap<int,QString> DmgThrustTable;
		// tabulka zranenia machnutim
		QMap<int,QString> DmgSwingTable;
		// tabulka dovednosti
		QMap<QString, Skill> SkillTable;
		// tabulka ras
		QMap<QString, Body> RaceTable;
		// globalna definicia objektov
		QMap<QString, RPGObject*> objects;
		// tabulka modifikatorov velkosti
		QMap<double,int> sizeModTable;

		// tabulka cien postupu dovednosti 
		struct {
			QMap<int,int> easyCosts;
			QMap<int,int> averageCosts;
			QMap<int,int> hardCosts;
			int easyStep;
			int averageStep;
			int hardStep;
		} skillCosts;
	};	

	/** Trieda reprezentuje herny hod - 1 kockou alebo viacerymi */
	class Die
	{
	public:
		Die();
		/** Nastavi hod kockou z retazca */
		Die(QString &str);

		/** Nastavi hod kockou z retazca */
		void setDie(QString &str); //z retazca v tvare 2k6+10
		/** Nastavi hod kockou */
		void setDie(int count, int type, int modifier);
		/** Prida modifikator */
		void addMod(int mod);		
		/** Vrati textovy popis kocky */
		QString getDie() const;
		/** Vrati vysledok hodu kockou */
		int throwDie() const;
		/** Vrati maximalnu hodnotu kocky */
		int getMaxVal() const;
		/** Vymaze kocku */
		void clear();

		//ak kocka sluzi ako damage roll tak tu bude typ zranenia
		DamageType dmgType;			

	private:
		/* count*type+modifier */
		/* 2k6+10 - hodime 2x 6stennou kockou a pridame k vysledku 10 */
		QString name;
		//pocet hadzanych kociek
		int count;	
		//typ kocky
		int type;   
		//modifikator kocky
		int modifier; 
	};

	/** Trieda reprezentujuca hod na uspech */
	class SuccessRoll
	{
	public:
		SuccessRoll();

		/** Nastavi hod na uspech  postavy ch proti dovednosti s id skillname s modifikatorom modifiers */
		void setRoll(Character *ch, const QString &skillname, int modifiers);
		/** Nastavi hod na uspech  postavy ch proti dovednosti skill s modifikatorom modifiers */
		void setRoll(Character *ch, Skill *skill, int modifiers);
		/** Nastavi hod na uspech proti efektivnej dovednosti effective */
		void setRoll(int effective);
		/** Hodi kockou vrati vysledok hodu na uspech */		
		SuccessRollResult roll();  
		//vrati MOS minuleho hodu
		int getMarginOfSuccess() const;   

	private:
		// efektivna dovednost
		int effectiveSkill;
		// hod kocku
		Die die;
		// minuly hod 
		int lastRoll;
		// margin of success
		int mos;	
	};

	/** Trieda reprezentujuca typ zranenia */
	class Damage
	{
	public:
		Damage();

		/** Nastavi typ zranenia */
		void setDamage(QString &dmg);
		/** Vrati textovu reprezentaciu typu zranenia */
		const QString& getDamageString() const;
		/** Vrati typ kocky pouziej v damage roll s tymto zranenim */
		DamageDieType getDamageDieType() const;
		/** Vrati kocku ktora moze byt pouzita v damage roll s tymto zranenim */
		Die getDamageDie(Character *) const;
		Die getDamageDieThr(Character *) const;
		Die getDamageDieSw(Character *) const;

	protected:
		/** Vyparsuje typ zranenia z retazca */
		DamageType parseDamageType(QString &dmg) const;

		// textova reprezentacia typu zranenia 
		// damage bude v tvare sw+2 (popr thr-1) a podobne alebo priamo v tvare hodu kockou (ie. 3d6+3)
		QString damage;
		
		// udava typ THRUST alebo SWING alebo DIE ak pouzivame kocku
		DamageDieType DieType;	

		// modifikator k THRUST damage	
		int DmgThrMod;		
		DamageType DmgThrType;

		// modifikator k SWING damage
		int DmgSwMod;		
		DamageType DmgSwType;

		// damage ak bude zadane priamo v tvare hodu kockou
		Die DmgDie;				
	};

	/** Zakladna trieda pre objekty hry */
	class RPGObject : public QObject
	{
		Q_OBJECT
	public:
		RPGObject();
		RPGObject(const RPGObject&);
		RPGObject &operator=(const RPGObject&);

		~RPGObject();
		
		/** Vymaze objekt - pouzivam namiesto delete */
		virtual void release() { 
			removeFromLocation();
			delete this;
		};

		/** Funkcie na kreslenie */
		void drawBackground(const QColor &bgcolor = Qt::black);
		void drawText(const QString &str);
		void drawCircle(const QColor &bgcolor);
		void drawTexture(const QImage &image, int x, int y, int sx, int sy, bool fit = 0);
		/** Vrati pixmapu objektu */
		const QPixmap& getPixmap() const;

		/** Vrati informacie o objekte */
		virtual QString getInfo() const { return about;}
		/** Nastavi about string objektu */
		void setAboutString(const QString & about) { this->about=about;}
		/** Vrati identifikator objektu */
		const QString &getID() const { return id;}
		/** Nastavi identifikator objektu */
		void setID(const QString &id) { this->id=id.toLower();}
		/** Nastavi meno objektu */
		void setName(const QString &name) { this->name = name; }
		/** Vrati meno objektu */
		const QString& getName() const { return name;}
		/** Pristup k lokacii objektu */
		ObjectLocation *location() { return &objectLocation; }
		/** Vymaze objekt zo svojej aktualnej lokacie */
		void removeFromLocation();
		/** Pohne objekt na mape na bunku (tox;toy). spendmove udava ci ma minut movePt postav (ak je objekt postava) */
		virtual bool moveOnMap(int tox, int toy, bool spendmove=false);

	signals:
		/** Emitnuty ked sa objekt zmeni */
		void changed();

	protected:
		// identifikator
		QString id;			
		// meno ktore sa bude zobrazovat v programe
		QString name;		
		// grafika objektu
		QPixmap *pixmap;
		// obdlznik velkosti grafiky objektu
		QRectF pixmapRect;
		// painter pre potreby kreslenia na grafiku objektu
		QPainter *painter;
		// about string
		QString about;
		// lokacia objektu
		ObjectLocation objectLocation;
	};


	/* parametre programu */
	// rozmer hrany bunky mapy a grafiky (v pixeloch)
	static int sidesize=50;
}

// globalna definicia pravidiel hry
extern RPG::RPGRules rules;
// globalna premenna kampane
extern RPG::Campaign *campaign;

/** Pomocna funkcia na vymazanie medzier z retazca */
void removeWhitespaces(QString &str);

#endif