#include <QFile>
#include <QString>
#include <QtXml/QXmlStreamReader>

#include "xmlreader.h"
#include "triggerevent.h"

using namespace XMLReader;
using namespace RPG;


/* misc functions */

void XMLReader::readUnknownElement(QXmlStreamReader *reader)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement())
			break;

		if (reader->isStartElement())
			readUnknownElement(reader);
	}
}

void XMLReader::readGraphics(QXmlStreamReader *reader, const QString& cwd, RPGObject *object) 
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement())
			break;

		if (reader->isStartElement()) {
			if (reader->name() == "texture") {
				QString filename = reader->attributes().value("href").toString();
				if (!filename.endsWith(".png")) 
					reader->raiseError("Bad image format");
				QImage image(cwd+'/'+filename);
				int x = reader->attributes().value("x").toString().toInt();
				int y = reader->attributes().value("y").toString().toInt();

				int sx = reader->attributes().value("sx").toString().toInt();
				int sy = reader->attributes().value("sy").toString().toInt();
				int fit = (reader->attributes().value("fit").toString() == "true")?true:false;
				object->drawTexture(image,x,y,sx,sy,fit);

				reader->readNext();
			}
			else if (reader->name()=="bgcolor") {
				object->drawBackground(reader->attributes().value("color").toString());
				reader->readNext();
			}
			else if (reader->name()=="circle") {
				object->drawCircle(reader->attributes().value("color").toString());
				reader->readNext();
			}
			else if (reader->name()=="text") 
				object->drawText(reader->readElementText());
		}
	}
}

void XMLReader::readStats(QXmlStreamReader *reader, Character *ch)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement())
			break;

		if (reader->isStartElement()) {
			if (reader->name()=="st") {
				ch->setST(reader->readElementText().toUInt());
			}
			else if (reader->name()=="dx") {
				ch->setDX(reader->readElementText().toUInt());
			}
			else if (reader->name()=="iq") {
				ch->setIQ(reader->readElementText().toUInt());
			}
			else if (reader->name()=="ht") {
				ch->setHT(reader->readElementText().toUInt());
			}
			else readUnknownElement(reader);
		}
	}
}

void XMLReader::readSkill(QXmlStreamReader *reader, RPG::Skill *skill)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement()) 
			break;

		if (reader->isStartElement()) {
			if (reader->name()=="name") 
				skill->name = reader->readElementText();
			else if (reader->name()=="info")
				skill->info=reader->readElementText();
			else if (reader->name()=="attr") {
				QString tmp=reader->readElementText();
				if (tmp=="st") skill->setAttribute(STRENGTH);
				else if (tmp=="dx") skill->setAttribute(DEXTERITY);
				else if (tmp=="iq") skill->setAttribute(INTELLIGENCE);
				else if (tmp=="ht") skill->setAttribute(HEALTH);
				else skill->setAttribute(DEFAULTATTR);
			}
			else if (reader->name()=="difficulty") {
				QString tmp=reader->readElementText();
				if (tmp=="easy") skill->setDifficulty(EASY);
				else if (tmp=="average") skill->setDifficulty(AVERAGE);
				else skill->setDifficulty(DEFAULTDIFF);
			}
			/*
			else if (reader->name()=="level") {
				bool ok;
				int level = reader->readElementText().toInt(&ok);
				if (!ok)
					reader->raiseError("Bad number format");
				skill->setLevel(level);
			}   */
			else if (reader->name()=="default") {
				QString tmp=reader->readElementText();
				if (tmp=="none") skill->setDefault(NONE);
				else {
					bool ok;
					int level = reader->readElementText().toInt(&ok);
					if (!ok)
						reader->raiseError("Bad number format");
					skill->setDefault(level);
				}
			}
			else readUnknownElement(reader);
		}
	}
}

template <class T> 
void XMLReader::readInventory(QXmlStreamReader *reader, const QString& cwd, T *inventory)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement()) 
			break;

		if (reader->isStartElement()) {
			if (reader->name()=="item") {
				QStringRef type = reader->attributes().value("type");
				QString loc = reader->attributes().value("loc").toString();
				QString id = reader->attributes().value("id").toString();
				removeWhitespaces(id);
				if (id.startsWith("objdb/",Qt::CaseInsensitive)) {
					id = id.mid(id.indexOf("/")+1);
					InventoryObject *obj = dynamic_cast<InventoryObject*>(rules.getObjectType(id));
					if (obj != NULL)
						inventory->addItem(obj,loc);

					reader->readNext();
				}
				else {
					if (type == "weapon") {
						Weapon *weapon = new Weapon();
						if (!id.isEmpty()) 
							weapon->setID(id);
						readItem<Weapon,RPGRules>(reader, cwd, weapon,&rules);
						inventory->addItem(weapon, loc);
					}
					else if (type == "armor") {
						Armor *armor = new Armor();
						if (!id.isEmpty())
							armor->setID(id);
						readItem<Armor,RPGRules>(reader, cwd, armor,&rules);
						inventory->addItem(armor, loc);
					}
					else {
						InventoryObject *obj = new InventoryObject();
						if (!id.isEmpty())
							obj->setID(id);
						readItem<InventoryObject,RPGRules>(reader,cwd,obj,&rules);
						inventory->addItem(obj,loc);
					}
				}
			}
			else readUnknownElement(reader);
		}
	}
}

template <class T, class R>
void XMLReader::readItem(QXmlStreamReader *reader, const QString& cwd, T *item, R *objdb)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement()) 
			break;

		if (reader->isStartElement()) {
			if (reader->name()=="name") 
				item->setName(reader->readElementText());
			else if (reader->name()=="dmg") {
				if (typeid(*item) == typeid(Weapon)) {
					Weapon *wp = dynamic_cast<Weapon*>(item);
					if (wp == NULL)
						reader->raiseError();
					wp->setDamage(reader->readElementText());
				}
				else reader->raiseError("Dmg specified in non weapon description");
			}
			else if (reader->name()=="dr") {
				if (typeid(*item) == typeid(Armor)) {
					bool ok;
					int dr = reader->readElementText().toInt(&ok);
					if (!ok) 
						reader->raiseError("Bad DR number format");
					Armor *ar = dynamic_cast<Armor*>(item);
					if (item == NULL) 
						reader->raiseError();
					ar->setDR(dr);
				}
				else reader->raiseError("DR specified in non armor description");
			}
			else if (reader->name()=="weight") {
				bool ok;
				double w = reader->readElementText().toDouble(&ok);
				if (!ok)
					reader->raiseError("Bad weight number format");
				item->setWeight(w);
			}
			else if (reader->name()=="skill") {
				if (typeid(*item) == typeid(Weapon)) {
					Weapon *wp = dynamic_cast<Weapon*>(item);
					if (wp == NULL)
						reader->raiseError();
					wp->addSkill(reader->readElementText());
				}
				else reader->raiseError("Skill specified in non weapon description");
				
			}
			else if (reader->name()=="graphics") {
				readGraphics(reader, cwd, item);
				reader->readNext();
			}
			// citam povolene lokacie kde moze byt predmet noseny
			else if (reader->name()=="equip") {
				QString text=reader->readElementText();
				text=text.simplified();
				text=text.remove(QRegExp("\\s"));
				QStringList list=text.split(',',QString::SkipEmptyParts);
				foreach( QString str, list)
					item->addEquipLocation(str);
			}
			else if (reader->name()=="minst") {
				bool ok;
				int st = reader->readElementText().toInt(&ok);
				if (!ok)
					reader->raiseError("Bad number format");
				item->setMinimumST(st);
			}
			else if (reader->name()=="about") {
				item->setAboutString(reader->readElementText());
			}
			// nacitanie triggerov
			else if (reader->name()=="trigger") {
				QString type = reader->attributes().value("type").toString().toLower();
				QString id = reader->attributes().value("id").toString();
				uint uses = -1;
				if (type == "onuse") {
					bool ok;
					uses = reader->attributes().value("uses").toString().toInt(&ok);
					if (!ok) {
						uses = -1;
					}
				}

				removeWhitespaces(id);
				Trigger *trig;
				
				if (id.startsWith("objdb/",Qt::CaseInsensitive)) {
					id = id.mid(id.indexOf("/")+1);
					trig = dynamic_cast<Trigger*>(objdb->getObjectType(id));
					if (trig == NULL) {
						reader->raiseError();
						return;
					}
					reader->readNext();
				}
				else {
					trig = new Trigger();
					trig->setScript(reader->readElementText());
				}
				
				if (type == "onequip") {
					item->setEquipTrigger(trig);
				}
				else if (type == "onunequip") {
					item->setUnEquipTrigger(trig);
				}
				else if (type == "oninventory") {
					item->setInventoryTrigger(trig);
				}
				else if (type == "onuninventory") {
					item->setUnInventoryTrigger(trig);
				}
				else if (type == "onuse") {
					item->setUses(uses);
					item->setUseTrigger(trig);
				}
				else {
					delete trig;
					reader->raiseError("Bad inventory item trigger type");
					return;
				}
				delete trig;
			}
			else readUnknownElement(reader);

		}
	}
}

template<class T>
void XMLReader::readRPGCharacter(QXmlStreamReader *reader, const QString& cwd, Character *ch, T *objdb)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement())
			break;
	
		if (reader->isStartElement()) {
			if (reader->name()=="name") {
				ch->setName(reader->readElementText());
			}
			else if (reader->name()=="position") {
				if (typeid(*ch) == typeid(PlayerNode)) {
					PlayerNode *pl = dynamic_cast<PlayerNode*>(ch);
					if (pl != NULL) 
						pl->setCoord(reader->attributes().value("x").toString().toUInt(),
								    reader->attributes().value("y").toString().toUInt());
				}
				reader->readNext();
			}
			else if (reader->name()=="graphics") {
				readGraphics(reader, cwd, ch);
				reader->readNext();
			}
			else if (reader->name()=="stats") 
				readStats(reader,ch);
			else if (reader->name()=="size") {
				bool ok;
				int val = reader->readElementText().toDouble(&ok);
				if (!ok)
					reader->raiseError("Bad number format");
				ch->setSize(val);
			}
			/*else if (reader->name()=="weapon") {
				Weapon *wp=new Weapon();
				readItem<Weapon>(reader,cwd,wp);
				//Character->addWeapon(wp);
			}  */
			else if (reader->name()=="inventory") {
				readInventory<RPG::Inventory>(reader, cwd, ch->getInventory());
			}
			else if (reader->name()=="skill") {
				QString tmp="";
				int level;
				bool ok;
				tmp = reader->attributes().value("id").toString();
				level = reader->attributes().value("level").toString().toInt(&ok);
				if (!ok) 
					reader->raiseError("Bad integer format in skill level");
				ch->addSkill(tmp,level);
				reader->readNext();
			}
			else if (typeid(*ch) == typeid(Monster)) {
				if (reader->name()=="xpgain") {
					bool ok;
					int xp = reader->readElementText().toInt(&ok);
					if (!ok)
						reader->raiseError("Bad number format");
					Monster *mon = dynamic_cast<Monster*>(ch);
					mon->setXPGain(xp);
				}
				// nacitanie triggerov priserky (jednoduche pravidla pre ovladanie NPC)
				else if (reader->name()=="trigger") {
					QString type = reader->attributes().value("type").toString().toLower();
					QString id = reader->attributes().value("id").toString();
					removeWhitespaces(id);
					Trigger *trig;
					Monster *mon = dynamic_cast<Monster*>(ch);

					if (id.startsWith("objdb/",Qt::CaseInsensitive)) {
						id = id.mid(id.indexOf("/")+1);
						trig = dynamic_cast<Trigger*>(objdb->getObjectType(id));
						if (trig == NULL) {
							reader->raiseError("Monster trigger with specified id not found");
							return;
						}
						reader->readNext();
					}
					else {
						trig = new Trigger();
						trig->setScript(reader->readElementText());
					}

					// trigger pre dialogy
					if (type == "ontalk") {
						mon->setOnTalkTrigger(trig);
					}
					else if (type == "ontarget") {
						mon->setTargetEnemyTrigger(trig);
					}
					else if (type == "onmaneuver") {
						mon->setCombatManeuverTrigger(trig);
					}
					else if (type == "onkill") {
						mon->setOnKillTrigger(trig);
					}
					else {
						delete trig;
						reader->raiseError("Bad monster trigger type");
						return;
					}
					delete trig;
				}
			}
			else if (reader->name() == "totalxp") {
				bool ok;
				int xp = reader->readElementText().toInt(&ok);
				if (!ok)
					reader->raiseError("Bad number format");
				ch->setTotalXP(xp);
			}
			else if (reader->name() == "unusedxp") {
				bool ok;
				int xp = reader->readElementText().toInt(&ok);
				if (!ok)
					reader->raiseError("Bad number format");
				ch->setUnusedXP(xp);
			}
			else readUnknownElement(reader);
		}
	}
}

template<class T>
void XMLReader::readObjectTypedef(QXmlStreamReader *reader, const QString& cwd, T *objdb)
{
	while (!reader->atEnd()) {
		reader->readNext();

		if (reader->isEndElement())
			break;
		if (reader->isStartElement()) {
			if (reader->name()=="monster") {
				Monster *monster=new Monster();
				QString monsterID=reader->attributes().value("id").toString();
				readRPGCharacter<T>(reader,cwd,monster,objdb);
				monster->setID(monsterID);
				objdb->addObjectType(monsterID, monster);
			} 
			else if (reader->name()=="item") {
				QStringRef type = reader->attributes().value("type");
				QString id = reader->attributes().value("id").toString();
				removeWhitespaces(id);
				if (type == "weapon") {
					Weapon *weapon = new Weapon();
					if (!id.isEmpty())
						weapon->setID(id);
					readItem<Weapon,T>(reader, cwd, weapon,objdb);
					objdb->addObjectType(id,weapon);
				}
				else if (type == "armor") {
					Armor *armor = new Armor();
					if (!id.isEmpty())
						armor->setID(id);
					readItem<Armor,T>(reader, cwd, armor,objdb);
					objdb->addObjectType(id, armor);
				}
				else {
					InventoryObject *obj = new InventoryObject();
					if (!id.isEmpty())
						obj->setID(id);
					readItem<InventoryObject,T>(reader,cwd,obj,objdb);
					objdb->addObjectType(id, obj);
				}
			}
			else if (reader->name() == "trigger") {
				Trigger *trig=new Trigger();
				QString id = reader->attributes().value("id").toString();
				if (!id.isEmpty()) 
					trig->setID(id);
				trig->setScript(reader->readElementText());
				objdb->addObjectType(id, trig);
			}
			else readUnknownElement(reader);
		}
	}
}
		
void XMLReader::readMap(QXmlStreamReader *r, const QString& cwd, Map *map)
{
	map->setCwd(cwd);
	while (!r->atEnd()) {
		r->readNext();

		if (r->isEndElement())
			break;

		if (r->isStartElement()) {
			if (r->name()=="name")
				map->setName(r->readElementText());
			else if (r->name()=="size") {
				map->setSize(r->attributes().value("width").toString().toUInt(),
					r->attributes().value("height").toString().toUInt());
				r->readNext();
			}
			//pociatocna pozicia
			else if (r->name()=="startpos") {
				map->setStartPos(r->attributes().value("x").toString().toUInt(),
					r->attributes().value("y").toString().toUInt());
				r->readNext();
			}
			else if (r->name()=="objects")
				readObjectTypedef<RPG::Map>(r,cwd,map);
			else if (r->name()=="terraindef")  
				readTerrainTypedef(r, cwd, map);
			else if (r->name()=="mapnode") {
				RPG::MapNode *node=new MapNode();
				node->setMap(map);
				node->setCoord(r->attributes().value("x").toString().toUInt(),
					r->attributes().value("y").toString().toUInt());
				node->setTerrain(r->attributes().value("terrain").toString());
				readMapNode(r,cwd,map,node);
				map->addNode(node);
			} 
			else readUnknownElement(r);
		}
	}
}

void XMLReader::readMapNode(QXmlStreamReader *r, const QString& cwd, RPG::Map *map, RPG::MapNode* node)
{
	while (!r->atEnd()) {
		r->readNext();

		if (r->isEndElement())
			break;

		if (r->isStartElement()) {
			if (r->name()=="object") {
				QString id=r->attributes().value("id").toString();

				removeWhitespaces(id);
				RPGObject *obj = map->getObjectType(id);
				if (obj != NULL)
					node->addObject(obj);

				r->readNext();
			}
			else if (r->name()=="trigger") {
				Trigger *trig=new Trigger();
				trig->setNode(node);
				QString id = r->attributes().value("id").toString();
				if (!id.isEmpty()) 
					trig->setID(id);
				trig->setScript(r->readElementText());
				node->addObject(trig);
			}
			else readUnknownElement(r);
		}

	}
}

void XMLReader::readTerrainTypedef(QXmlStreamReader *r, const QString& cwd, RPG::Map *map)
{
	while (!r->atEnd()) {
		r->readNext();

		if (r->isEndElement())
			break;
		if (r->isStartElement()) {
			if (r->name()=="terrain") {
				RPGTerrain *terrain=new RPGTerrain();
				QString terrainID=r->attributes().value("id").toString();
				readTerrain(r,cwd, terrain);
				terrain->setID(terrainID);
				map->addTerrainType(terrainID, terrain);
			} 
			else readUnknownElement(r);
		}
	}
}

void XMLReader::readTerrain(QXmlStreamReader *r, const QString& cwd, RPGTerrain *terrain)
{
	while (!r->atEnd()) {
		r->readNext();

		if (r->isEndElement())
			break;

		if (r->isStartElement()) {
			if (r->name()=="graphics") {
				readGraphics(r, cwd, terrain);
				r->readNext();
			}
			else if (r->name()=="walkable") {
				terrain->setWalkable(r->attributes().value("value").toString()=="true"?true:false);
				r->readNext();
			}
			else if (r->name()=="cost") {
				bool ok;
				int val = r->readElementText().toInt(&ok);
				if (!ok) 
					continue;
				terrain->setCost(val);
			}
			else readUnknownElement(r);
		}
	}
}

/* class CampaignXMLReader */
CampaignXMLReader::CampaignXMLReader(QIODevice *device, QString& cwd) 
	: QXmlStreamReader(device)
{
	this->cwd=cwd;
}

bool CampaignXMLReader::read()
{
	while (!atEnd()) {
		readNext();

		if (isEndElement())
			break;

		if (isStartElement()) {
			if (name() == "campaign") {
				campaign->clear();
				readCampaign();
			}
			else if (name()=="rules") {
				rules.clear();
				readRules(rules);
			}
			else raiseError(QObject::tr("Not a campaign file"));
		}
	}
	return error();
}

void CampaignXMLReader::readCampaign() 
{
	while (!atEnd()) {
		readNext();

		if (isEndElement())
			break;

		if (isStartElement()) {
			if (name()=="name")
				campaign->setName(readElementText());
			else if (name()=="rules") {
				rules.clear();
				readRules(rules);
			}
			else if (name()=="include") {
				QString attr = attributes().value("type").toString();
				QString filename=attributes().value("href").toString();
				QFile file(cwd+'/'+filename);
				if (!file.open(QFile::ReadOnly | QFile::Text)) {
					raiseError(QObject::tr("Include file does not exist"));
					return;
				}

				QFileInfo fileInfo(file);
				if (attr.toLower() == "map") {
					Map *map = new Map();
 					MapXMLReader reader(&file, fileInfo.absolutePath() , map);
					if (reader.read()) 
						raiseError(reader.errorString());
					campaign->setCurrentMap(map);
				}
				else {
					CampaignXMLReader reader(&file, fileInfo.absolutePath());
					if (reader.read()) 
						raiseError(reader.errorString());
				}
				readNext();
			} 
			else if (name()=="map") {
				Map *map = new Map();
				readMap(this,cwd,map);
				campaign->setCurrentMap(map);
			}
			else if (name()=="rules") {
				rules.clear();
				readRules(rules);
			}
			else readUnknownElement(this);
		}
	}
}

void CampaignXMLReader::readRules(RPGRules &rules)
{
	while (!atEnd()) {
		readNext();

		if (isEndElement())
			break;

		if (isStartElement()) {
			if (name()=="dmgthrust") 
				while(!atEnd()) {
					readNext();

					if (isEndElement())
						break;

					if (isStartElement()) {
						if (name()=="data") { 
							bool ok;
							int x=attributes().value("st").toString().toUInt(&ok);
							if (!ok)
								raiseError("Bad number format");
							rules.addDmgThrustType(x,readElementText());
						}
						else readUnknownElement(this);
					}
				}
			else if (name()=="dmgswing") 
				while(!atEnd()) {
					readNext();

					if (isEndElement())
						break;

					if (isStartElement()) {
						if (name()=="data") { 
							bool ok;
							int x=attributes().value("st").toString().toUInt(&ok);
							if (!ok)
								raiseError("Bad number format");
							rules.addDmgSwingType(x,readElementText());
						}
						else readUnknownElement(this);
					}
				}
			else if (name()=="skills") {
				while (!atEnd()) {
					readNext();

					if (isEndElement()) 
						break;

					if (isStartElement()) {
						if (name()=="skill") {
							Skill tmp;
							QString id = attributes().value("id").toString();
							if (!id.isEmpty()) 
								tmp.setID(id);
							readSkill(this,&tmp);
							rules.addSkill(tmp);
						}
						else readUnknownElement(this);
					}
				}
			}
			else if (name() == "skillcosts") {
				while (!atEnd()) {
					readNext();

					if (isEndElement()) 
						break;

					if (isStartElement()) {
						if (name()=="easy") {
							while (!atEnd()) {
								readNext();

								if (isEndElement()) 
									break;

								if (isStartElement()) {
									if (name()=="data") {
										bool ok;
										double level=attributes().value("level").toString().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										int val=readElementText().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										rules.addSkillCost(level,val,EASY);
									}
									else if (name()=="step") {
										bool ok;
										int val=readElementText().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										rules.addSkillStep(val,EASY);
									}
									else readUnknownElement(this);
								}
							}
						}
						else if (name()=="average") {
							while (!atEnd()) {
								readNext();

								if (isEndElement()) 
									break;

								if (isStartElement()) {
									if (name()=="data") {
										bool ok;
										double level=attributes().value("level").toString().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										int val=readElementText().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										rules.addSkillCost(level,val,AVERAGE);
									}
									else if (name()=="step") {
										bool ok;
										int val=readElementText().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										rules.addSkillStep(val,AVERAGE);
									}
									else readUnknownElement(this);
								}
							}
						}
						else if (name()=="hard") {
							while (!atEnd()) {
								readNext();

								if (isEndElement()) 
									break;

								if (isStartElement()) {
									if (name()=="data") {
										bool ok;
										double level=attributes().value("level").toString().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										int val=readElementText().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										rules.addSkillCost(level,val,HARD);
									}
									else if (name()=="step") {
										bool ok;
										int val=readElementText().toInt(&ok);
										if (!ok)
											raiseError("Bad number format");
										rules.addSkillStep(val,HARD);
									}
									else readUnknownElement(this);
								}
							}
						}
						else readUnknownElement(this);
					}
				}
			}
			else if (name()=="actioncosts") {
				while (!atEnd()) {
					readNext();

					if (isEndElement()) 
						break;

					if (isStartElement()) {
						QStringRef nm = name();
						bool ok;
						uint cost =	readElementText().toUInt(&ok);
						if (!ok)
							raiseError("Bad number format");
						if (nm=="talk") {
							rules.actionCosts.talk = cost;
						}
						else if (nm=="attack") {
							rules.actionCosts.attack = cost;
						}
						else if (nm=="determinedattack") 
							rules.actionCosts.determinedAttack = cost;
						else if (nm == "strongattack") {
							rules.actionCosts.strongAttack = cost;
						}
						else if (nm == "doubleattack") 
							rules.actionCosts.doubleAttack = cost;
						else if (nm == "useitem")
							rules.actionCosts.useItem = cost;
						else if (nm == "doubledefense")
							rules.actionCosts.doubleDefense = cost;
						else if (nm == "strongdefense")
							rules.actionCosts.strongDefense = cost;
						else if (nm == "pickitem")
							rules.actionCosts.pickItem = cost;
						else readUnknownElement(this);
					}
				}
			}
			else if (name()=="sizemod") {
				while(!atEnd()) {
					readNext();

					if (isEndElement())
						break;

					if (isStartElement()) {
						if (name()=="data") { 
							bool ok;
							double x=attributes().value("size").toString().toDouble(&ok);
							if (!ok)
								raiseError("Bad number format");
							int y=readElementText().toInt(&ok);
							if (!ok)
								raiseError("Bad number format");
							rules.addSizeMod(x,y);
						}
						else readUnknownElement(this);
					}
				}
			}
			else if (name()=="modifiers") {
				while (!atEnd()) {
					readNext();

					if (isEndElement()) 
						break;

					if (isStartElement()) {
						QStringRef nm = name();
						bool ok;
						double cost = readElementText().toDouble(&ok);
						if (!ok)
							raiseError("Bad number format");

						if (nm == "determinedattack") 
							rules.determinedAttackMod = (int) cost;
						else if (nm == "strongattack")
							rules.strongAttackMod = (int) cost;
						else if (nm == "strongdefense")
							rules.strongDefenseMod = (int) cost;
						else if (nm == "lowhpmod")
							rules.lowHPMod = cost;
						else if (nm == "burndmg")
							rules.dmgMods[BURN] = cost;
						else if (nm == "crushdmg")
							rules.dmgMods[CRUSHING] = cost;
						else if (nm == "cutdmg")
							rules.dmgMods[CUTTING] = cost;
						else if (nm == "impdmg")
							rules.dmgMods[IMPALING] = cost;
						else if (nm == "spidmg")
							rules.dmgMods[SMALLPIERCING] = cost;
						else if (nm == "pidmg")
							rules.dmgMods[PIERCING] = cost;
						else if (nm == "lpidmg")
							rules.dmgMods[LARGEPIERCING] = cost;
						else readUnknownElement(this);
					}
				}
			}
			else if (name()=="reaction") {
				while (!atEnd()) {
					readNext();

					if (isEndElement()) 
						break;

					if (isStartElement()) {
						QStringRef nm = name();
						bool ok;
						uint react = readElementText().toUInt(&ok);
						if (!ok)
							raiseError("Bad number format");

						if (nm=="bad")
							rules.reactionLimit.reactionBad = react;
						else if (nm=="neutral")
							rules.reactionLimit.reactionNeutral = react;
						else if (nm=="good")
							rules.reactionLimit.reactionGood = react;
						else readUnknownElement(this);
					}
				}
			}
			else if (name()=="objects") {
				readObjectTypedef<RPG::RPGRules>(this,cwd,&rules);
			}
			else readUnknownElement(this);
		}
	}
}

/* class MapXMLReader */
MapXMLReader::MapXMLReader(QIODevice *device, QString& cwd, Map *map) 
	: QXmlStreamReader(device)
{
	this->map = map;
	map->setCwd(cwd);
	this->cwd = cwd;
}

bool MapXMLReader::read()
{
	while (!atEnd()) {
		readNext();

		if (isEndElement())
			break;

		if (isStartElement()) {
			if (name() == "map") {
				map->clear();
				readMap(this,cwd,map);
			}
			else raiseError(QObject::tr("Not a map file"));
		}
	}
	return error();
}

/* class PlayerXMLRead */

PlayerXMLRead::PlayerXMLRead(QIODevice *device, QString& cwd, PlayerNode *ch) : QXmlStreamReader(device)
{
	this->ch=ch;
	this->cwd=cwd;
}

bool PlayerXMLRead::read()
{
	while (!atEnd()) {
		readNext();

		if (isEndElement())
			break;
	
		if (isStartElement()) {
			if (name() == "character") 
				readRPGCharacter<RPGRules>(this,cwd,ch,&rules);
			else raiseError(QObject::tr("Not a character file"));
		}
	}

	return error();
}