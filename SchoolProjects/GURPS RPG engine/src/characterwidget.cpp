#include <QtGui>

#include "characterwidget.h"
#include "rpgcampaign.h"

using namespace RPG;

/* class RPGDialog */
RPGDialog::RPGDialog( QWidget *parent) : QDialog(parent) 
{
	layout = new QVBoxLayout();
	text = new QLabel();
	text->setAlignment(Qt::AlignCenter);
	text->setWordWrap(true);
	layout->addWidget(text);
	this->setLayout(layout);
	sigmap = new QSignalMapper(this);
	connect(sigmap,SIGNAL(mapped(int)),this,SLOT(done(int)));
};

int RPGDialog::display(const QString &message, const QStringList &answers) 
{
	text->setText("<b>"+message+"</b>");
	int i = 1;
	
	foreach(QString ans,answers) {
		MyLabel *lab = new MyLabel("<hr>"+ans);
		lab->setAlignment(Qt::AlignCenter);
		lab->setWordWrap(true);
		sigmap->setMapping(lab,i);
		connect(lab,SIGNAL(clicked()),sigmap,SLOT(map()));
		layout->addWidget(lab);
		++i;			 
	}
	
	this->exec();
	int result = this->result();
	return result;
}

void RPGDialog::setText(const QString &message) 
{
	text->setText("<b>"+message+"</b>");
}

void RPGDialog::clear() 
{
	foreach(QPushButton *but, answers)
		delete but;
	answers.clear();
}

QPushButton *RPGDialog::addAnswer(const QString &msg) 
{
	int n = answers.size();
	QPushButton *but = new QPushButton(msg);
	sigmap->setMapping(but,n);
	connect(but,SIGNAL(clicked()),sigmap,SLOT(map()));
	layout->addWidget(but);
	answers.push_back(but);
	return but;
}

QPushButton *RPGDialog::clickedButton() 
{
	int n = this->result();
	if (n < answers.size())
		return answers[n];
	return NULL;
}

int RPGDialog::display() {
	this->exec();
	return this->result();
}

/* class CampaignInfo */
CampaignInfo::CampaignInfo(QWidget *parent) : QGroupBox(parent)
{
	player = NULL;
	establishedConn = false;
	setVisible(false);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
	this->setTitle("Campaign info");
	campName=new QLabel();
	updateCampaignName();
	mapName=new QLabel();
	updateMapInfo();
	turnNumber = new QLabel();
	updateTurnNumber();
	movePoints = new QLabel();
	updateMovePoints();
	QGridLayout *layout=new QGridLayout();
	layout->addWidget(campName,0,0);
	layout->addWidget(mapName,0,1);
	layout->addWidget(turnNumber,1,0);
	layout->addWidget(movePoints,1,1);
	setLayout(layout);
}

/*QSize CampaignInfo::sizeHint() const
{
	return QSize(CharacterWidgetWidth,100);
}
*/

void CampaignInfo::establishConnections()
{
	if (!establishedConn) {
		connect(campaign,SIGNAL(campaignNameChanged()),this,SLOT(updateCampaignName()));
		connect(campaign,SIGNAL(currentMapChanged()),this,SLOT(updateMapInfo()));
		connect(campaign,SIGNAL(turnChanged()),this,SLOT(updateTurnNumber()));
		establishedConn = true;
	}
}

void CampaignInfo::updateCampaignName() 
{
	if (player == NULL) return;
	campName->setText(tr("<b>Campaign:</b> %1").arg(campaign->getName()));
}

void CampaignInfo::updateMapInfo()
{
	if (player == NULL) return;
	mapName->setText(tr("<b>Map:</b> %1").arg(campaign->getCurrentMap()->getName()));
}

void CampaignInfo::updateTurnNumber()
{
	if (player == NULL) return;
	turnNumber->setText(tr("<b>Turn:</b> %1").arg(campaign->getTurn()));
}

void CampaignInfo::updateMovePoints()
{
	if (player == NULL) return;
	movePoints->setText(tr("<b>Move Pt:</b> %1/%2").arg(player->getMovePt()).arg(player->getTotalMovePt()));
}

void CampaignInfo::setPlayer(Player *pl) 
{
	this->player = pl;
	if (pl == NULL) {
		this->setVisible(false);
		return;
	}
	this->setVisible(true);
	establishConnections();
	updateCampaignName();
	updateMapInfo();
	updateTurnNumber();
	connect(player,SIGNAL(movePointsChanged()),this,SLOT(updateMovePoints()));
	updateMovePoints();
}

/* class RPGCharacterSheet */
RPGCharacterSheet::RPGCharacterSheet(QWidget *parent) : QWidget(parent)
{
	stats=new QTextEdit(this);
	stats->setReadOnly(true);
	stats->setFrameShape(QFrame::NoFrame);
	QVBoxLayout *layout=new QVBoxLayout(this);
	layout->addWidget(stats);
	this->setLayout(layout);
}

void RPGCharacterSheet::setText(QString &str)
{
	stats->setText(str);
}

/* class SkillLevelDialog */
SkillLevelDialog::SkillLevelDialog(QWidget *parent) : QDialog(parent)
{
	label = new QLabel("<b>Skill:</b>");
	skills = new QComboBox();
	info = new QLabel("Info");

	QGridLayout *toplayout = new QGridLayout();
	toplayout->addWidget(label,0,0);
	toplayout->addWidget(skills,0,1,1,2);
	toplayout->addWidget(info,1,0,1,3);

	okbutton=new QPushButton(tr("&OK"));
	okbutton->setDefault(true);
	connect(okbutton,SIGNAL(clicked()),this,SLOT(accept()));

	cancelbutton=new QPushButton(tr("&Cancel"));
	connect(cancelbutton,SIGNAL(clicked()),this,SLOT(reject()));

	QVBoxLayout *buttonslayout=new QVBoxLayout();
	buttonslayout->addWidget(okbutton);
	buttonslayout->addWidget(cancelbutton);
	buttonslayout->addStretch();

	QHBoxLayout *layout = new QHBoxLayout();
	layout->addLayout(toplayout);
	layout->addLayout(buttonslayout);

	setLayout(layout);
	
	setWindowTitle("Skill level");
}

void SkillLevelDialog::display(PlayerNode *pl) 
{
	this->pl = pl;
	if (pl == NULL) return;

	names = rules.getSkillsNames();
	disconnect(skills,SIGNAL(currentIndexChanged(int)),this,SLOT(updateInfo(int)));
	skills->clear();
	foreach(QString name, names) {
		skills->addItem(rules.getSkill(name)->name);
	}
	if (skills->count() == 0 )
		return;
	connect(skills,SIGNAL(currentIndexChanged(int)),this,SLOT(updateInfo(int)));
	updateInfo(0);
	exec();
	int res = this->result();

	if (res == Accepted) {
		Skill *skill = pl->getSkill(names[skills->currentIndex()]);
		int cost = skill->getNextLevelCost();
		skill->processNextLevel();
		pl->spendUnusedXP(cost);
	}
	else 
		return;
}

void SkillLevelDialog::updateInfo(int ind)
{
	// ziskame skill
	Skill *skill = pl->getSkill(names[ind]);
	if (skill == NULL) { 
		// ak ho postava nema tak ho pridame (ak existuje)
		skill = pl->addSkill(names[ind],NONE);
		if (skill == NULL)
			return;
	}

	int cost = skill->getNextLevelCost();

	info->setText(tr("<b>Level:</b>%1/%2(%3)<br><b>Cost:</b>%4").arg(skill->getFinalLevel(pl))
		.arg(skill->getLevel()).arg(skill->getBonus()).arg(cost));				

	if (cost <= pl->getUnusedXP()) {
		okbutton->setEnabled(true);
	}
	else {
		okbutton->setEnabled(false);
	}
}

/* class ItemDialog */
ItemDialog::ItemDialog(Inventory *inv, QWidget *parent) : QDialog(parent)
{
	this->inventory=inv;
	RPGBody=inventory->getBodyParts();

	name=new QLabel();
	name->setText(tr("name"));
	weight=new QLabel();
	weight->setText(tr("weight"));	
	minst= new QLabel();
	minst->setText(tr("minst"));
	
	label=new QLabel();
	label->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
	label->setAutoFillBackground(true);
	label->setFixedSize(sidesize,sidesize);
	
	stats=new QTextEdit(this);
	stats->setReadOnly(true);

	QLabel *loclabel=new QLabel();
	loclabel->setText(tr("<b>Location:</b>"));
	location=new QComboBox();
	
	QGridLayout *toplayout=new QGridLayout();
	toplayout->addWidget(label,0,0,3,1);
	toplayout->addWidget(name,0,1);
	toplayout->addWidget(weight,1,1);
	toplayout->addWidget(minst,2,1);
	toplayout->addWidget(loclabel,4,0);
	toplayout->addWidget(location,4,1);
	toplayout->addWidget(stats,3,0,1,2);
	
	okbutton=new QPushButton(tr("&OK"));
	okbutton->setDefault(true);
	connect(okbutton,SIGNAL(clicked()),this,SLOT(accept()));

	cancelbutton=new QPushButton(tr("&Cancel"));
	connect(cancelbutton,SIGNAL(clicked()),this,SLOT(reject()));

	dropbutton = new QPushButton(tr("&Drop"));
	connect(dropbutton,SIGNAL(clicked()),this,SLOT(dropItem()));

	usebutton = new QPushButton(tr("&Use"));
	connect(usebutton,SIGNAL(clicked()),this,SLOT(useItem()));

	QVBoxLayout *buttonslayout=new QVBoxLayout();
	buttonslayout->addWidget(okbutton);
	buttonslayout->addWidget(cancelbutton);
	buttonslayout->addStretch();
	buttonslayout->addWidget(usebutton);
	buttonslayout->addWidget(dropbutton);

	QHBoxLayout *layout = new QHBoxLayout();
	layout->addLayout(toplayout);
	layout->addLayout(buttonslayout);
		
	setLayout(layout);

	setWindowTitle("Item");
}

void ItemDialog::display(const QModelIndex &index)
{
	if (inventory == NULL) return;
	BodyLocation *tmploc;
	InventoryObject *obj=inventory->getItem(index.row());
	name->setText(tr("<b>Name:</b> %1").arg(obj->getName()));
	weight->setText(tr("<b>Weight:</b> %1").arg(obj->getWeight()));
	minst->setText(tr("<b>Minimum ST:</b> %1").arg(obj->getMinimumST()));
	label->setPixmap(obj->getPixmap());

	stats->setText(obj->getInfo());

	location->clear();
	location->addItem(QString("Inventory"));
	foreach(QString loc,obj->getEquipLocations()) {
		location->addItem(RPGBody[loc].name);
	}
	location->setCurrentIndex(location->findText(RPGBody[obj->getBodyLocation()].name,Qt::MatchContains));
	
	int uses = obj->getUses();
	if (uses > 0 || uses == -1) {
		if (uses == -1) 
			usebutton->setText(tr("&Use"));
		else 
			usebutton->setText(tr("&Use (%1)").arg(uses));
		usebutton->setVisible(true);
	}
	else 
		usebutton->setVisible(false);

	this->exec();
	int result = this->result();
	if (result == Accepted) {
		//nastavime lokaciu
		QString tmp=this->location->currentText();
		if (tmp=="Inventory") {
			inventory->setItemBodyLocation(index.row(),QString("inventory"));
			return;
		}
		foreach(BodyLocation RPGBodyloc, RPGBody) {
			if (RPGBodyloc.name==tmp) {
				inventory->setItemBodyLocation(index.row(),RPGBodyloc.getID());
				return;
			}
		}
	}
	else if (result == DropItem) {
		InventoryObject *obj = inventory->removeItem(index.row());
		int x = campaign->getPlayer()->getX();
		int y = campaign->getPlayer()->getY();
		MapNode *node = campaign->getCurrentMap()->getNode(x, y);
		if (node != NULL)
			node->addObject(obj);
	}
	else if (result == UseItem) {
		InventoryObject *obj = inventory->getItem(index.row());
		obj->use();
	}
}

void ItemDialog::dropItem()
{
	done(DropItem);
	
}

void ItemDialog::useItem()
{
	done(UseItem);
}

/*class Inventory*/
InventoryWidget::InventoryWidget(Inventory *model, QWidget *parent) : QTableView(parent)
{
	this->model=model;

	setFixedWidth(CharacterWidgetWidth);
	setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	//nastavenie widgetu pre zobrazenie tabulky
	verticalHeader()->hide();		//vertical header nie je implementovany -> bez tohoto to pada
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	viewport()->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

	//priradime mu model
	setModel(model);
	//priradime hlavicku
	QHeaderView *header=new QHeaderView(Qt::Horizontal,this);
	header->setModel(model);
	setHorizontalHeader(header);
	horizontalHeader()->show();

	//nastavime rozmery
	int side=viewport()->width() - sidesize - 10;
	setColumnWidth(0,sidesize+10);
	setColumnWidth(1,(int)5*side/ 10);
	setColumnWidth(2,(int)3*side/ 10);
	setColumnWidth(3,(int)2*side/ 10);
	setSelectionBehavior(QAbstractItemView::SelectRows);
	
	//ak sa prida novy riadok nastavia sa jeho rozmery
	connect(model, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
		    this, SLOT(updateRowHeight(const QModelIndex &, int, int)));
	updateRowHeight(QModelIndex(),0,0);	

	//vytvorime dialog sluziaci na editaciu umiestnenia predmetov + na zobrazenie informacii o nich
	itemdialog=new ItemDialog(model,this);
	connect(this,SIGNAL(activated(const QModelIndex &)),itemdialog,SLOT(display(const QModelIndex &)));
}

void InventoryWidget::updateRowHeight(const QModelIndex& parent, int start, int end)
{
	for(int i=start;i!=end+1;++i)
		setRowHeight(i,sidesize+10);
}

InventoryWidget::~InventoryWidget()
{
}


/* class CharacterWidget */
CharacterWidget::CharacterWidget(QWidget *parent) : QWidget(parent)
{
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	setMinimumSize(CharacterWidgetWidth, CharacterWidgetHeight);
	setFixedWidth(CharacterWidgetWidth);
	sheet=NULL;
	inventory=NULL;
	campInfo = NULL;
	tabs = NULL;
	tabs = new QTabWidget(this);
	tabs->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	campInfo = new CampaignInfo(this);
	QVBoxLayout *layout = new QVBoxLayout();
	layout->addWidget(campInfo);
	layout->addWidget(tabs);
	this->setLayout(layout);
}

void CharacterWidget::setPlayer(Player *player)
{
	clear();
	this->player=player;
	campInfo->setPlayer(player);
	sheet=new RPGCharacterSheet(this);
	inventory=new InventoryWidget(player->getInventory(),this);
	tabs->addTab(sheet,"Stats");
	tabs->addTab(inventory,"Inventory");
	connect(player,SIGNAL(changed()),this,SLOT(update()));
	update();
}

void CharacterWidget::clear()
{
	if (sheet!=NULL) {
		delete sheet;
		sheet=NULL;
	}
	if (inventory!=NULL) {
		delete inventory;
		inventory=NULL;
	}
	if (campInfo!=NULL) {
		campInfo->setPlayer(NULL);
	}
	tabs->clear();
}

void CharacterWidget::update()
{
	sheet->setText(player->getInfo());
}