/******************************************************************************
 * V tombo subore su nadefinovane triedy hraca a 
******************************************************************************/
#ifndef RPGCharacter_H
#define RPGCharacter_H

#include <QObject>
#include <QMap>
#include <QVector>

#include "rpgobject.h"
#include "inventoryobject.h"

namespace RPG {

	// dopredne deklaracie
	class Die;
	class Character;
	class Inventory;
	class InventoryObject;
	class Weapon;
	class Body;
	class Map;
	class Skill;
	class BodyLocation;
	class Trigger;
	class Campaign;

	/** Trieda reprezentujuca lokaciu na tele postavy */
	class BodyLocation
	{
	public:
		BodyLocation();

		/** Nastavenie objektu, kt. je equipnuty na lokacii */
		void setObject(InventoryObject *object);
		/** Ziska equipnuty objekt */
		InventoryObject *getObject();
		/** Ziska identifikator lokacie */
		const QString &getID() const { return id;}
		/** Nastavi identifikator lokacie */
		void setID(const QString& str) { id=str.toLower();}
		/** Nastavi ktore lokacie tento objekt blokuje na equipnutie */
		void setBlockLocations(const QStringList &block);
		/** Ziska blokovane lokacie */
		const QStringList &getBlockLocations() const { return blocking; }
		/** Zisti ci je lokacia blokovana inym objektom */
		bool isBlocked() const { return blockLocation!=NULL; }
		/** Vrati objekt ktory blokuje tuto lokaciu */
		BodyLocation *getBlockingLocation() { return blockLocation; }
		/** Nastavi objekt blokujuci tuto lokaciu */
		void setBlockingLocation(BodyLocation *loc) { blockLocation = loc; }

		/** Zisti ci je mozne s lokaciou utocit */
		bool isAttackable() const { return canAttack; }
		/** Nastavi ci je mozne s lokaciou utocit */
		void setAttackable(bool value) { canAttack = value; }
		
		// zobrazovany nazov lokacie 
		QString name;

		// not implemented - TODO: bonusy na utocenie touto lokaciou a na tuto lokaciu
		void setAttackMod(int mod) { attackMod = mod; }
		int getAttackMod() const { return attackMod; } 
		void setHitMod(int mod) { hitMod = mod; }
		int getHitMod() const { return hitMod; }
		void setDmgMod(int mod) { dmgMod = mod; }
		int getDmgMod() const { return dmgMod; }

	protected:
		// identifikator lokacie
		QString id;
		// objekt ktory sa nachadza na lokacii
		InventoryObject *object;
		// ktore lokacie blokuje tato lokacia
		QStringList blocking;			
		// ktora lokacia blokuje tuto lokaciu
		BodyLocation *blockLocation;	
		// atribut ci moze lokacia utocit
		bool canAttack;

		// not implemented
		int attackMod;
		int hitMod;
		int dmgMod;
	};

	/** Trieda reprezentujuca telo postavy */
	class Body 
	{
	public:
		Body();

		/** Prida lokaciu na tele */
		void addRPGBodyPart(BodyLocation loc);
		/** Ziska lokacie na tele - asoc. pole (id;lokacia) */
		const QMap<QString,BodyLocation>& getBodyParts() const;
		/** Ziska identifikatory lokacii, ktore mozu utocit */
		QStringList getAttackableParts() const;
		/** Ziska celkove cislo brnenia tela */
		int getDamageResistance() const;
		/** Ziska identifikator tela */
		const QString &getID() const { return id;}
		/** Nastavi identifikator tela */
		void setID(const QString& id) { this->id=id.toLower(); }
		/** Vrati lokaciu s identifikatorom loc */
		BodyLocation *getLocation(const QString &loc);
		/** Nastavi aktivnu lokaciu na utocenie */
		void setActiveWeaponLoc(const QString &loc);
		/** Vrati identifikator aktivnej lokacie na utocenie */
		const QString &getActiveWeaponLoc() { return activeWeaponLoc;}
		/** Prida na lokaciu location objekt object */		
		bool equipObject(const QString &location, InventoryObject *object);

		// Nazov tela (rasy) - rasy nie su implementovane
		QString name;

	protected:
		// identifikator tela
		QString id;
		// lokacie tela
		QMap<QString,BodyLocation> body;
		// identifikator aktivnej utociacej lokacie
		QString activeWeaponLoc;
	};

	/** Trieda zakladnej hernej postavy - hrac, prisery, NPC */
	class Character : public RPGObject
	{
		Q_OBJECT
	public:
		Character();
		Character(const Character&);
		Character &operator=(const Character&);
		~Character();

		/** Pomocna funkcia - prida zakladne definicie, nastavi niektore parametre triedy */
		void addBasicRules();
		/** Ziska informacie o postave */
		virtual QString getInfo() const { return info;}
		/** Nastavi meno postavy */
		void setName(QString name) { this->name=name;}				
		/** Nastavi rasu postavy */
		void setRace(const QString &race);
		/** Ziska telo postavy */
		Body *getBody() { return &body; }
		/** Prida dovednost s identifikatorom id na urovni level */
		Skill *addSkill(const QString &id, int level);
		/** ziska dovednost s identifikatorom id */
		Skill *getSkill(const QString &id) const;

		/**	Vrati hodnotu sily aj s modifikatorom */
		inline int getST() const { return ST+STbonus; }
		/** Vrati hodnotu obratnosti aj s modifikatorom */
		inline int getDX() const { return DX+DXbonus; }
		/** Vrati hodnotu inteligencie aj s modifikatorom */
		inline int getIQ() const { return IQ+IQbonus; }
		/** Vrati hodnotu zdravia aj s modifikatorom */
		inline int getHT() const { return HT+HTbonus; }
		/** Vrati hodnotu bodov zdravia aj s modifikatorom */
		inline int getHP() const { return HP+HPbonus; }
		/** Vrati celkove xp */
		inline int getTotalXP() const { return totalXP; }
		/** Vrati nepouzite xp */
		inline int getUnusedXP() const { return unusedXP; }
		/** Vrati aktualne body zdravia */
		inline int getcHP() const { return cHP; }
		/** Nepouzivane - todo fog of war - ziska vzdialenost viditelnosti postavy */
		inline int getScoutingRadius() const { return scoutRadius; }
		/** Vrati velkost postavy */
		inline double getSize() const { return size;}
		/** Vrati BSpeed s modifikatorom */
		double getBasicSpeed() const;
		/** Vrati BMove s modifikatorom */
		int getBasicMove() const;
		/** Vrati kocku zranenia bodnutim */
		inline Die getDmgThr() const { return dmgThr; }
		/** Vrati kocku zranenia machnutim */
		inline Die getDmgSw() const { return dmgSw; }
		/** Vrati bonus atributu v parametri */
		int getBonus(Attribute attr);
		/** Vrati bonus atributu s retazcom str - ostatne atributy */
		int getBonus(const QString &str);
		/** Vrati cislo brnenia postavy */
		inline int getDR() const;

		/** Vrati pointer na inventar postavy */
		Inventory* getInventory() { return inventory; }
		/** Prida do inventara predmet obj */
		bool addInventoryObject(InventoryObject *obj);

		/** Vrati aktualne body pohybu */
		inline int getMovePt() { return movePt; }
		/** Vrati celkove body pohybu na tah */
		int getTotalMovePt() const;
		/** Pouzije body pohybu */
		void spendMovePt(int pt);
		/** Pouzije body pohybu na manever */
		void spendMovePt(CombatManeuver man);
		/** Obnovi body pohybu */
		void replenishMovePt();

		/** Zistenie ci je postava v boji */
		inline bool isInCombat() const { return inCombat; }
		/** Nastavi ci je postava v boji */
		inline void setInCombat(bool val) { inCombat = val; }
		
		/** Zisti ci je postava nazive */
		inline bool isAlive() const { return state != DEAD; }

		/** Vrati aktivnu zbran postavy */
		Weapon *getActiveWeapon();
		/** Vrati hodnotu obrany postavy */
		int getDefenseDodge();

		friend class Inventory;

	public slots:
		/** Nastavi silu */
		void setST(int ST);
		/** Nastavi obratnost */
		void setDX(int DX);
		/** Nastavi inteligenciu */
		void setIQ(int IQ);
		/** Nastavi zdravie */
		void setHT(int HT);
		/** Nastavi body zdravia */
		void setHP(int HP);
		/** Nastavi velkost */
		void setSize(double sz);
		/** Ziska modifikator velkosti v boji */
		int getSizeMod();
		/** Nastavi aktualne body zdravia */
		void setcHP(int cHP);
		/** Nastavi aktualne body pohybu */
		void setMovePt(int movept);
		/** Nastavi celkove xp */
		void setTotalXP(int xp);
		/** Nastavi nepouzite xp */
		void setUnusedXP(int xp);
		/** Postava ziska xp */
		void earnXP(int xp);
		/** Pouzitie nepouzitych bodov xp */
		void spendUnusedXP(int xp);
		/** Nastavi bonus atributu */
		void setBonus(Attribute attr, int bonus); 
		/** Nastavi bonus atributu - retazec (ostatne atributy) */
		void setBonus(const QString &attr, int bonus);
		/** Sposobi postave zranenie dmg */
		void inflictDmg(int dmg);
		/** Nastavi stav postavy (zdrava, umierajuca ... ) */
		void setState(CharacterState state);
		/** Update sekundarnych statov */
		void updateSecStats();
		/** Update informacii o postave */
		void updateStatsInfo();
		/** Emitnutie signalu o zmene informacii postavy */
		void emitStatsInfoChanged() {
			updateStatsInfo();
			emit statsInfoChanged();
		}
		/** Spracovanie udalosti tahu */
		void processTurnEvents();		
		/** Spracovanie udalosti zdravia */
		void processHealth();

	signals:
		/** Emitnuty pri zmene statov */
		void statsChanged();
		/** Emitnuty pri zmene retazca info */
		void statsInfoChanged();
		/** Emitnuty pri zmene poctu movePt */
		void movePointsChanged();
		/** Emitnuty pri resetnuti poctu movePt (koniec tahu) */
		void movePointsReset();

	protected:
		/** VYmazanie dat */
		void clear();

		// telo
		Body body;
		// informacie o postave
		QString info;
		/* primary stats */
		int ST;				// Strenght
		int DX;				// Dexterity
		int IQ;				// Inteligence
		int HT;				// Health
		/* secondary stats */
		int HP;				// Health points
		int cHP;			// current HP
		double BasicSpeed;
		int scoutRadius;
		/* bonusy */
		int STbonus;
		int DXbonus;
		int IQbonus;
		int HTbonus;
		int HPbonus;
		double BasicSpeedBonus;
		int BasicMoveBonus;
		// nosnost postavy
		double encumbrance;
		// velkost postavy
		double size;
		/* zakladny damage */
		Die dmgThr;
		Die dmgSw;
		// dovednosti 
		QMap<QString,Skill*> skills;
		typedef QMap<QString,Skill*>::iterator skills_iterator;
		// inventar
		Inventory *inventory;
		// zakladna zbran pre boj bez zbrane
		Weapon *basicWeapon;
		// body pohybu
		int movePt;
		// total xp points
		int totalXP;
		// unused xp points
		int unusedXP;
		// zistenie ci je postava v suboji
		bool inCombat;
		// zdravotny stav postavy
		CharacterState state;
		// vysledok minuleho hodu na umrtie (pod 0 zivotov.. ) */
		int lastDeadThrow;
	};

	/** Trieda NPC postavy alebo prisery */
	class Monster : public Character
	{
		Q_OBJECT
	public:
		Monster();
		Monster(const Monster&);
		Monster &operator=(const Monster&);
		~Monster();
		virtual void release();

		/** Vrati nepriatela na ktoreho zautocit */
		Character *decideEnemyToAttack();
		/** Vrati manever, ktory chce postava vykonat */
		CombatManeuver decideCombatManeuver();
		
		/** Vymaze objekt, kt je umiestneny na tomto predmete (vymazanie triggerov) */
		void removeEmbeddedObject(RPGObject *obj);

		/** Nastavi onTalk triggeru */
		void setOnTalkTrigger(Trigger *trig);
		/** Nastavi combatManeuver trigger */
		void setCombatManeuverTrigger(Trigger *trig);
		/** Nastavi targetEnemy trigger */
		void setTargetEnemyTrigger(Trigger *trig);
		/** Nastavi onKill trigger */
		void setOnKillTrigger(Trigger *trig);
		
		/** Zisti ci je mozne k postave rozpravat */
		bool isTalkable();
		
		/** Pohyb na (tox;toy) (herne suradnice) */
		void moveTo(int tox, int toy);
		/** NPC postava prehovori k postave ch */
		void talkTo(Character *ch);
		/** Vykona onKill trigger na postave ch */
		void killedBy(Character *ch);
		/** Nastavi pocet xp, ktore nepriatel dostane za zabitie tejto postavy */
		void setXPGain(int xp) { xpgain = xp; }
		/** Ziska pocet xp */ 
		int getXPGain() const { return xpgain; }

		/** Vrati minuly vybraty manever v suboji */
		CombatManeuver getLastManeuver() const { return lastman; }

	private:
		// trigger, ktory sa vykona pri rozpravani k postave
		Trigger *onTalk;
		// trigger, ktory sa vykona pri vybere manevra v suboji
		Trigger *combatManeuver;
		// trigger, ktory sa vykona pri vybere nepriatela na ktoreho utocit v suboji
		Trigger *targetEnemy;
		// trigger, ktory sa vykona pri umrti postavy
		Trigger *onKill;
		// pocet xp, kt. nepriatel dostane za zabitie tejto postavy
		int xpgain;
		// posledny vybraty ciel na zautocenie
		Character *target;
		// posledne vybraty manever
		CombatManeuver lastman;
	};

	/** Trieda reprezentujuca hraca */
	class Player : public Character
	{
		Q_OBJECT
	public:
		Player();
		~Player();
	};
}

#endif
