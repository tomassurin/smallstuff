/******************************************************************************
 * Implementacia inventarnych objektov a objektu inventara
******************************************************************************/
#ifndef INVENTORYOBJECT_H
#define INVENTORYOBJECT_H

#include <QAbstractTableModel>
#include <QModelIndex>

#include "rpgobject.h"

namespace RPG {

	// dopredne deklaracie
	class Die;
	class Character;
	class Inventory;
	class InventoryObject;
	class Weapon;
	class Body;
	class Map;
	class BodyLocation;
	class Trigger;
	class Campaign;

	/** Trieda zakladneho predmetu. Reprezentuje predmet, kt. vsetky interakcie s okolim su riadene udalostami. */
	class InventoryObject : public RPGObject
	{
	public:
		InventoryObject();
		InventoryObject(const InventoryObject& obj);
		InventoryObject& operator=(const InventoryObject& obj);
		~InventoryObject();

		virtual void release();
		/** Vymaze data objektu */
		void clear();

		/** Prida lokaciu na tele postavy kde moze byt predmet noseny */
		void addEquipLocation(const QString &loc);
		/** Ziska zoznam povolenych lokacii na tele postavy */
		QStringList getEquipLocations() const;

		/** Vyziada si od predmetu jeho umiestnenie na lokaciu danu retazcom v parametri */
		void equip(const QString &location);
		/** Ziska aktualnu lokaciu predmetu */
		const QString& getBodyLocation() const;

		/** Ziska wahu predmetu */
		double getWeight() const { return weight; }
		/** Nastavi vahu predmetu */
		void setWeight(double w) { weight=w; }

		/** Nastavi hodnotu minimalnej sily pre pouzivanie predmetu */
		void setMinimumST(int min) { minimumST = min;}
		/** Ziska hodnotu minimalnej sily */
		int getMinimumST() const { return minimumST;}

		/** Nastavi vlastnika predmetu */
		void setOwner(Character *character);	
		/** Ziska vlastnika predmetu */
		Character *getOwner();

		/** Vymaze objekt, kt je umiestneny na tomto predmete (jedna sa o triggery) */
		void removeEmbeddedObject(RPGObject *obj);
		/** Nastavi onEquip trigger */
		void setEquipTrigger(Trigger *trig);
		/** Nastavi onUnEquip trigger */
		void setUnEquipTrigger(Trigger *trig);
		/** Nastavi onInventory trigger */
		void setInventoryTrigger(Trigger *trig);
		/** Nastavi onUnInventory trigger */
		void setUnInventoryTrigger(Trigger *trig);
		/** Nastavi onUse trigger */
		void setUseTrigger(Trigger *trig);

		/** Pouzije predmet */
		void use();
		/** Nastavi pocet pouziti predmetu, -1 je nekonecne vela */
		void setUses(int u) { uses = u; }
		/** Ziska pocet pouziti predmetu */
		int getUses() { return uses; }

		friend class Inventory;
		friend class Body;

	protected:
		// kde sa predmet nachadza na tele postavy
		QString bodyLocation;	
		// vaha predmetu
		double weight;
		// kde na tele postavy mozme predmet nosit
		QStringList equipLoc;		
		// vlastnik predmetu
		Character *owner;

		// trigger sa aktivuje pri equipe objektu	(setLocation)
		Trigger *onEquip;		
		// trigger sa aktivuje pri vrateni naspat z tela do inventara (setLocation)
		Trigger *onUnEquip;		
		// trigger sa aktivuje pri pridani objektu do inventara
		Trigger *onInventory;	
		// trigger sa aktivuje pri odobrati objektu z inventara
		Trigger *onUnInventory;
		// trigger sa aktivuje pri pouziti objektu
		Trigger *onUse;

		// pocet pouziti predmetu
		int uses;
		// minimalna sila pre pouzivanie predmetu
		int minimumST;
	};

	/** Trieda inventara */
	class Inventory : public QAbstractTableModel
	{
	public:
		Inventory();
		Inventory(const Inventory& obj);
		Inventory& operator=(const Inventory& obj);
		~Inventory();

		/** Prida objekt object do inventara a nastavi jeho lokaciu na loc */
		void addItem(InventoryObject *object, const QString &loc = "inventory");
		/** Ziska objekt na indexe index */
		InventoryObject *getItem(int index);
		/** Ziska objekt s identifikatorom id */
		InventoryObject *getItem(const QString &id);
		/** Vymaze objekt na indexe index */
		InventoryObject* removeItem(int index);

		/** Nastavi vlastnika inventara na ch*/
		void setOwner(Character *ch);
		/** Vrati vlastnika inventara */
		Character *getOwner();

		/** Nastavi lokaciu predmetu na indexe index na loc
		  * Specialne loc=inventory ak naspat do inventory */
		void setItemBodyLocation(int index, const QString &loc);

		/** Vrati vsetky lokacie tela postavy */
		const QMap<QString,BodyLocation>& getBodyParts() const;
		
		/** Vrati totalnu vahu inventara */
		double getInventoryWeight() { return totalweight; }

		/** Funkcie pre potreby model-view architektury */
		/** Vrati pocet riadkov inventara */
		int rowCount ( const QModelIndex &parent=QModelIndex()) const;
		/** Vrati pocet stlpcov inventara */
		int columnCount(const QModelIndex &parent=QModelIndex()) const;
		/** Vrati data ktore sa zobrazia v hlavicke tabulky inventara */
		QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
		/** Vrati data na indexe index a s rolou role */		
		QVariant data ( const QModelIndex &index, int role=Qt::DisplayRole) const;  
	
		friend class Character;

	protected:
		// predmety v inventari
		QVector<InventoryObject*> items;
		// uplna vaha inventara
		qreal totalweight;
		// vlastnik inventara
		Character *owner;
	};

/*items*/
	/* Trieda zbrane na boj zblizka */
	class Weapon : public InventoryObject
	{
	public:
		Weapon();
		Weapon(const Weapon& obj);
		Weapon& operator=(const Weapon& obj);

		/** Prida specifikaciu dovednosti zbrane */
		void addSkill(QString& skill);
		/** Ziska dovednosti zbrane */
		const QString& getSkillInfo() const;	
		/** Vrati efektivnu dovednost, ktoru ma dana postava ch s touto zbranou */
		Skill *getEffectiveSkill(const Character *ch) const;
		/** Vrati efektivu dovendost vlastnika zbrane */
		Skill *getEffectiveSkill() const;
		/** Nastavi zranenie zbrane */
		void setDamage(QString &dmg);
		/** Ziska zranenie zbrane */
		const QString& getDamageString() const;
		/** Ziska informacie o zbrani */
		virtual QString getInfo() const;
		/** Vrati kocku ktora moze byt pouzita v damage roll s touto zbranou hraca ch */
		Die getDamageDie(Character *ch) const;
		/** Vrati kocku, ktora moze byt pouzita v dmg roll s touto zbranou vlastnika */
		Die getDamageDie() const;

	protected:
		// zranenie zbranou
		Damage damage;

		struct SkillInfo {
			QString id;
			int mod;
		};

		// specifikacia dovednosti zbrane
		QString skillInfo;
		// dovednosti zbrane
		QVector<SkillInfo> skills;
	};

	/** Trieda brnenia */
	class Armor : public InventoryObject
	{
	public:
		Armor();
		Armor(const Armor& obj);
		Armor& operator=(const Armor& obj);

		/** Nastavi cislo brnenia */
		void setDR(int dr) { this->dr = dr;}
		/** Ziska cislo brnenia */
		int getDR() const { return dr;}
		/** Ziska informacie o brneni */
		QString getInfo() const;

	private:
		// cislo brnenia
		int dr;
	};
}

#endif