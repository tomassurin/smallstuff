/******************************************************************************
 * Hlavne okno aplikacie sluzi pre vytvorenie GUI
******************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "rpgcampaign.h"
#include "GeneratedFiles/ui_NewCharacterDialog.h"

class RPG::PlayerNode;

/** Trieda dialoga pre vytvorenie novej postavy */
class NewCharacterDialog : public QDialog, private Ui::NewCharacterDialog
{
	Q_OBJECT
public:
	NewCharacterDialog(QWidget *parent = 0);
	/** Vratenie vyslednej postavy, kt. vlastnosti boli nastavene v dialogu */
	RPG::PlayerNode *getCharacter();
};

/** Trieda hlavneho okna aplikacie */
class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = 0, Qt::WFlags flags = 0);

public slots:
	// koniec hraca
	void PlayerEnded();
	// otvorenie suboru kampane
	void openCampaign();
	// otvorenie suboru hraca
	void openCharacter();
	// vytvorenie novej postavy hraca
	void newCharacter();
	// zavretie kampane
	void close();

private:
	/** Funkcie pre vytvorenie GUI, menu a prikazov v menu */
	void createMenus();
	void createActions();
	void createWidgets();
	
	/** Definicie widgetov */
	QAction *openCampaignAct;
	QAction *openCharacterAct;
	QAction *exitAct;
	QAction *aboutAct;
	QAction *closeAct;
	QAction *newCharacterAct;
	QMenu *fileMenu;
	QMenu *characterMenu;

//	RPG::Campaign *campaign;
};

#endif