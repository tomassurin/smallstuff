#include "triggerevent.h"
#include "characterwidget.h"

#include <QStringList>
#include <QRegExp>
#include <QStack>
#include <QPair>
#include <QtCore/QMetaObject>
#include <QInputDialog>
#include <QSet>
#include <typeinfo>
#include <qwaitcondition.h>

using namespace RPG;

/* class Value */
Value::Value()
{
	clear();
}

Value::Value(const Value &obj)
{
	operator=(obj);
}

Value& Value::operator=(const Value &obj)
{
	clear();
	type = obj.type;
	QList<QString> keys;
	switch (type) {
		case BOOL:
			logical = obj.logical;
			break;
		case INTEGER:
			integer = obj.integer;
			break;
		case FLOATING:
			floating = obj.floating;
			break;
		case VALUE_ERROR:
		case STRING:
			str = obj.str;
			break;
		case RPGOBJECT:
			this->obj = obj.obj;
			break;
		default: 
			break;
	}

	return *this;
}

Value::~Value()
{
	clear();
}

inline void Value::clear()
{
	type = VOID;
}

Value::Value(int val)
{
	setInt(val);
}

Value::Value(double val)
{
	setDouble(val);
}

Value::Value(const QString &val)
{
	setString(val);
}

Value::Value(bool val)
{
	setBool(val);
}

Value::Value(RPGObject *obj)
{
	setObject(obj);
}

void Value::setBool(bool val)
{
	clear();
	this->logical = val;
	this->type = BOOL;
}

void Value::setInt(int val)
{
	clear();
	this->integer = val;
	this->type = INTEGER;
}

void Value::setDouble(double val)
{
	clear();
	this->floating = val;
	this->type = FLOATING;
}

void Value::setString(const QString &val)
{
	clear();
	this->str = val;
	this->type = STRING;
}

void Value::setObject(RPGObject *obj)
{
	clear();
	this->obj = obj;
	this->type = RPGOBJECT;
}

bool Value::getBool()
{
	return (bool) this;
}

int Value::getInt()
{
	return (int) *this;
}

double Value::getDouble()
{
	return (double) *this;
}

QString Value::getString()
{
	return (QString) *this;
}

RPGObject *Value::getObject()
{
	if (type == RPGOBJECT)
		return obj;
	else 
		return NULL;
}

Value Value::createError(const QString &err)
{
	Value val;
	val.type = VALUE_ERROR;
	val.str = err;
	return val;
}

Value Value::createError(int error) {
	Value val;
	val.type = VALUE_ERROR;
	switch (error) {
		case FUNCT:
			val.str = "function error";
			break;
		case FUNCT_ARG_NUM:
			val.str = "function error - bad number of arguments";
			break;
		case FUNCT_ARG_TYPE:
			val.str = "function error - bad arguments type";
			break;
		case FUNCT_NUM:
			val.str = "function error - bad number of parameters";
			break;
		case FUNCT_ARG:
			val.str = "function error = bad argumets";
			break;
		default: 
			val.str = "error no "+error;
	}
	return val;
}

void Value::error(const QString &err)
{
	this->type = VALUE_ERROR;
	this->str = err;
}

Value Value::createVoid() 
{
	Value val;
	val.type = VOID;
	return val;
}

Value::operator int () 
{
	switch (type) {
		case FLOATING:
			return (int) floating;
		case BOOL:
			return (int)logical;
		case INTEGER:
			return integer;
		case STRING:
			return str.toInt();
		default:
			return 0;
	}
}

Value::operator bool () 
{ 
	switch (type) {
		case BOOL:
			return logical;
		case INTEGER:
			return (bool) integer;
		case FLOATING:
			return (bool) floating;
		case STRING:
			return str=="true"?true:false;
		case VOID:
			return false;
		default:
			return false;
	}
}

Value::operator double () 
{
	switch (type) {
		case FLOATING:
			return floating;
		case BOOL:
			return (double) logical;
		case INTEGER:
			return (double) integer;
		case STRING:
			return str.toDouble();
		default:
			return 0.0;
	}
}

Value::operator QString () 
{ 
	switch (type) {
		case FLOATING:
			return QString("%1").arg(floating);
		case BOOL:
			return logical==false?QString("false"):QString("true");
		case INTEGER:
			return QString("%1").arg(integer);
		case STRING:
			return str;
		default:
			return QString();
	}
}

QString Value::getError() 
{
	if (type == VALUE_ERROR) 
		return str;
	else 
		return QString();
}

//metoda convert konsoliduje typy svojich parametrov na odpovedajuce vracia spolocny typ
ValueType Value::convert(Value &a, Value &b)
{
	switch (a.type) {
		case BOOL:
			if (b.type == BOOL) 
				return BOOL;
			else if (b.type == INTEGER) {
				b = Value((bool)b.integer);
				return BOOL;
			}
			else if (b.type == FLOATING) {
				a = Value((double)a.integer);
				return FLOATING;
			}
			else if (b.type == STRING) {
				a = Value(a.logical==false?QString("false"):QString("true"));
				return STRING;
			}
			break;
		case INTEGER:
			if (b.type == BOOL) {
				a = Value((bool)a.integer);
				return BOOL;
			}
			else if (b.type == INTEGER) 
				return INTEGER;
			else if (b.type == FLOATING) {
				a = Value((double)a.integer);
				return FLOATING;
			}
			else if (b.type == STRING) {
				a = Value(QString("%1").arg(a.integer));
				return STRING;
			}
			break;
		case FLOATING:
			if (b.type == BOOL) {
				b = Value((double)a.logical);
				return FLOATING;
			}
			else if (b.type == INTEGER) {
				b = Value((double)b.integer);
				return FLOATING;
			}
			else if (b.type == FLOATING) 
				return FLOATING;
			else if (b.type == STRING) {
				a = Value(QString("%1").arg(a.floating));
				return STRING;
			}
			break;
		case STRING:
			if (b.type == BOOL) {
				b = Value(b.logical==false?QString("false"):QString("true"));
				return STRING;
			}
			else if (b.type == INTEGER) {
				b = Value(QString("%1").arg(b.integer));
				return STRING;
			}
			else if (b.type == FLOATING) {
				b = Value(QString("%1").arg(b.floating));
				return STRING;
			}
			else if (b.type == STRING) 
				return STRING;
			break;
		default:
			break;
	}
	return VALUE_ERROR;
}

/* class EvaluateFunction */
FunctionEval::FunctionEval(Trigger *trig)
{
	this->trig = trig;
	lastmos = 0;
}

FunctionEval::~FunctionEval()
{
	clearVariables();
}

void FunctionEval::setVariable(const QString &name, Value *val)
{
	if (val == NULL) return;
	if (variables.contains(name)) {
		Value *tmp = variables[name];
		if (tmp != NULL)
			delete tmp;
	}
	variables[name] = val;
}

Value *FunctionEval::getVariable(const QString &name)
{
	if (name == "this") {
		setVariable("this",new Value(trig));
	}
	else if (name == "player") {
		setVariable("player",new Value(campaign->getPlayer()));
	}
	else if (trig->getType() == MAPTRIGGER) {
		if (name == "thismonster")		
			setVariable("thismonster", new Value(trig->getMonster()));
	}
	else if (trig->getType() == ITEMTRIGGER) {
		if (name == "thisitem") 
			setVariable("thisitem", new Value(trig->getItem()));
		else if (name == "owner")
			setVariable("owner", new Value(trig->getItem()->getOwner()));
	}
	else if (name == "object") 
		setVariable("object", new Value(trig->getObject()));
	
	return variables[name];
}

void FunctionEval::setVariables(const TriggerVariables &vars)
{
	variables = vars;
}
const TriggerVariables &FunctionEval::getVariables()
{
	return variables;
}

void FunctionEval::clearVariables() 
{
	foreach(Value *val, variables) {
		delete val;
	}
	variables.clear();
}

//metoda na ciselne vyhodnotenie funkcii
Value FunctionEval::eval(FunctionDefinition &funct)
{
	//pomocne premenne
	int x, y;
	int tmp;
	QVector<Value> args;
	int n = funct.size();

	if (n<1) 
		return Value::createError(FUNCT_NUM);		   

	// konstanty pre logicke hodnoty a posledny margin of success
	if (funct[0].name == "true")	
		return true;
	else if (funct[0].name == "false")
		return false;
	else if (funct[0].name == "lastmos")
		return lastmos;	
	else if (funct[0].name == "null")
		return NULL;

//funkcie pre pristup k hernym objektom
	if (funct[0].name == "object") {
		if (n < 2)
			return Value::createError(FUNCT_NUM);

		if (funct[1].name == "delete") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != RPGOBJECT)
				return Value::createError(FUNCT_ARG_TYPE);
			if (args[0].obj != NULL) {
				args[0].obj->removeFromLocation();
				args[0].obj->release();
			}
			args[0].obj = NULL;
			return true;
		}
		if (funct[1].name == "new") {
			if (n < 3) {
				args = evaluatearguments(funct[1].args);
				if (args.size() != 1)
					return Value::createError(FUNCT_ARG_NUM);
				Map *map = campaign->getCurrentMap();
				if (map == NULL)
					return Value::createError(FUNCT);
				RPGObject *obj = map->getObjectType(args[0]);
				//if (n == 2)
				return obj;
			}
			else {
				if (funct[2].name == "weapon") {
					Weapon *wp = new Weapon();
					return Value(wp);
				}
				else if (funct[2].name == "armor") {
					Armor *ar = new Armor();
					return Value(ar);
				}
				else if (funct[2].name == "monster") {
					Monster *mon = new Monster();
					return Value(mon);
				}
			}
		}
		return Value::createError(FUNCT); 

	}

	// pristup k mape map(x,y) urcuje bunku na mape
	if (funct[0].name == "map") {
		Map *map = campaign->getCurrentMap();
		if (map == NULL) 
			return Value::createError(FUNCT);
		MapNode *node;
		args = evaluatearguments(funct[0].args);

		//ak nie su parametre specifikovane pri volani map tak pouzijeme bunku na ktorej sa nachadzame (ak typ triggeru je MAPTRIGGER)
		if (args.size()==0) {
			if (trig->getType() == MAPTRIGGER) 
				node = trig->getNode();
			else node = map->getNode(0,0);
		}
		else {
			if (args.size() != 2) 
				return Value::createError(FUNCT_ARG_NUM);
			x = args[0];
			y = args[1];
			node = map->getNode(x,y);
		}
		if (n<2) 
			return Value::createError(FUNCT_NUM);

		// vytvorenie bunky na mape
		if (funct[1].name == "createnode") {
			if (node == NULL) {
				RPG::MapNode *node=new MapNode();
				node->setMap(map);
				node->setCoord(x,y);
				node->setTerrain("default");
				campaign->getCurrentMap()->addNode(node);
				return true;
			}
			return true;
		}

		// pristup k objektom na tejto bunke mapy s id specifikovanym v argumente funkcie
		else if (funct[1].name == "getobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			if (node == NULL) 
				return Value::createError(FUNCT);
			RPGObject *obj = node->getObject(args[0].getString());
			if (obj == NULL) 
				return NULL;
			if (n==2) 
				return obj;
			funct.remove(0,2);
			return objecteval(funct,obj);
		}
		else if (funct[1].name == "addobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1 && args[0].type != RPGOBJECT) 
				return Value::createError(FUNCT_ARG);
			if (node == NULL) 
				return Value::createError(FUNCT);
			if (args[0].getObject() == NULL)
				return Value::createError(FUNCT_ARG_TYPE);
			node->addObject(args[0].obj);
			return Value::createVoid();
		}
		else if (funct[1].name == "removeobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			if (node == NULL) 
				return Value::createError(FUNCT);
			if (args[0].type == STRING) {
				RPGObject *obj = node->removeObject(args[0]);
				if (obj == NULL)
					return false;
				return obj;
			}
			else if (args[0].type == RPGOBJECT) {
				RPGObject *obj = node->removeObject(args[0].obj);
				if (obj == NULL)
					return obj;
				funct.remove(0,2);
				return objecteval(funct,obj);
			} 
			else
				return Value::createError(FUNCT_ARG);
		}
		else if (funct[1].name == "deleteobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			if (node == NULL) 
				return Value::createError(FUNCT);
			if (args[0].type == STRING) {
				RPGObject *obj = node->removeObject(args[0]);
				if (obj == NULL)
					return false;
				obj->release();
				return true;
			}
			else if (args[0].type == RPGOBJECT) {
				RPGObject *obj = node->removeObject(args[0].obj);
				if (obj == NULL)
					return false;
				obj->release();
				return true;
			} 
			else
				return Value::createError(FUNCT);
		}

		//pristup k terenu na mape
		else if (funct[1].name == "terrain") {
			if (n!=3) 
				return Value::createError(FUNCT_NUM);
			// zmena typu terenu
			if (funct[2].name == "change") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1) 
					return Value::createError(FUNCT_ARG_NUM);
				// ak bunka neexistuje tak ju vytvori
				if (node == NULL) {
					RPG::MapNode *node=new MapNode();
					node->setMap(map);
					node->setCoord(x,y);
					campaign->getCurrentMap()->addNode(node);
					node->setTerrain(args[0].getString());
					return true;
				}
				node->setTerrain(args[0].getString());
				return true;
			}
			if (node == NULL) 
				return Value::createError(FUNCT);
			RPGTerrain *terrain = node->getTerrain();
			if (terrain == NULL) 
				return Value::createError(FUNCT);
			// zistenie ci je teren priechodny
			if (funct[2].name == "iswalkable") 
				return terrain->isWalkable();
			return Value::createError(FUNCT);
		}
		//zmena mapy
		else if (funct[1].name == "change") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			Map *map = campaign->getCurrentMap();
			if (map != NULL)
				return campaign->setCurrentMap(map->getCwd()+"/"+args[0].getString(), trig);
			else 
				return false;
		}
		else 
			return Value::createError(FUNCT);
	}

	if (trig->getType() == MAPTRIGGER) {
		// ziskanie pozicie triggeru na mape (x, y)
		if (funct[0].name == "thispos") {
			if (n!=2) 
				return Value::createError(FUNCT_NUM);
			if (funct[1].name == "x") 
				return trig->getNode()->getX();
			else if (funct[1].name == "y") 
				return trig->getNode()->getY();
			else return Value::createError(FUNCT);
		}
	}

	if (trig->getType() == MONSTERTRIGGER) {
		if (funct[0].name == "thismonster") {
			Monster *ch = trig->getMonster();
			if (ch == NULL)
				return ch;
			if (n==1)
				return ch;
			funct.remove(0);
			return objecteval(funct,ch);
		}
	}

	if (trig->getType() == ITEMTRIGGER) {
		// praca s vlastnikom predmetu
		if (funct[0].name == "owner") {
			Character *ch = trig->getItem()->getOwner();
			if (ch == NULL)
			   return Value::createError(FUNCT);
			if (n == 1) 
				return ch;
			funct.remove(0);
			return objecteval(funct, ch);
		}
		if (funct[0].name == "thisitem") {
			InventoryObject *it = trig->getItem();
			if (it == NULL)
				return it;
			if (n == 1)
				return it;
			funct.remove(0);
			return itemeval(funct,it);
		}
	}

	if (funct[0].name == "thisobject" ) {
		RPGObject *obj = trig->getObject();
		if (obj == NULL)
			return obj;
		if (n==1)
			return obj;
		funct.remove(0);
		return objecteval(funct,obj);
	}

	if (funct[0].name == "schedule") {
		args = evaluatearguments(funct[0].args);
		if (args.size()!=2) 
			return Value::createError(FUNCT_ARG_NUM);
		
		Trigger *trigger = dynamic_cast<Trigger*>(args[1].obj);
		if (trigger == NULL)
			return Value::createError(FUNCT);
		
		campaign->schedule(args[0].getInt(),trigger);
		return Value::createVoid();
	}


	// funkcia vyhodnoti svoj parameter ako vyraz
	if (funct[0].name == "evaluate") {
		if (n!=1) 
			return Value::createError(FUNCT_ARG_NUM);
		args = evaluatearguments(funct[0].args);
		if (args.size()!=1) 
			return Value::createError(FUNCT_ARG_NUM);
		Expression expr(this);
		expr.setExpression(args[0]);
		return expr.eval();
	}

	if (funct[0].name == "gui") {
		if (n!=2) 
			return Value::createError(FUNCT_NUM);
		//funkciou writestatus vypiseme retazec do status widgetu
		if (funct[1].name == "write") {
			if (n!=2) 
				return Value::createError(FUNCT_NUM);
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			campaign->writeStatus(args[0]);
			return Value::createVoid();
		}
		// vypisanie boxu s hlaskou
		else if (funct[1].name == "message") {
			if (n!=2) 
				return Value::createError(FUNCT_NUM);
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			trig->setEnabled(false);
			QMessageBox::information(campaign, QString("Info"), QString(args[0]),
									 QMessageBox::Ok,QMessageBox::Ok);
			trig->setEnabled(true);
			return Value::createVoid();
		}
		// jednoduchy input dialog
		else if (funct[1].name == "input") {
			if (n!=2) 
				return Value::createError(FUNCT_NUM);
			QString temp;
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				temp = "";
			else 
				temp = args[0];
			trig->setEnabled(false);
			temp = QInputDialog::getText(campaign, QString("Input"), temp, QLineEdit::Normal);
			trig->setEnabled(true);
			return temp;
		}
		else return Value::createError(FUNCT);
	}

	// suboj
	if (funct[0].name == "combat") {
		if (n < 2)
			return Value::createError(FUNCT_NUM);

		if (funct[1].name == "add") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!= 1)
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != RPGOBJECT)
				return Value::createError(FUNCT_ARG_TYPE);
			Character *en = dynamic_cast<Character*>(args[0].getObject());
			if (en == NULL)
				return Value::createError(FUNCT_ARG_TYPE);

			// pridame do suboja do suboja
			if (!en->isInCombat()) {
				CombatCharacter *tmp = new CombatCharacter();
				tmp->setCharacter(en);
				campaign->combat->addCharacter(tmp);	
			}
		}
		else if (funct[1].name == "count") {
			return campaign->combat->turnSequenceSize();
		}
		else if (funct[1].name == "get") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!= 1)
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != INTEGER)
				return Value::createError(FUNCT_ARG_TYPE);

			Character *ch = campaign->combat->turnSequenceAt(args[0].getInt());
			if (ch == NULL)
				return NULL;
			if (n == 2)
				return ch; 

			funct.remove(0,2);
			return objecteval(funct,ch);
		}
		else if (funct[1].name == "isrunning") {
			return campaign->combat->isRunning();
		}
		else
			return Value::createError(FUNCT);
	}

	//hody
	if (funct[0].name == "throw") {
		if (n!=2) 
			return Value::createError(FUNCT_NUM);
		if (funct[1].name == "success") {
			args = evaluatearguments(funct[1].args);
			if (args.size()<2) 
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != RPGOBJECT) 
				return Value::createError(FUNCT_ARG_TYPE);
			if (args.size()>2 && args[2].type == INTEGER)
				tmp = args[2].integer;
			else 
				tmp = 0;		

			Character *ch = dynamic_cast<Character*>(args[0].obj);
			if (ch == NULL)
				return Value::createError(FUNCT);
			
			SuccessRoll roll;
			roll.setRoll(ch, args[1].getString(), tmp);
			SuccessRollResult result = roll.roll();
			lastmos = roll.getMarginOfSuccess();
			if (n<3) return result;
			if (funct[2].name == "mos")
				return lastmos;
			else 
				Value::createError(FUNCT);
		}
		else if (funct[1].name == "reaction") {
			args = evaluatearguments(funct[1].args);
			int mod = 0;
			if (args.size()==1) {
				mod = args[0].getInt();			
			}
			Die die;
			die.setDie(3,6,mod);
			int res = die.throwDie();
			if (res <= rules.reactionLimit.reactionBad)
				return REACTION_BAD;
			else if (res <= rules.reactionLimit.reactionNeutral) 
				return REACTION_NEUTRAL;
			else 
				return REACTION_GOOD;
		}
		else if (funct[1].name == "die") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != STRING) 
				return Value::createError(FUNCT_ARG_TYPE);
			Die die(args[0].getString());
			return die.throwDie();
		}
		else return Value::createError(FUNCT);
	}

	if (funct[0].name == "dialogue") {
		args = evaluatearguments(funct[0].args);
		if (args.size()<1) 
			return Value::createError(FUNCT_ARG_NUM);

		QStringList	answers;
		RPGDialog dial;
		for(int i=1; i < args.size(); ++i)
			answers.append(args[i].getString());		
		return dial.display(args[0].getString(),answers);
	}

	// pomocne funkcie a konstanty
	if (funct[0].name == "utils") {
		if (n<2) return Value::createError(FUNCT_NUM);
		//konstanty pre typy atributov
		if (funct[1].name == "attribute") {
			if (n!=3) return Value::createError(FUNCT_NUM);
			if (funct[2].name == "st")	
				return STRENGTH;
			else if (funct[2].name == "dx")
				return DEXTERITY;
			else if (funct[2].name == "iq")
				return INTELLIGENCE;
			else if (funct[2].name == "ht")
				return HEALTH;
			else if (funct[2].name == "default")
				return DEFAULTATTR;
		}
		else if (funct[1].name == "maneuver") {
			if (n != 3) return Value::createError(FUNCT_NUM);
			if (funct[2].name == "attack")
				return ATTACK;
			else if (funct[2].name == "strongattack") 
				return ATTACK_STRONG;
			else if (funct[2].name == "determinedattack")
				return ATTACK_DETERMINED;
			else if (funct[2].name == "doubleattack")
				return ATTACK_DOUBLE;
			else if (funct[2].name == "strongdefense")
				return DEFENSE_STRONG;
			else if (funct[2].name == "doubledefense")
				return DEFENSE_DOUBLE;
			else if (funct[2].name == "wait")
				return DO_NOTHING;
			else if (funct[2].name == "other")
				return OTHERMAN;
			else
				return 0;
		}	
		//konstanty pre obtiaznost dovednosti
		else if (funct[1].name == "difficulty") {
			if (n!=3) return Value::createError(FUNCT_NUM);
			if (funct[2].name == "easy")
				return EASY;
			else if (funct[2].name == "average")
				return AVERAGE;
			else if (funct[2].name == "hard")
				return HARD;
			else if (funct[2].name == "default")
				return DEFAULTDIFF;
		}
		//konstanty pre vysledky hodov
		else if (funct[1].name == "throwresult") {
			if (n!=3) return Value::createError(FUNCT_NUM);
			
			if (funct[2].name == "criticalsuccess")
				return CRITICALSUCCESS;
			else if (funct[2].name == "success")
				return SUCCESS;
			else if (funct[2].name == "failure")
				return FAILURE;
			else if (funct[2].name == "criticalfailure")
				return CRITICALFAILURE;
		}
		else if (funct[1].name == "reactionresult") {
			if (n!=3) return Value::createError(FUNCT_NUM);
		
			if (funct[2].name == "bad")
				return rules.reactionLimit.reactionBad;
			else if (funct[2].name == "neutral")
				return rules.reactionLimit.reactionNeutral;
			else if (funct[2].name == "good")
				return rules.reactionLimit.reactionGood;
			else 
				return Value::createError(FUNCT);
		}

		//niektore pomocne funkcie
		if (funct[1].name == "floor") {
			if (n!=2) return Value::createError(FUNCT_NUM);
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) return Value::createError(FUNCT_ARG_NUM);
		
			switch (args[0].type) {
				case FLOATING:
				case INTEGER:
					return (int) floor(args[0]);
				default:
					return Value::createError(FUNCT_ARG_TYPE);
			}		
		}
		else if (funct[1].name == "ceil") {
			if (n!=2) return Value::createError(FUNCT_NUM);
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) return Value::createError(FUNCT_ARG_NUM);
		
			switch (args[0].type) {
				case FLOATING:
				case INTEGER:
					return (int) ceil(args[0]);
				default:
					return Value::createError(FUNCT_ARG_TYPE);
			}
		}
		else if (funct[1].name == "round") {
			if (n!=2) return Value::createError(FUNCT_NUM);
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) return Value::createError(FUNCT_ARG_NUM);
			double d;
			switch (args[0].type) {
				case FLOATING:
					d = abs(args[0].floating);
					d = d - floor(d);
					if (args[0].floating>0) {
						if (d>=0.5) return floor(args[0])+1;
						else return floor(args[0]);
					}
					else if (d>=0.5) return floor(args[0]);
						else return floor(args[0])+1;
				case INTEGER:
					return args[0].integer;
				default:
					return Value::createError(FUNCT_ARG_TYPE);
			}
		}
		else if (funct[1].name == "abs") {
			if (n!=2) return Value::createError(FUNCT_NUM);
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) return Value::createError(FUNCT_ARG_NUM);
		
			switch (args[0].type) {
				case BOOL:
					return args[0];
				case INTEGER:
					return args[0].integer>=0?args[0].integer:-args[0].integer;
				case FLOATING:
					return args[0].floating>=0?args[0].floating:-args[0].floating;
				default:
					return Value::createError(FUNCT_ARG_TYPE);
			}
		}
		
		return Value::createError(FUNCT);
	}

	// funkcia pre pracu s globalnymi premennymi triggerov (definovane v Campaign)
	if (funct[0].name == "global") {
		if (n != 2) 
			return Value::createError(FUNCT_NUM);
		
		if (funct[1].name == "remove") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 1) 
				return Value::createError(FUNCT_ARG_NUM);
			Value val = campaign->getTriggerVar(args[0].getString());
			campaign->removeTriggerVar(args[0].getString());
			return val;
		}
		else if (funct[1].name == "set") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 2) 
				return Value::createError(FUNCT_ARG_NUM);
			campaign->setTriggerVar(args[0], args[1]);	
			return Value::createVoid();
		}
		if (funct[1].name == "get") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 1) 
				return Value::createError(FUNCT_ARG_NUM);
			return campaign->getTriggerVar(args[0]);		
		}
		return Value::createError(FUNCT);			
	}

	//funkcie na konverziu datovych typov
	if (funct[0].name == "int") {
		args = evaluatearguments(funct[0].args);
		if (args.size()!=1) 
			return Value::createError(FUNCT_ARG_NUM);
		return (int)args[0];
	}
	else if (funct[0].name == "bool") {
		args = evaluatearguments(funct[0].args);
		if (args.size()!=1) 
			return Value::createError(FUNCT_ARG_NUM);
		return (bool)args[0];
	}
	else if (funct[0].name == "float") {
		args = evaluatearguments(funct[0].args);
		if (args.size()!=1) 
			return Value::createError(FUNCT_ARG_NUM);
		return (double)args[0];
	}
	else if (funct[0].name == "string") {
		args = evaluatearguments(funct[0].args);
		if (args.size()!=1) 
			return Value::createError(FUNCT_ARG_NUM);
		return (QString)args[0];
	}
	//funkcia na zistenie typu hodnoty 
	else if (funct[0].name == "valuetype") {
		args = evaluatearguments(funct[0].args);
		if (args.size()<1) {
			if (n!=2) 
				return Value::createError(FUNCT_NUM);
			//konstanty typov hodnot
			if (funct[1].name == "bool")
				return (int)BOOL;
			else if (funct[1].name == "integer")
				return (int)INTEGER;
			else if (funct[1].name == "floating")
				return (int)FLOATING;
			else if (funct[1].name == "string")
				return (int)STRING;
			else if (funct[1].name == "object")
				return (int)RPGOBJECT;
			else if (funct[1].name == "error")
				return (int)VALUE_ERROR;
			else if (funct[1].name == "void")
				return (int)VOID;
		}
		if (n!=1) 
			return Value::createError(FUNCT_NUM);
		return (int)args[0].type;
	}

	return Value::createError(FUNCT);
}

Value FunctionEval::eval(FunctionDefinition &funct, Value &val)
{
	QVector<Value> args;
	int n = funct.size();
	
	if (val.type == RPGOBJECT) {
		return objecteval(funct,val.obj);
	}
	
	return Value::createError(FUNCT);
}

Value FunctionEval::objecteval(FunctionDefinition &funct, RPGObject *obj) 
{
	if (obj == NULL) return Value::createError("object does not exist");
	QVector<Value> args;
	int n = funct.size();

	if (n<1) 
		return Value::createError(FUNCT_NUM);		   

	// pristup k zakladnym nastaveniam objektu
	if (funct[0].name == "setid") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		obj->setID(args[0]);
		return Value::createVoid();
	}
	else if (funct[0].name == "getid") {
		return obj->getID();
	}
	else if (funct[0].name == "setname") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		obj->setName(args[0]);
		return Value::createVoid();
	}
	else if (funct[0].name == "getname") {
		return obj->getName();
	}
	else if (funct[0].name == "getinfo") {
		return obj->getInfo();
	}
	else if (funct[0].name == "setabout") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		obj->setAboutString(args[0]);
		return Value::createVoid();
	}
	// nastavenie grafiky objektu
	else if (funct[0].name == "setgraphics") {
		if (n < 2)
			return Value::createError(FUNCT_NUM);
		if (funct[1].name == "bgcolor") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);
			obj->drawBackground(args[0].getString());
			return Value::createVoid();
		}
		else if (funct[1].name == "circle") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);
			obj->drawCircle(args[0].getString());
			return Value::createVoid();
		}
		else if (funct[1].name == "text") {
			args = evaluatearguments(funct[1].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);
			obj->drawText(args[0].getString());
			return Value::createVoid();
		}
		else if (funct[1].name == "texture") {
			args = evaluatearguments(funct[1].args);
			if (args.size() < 1)
				return Value::createError(FUNCT_ARG_NUM);
			Map *map = campaign->getCurrentMap();
			if (map == NULL)
				return Value::createError(FUNCT);
			if (!args[0].getString().endsWith(".png"))
				return Value::createError("Bad image type");
			QImage image(map->getCwd()+'/'+args[0]);
			int x = 0;
			int y = 0;
			int sx = 0;
			int sy = 0;
			int fit = true;

			if (args.size() == 6) {
				x = args[1];
				y = args[2];
				sx = args[3];
				sy = args[4];
				fit = args[5];
			}
			if (args.size() != 6 && args.size() > 1)
				return Value::createError(FUNCT_ARG_NUM);
			
			obj->drawTexture(image,x,y,sx,sy,fit);
			return Value::createVoid();
		}
		return Value::createError(FUNCT);
	}

	// pristup k objektom typu character
	Character *ch = dynamic_cast<Character*>(obj);
	if (ch != NULL)
		return charactereval(funct,ch);

	// pristup k objektom typu item
	InventoryObject *it= dynamic_cast<InventoryObject*>(obj);
	if (it != NULL) {
		return itemeval(funct,it);
	}

	// pristup k objektom typu trigger
	Trigger *tr = dynamic_cast<Trigger*>(obj);
	if (tr != NULL)
		return triggereval(funct,tr);

	return Value::createError(FUNCT);
}

Value FunctionEval::itemeval(FunctionDefinition &funct, InventoryObject *obj) 
{
	if (obj == NULL) return Value::createError("object does not exist");
	int n = funct.size();
	QVector<Value> args;
	int tmp;
	if (n<1) 
		return Value::createError(FUNCT);

	if (funct[0].name == "setowner") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		if (args[0].type != RPGOBJECT)
			return Value::createError(FUNCT_ARG_TYPE);

		Character *ch = dynamic_cast<Character*>(args[0].obj);
		if (ch == NULL)
			return Value::createError(FUNCT_ARG_TYPE);
		
		obj->setOwner(ch);
		return Value::createVoid();
	}
	else if (funct[0].name == "getowner")  {
		return obj->getOwner();
	}
	else if (funct[0].name == "equip") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		
		obj->equip(args[0].getString());
		return Value::createVoid();
	}
	else if (funct[0].name == "setuses") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		obj->setUses(args[0].getInt());
		return Value::createVoid();
	}
	else if (funct[0].name == "getuses") {
		return obj->getUses();
	}
	else if (funct[0].name == "getweight") {
		return obj->getWeight();
	}
	else if (funct[0].name == "setweight") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		obj->setWeight(args[0].getInt());
		return Value::createVoid();
	}
   
	// pristup k objektom typu weapon
	Weapon *wp = dynamic_cast<Weapon*>(obj);
	if (wp != NULL) {
		if (funct[0].name == "addskill") {
			args = evaluatearguments(funct[0].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);
			
			wp->addSkill(args[0].getString());
			return Value::createVoid();
		}
		else if (funct[0].name == "getskillinfo") {
			return wp->getSkillInfo();
		}
		else if (funct[0].name == "setdamage" ) {
			args = evaluatearguments(funct[0].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);

			wp->setDamage(args[0].getString());
			return Value::createVoid();
		}
		else if (funct[0].name == "getdamage") {
			return wp->getDamageString();
		}
	}
	// pristup k objektom typu armor
	Armor *ar = dynamic_cast<Armor*>(obj);
	if (ar != NULL) {				 
		if (funct[0].name == "setdr") {
			args = evaluatearguments(funct[0].args);
			if (args.size() != 1)
				return Value::createError(FUNCT_ARG_NUM);
			ar->setDR(args[0].getInt());
			return Value::createVoid();
		}
		else if (funct[0].name == "getdr") {
			return ar->getDR();
		}
	}
	
	return Value::createError(FUNCT);
}

Value FunctionEval::triggereval(FunctionDefinition &funct, Trigger *trig) 
{
	if (trig == NULL)
		return Value::createError(FUNCT);
	
	if (funct.size() < 1)
		return Value::createError(FUNCT_NUM);

	if (funct[0].name == "disable") {
		trig->setEnabled(false);
		return Value::createVoid();
	}
	else if (funct[0].name == "enable") {
		trig->setEnabled(true);
		return Value::createVoid();
	}
	else if (funct[0].name == "remove") {
		trig->setEnabled(false);
		trig->setRemove(true);
		return Value::createVoid();
	}
	return Value::createError(FUNCT);
}

Value FunctionEval::charactereval(FunctionDefinition &funct, Character *ch)
{
	if (ch == NULL) 
		return Value::createError(FUNCT);

	int n = funct.size();
	QVector<Value> args;
	int tmp;

	if (n<1) 
		return Value::createError(FUNCT_NUM);

	// nastavenie pozicie na mape
	if (funct[0].name == "setpos") {
		if (n != 1) 
			return Value::createError(FUNCT_NUM);
		args = evaluatearguments(funct[0].args);
		if (args.size() != 2) 
			return Value::createError(FUNCT_ARG_NUM);
		return ch->moveOnMap(args[0], args[1]);
	}
	// vymazanie z mapy
	if (funct[0].name == "remove") {
		if (typeid(*ch) != typeid(PlayerNode)) {
			if (n != 1) 
				return Value::createError(FUNCT_NUM);
			ch->removeFromLocation();
			ch->release();
		}
		else {
			if (campaign->getPlayer()!=ch)
				return false;
			campaign->clearPlayer();
		}
		return true; 
	}
	// pohyb na mape
	if (funct[0].name == "move") {
		if (n != 2) 
			return Value::createError(FUNCT_NUM);
		
		int x = ch->location()->getX();
		int y = ch->location()->getY();

		if (funct[1].name == "left") 
			return ch->moveOnMap(x-1, y,true);
		else if (funct[1].name == "right") 
			return ch->moveOnMap(x+1, y,true);
		else if (funct[1].name == "up")
			return ch->moveOnMap(x, y-1,true);
		else if (funct[1].name == "down")
			return ch->moveOnMap(x, y+1,true);
		return Value::createError(FUNCT);
	}
	if (funct[0].name == "moveto") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 2) 
			return Value::createError(FUNCT_ARG_NUM);
		if (typeid(*ch) == typeid(PlayerNode)) {
			PlayerNode *player = dynamic_cast<PlayerNode*>(ch);
			MapFindPath pathFinder(campaign->getCurrentMap());
			MapFindPath::PathList path = pathFinder.find(player->getX(),player->getY(), args[0].getInt(), args[1].getInt());
			campaign->mapWidget->setPlayerPath(path);
			return true;
		}
		if (typeid(*ch) == typeid(Monster)) {
			MapFindPath pathFinder(campaign->getCurrentMap());
			MapFindPath::PathList path = pathFinder.find(ch->location()->getX(),ch->location()->getY(),args[0].getInt(),args[1].getInt());
			if (path.empty()) return false;
			campaign->mapWidget->setObjectPath(path,ch);
			return true;
		}
	}

	if (funct[0].name == "distance") {
		args = evaluatearguments(funct[0].args);
		int x = 0;
		int y = 0;
		if (args.size() == 2) {
			x = args[0].getInt();
			y = args[1].getInt();
		}
		else if (args.size() == 1 && args[0].type == RPGOBJECT) {
			RPGObject *obj = args[0].getObject();
			if (obj == NULL)
				return -1;
			x = obj->location()->getX();
			y = obj->location()->getY();
		}
		else 
			return Value::createError(FUNCT_ARG);
		
		MapFindPath pathFinder(campaign->getCurrentMap());
		MapFindPath::PathList path = pathFinder.find(ch->location()->getX(),ch->location()->getY(),
													 x,y);
		return path.size()-1;
	}

	//atributy
	if (funct[0].name == "attribute") {
		if (n<2) 
			return Value::createError(FUNCT_NUM);

		//STRENGTH
		if (funct[1].name == "st") {
			if (n<3) return ch->getST();

			if (funct[2].name == "get") 
				return ch->getST();
			else if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);

				ch->setST(args[0].integer);
				return ch->getST();
			}
			else if (funct[2].name == "modify") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);

				ch->setST(ch->getST()+args[0].integer);
			}
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(STRENGTH,args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus(STRENGTH);
			return ch->getST();
		}
		//DEXTERITY
		else if (funct[1].name == "dx") {
			if (n<3) return ch->getDX();

			if (funct[2].name == "get") 
				return ch->getDX();
			else if (funct[1].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setDX(args[0].integer);
			}
			else if (funct[2].name == "modify") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setDX(ch->getDX()+args[0].integer);
			}
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(DEXTERITY,args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus(DEXTERITY);
			return ch->getDX();
		}
		//INTELLIGENCE
		else if (funct[1].name == "iq") {
			if (n<3) return ch->getIQ();

			if (funct[2].name == "get") {
				return ch->getIQ();
			}
			else if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER)
					return Value::createError(FUNCT_ARG);
				ch->setIQ(args[0].integer);
			}
			else if (funct[2].name == "modify") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setIQ(ch->getIQ()+args[0].integer);
			}
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(INTELLIGENCE,args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus(INTELLIGENCE);
			return ch->getIQ();
		}
		//HEALT
		else if (funct[1].name == "ht") {
			if (n<3) return ch->getHT();

			if (funct[2].name == "get") {
				return ch->getHT();
			}
			else if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setHT(args[0].integer);
			}
			else if (funct[2].name == "modify") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setHT(ch->getHT()+args[0].integer);
			}
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(HEALTH,args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus(HEALTH);
			return ch->getHT();
		}
		//cHP
		else if (funct[1].name == "chp") {
			if (n<3) return ch->getcHP();

			if (funct[2].name == "get") {
				return ch->getcHP();
			}
			else if (funct[2].name == "replenish") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				int chp = ch->getcHP() + args[0].integer;
				if (chp > ch->getHP()) 
					chp = ch->getHP();
				ch->setcHP(chp);
			}
			else if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setcHP(args[0].integer);
			}
			else if (funct[2].name == "modify") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setcHP(ch->getcHP()+args[0].integer);
			}
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER)
					return Value::createError(FUNCT_ARG);
				ch->setBonus(QString("chp"),args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus(QString("chp"));
			return ch->getcHP();
		}
		//HP
		else if (funct[1].name == "hp") {
			if (n<3) return ch->getHP();

			if (funct[2].name == "get")
				return ch->getHP();
			else if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setHP(args[0].integer);
			}
			else if (funct[2].name == "modify") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setHP(ch->getHP()+args[0].integer);
			}
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(QString("hp"),args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus(QString("hp"));
			return ch->getHP();
		}
		//BasicMove
		else if (funct[1].name == "basicmove") {
			if (n<3) return ch->getBasicMove();

			if (funct[2].name == "get")
				return ch->getBasicMove();
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || args[0].type != INTEGER) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(QString("basicmove"),args[0].integer);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus("basicmove");
			return ch->getBasicMove();
		}
		//BasicSpeed
		else if (funct[1].name == "basicspeed") {
			if (n<3) return ch->getBasicSpeed();

			if (funct[2].name == "get")
				return ch->getBasicSpeed();
			else if (funct[2].name == "setbonus") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || ((args[0].type != INTEGER) && (args[0].type != FLOATING))) 
					return Value::createError(FUNCT_ARG);
				ch->setBonus(QString("basicspeed"),args[0]);
			}
			else if (funct[2].name == "getbonus") 
				return ch->getBonus("basicspeed");
			return ch->getBasicSpeed();
		}
		//move points
		else if (funct[1].name == "movept") {
			if (n<3) 
				return Value::createError(FUNCT_NUM);

			if (funct[2].name == "get")
				return ch->getMovePt();
			else if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER))
					return Value::createError(FUNCT_ARG);
				ch->setMovePt(args[0].getInt());
			}
			else if (funct[2].name == "total") {
				return ch->getTotalMovePt();
			}
			else if (funct[2].name == "spend") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER)) 
					return Value::createError(FUNCT_ARG);
				ch->spendMovePt(args[0].getInt());
			}	
			return ch->getMovePt();
		}
		// xp points
		else if (funct[1].name == "xp") {
			if (n<3)
				return Value::createError(FUNCT_NUM);

			if (funct[2].name == "settotal") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER)) 
					return Value::createError(FUNCT_ARG);
				ch->setTotalXP(args[0].getInt());
				return ch->getTotalXP();
			} 
			else if (funct[2].name == "setunused") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER)) 
					return Value::createError(FUNCT_ARG);
				ch->setUnusedXP(args[0].getInt());
			} 
			else if (funct[2].name == "earn") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER)) 
					return Value::createError(FUNCT_ARG);
				ch->earnXP(args[0].getInt());
			}
			else if (funct[2].name == "spend") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER)) 
					return Value::createError(FUNCT_ARG);
				int xp = args[0].getInt();
				if (ch->getUnusedXP() < xp)
					return false;
				ch->spendUnusedXP(xp);
				return true;
			}
			return ch->getUnusedXP();
		}
		else if (funct[1].name == "size") {
			if (n<3)
				return Value::createError(FUNCT_NUM);

			if (funct[2].name == "set") {
				args = evaluatearguments(funct[2].args);
				if (args.size()!=1 || (args[0].type != INTEGER && args[0].type != FLOATING))
					return Value::createError(FUNCT_ARG);
				
				ch->setSize(args[0].getDouble());
				return ch->getSize();
			} 
			else if (funct[2].name == "get")
				return ch->getSize();
			else return Value::createError(FUNCT);
		}
	}
	//dovednosti
	else if (funct[0].name == "skill") {
		args = evaluatearguments(funct[0].args);
		if (args.size() != 1)
			return Value::createError(FUNCT_ARG_NUM);
		// ziskame skill
		Skill *skill = ch->getSkill(args[0]);
		if (skill == NULL) { 
			// ak ho postava nema tak ho pridame (ak existuje)
			skill = ch->addSkill(args[0],NONE);
			if (skill == NULL)
				return Value::createError(FUNCT);
		}
		
		if (n != 2)
			return Value::createError(FUNCT_NUM);

		//ciselna konstanta pre typ atributu (enum RPG::Attribute)
		if (funct[1].name == "attribute")
			return skill->getAttribute();
		//dosiahnuta uroven dovednosti
		else if (funct[1].name == "level")
			return skill->getLevel();
		//uroven dovednosti prepocitana pre nasho hraca
		else if (funct[1].name == "final")
			return skill->getFinalLevel(ch);
		//ciselna konstanta udavajuca obtiaznost (enum RPG::Difficulty)
		else if (funct[1].name == "difficulty")
			return skill->getDifficulty();
		else if (funct[1].name == "setlevel") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1 || args[0].type != INTEGER) 
				return Value::createError(FUNCT_ARG);
			skill->setLevel(args[0].integer);
			ch->emitStatsInfoChanged();
			return skill->getLevel();
		}
		else if (funct[1].name == "modifylevel") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1 || args[0].type != INTEGER) 
				return Value::createError(FUNCT_ARG);
			skill->setLevel(skill->getLevel()+args[0].integer);
			ch->emitStatsInfoChanged();
			return skill->getLevel();
		}
		else if (funct[1].name == "setbonus") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1 || args[0].type != INTEGER) 
				return Value::createError(FUNCT_ARG);
			skill->setBonus(args[0].integer);
			ch->emitStatsInfoChanged();
			return skill->getLevel();
		}
		else if (funct[1].name == "getbonus") 
			return skill->getBonus();
		return Value::createError(FUNCT);
	}
	//hody
	else if (funct[0].name == "throw") {
		if (n<2) 
			return Value::createError(FUNCT_NUM);
		if (funct[1].name == "success") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != STRING) 
				return Value::createError(FUNCT_ARG_TYPE);
			if (args.size()>1 && args[1].type == INTEGER)
				tmp = args[1].integer;
			else 
				tmp = 0;		
			SuccessRoll roll;
			roll.setRoll(ch, args[0].getString(), tmp);
			SuccessRollResult result = roll.roll();
			lastmos = roll.getMarginOfSuccess();
			if (n<3) 
				return result;
			if (funct[2].name == "mos")
				return lastmos;
			else 
				return Value::createError(FUNCT);
		}
		if (funct[1].name == "reaction") {
			args = evaluatearguments(funct[1].args);
			int mod = 0;
			if (args.size()==1) {
				mod = args[0].getInt();			
			}
			Die die;
			die.setDie(3,6,mod);
			int res = die.throwDie();
			if (res <= rules.reactionLimit.reactionBad)
				return REACTION_BAD;
			else if (res <= rules.reactionLimit.reactionNeutral) 
				return REACTION_NEUTRAL;
			else 
				return REACTION_GOOD;
		}
		if (funct[1].name == "die") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);
			if (args[0].type != STRING) 
				return Value::createError(FUNCT_ARG_TYPE);
			if (args.size()>1 && args[1].type == INTEGER)
				tmp = args[1].integer;
			else
				tmp = 0;		
			Die die(args[0].getString());
			return die.throwDie()+tmp;
		}
	}
	// manipulacia s inventarom
	else if (funct[0].name == "inventory") {
		if (n < 2) 
			return Value::createError(FUNCT_NUM);
		
		Inventory *inv = ch->getInventory();
		if (inv == NULL) 
			return Value::createError(FUNCT);

		if (funct[1].name == "getobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);

			InventoryObject *obj;
			if (args[0].type == INTEGER)
				obj = inv->getItem(args[0].getInt());
			else 
				obj = inv->getItem(args[0].getString());

			if (obj == NULL) 
				return NULL;

			if (n==2) 
				return obj;
			funct.remove(0,2);
			return objecteval(funct,obj);
		}
		else if (funct[1].name == "count") {
			return inv->rowCount();
		}
		else if (funct[1].name == "addobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);	

			if (args[0].type != RPGOBJECT)
				return Value::createError(FUNCT_ARG_TYPE);

			InventoryObject *obj = dynamic_cast<InventoryObject*> (args[0].obj);
			if (obj == NULL)
				return Value::createError(FUNCT);

			inv->addItem(obj);
			return Value::createVoid();
		}
		else if (funct[1].name == "removeobject") {
			args = evaluatearguments(funct[1].args);
			if (args.size()!=1) 
				return Value::createError(FUNCT_ARG_NUM);	

			InventoryObject *obj;

			if (args[0].type == INTEGER)
				obj = inv->removeItem(args[0].getInt());
			else {
				obj = inv->getItem(args[0].getString());
				if (obj != NULL)
					obj->removeFromLocation();
			}

			if (obj == NULL) 
				return NULL;
			
			return obj;
		}
	}

	Monster *mon = dynamic_cast<Monster*>(ch);
	if (mon != NULL) {
		if (funct[0].name == "lastmaneuver") {
				return mon->getLastManeuver();
		}
	}

	return Value::createError(FUNCT);
}

//spracovanie argumentov - argumenty su oddelene ciarkami
//metoda vracia vektor jednolivych vypocitanych argumentov 
QVector<Value> FunctionEval::evaluatearguments(const QString &args)
{
	QVector<Value> a;
	QStringList templist;
	QString temp;
	int z = 0;
	for(int i=0; i< args.size();++i) {
		if (args[i] == '"') {
			temp.append('"');
			++i;
			while (i < args.size() && args[i] != '"') {
				if (args[i] == '\\' && i+1 < args.length()) {
					temp += args[i];
					temp += args[i+1];
					i+=2;
					continue;
				}
				temp.append(args[i]);
				++i;
			}
			if (i < args.size()) {
				temp.append('"');
				continue;
			}
			return a;
		}
		if (args[i] == '(') ++z;
		else if (args[i] == ')') --z;
		else if (args[i] == ',' && z == 0) {
			templist.append(temp);
			temp.clear();
			continue;
		}

		temp.append(args[i]);
	}

	if (!temp.isEmpty())
		templist.append(temp);

	foreach(QString str, templist)
	{
		Expression texpr(this);
		texpr.setExpression(str);
		Value val = texpr.eval();
		if (val.type != VALUE_ERROR && val.type != VOID)
			a.push_back(val);
	}
	return a;
}


void Entry::clear() 
{
	type=NOTYPE;
	sign=SIGN_NO;
	priority=0;
}

ValueType Entry::getValueType() const
{
	if (type == VALUE) {
		return value.type;
	}
	return VALUE_ERROR;
}

/* class Expression */
bool isVariable(QChar ch)
{
	if (ch.isLetterOrNumber() || ch == '_') return true;
	return false;
}

bool parseFunctDef(const QString &s, int &offset, FunctionDefinition &funct)
{
	int i = offset;
	FunctionDef f;
	QString temp;
	int z = 0;

	funct.clear();
	while (true) {
		f.args.clear();
		f.name.clear();
		temp.clear();
		//ziskanie nazvu funkcie
		while ((i < s.length()) && isVariable(s[i]))
		{
			temp += s[i];
			++i;
		}
		f.name = temp;
		if (f.name.isEmpty()) {
			return false;
		}

		if (!(i < s.length())) {
			funct.push_back(f);
			break;
		}

		if (s[i]=='(') {			//ziskanie argumentu
			++i;     //posun zo znaku '('
			temp.clear();
			z = 1;   //udava pocet neuzavretych zatvoriek vovnutri argumentu
			while ((i < s.length()) && z != 0)	{
				if (s[i] == '"') {
					++i;
					temp+='"';
					while (i < s.length() && s[i] != '"') {
						if (s[i] == '\\' && i+1 < s.length()) {
							temp += s[i];
							temp += s[i+1];
							i+=2;
							continue;
						}						
						temp+=s[i];
						++i;
					}
					if (i < s.length()) {
						temp += '"';
						++i;
						continue;
					}
					else return false;
				}
				if (s[i] == '(') ++z;
				else if (s[i] == ')') --z;
				if (z != 0) {
					temp += s[i];
					++i;
				}
			}
			++i;
			f.args=temp;
			funct.push_back(f);
			if ((i < s.length()) && s[i]=='.') {
				++i;
				continue;
			}
			else break;
		}
		else if (s[i]=='.') {
			funct.push_back(f);
			++i;
		}
		else {
			funct.push_back(f);
			break;	
		}
	}
	offset = i;
	if (z != 0) return false;
	return true;
}

bool parseArrayIndex(const QString &s, int &offset, QString &index)
{
	int	i = offset;
	int z;
	if (s[i]=='[') {
		++i;     //posun zo znaku '['
		index.clear();
		z = 1; 
		while ((i < s.length()) && z != 0)	{
			if (s[i] == '[') ++z;
			else if (s[i] == ']') --z;
			if (z != 0) {
				index += s[i];
				++i;
			}
		}
		++i;
	}
	offset = i;
	if (z != 0) return false;
	return true;
}


Expression::Expression(FunctionEval *eval)
{
	this->functeval = eval;
}

void Expression::setExpression(const QString &str)
{
	original = str;
	error = "";
	expr.clear();
}

// metoda vyhodnoti vyraz v infixovej notacii
Value Expression::eval()
{
	//vyparsovanie vyrazu
	parse();

	//ak nie je chyba v sucastnom vyraze tak vyhodnot
	if (error.isEmpty())
	{
		infixtopostfix();  //vyparsovany vyraz prevedieme do postfixu
	    
		if (error.isEmpty())
		{
			Value out = evaluatepostfix();
			if (error.isEmpty())
				return out;
		}
	}

	Value err;
	err.error(error);
	return err;
}

//metoda vyparsuje vstupny string na Expression ulozeny ako QList<Entry>
void Expression::parse()
{
	QString s=original;

	//inicializacia premennych a konstant
	bool zaporne = false;   //zaporne znamienko
	int z = 0;
	QString temp;
	FunctionDef f;
	FunctionDefinition funct;
	Value *pval;
	Value tempval;

	Entry t;
	QRegExp numberRegExp("^([0-9]+[\\.,]?[0-9]*(e[+-])?[0-9]*)");
	QRegExp signsRegExp(QString("[-/+*&!=<>\\|(]"));
	QRegExp variableRegExp("^\\$([0-9a-z_]+)");

	s=s.simplified();
	//pridanie zatvorky na zaciatok zoznamu
	expr.clear();
	t.clear();
	t.type = BRACKET;
	t.sign = BRAC_OP; 
	t.priority = 0; 
	expr.append(t);

	//prechadzanie vyrazu v tvare (s) od indexu 1 az po predposledny index (tj. nie poslednu zatvorku)
	for (int i = 0; i < s.length(); ++i)
	{
		//odignorovanie medzier
		if (s[i] == ' ') continue;
		t.clear();
		//spracovanie zatvoriek - najnizsia priority 0
		if (s[i] == '(') {
			t.sign = BRAC_OP;
			t.type = BRACKET;
			t.priority = 0;
			expr.push_back(t);
		}
		else if (s[i] == ')') {
			t.sign = BRAC_CL;
			t.type = BRACKET;
			t.priority = 0;
			expr.push_back(t);
		}
		//spracovanie znamienok '+', '-'
		else if (s[i] == '+' || s[i] == '-') {
			//ak je znamienko + alebo - bez medzery pred cislom a jeho predchodca je tiez
			//znamienko tak sa toto znamienko prida k cislu a nebude sa vyhodnocovat
			if ( i-1 <0 || (i+1<s.length() && signsRegExp.exactMatch(QString(s[i - 1])) 
										   && s[i + 1] != ' ')) {                                                           
				if (s[i] == '-') zaporne = zaporne == true ? false : true;
			}
			else {
				t.sign = s[i].toAscii()=='+'?SIGN_PLUS:SIGN_MINUS;
				t.type = SIGN;
				t.priority = 7;
				expr.push_back(t);
			}
		}
		//spracovanie znamienok '*', '/'
		else if (s[i] == '*') {
			t.sign = SIGN_MUL;
			t.type = SIGN;
			t.priority = 6;
			expr.push_back(t);
		}
		else if (s[i] == '/') {
			t.sign = SIGN_DIV;
			t.type = SIGN;
			t.priority = 6;
			expr.push_back(t);
		}
		else if (s[i] == '%') {
			t.sign = SIGN_MOD;
			t.type = SIGN;
			t.priority = 6;
			expr.push_back(t);
		}
		//spracovanie znamienok '&', '|' - priority 1
		else if (s[i] == '&' || s[i] == '|') {
			t.type = SIGN;
			if (s[i] == '&') { 
				t.priority = 14;
				t.sign = SIGN_AND;
			}
			else {
				t.priority = 15;
				t.sign = SIGN_OR;
			}
			expr.push_back(t);
		}
		//spracovanie znamienok '<', '>' - priority 2
		else if (s[i] == '<')
		{
			t.type = SIGN;
			t.sign = SIGN_LT;
			t.priority = 10;
			if (s[i+1] == '=') {
				t.sign = SIGN_LE;
				++i;
			}
			expr.push_back(t);
		}
		else if (s[i] == '>')
		{
			t.type = SIGN;
			t.sign = SIGN_GT;
			t.priority = 10;
			if (s[i+1] == '=') {
				t.sign = SIGN_GE;
				++i;
			}
			expr.push_back(t);
		}
		else if (s[i] == '=' && s[i+1] == '=')
		{
			t.type = SIGN;
			t.sign = SIGN_EQ;
			t.priority = 10;
			++i;
			expr.push_back(t);
		}
		else if (s[i]=='!' && s[i+1]=='=') 
		{
			t.type = SIGN;
			t.priority = 10;
			t.sign = SIGN_NE;
			++i;
			expr.push_back(t);
		}
		// spracovanie znamienka negacie '!' - priority 1
		else if (s[i] == '!') {
			t.sign = SIGN_NOT;
			t.type = SIGN;
			t.priority = 3;
			expr.push_back(t);
		}
		//spracovanie cisiel
		else if (s[i].isDigit())
		{
			temp.clear();
			int pos = i;
			pos = numberRegExp.indexIn(s,pos,QRegExp::CaretAtOffset);
			if (pos == -1) {
				error = "Expr error: bad VALUE format"; 
				return;
			}
			temp = numberRegExp.cap(1);
			//osetrenie vstupu v tvare 3e+3 alebo 3e-3
			i += numberRegExp.matchedLength() - 1;
			t.type = VALUE;
			bool ok=true;
			int integer = temp.toInt(&ok);
			if (!ok) {
				double floating = temp.toDouble(&ok);
				if (!ok) {
					error = "Expr error: bad VALUE format"; 
					return;
				}
				else t.value.setDouble(floating);
			}
			else t.value.setInt(integer);

			//ak zaporny tak sa ulozi value zaporna - je ovplyvnene prve cislo (aj variableRegExp alebo funkcia) po zmene hodnoty premennej zaporny
			if (zaporne) { 
				switch (t.getValueType()) {
					case INTEGER:
						t.value.integer *= -1;
						break;
					case FLOATING:
						t.value.floating *=-1;
						break;
					default: 
						break;
				}
				zaporne = false; 
			}
			expr.push_back(t);
		}
		else if (s[i] == '\"') {
			++i;
			temp = "";
			
			while (i < s.length()-1 && s[i]!='"') {
				if (s[i] == '\\' && i+1 < s.length()-1) {
					
					temp += s[i+1];
					i+=2;
					continue;
				}
				temp += s[i];
				++i;
			}

			if (i >= s.length()) {
				error = "Expr error: string format!";
				return;
			}

			t.type = VALUE;
			t.value.setString(temp);
			expr.push_back(t);
		}
		else if (s[i] == '$') {
			// v temp budem ukladat nazov premennej
			temp.clear();
			int pos = variableRegExp.indexIn(s, i, QRegExp::CaretAtOffset);
			if (pos == -1) {
				error = "Expr error: variable name!";
				return;
			}
			temp = variableRegExp.cap(1);
			i += variableRegExp.matchedLength();
			if (temp.isEmpty()) {
				error = "Expr error: bad variable name!";
				return;
			}
			if (i < s.length() && s[i] == '[') {
				QString indexscript;
				if (!parseArrayIndex(s,i,indexscript)) {
					error = "Expr error: bad array index!";
					return;
				}
				Expression express(functeval);
				express.setExpression(indexscript);
				Value val = express.eval();
				if (val.type == VALUE_ERROR) {
					error = "Expr error: bad array index!";
					return;
				}
				pval = functeval->getVariable(temp+'['+(QString)val+']');
				if (pval == NULL) {
					error = "Expr error: Bad array index!";
					return;
				}
			}
			else {
				pval = functeval->getVariable(temp);
				if (pval == NULL) {
					error = "Expr error: Variable does not exist!";
					return;
				}
			}
	
			t.type = VALUE;
			t.value = *pval;
			
			if (i < s.length() && s[i] == '.') {
				//spracovanie funkcii
				++i;
				if (!parseFunctDef(s,i,funct)) {
					error="Expr error: function error!";
					return;
				}
				tempval = functeval->eval(funct,t.value);
				if (tempval.type == VALUE_ERROR) {
					error="Expr error: function error!";
					return;
				}
				t.value = tempval;
			}

			if (zaporne) { 
					switch (t.getValueType()) {
						case INTEGER:
							t.value.integer *= -1;
							break;
						case FLOATING:
							t.value.floating *=-1;
							break;
						case VALUE_ERROR:
							error="Expr error: function error!";
							return;
						default: 
							break;
					}
					zaporne = false; 
			}
			expr.push_back(t);
			--i;
		}
		// spracovanie funkcii - mozu obsahovat cisla a znaky malej abecedy
		// funkcie sa vyhodnotia a ulozia ako vysledok
		else                 
		{
			if (!parseFunctDef(s,i,funct)) {
				error="Expr error: function error!";
				return;
			}

			t.type = VALUE;
			if (!funct.isEmpty()) {
				tempval = functeval->eval(funct);
				if (tempval.type == VALUE_ERROR) //|| pval->type == VOID) 
				{
					error="Expr error: function error!";
					return;
				}
				t.value = tempval;
				if (zaporne) { 
					switch (t.getValueType()) {
						case INTEGER:
							t.value.integer *= -1;
							break;
						case FLOATING:
							t.value.floating *=-1;
							break;
						case VALUE_ERROR:
							error="Expr error: function error!";
							return;
						default: 
							break;
					}
					zaporne = false; 
				}
			}
			expr.push_back(t);
			--i;
		}
	}
	//pridanie zatvorky na koniec zoznamu
	t.type = BRACKET;
	t.sign = BRAC_CL; 
	t.priority = -1;
	expr.push_back(t);

	//osetrenie chyb v infixnom vyraze - infixtopostfix niektore chyby vo vyraze ignorovalo a potom zle pocitalo
	for (int j = 1; j <= expr.count() - 2; ++j)
	{
		if (expr[j].type == SIGN && (expr[j - 1].type == SIGN || expr[j + 1].type == SIGN)) 
		{
			error = "Expr error: 2 signs following each other ";
			break;
		}
		if (expr[j].type == VALUE && (expr[j-1].type==VALUE || expr[j+1].type==VALUE)) 
		{
			error = "Expr error: sign is missing";
			break;
		}
		if (expr[j].sign == BRAC_OP && (expr[j-1].type==VALUE || 
			((expr[j+1].type==SIGN) && expr[j+1].sign != SIGN_NOT)))
		{
			error = "Expr error: opening bracket!";
			break;
		}
		if (expr[j].sign == BRAC_CL && (expr[j-1].type==SIGN || expr[j+1].type==VALUE))
		{
			error="Expr error: closing bracket!";
			break;
		}
	}
	if ((expr[1].type == SIGN && expr[1].sign != SIGN_NOT) || expr[expr.count() - 2].type == SIGN) 
		error = "Expr error: sign not expected";          
}

//metoda prevedie ciselny infixny zapis so znamienkami a zatvorkami na postfixny zapis
void Expression::infixtopostfix()
{
	//zasobnik na ukladanie znamienok
	QStack<Entry> zas;
	//pomocne pole na vysledok
	QVector<Entry> t;

	for (int i = 0; i < expr.count(); ++i)
		switch (expr[i].type)
		{
			//cisla idu na vystup (v nasom pripade vysledny Expression)
			case VALUE: 
				t.push_back(expr[i]); 
				break;
			//pri pridavani signsRegExp najskor vyprazdnit zo zasobnika signsRegExp s vyssou alebo rovnakou prioritou
			case SIGN:
				while ((!zas.isEmpty()) && (zas.top().sign!=BRAC_OP) && (zas.top().priority <= expr[i].priority))
					t.push_back(zas.pop());
				zas.push(expr[i]);
				break;
			//pridanie zatvorky '('
			//uzatvaracia zatvorka vyhodi vsetky signsRegExp az po dalsiu otvaraciu zatvorku na vystup
			case BRACKET:
				if (expr[i].sign == BRAC_OP) 
					zas.push(expr[i]);
				else {
					while (!zas.isEmpty() && ( zas.top().sign != '('))
						t.push_back(zas.pop()); 
					if (!zas.isEmpty()) zas.pop();
				}
				break;
			default: { }
				break;
		}
	expr = t;
}


//metoda vyhodnoti vyraz ktory obsahuje len cisla a signsRegExp v postfixnej notacii
Value Expression::evaluatepostfix()
{
	QStack<Value> zas;  //zasobnik pouzity pri vyhodnocovani
	Value a, b, t;       //pomocne premenne

	for (int i = 0; i < expr.count(); ++i) 
	{
		if (expr[i].type == VALUE) 
			zas.push(expr[i].value);
		else if (expr[i].type == SIGN) {
			switch (expr[i].sign)
			{
				case SIGN_PLUS:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer + b.integer));
							break;
						case FLOATING:
							zas.push(Value(a.floating + b.floating));
							break;
						case STRING:
							zas.push(Value(a.str + b.str));
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_MINUS:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer - b.integer));
							break;
						case FLOATING:
							zas.push(Value(a.floating - b.floating));
							break;
						case STRING:
							error = "Can't substract 2 strings!";
							return Value();
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_MUL:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer * b.integer));						
							break;
						case FLOATING:
							zas.push(Value(a.floating * b.floating));							
							break;
						case STRING:
							error = "Can't multiply 2 strings!";
							return Value();
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_DIV:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							if (b.integer == 0) {
								error = "Division by zero!";
								return Value();
							}
							zas.push(Value((double)a.integer / (double)b.integer));							
							break;
						case FLOATING:
							if (b.floating == 0) {
								error = "Division by zero!";
								return Value();
							}
							zas.push(Value(a.floating / b.floating));							
							break;
						case STRING:
							error = "Can't divide 2 strings!";
							return Value();
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_MOD:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							if (b.integer == 0) {
								error = "Division by zero!";
								return Value();
							}
							zas.push(Value(a.integer % b.integer));							
							break;
						case FLOATING:
							error = "Can't mod floating numbers!";
							return Value();
						case STRING:
							error = "Can't mod 2 strings!";
							return Value();
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_AND:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer && b.integer));							
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_OR:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer || b.integer));							
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_LT:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer < b.integer));							
							break;
						case FLOATING:
							zas.push(Value(a.floating < b.floating));							
							break;
						case STRING:
							zas.push(Value(a.str < b.str));							
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_GT:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer > b.integer));							
							break;
						case FLOATING:
							zas.push(Value(a.floating > b.floating));							
							break;
						case STRING:
							zas.push(Value(a.str > b.str));							
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_NOT:
					a = zas.pop(); 
					switch (a.type) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer==0?true:false));
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_LE:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer <= b.integer));
							break;
						case FLOATING:
							zas.push(Value(a.floating <= b.floating));
							break;
						case STRING:
							zas.push(Value(a.str <= b.str));
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_GE:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer >= b.integer));
							break;
						case FLOATING:
							zas.push(Value(a.floating >= b.floating));
							break;
						case STRING:
							zas.push(Value(a.str >= b.str));
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_EQ:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer == b.integer));
							break;
						case FLOATING:
							zas.push(Value(a.floating == b.floating));
							break;
						case STRING:
							zas.push(Value(a.str == b.str));
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					 }
					break;
				case SIGN_NE:
					b = zas.pop(); 
					a = zas.pop(); 
					switch (Value::convert( a, b)) {
						case BOOL:
						case INTEGER:
							zas.push(Value(a.integer != b.integer));
							break;
						case FLOATING:
							zas.push(Value(a.floating != b.floating));
							break;
						case STRING:
							zas.push(Value(a.str != b.str));
							break;
						default: 
							error = "Can't execute operation! - mismatching types!";
							return Value();
					}
					break;
				default: break;
			}

		}
	}
	if (zas.isEmpty()) {
		error = "Error in expression!";
		return Value();
	}
	a = zas.pop();
	//toto je maly hack kedze pri pridani funkcie zaporneho cisla v niektorych specifickych situaciach nemame znamienko 
	//cisla ktore toto znamienko zobrali su bud kladne alebo zaporne (tj. staci zostavajuce cisla scitat)
	//napr. 3-sin(0)-3 = 6 bez tejto upravy
	while (!zas.isEmpty())
	{
		b = zas.pop();
		switch (Value::convert( a, b)) {
			case BOOL:
			case INTEGER:
				a.integer+=b.integer;
				break;
			case FLOATING:
				a.floating+=b.floating;
				break;
			case STRING:
				a.str+=b.str;
				break;
			default: 
				error = "Can't execute operation! - mismatching types!";
				return Value();
		}
	}
	return a;
}

/* class Trigger */
static QRegExp assign;
static QRegExp returning;
static QRegExp branch;
static QRegExp elseline;
static QRegExp whilecycle;
static QRegExp gotoexpr;

bool createPatterns() {
	assign.setPattern("^\\s*([0-9a-z_]+)(?:\\[(.*)\\])?\\s*:=\\s*(.*)\\s*$");
	returning.setPattern("^\\s*return\\s+(.*)\\s*$");					//return vyr
	branch.setPattern("^\\s*if\\s*\\((.*)\\)\\s*(.*)\\s*$");			//podmienky (cap(2) je kvoli if za kt. nenasleduje zlozeny prikaz
	elseline.setPattern("^\\s*else\\s+(.*)\\s*$");						//else v podmienke 
	whilecycle.setPattern("^\\s*while\\s*\\((.*)\\)\\s*(.*)\\s*$");	    //cyklus while
	gotoexpr.setPattern("^\\s*goto\\s+(\\d*)\\s*$");
	return true;
}

static bool patt = createPatterns();

Trigger::Trigger() : RPGObject()
{
	this->type = NOTRIGGER;
	running = false;
	remove = false;
	constructorDef();
}

void Trigger::constructorDef()
{
	error.clear();
	eval = new FunctionEval(this);
	expr = new Expression(eval);
	enabled = false;
	item = NULL;
	node = NULL;
}

Trigger::Trigger(const Trigger &obj) : RPGObject(obj)
{
	running = false;
	remove = false;
	operator=(obj);
}

Trigger &Trigger::operator=(const Trigger &trig)
{
	script = trig.script;
	remove = false;
	lines = trig.lines;
	error = trig.error;
	eval = new FunctionEval(*trig.eval);
	eval->setTrigger(this);
	expr = new Expression(*trig.expr);
	expr->setFunctEval(eval);
	item = trig.item;
	node = trig.node;
	object = trig.object;
	monster = trig.monster;
	enabled = trig.enabled;
	type = trig.type;
	
	return *this;
}

Trigger::~Trigger()
{
	delete eval;
	delete expr;
}

QString Trigger::getInfo() const
{
	return script;
}

void Trigger::setObject(RPGObject *obj)
{
	this->object = obj;
}

void Trigger::setItem(InventoryObject *obj) 
{
	if (obj == NULL)
		return;
	type = ITEMTRIGGER;
	this->location()->setItem(obj);
	this->item = obj; 
}

void Trigger::setMonster(Monster *mon)
{
	if (mon == NULL)
		return;
	type = MONSTERTRIGGER;
	this->location()->setMonster(mon);
	this->monster = mon;
}

void Trigger::setNode(MapNode *node)
{
	type = MAPTRIGGER;
	this->node = node;
}


void Trigger::setScript(const QString &script)
{
	QStringList temp;
	this->script = script;
	error.clear();
	enabled = false;

	//spracovanie skriptu
	QString tmp;
	tmp = script;
	tmp = tmp.replace(QString("{"),";{;");		//chcem dostat { resp } na samostatnu polozku
	tmp = tmp.replace(QString("}"),";};");
	lines=tmp.split(";");
	for(QStringList::iterator it= lines.begin();it != lines.end(); ++it) {
		*it = it->simplified();
		
		if (it->isEmpty()) it = lines.erase(it);
		if (it == lines.end()) break;

		if (branch.exactMatch(*it)) {
			*it = QString("if (%1)").arg(branch.cap(1).simplified());
			tmp = branch.cap(2).simplified();
			if (!tmp.isEmpty()) it = lines.insert(++it,tmp);
		}
		else if (elseline.exactMatch(*it)) {
			temp = elseline.capturedTexts();
			*it = QString("else");
			tmp = elseline.cap(1).simplified();
			if (!tmp.isEmpty()) it = lines.insert(++it,tmp);
		}
		if (whilecycle.exactMatch(*it)) {
			*it = QString("while (%1)").arg(whilecycle.cap(1).simplified());
			tmp = whilecycle.cap(2).simplified();
			if (!tmp.isEmpty()) it = lines.insert(++it,tmp);
		}
	}

	// nahradenie while, break, continue s goto <riadok>
	QString whilecond;
	int jumpindex;
	int s, e;
	for(int i=0; i < lines.count(); ++i) {
		if (whilecycle.exactMatch(lines[i])) {
			whilecond = whilecycle.cap(1).simplified();
			lines[i] = QString("if (%1)").arg(whilecond);
			i++;
			jumpindex = i;
			if (i < lines.count() && lines[i] != "{") {
				if (lines[i] == "break" || lines[i] == "continue") {
					lines.removeAt(i);
					lines.removeAt(i-1);
					i -=2;
					continue;
				}
				lines.insert(i+1, QString("}"));
				lines.insert(i+1, QString("goto %1").arg(jumpindex+2));
				lines.insert(i+1, QString("if (%1)").arg(whilecond));
				lines.insert(i, QString("{"));
				i+=4;
				continue;
			}
			if (i >= lines.count() || lines[i] != "{") {
				error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
				return;
			}
			s = i;
			findBlock(s,e);
			if (s == -1 || e ==  -1) {
					error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
					return;
			}
			lines.insert(e, QString("goto %1").arg(jumpindex+2));
			lines.insert(e, QString("if (%1)").arg(whilecond)); 
			// break:   goto e+4		// continue: goto i
			for(int j=i; j < e; ++j) {
				if (lines[j] == "break") 
					lines[j] = QString("goto %1").arg(e+4);
				else if (lines[j] == "continue")
					lines[j] = QString("goto %1").arg(i);
				else if (whilecycle.exactMatch(lines[j])) {
					if (j+1 < lines.count() && lines[j+1] != "{") {
						++j;
						continue;
					}
					if (j+1 < lines.count() && lines[j+1] == "{") {
						int s2 = j+1;
						int e2;
						findBlock(s2,e2);
						if (s2 == -1 || e2 ==  -1) {
								error = tr("Error in script: (line: %1) %2").arg(j+1).arg(lines[j]);
								return;
						}
						j = e2;
						continue;
					}
				}
			}
		}
	}
	
	enabled = true;
}

void Trigger::setVariables(const TriggerVariables &vars) 
{
	eval->setVariables(vars);
}

const TriggerVariables &Trigger::getVariables()
{
	return eval->getVariables();
}

Value Trigger::execute()
{
	running = true;
	if (!enabled) return Value();
	eval->clearVariables();
	Value temp = evalBlock(0, lines.size());
	if (!temp.getError().isEmpty())
		campaign->writeStatus(temp.getError());
	if (remove) {
		this->release();
		return temp;
	}
	running = false;
	return temp;
}

bool Trigger::isRunning() 
{
	return running;
}

//vyhodnoti blok prikazov na poziciach lines[<start;end)]
Value Trigger::evalBlock(int start, int end)
{
	if (start >= lines.size() || end > lines.size() || start < 0 || end <0) return false;

	QStringList tmp;
	Value t;
	FunctionDef f;
	FunctionDefinition funct;
	Value val;
	int n = lines.size();
		
	bool ok;
	int tmpindex;

	for(int i=start; i < end; ++i) 
	{
		if (lines[i].isEmpty()) continue;

		if (lines[i] == "}" || lines[i] == "{" ) 
			continue;

		// priradenie hodnoty premennej z vyrazu
		if (assign.exactMatch(lines[i])) {
			tmp = assign.capturedTexts();
			if (assign.cap(1).isEmpty() || assign.cap(3).isEmpty()) {
				error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
				return Value::createError(error);
			}
			expr->setExpression(assign.cap(3).simplified());
			QString varname = assign.cap(1).simplified();
			if (!assign.cap(2).isEmpty()) {
				Expression express(eval);
				express.setExpression(assign.cap(2).simplified());
				val = express.eval();
				if (val.type == VALUE_ERROR) {
					error = tr("Error in script: (line: %1) %2: %3").arg(i+1).arg(lines[i]).arg(val.getError());
					return Value::createError(error);
				}
				name = QString("%1[%2]").arg(varname).arg((QString)val);				
				val = expr->eval();
				if (val.type == VALUE_ERROR) {
					error = tr("Error in script: (line: %1) %2: %3").arg(i+1).arg(lines[i]).arg(val.getError());
					return Value::createError(error);
				}
				eval->setVariable(name, new Value(val));
				Value * pval = eval->getVariable(varname);
				if (pval == NULL || pval->type != INTEGER) {
					eval->setVariable(varname,new Value(1));
				}
				else 
					eval->setVariable(varname,new Value(pval->getInt()+1));
				continue;
			}
			val = expr->eval();
			if (val.type == VALUE_ERROR) {
				error = tr("Error in script: (line: %1) %2: %3").arg(i+1).arg(lines[i]).arg(val.getError());
				return Value::createError(error);
			}
			eval->setVariable(varname, new Value(val));						
			continue;
		}

		// goto 
		if (gotoexpr.exactMatch(lines[i])) {
			tmpindex = gotoexpr.cap(1).toUInt(&ok);
			if (!ok) {
				error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
				return Value::createError(error);
			}
			i = tmpindex - 2;
			continue;
		}

		// podmienky
		if (branch.exactMatch(lines[i])) {
			expr->setExpression(branch.cap(1).simplified());
			val = expr->eval();
			if (val.type == VALUE_ERROR) {
				error = tr("Error in script: (line: %1) %2: %3").arg(i+1).arg(lines[i]).arg(val.getError());
				return Value::createError(error);
			}
			if (val) {
				//vykonavame tuto vetvu ak podmienka vyhodnotena ako true
				//najskor vykoname prikaz ktory bol v tele prikazu if (tj. ak za if nenasleduje zlozeny prikaz)
				if (i+1 < n && lines[i+1] != "{") {
					continue;
				}
				//vykonave zlozeny prikaz 
				if (i+1 < n && lines[i+1] == "{") {
					i++;
					continue;
				}
				error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
				return Value::createError(error);
			}
			else {
				//tuto vetvu vykonavame ak podmienka neplati
				//najskor preskocime blok prikazov ktore by nastali ak by podmienka platila
				if (i+1 < n && lines[i+1] == "{") {
					int s = i+1;
					int e;
					findBlock(s,e);
					if (s == -1 || e ==  -1) {
							error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
							return Value::createError(error);
					}
					i = e+1;
				}
				else i+=2;
				//vykoname prikaz v tele else (tj. ak za else nenasleduje zlozeny prikaz)
				if (i < n && lines[i] == "else") {
					if (i+1 < n && lines[i+1] != "{") 
						continue;
					else if (i+1 < n && lines[i+1] == "{") {
						i++;
						continue;
					}
					error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
					return Value::createError(error);
					
				}
				else {
					--i;
					continue;
				}
			}
		}
		// preskocime nevykonane else
		else if (lines[i] == "else") {
			if (i+1 < n && lines[i+1] != "{") {
					i++;
					continue;
			}
			if (i+1 < n && lines[i+1] == "{") {
				int s = i+1;
				int e;
				findBlock(s,e);
				if (s == -1 || e ==  -1) {
						error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
						return Value::createError(error);
				}
				i = e;
				continue;
			}
			return val;
		}

		// return
		if (returning.exactMatch(lines[i])) {
			expr->setExpression(returning.cap(1).simplified());
			val = expr->eval();
			if (val.type == VALUE_ERROR) {
				error = tr("Error in script: (line: %1) %2: %3").arg(i+1).arg(lines[i]).arg(val.getError());
				return Value::createError(error);
			}
			return val;
		}

		if (lines[i].at(0) == '$') {
			expr->setExpression(lines[i]);
			val = expr->eval();
			if (val.type == VALUE_ERROR) {
				error = tr("Error in script: (line: %1) %2: %3").arg(i+1).arg(lines[i]).arg(val.getError());
				return Value::createError(error);
			}
			continue;
		}

		//spracovanie funkcii
		int pos = 0;
		if (!parseFunctDef(lines[i], pos, funct)) {
			error = tr("Error in script: (line: %1) %2").arg(i+1).arg(lines[i]);
			return Value::createError(error);
		}
		t = eval->eval(funct);
	}
	return t;
}

void Trigger::findBlock(int &s, int &e)
{
	if (lines[s] != "{") {
		s = -1; 
		e = -1;
		return;
	}
	++s;
	int i = s;
	int z = 1;   //udava pocet neuzavretych zatvoriek vovnutri argumentu
	while ((i < lines.size()) && z != 0)	{
		if (lines[i] == "{") ++z;
		else if (lines[i] == "}") --z;
		if (z != 0) 
			++i;
	}
	if ( i < lines.size() && lines[i] == "}") {
		e = i;
	}
	else {
		s = -1;
		e = -1;
	}
}
