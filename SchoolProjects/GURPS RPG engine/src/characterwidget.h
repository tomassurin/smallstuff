/******************************************************************************
 * Implementacia widgetov pre zobrazovanie informacii o postave a dialogy hry
******************************************************************************/
#ifndef RPGCharacterWIDGET_H
#define RPGCharacterWIDGET_H

#include <QTabWidget>
#include <QWidget>
#include <QMap>
#include <QtGui>
#include <QVector>
#include <QTableView>
#include <QDialog>

#include "gameobjects.h"

class QTextEdit;

namespace RPG {

	static int InventoryWidgetSize=500;
	static int CharacterWidgetWidth=400;
	static int CharacterWidgetHeight=685;	

	class PlayerNode;

	/** Trieda reprezentuje dialog s dotazom na hraca */
	class RPGDialog : public QDialog
	{
		Q_OBJECT
	public:
		RPGDialog (QWidget *parent = 0);
	
		/** Nastavime hlasku ktoru dialog zobrazi */
		void setText(const QString &msg);
		/** Pridame odpoved msg do dialogu. Vrati tlacitko, reprezentujuce odpoved */
		QPushButton* addAnswer(const QString &msg);
		/** Vrati tlacitko, ktore bolo vybrate */
		QPushButton* clickedButton();
		/** Vymaze dialog */
		void clear();

	public slots:
		/** Zobrazi dialog s hlaskou message a hlaskami v zozname retazcov answers.
		  * Vrati poradove cislo hlasky, ktora bola vybrana */
		int display(const QString &message, const QStringList &answers);
		/** Zobrazi dialog s hlaskou specifikovanou v cez metodu setText a odpovedami 
		  * specifikovanymi metodami addAnswer. Vrati vysledok dialogu */
		int display();
	private:
		QVBoxLayout *layout;
		QSignalMapper *sigmap;
		QLabel *text;
		QVector<QPushButton*> answers;
	};

	/** Trieda reprezentujuca klikatelny popisok */
	class MyLabel : public QLabel 
	{
		Q_OBJECT
	public:
		MyLabel(const QString &str) : QLabel(str) { };
	signals:
		void clicked();
	private:
		void mousePressEvent(QMouseEvent *ev) {
			emit clicked();
		}
	};

	/* Widget na zobrazovanie informacii o campani */
	class CampaignInfo : public QGroupBox
	{
		Q_OBJECT
	public:
		CampaignInfo(QWidget *parent=0);

		/** Nastavime hraca */
		void setPlayer(Player *pl);

	public slots:
		void updateCampaignName();
		void updateMapInfo();
		void updateTurnNumber();
		void updateMovePoints();
		void establishConnections();

	private:
		QLabel *campName;
		QLabel *mapName;
		QLabel *turnNumber;
		QLabel *movePoints;
		
		Player *player;
		bool establishedConn;
	};

	/** Widget na zobrazenie statov postavy
	  *	Obsahuje jednoduchy textovy prehlad statistik postavy */
	class RPGCharacterSheet : public QWidget
	{
		Q_OBJECT
	public:
		RPGCharacterSheet(QWidget *parent=0);
	
		/** Nastavime zobrazovany text */
		void setText(QString &str);
	private:
		QTextEdit *stats;
	};

	/** Dialog na vylepsenie dovednosti postavy */
	class SkillLevelDialog : public QDialog
	{
		Q_OBJECT
	public:
		SkillLevelDialog(QWidget *parent = 0);
	public slots:
		/** Zobrazime dialog s dovednostami hraca pl */
		void display(PlayerNode *pl);
		/** Update informacii prave vybratej dovednosti */
		void updateInfo(int i);
	private:
		QLabel *label;
		QComboBox *skills;
		QLabel *info;

		QPushButton *okbutton;
		QPushButton *cancelbutton;

		// nazvy dovednosti - pre potreby updateInfo, parameter i v metode udava index do tohoto pola
		QList<QString> names;
		PlayerNode *pl;
	};

	/** Dialog pre pracu s predmetmi */
	class ItemDialog : public QDialog
	{
		Q_OBJECT
	public:
		ItemDialog(Inventory *inv, QWidget *parent=0);

		enum ItemDialogResult { DropItem = 1000, UseItem = 1001};

	public slots:
		/** Zobrazi dialog predmetu na indexe index inventara uvedeneho v konstruktore */
		void display(const QModelIndex &index);
		/** Nastavenie navratovej hodnoty dialogu na DropItem */
		void dropItem();
		/** Nastavenie navratovej hodnoty dialogu na UseItem */
		void useItem();

	private:
		QTextEdit *stats;
		QLabel *name;
		QLabel *weight;
		QLabel *minst;

		QLabel *label;
		QComboBox *location;

		QPushButton *okbutton;
		QPushButton *cancelbutton;
		QPushButton *dropbutton;
		QPushButton *usebutton;

		Inventory *inventory;
		// 
		QMap<QString,BodyLocation> RPGBody;
	};

	/** Widget pre zobrazenie inventara postavy */
	class InventoryWidget : public QTableView
	{
		Q_OBJECT
	public:
		InventoryWidget(Inventory *model, QWidget *parent=0);
		~InventoryWidget();

		friend class CharacterWidget;

	public slots:
		/** Funkcia sluzi na nastavenie vysky novo pridaneho riadku */
		void updateRowHeight(const QModelIndex& parent, int start, int end);
	private:
		Inventory *model;
		ItemDialog *itemdialog;
	};
	
	/** Spojenie viacerych widgetov popisujucich stav postavy  do zaloziek */
	class CharacterWidget : public QWidget
	{
			Q_OBJECT
	public:
		CharacterWidget(QWidget *parent=0);
		
		/** Vymaze CharacterWidget */
		void clear();
		
		/** Nastavi aktualneho hraca */
		void setPlayer(Player *player);

		friend class Campaign;

	public slots:
		/** Updatne data widgetu */
		void update();

	protected:
		CampaignInfo *campInfo;
		RPGCharacterSheet *sheet;
		InventoryWidget *inventory;
		
		QTabWidget *tabs;
		Player *player;
	};
}
#endif