#include <QtGui>
#include <QDockWidget>

#include "mainwindow.h"
#include "xmlreader.h"
#include "rpgcampaign.h"

using namespace RPG;

NewCharacterDialog::NewCharacterDialog(QWidget *parent) : QDialog(parent)
{
	setupUi(this);
	QList<QString> skills = rules.getSkillsNames();
	skill1Box->addItem("None");
	skill1Box->setCurrentIndex(0);
	skill2Box->addItem("None");
	skill2Box->setCurrentIndex(0);
	skill3Box->addItem("None");
	skill3Box->setCurrentIndex(0);
	skill4Box->addItem("None");
	skill4Box->setCurrentIndex(0);
	skill5Box->addItem("None");
	skill5Box->setCurrentIndex(0);

	QList<QString>::const_iterator it;
	for(it = skills.begin();it != skills.end();++it) {
		skill1Box->addItem(*it);
		skill2Box->addItem(*it);
		skill3Box->addItem(*it);
		skill4Box->addItem(*it);
		skill5Box->addItem(*it);
	}
}

PlayerNode *NewCharacterDialog::getCharacter()
{
	PlayerNode *pl = new PlayerNode();

	pl->setName(nameBox->text());
	pl->setSize(sizeBox->value());
	pl->setST(stBox->value());
	pl->setDX(dxBox->value());
	pl->setIQ(iqBox->value());
	pl->setHT(htBox->value());
	if (skill1Box->currentText() != "None") {
		pl->addSkill(skill1Box->currentText(),0);
	}
	if (skill2Box->currentText() != "None") {
		pl->addSkill(skill2Box->currentText(),0);
	}
	if (skill3Box->currentText() != "None") {
		pl->addSkill(skill3Box->currentText(),0);
	}
	if (skill4Box->currentText() != "None") {
		pl->addSkill(skill4Box->currentText(),0);
	}
	if (skill5Box->currentText() != "None") {
		pl->addSkill(skill5Box->currentText(),0);
	}
	return pl;
}

MainWindow::MainWindow(QWidget *parent, Qt::WFlags flags)
	: QMainWindow(parent, flags)
{
	campaign = new RPG::Campaign(this);
	setCentralWidget(campaign);
	//this->setFixedSize(campaign->sizeHint());

	//inicializacia generatora nahodnych cisel
	srand(QTime(0,0,0).secsTo(QTime::currentTime()));

	createActions();
	createMenus();

	setWindowTitle(tr("Rpg Game Project"));
	campaign->writeStatus(tr("> <b>Application is ready</b>"));
	campaign->writeStatus(tr("> To start game load campaign file"));

	connect(campaign,SIGNAL(PlayerEnded()),this,SLOT(PlayerEnded()));
}

void MainWindow::openCampaign() 
{
	QString filename= 
		QFileDialog::getOpenFileName(this,tr("Open campaign file"),
									QDir::currentPath(),
									tr("XML Files (*.xml)"));
	if (filename.isEmpty())
		return;

	QFile file(filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) {
		campaign->writeStatus(tr("> Cannot read file %1:\n%2.").arg(filename).arg(file.errorString()));
		return;
	}
	QFileInfo fileInfo(file);

	XMLReader::CampaignXMLReader reader(&file, fileInfo.absolutePath());
	if (reader.read()) {
		campaign->writeStatus(tr("> Parse error in file %1 at line %2, column %3:\n%4")
					.arg(filename).arg(reader.lineNumber()).arg(reader.columnNumber()).arg(reader.errorString()));
	} else {	
		if (rules.isValid()) {
			campaign->writeStatus(tr("> <b>Campaign data loaded</b>"));
			campaign->writeStatus(tr("> Now load your Character file"));

			setWindowTitle(campaign->getName());	

			openCampaignAct->setDisabled(true);
			openCharacterAct->setEnabled(true);
			newCharacterAct->setEnabled(true);
		}
		else {
			campaign->writeStatus(tr("> <b>Error loading campaign data: invalid rules specified</b>"));
			campaign->clear(); 
		}
	}
}

void MainWindow::PlayerEnded()
{
	openCharacterAct->setEnabled(true);
	newCharacterAct->setEnabled(true);
	campaign->clearPlayer();
}

void MainWindow::openCharacter() 
{
	QString filename= 
		QFileDialog::getOpenFileName(this,tr("Open Character file"),
									QDir::currentPath(),
									tr("XML Files (*.xml)"));
	if (filename.isEmpty())
		return;

	QFile file(filename);
	if (!file.open(QFile::ReadOnly | QFile::Text)) {
		campaign->writeStatus(tr("> Cannot read file %1:\n%2.").arg(filename).arg(file.errorString()));
		return;
	}
	QFileInfo fileInfo(file);
	
	PlayerNode *player=new PlayerNode();
	player->setMap(campaign->getCurrentMap());

	XMLReader::PlayerXMLRead reader(&file, fileInfo.absolutePath(), player);
	if (reader.read()) {
		campaign->writeStatus(tr("> Parse error in file %1 at line %2, column %3:\n%4")
					.arg(filename).arg(reader.lineNumber()).arg(reader.columnNumber()).arg(reader.errorString()));
		delete player;
	} else {
		campaign->writeStatus(tr("> <B>Character data loaded</b>"));
		campaign->addPlayer(player);
		openCharacterAct->setDisabled(true);
		newCharacterAct->setDisabled(true);
	}
}

void MainWindow::newCharacter()
{
	NewCharacterDialog *dial = new NewCharacterDialog(this);
	int res = dial->exec();
	if (res == QDialog::Accepted) {
		PlayerNode *pl = dial->getCharacter();
		if (pl == NULL)
			return;

		pl->setMap(campaign->getCurrentMap());
		pl->drawCircle("blue");
		pl->drawText("PL");
		campaign->writeStatus(tr("> <b>Character data loaded</b>"));
		campaign->addPlayer(pl);
		openCharacterAct->setDisabled(true);
		newCharacterAct->setDisabled(true);
	}
	
}

void MainWindow::close()
{
	campaign->clear();
	newCharacterAct->setEnabled(false);
	openCampaignAct->setEnabled(true);
	openCharacterAct->setEnabled(false);
}

void MainWindow::createActions()
{
	openCampaignAct=new QAction(tr("&Open Campaign"),this);
	openCampaignAct->setShortcut(QKeySequence(tr("Ctrl+O")));
	connect(openCampaignAct,SIGNAL(triggered()),this, SLOT(openCampaign()));
	openCharacterAct=new QAction(tr("Open &Character"),this);
	openCharacterAct->setShortcut(QKeySequence(tr("Ctrl+C")));
	openCharacterAct->setDisabled(true);
	connect(openCharacterAct,SIGNAL(triggered()),this, SLOT(openCharacter()));
	closeAct=new QAction(tr("Close"),this);
	closeAct->setShortcut(QKeySequence(tr("Ctrl+Q")));
	connect(closeAct, SIGNAL(triggered()), this, SLOT(close()));
	exitAct=new QAction(tr("&Exit"),this);
	connect(exitAct,SIGNAL(triggered()), qApp, SLOT(quit()));
	newCharacterAct=new QAction(tr("&New Character"),this);
	newCharacterAct->setDisabled(true);
	newCharacterAct->setShortcut(QKeySequence(tr("Ctrl+N")));
	connect(newCharacterAct,SIGNAL(triggered()),this, SLOT(newCharacter()));
}

void MainWindow::createMenus()
{
	fileMenu=menuBar()->addMenu(tr("&Campaign"));
	fileMenu->addAction(openCampaignAct);
	fileMenu->addSeparator();
	fileMenu->addAction(closeAct);
	fileMenu->addAction(exitAct);
	characterMenu=menuBar()->addMenu(tr("&Character"));
	characterMenu->addAction(newCharacterAct);
	characterMenu->addAction(openCharacterAct);

}