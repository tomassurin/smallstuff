package calculator;

import java.math.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Trieda Reprezentujuca vyraz. */
class Expression {
        /** Retazec v ktorom sa uklada chyba. */
    	private String error = "";
        /** Retazec v ktorom sa uklada originalny nevyparsovany vstup. */
	private String original = "";
        /** Reprezentacia uz ulozeneho vyrazu. */
	private Vector<Entry> expr = new Vector<Entry>();
        /** Referencia na triedu pre vyhodnocovanie funkcii a premennych. */
        private FunctionEval funct = null;

        /** Definicia regularnych vyrazov pouzivanych pri parsovani. */
        private static Pattern numberpattern = Pattern.compile("(\\d+\\.?\\d*([eE][+-]?)?\\d*)");
        /** Definicia regularnych vyrazov pouzivanych pri parsovani. */
        private static Pattern variablepattern = Pattern.compile("\\$([0-9A-Za-z_]+)");
        
        Expression(FunctionEval eval) {
            this.funct = eval;
        }

        /** Nastavy vyhodnocovac funkcii tohoto vyrazu.
         *
         * @param eval Referencia na vyhodnocovac funkcii.
         */
        public void setfunct(FunctionEval eval)
        {
            funct = eval;
        }

        /** Inicializuje vyhodnocovanie vyrazu ulozeneho v retazci str.
         *
         * @param str retazec obsahujuci vyhodnocovany vyraz.
         */
        public void setExpression(String str) {
            original = str;
            error = "";
            expr.clear();
        }

        /** Metoda vyhodnoti vyraz v infixnej notacii nastaveny cez setExpression.
         * @return Vyslednu hodnotu vyrazu.
         */
        public Value eval() {
            // vyparsovanie vyrazu
            parse();
            // ak nie je chyba v sucastnom vyraze tak vyhodnot
            if (error.isEmpty()) {
                infixtopostfix();  // infix - vyparsovany expr prevedieme do postfixu

                Value out = new Value();

                if (error.isEmpty()) {
                    try {
                        out = evaluatepostfix();
                    } catch (ArithmeticException ex) {
                        error = ex.getMessage();
                    }
                }

                if (error.isEmpty()) {
                    funct.setLast(out);
                    return out;
                }
            }
            Value err = new Value();
            err.setError(error);
            funct.setLast(err);
            return err;
        }

        /** Nastavenie presnosti.
         *
         * @param prec Nova presnost.
         */
        public void setPrecision(int prec) {
            if (prec < 0) return;
            MathContext mc = new MathContext(prec);
            funct.setMc(mc);
        }
        /** Ziskanie aktualnej presnosti.
         *
         * @return Aktualna presnost.
         */
        public int getPrecision() {
            return funct.mc().getPrecision();
        }

        /** Vyparsuje vstupny vyraz ulozeny v original do Vector&lt Entry &gt */
        private void parse() {
            //inicializacia premennych a konstant
            boolean negative = false;   //zaporne znamienko - pri zanedbani znamienka '-'
            //pomocne premenne
            int z = 0;
            StringBuffer temp;
            Entry t = new Entry();              //pomocna polozka

            // pridanie zaciatocnej a koncovej zatvorky - kvoli zapornym cislam
            String s = new String("("+original+")");

            expr.clear();
            //pridanie zatvorky na zaciatok vysledneho zoznamu
            t.type = EntryType.BRACKET;
            t.sign = SignType.BRAC_OP;
            t.priority = 0;
            expr.add(t);
            //prechadzanie vyrazu v tvare (s) od indexu 1 az po predposledny index (tj. nie poslednu zatvorku)
            for (int i = 1; i < s.length() - 1; ++i) {
                t = new Entry();
                //spracovanie zatvoriek 
                if (Character.isWhitespace(s.charAt(i)))
                    continue;
                else if (s.charAt(i) == '(') {
                    t.sign = SignType.BRAC_OP;
                    t.type = EntryType.BRACKET;
                    t.priority = 0;
                    expr.add(t);
                }
                else if (s.charAt(i) == ')') {
                    t.sign = SignType.BRAC_CL;
                    t.type = EntryType.BRACKET;
                    t.priority = 0;
                    expr.add(t);
                }
                //spracovanie znamienok '+', '-' 
                else if (s.charAt(i) == '+' || s.charAt(i) == '-') {
                    //ak je znamienko + alebo - bez medzery pred cislom a jeho predchodca je tiez
                    //znamienko tak sa toto znamienko prida k cislu a nebude sa vyhodnocovat
                    if (i-1 < 0 || (i+1<s.length() && isSign(s.charAt(i - 1)) && s.charAt(i+1) != ' ')) {
                        if (s.charAt(i) == '-') {
                            negative = negative == true ? false : true;
                            continue;
                        }
                    }
                    t.sign = s.charAt(i) == '+'?SignType.SIGN_PLUS:SignType.SIGN_MINUS;
                    t.type = EntryType.SIGN;
                    t.priority = 7;
                    expr.add(t);
                } //spracovanie znamienok '*', '/' - 
                else if (s.charAt(i) == '*') {
                    t.sign = SignType.SIGN_MUL;
                    t.type = EntryType.SIGN;
                    t.priority = 6;
                    expr.add(t);
                }  
                else if (s.charAt(i) == '/') {
                    t.sign = SignType.SIGN_DIV;
                    t.type = EntryType.SIGN;
                    t.priority = 6;
                    expr.add(t);
                }
                else if (s.charAt(i) == '%') {
                    t.sign = SignType.SIGN_MOD;
                    t.type = EntryType.SIGN;
                    t.priority = 6;
                    expr.add(t);
                }
                else if (s.charAt(i) == '/') {
                    t.sign = SignType.SIGN_DIV;
                    t.type = EntryType.SIGN;
                    t.priority = 6;
                    expr.add(t);
                }
                else if (s.charAt(i) == '&') {
                    t.sign = SignType.SIGN_AND;
                    t.type = EntryType.SIGN;
                    t.priority = 14;
                    expr.add(t);
                }
                else if (s.charAt(i) == '|') {
                    t.sign = SignType.SIGN_OR;
                    t.type = EntryType.SIGN;
                    t.priority = 15;
                    expr.add(t);
                }
                //spracovanie znamienok <, >, ==, !=, <=, >=
		else if (s.charAt(i) == '<') {
                    t.type = EntryType.SIGN;
                    t.sign = SignType.SIGN_LT;
                    t.priority = 10;
                    if (s.charAt(i+1) == '=') {
                            t.sign = SignType.SIGN_LE;
                            ++i;
                    }
                    expr.add(t);
		}
                else if (s.charAt(i) == '>') {
                    t.type = EntryType.SIGN;
                    t.sign = SignType.SIGN_GT;
                    t.priority = 10;
                    if (s.charAt(i+1) == '=') {
                            t.sign = SignType.SIGN_GE;
                            ++i;
                    }
                    expr.add(t);
		}
                else if (s.charAt(i) == '=' && s.charAt(i+1) == '=') {
			t.type = EntryType.SIGN;
			t.sign = SignType.SIGN_EQ;
			t.priority = 10;
			++i;
			expr.add(t);
		}
               	else if (s.charAt(i)=='!' && s.charAt(i+1)=='=') {
			t.type = EntryType.SIGN;
			t.priority = 10;
			t.sign = SignType.SIGN_NE;
			++i;
			expr.add(t);
		}
                // spracovanie znamienka negacie '!'
		else if (s.charAt(i) == '!') {
			t.sign = SignType.SIGN_NOT;
			t.type = EntryType.SIGN;
			t.priority = 3;
			expr.add(t);
		}
                //spracovanie cisiel
                else if (isNumber(s.charAt(i)) && s.charAt(i) != 'e' && s.charAt(i) != 'E') {
                    Matcher numberMatcher = Expression.numberpattern.matcher(s);
                    if (!numberMatcher.find(i)) {
                        error = "Expr error - bad number format!";
                        break;
                    }
                    String tmp = numberMatcher.group(1);
                    i+= tmp.length() - 1;
                    BigDecimal val = null;
                    try {
                        val = new BigDecimal(tmp,funct.mc());
                    } catch (NumberFormatException e)  {
                        error = "Expr error - bad number format!";
                        break;
                    }
                    catch (ArithmeticException e) {
                        error = "Expr error - bad number format!";
                        break;
                    }
                    if (negative) {
                        val = val.negate(funct.mc());
                        negative = false;
                    }
                    Value value = new Value(val);
                    t.setValue(value);
                    expr.add(t);
                }
                // spracovanie retazcov
                else if (s.charAt(i) == '\"') {
                    ++i;
                    temp = new StringBuffer();

                    while (i < s.length()-1 && s.charAt(i)!='\"') {
                        temp.append(s.charAt(i));
                        ++i;
                    }

                    if (i >= s.length()) {
                        error = "Expr error - bad string format!";
                        return;
                    }
                    Value value = new Value(temp.toString());
                    t.setValue(value);
                    expr.add(t);
                }
                // spracovanie premennych
                else if ( s.charAt(i) == '$') {
                    temp = new StringBuffer();
                    Matcher variableMatcher = Expression.variablepattern.matcher(s);
                    if (!variableMatcher.find(i)) {
                        error = "Expr error - bad variable name!";
                        break;
                    }
                    String tmp = variableMatcher.group(1);
                    i += tmp.length();
                    if (tmp.isEmpty()) {
                        error = "Expr error - bad variable name!";
                        break;
                    }
                    Value pval = funct.getVariable(tmp);
                    if (pval == null) {
                        error = "Expr error - unknown variable!";
                        return;
                    }
                    if (negative) {
                        switch (pval.getType()) {
                            case NUMBER:
                                pval.setNumber(pval.getNumber().negate(funct.mc()));
                                break;
                            default:
                                break;
                        }
                        negative = false;
                    }
                    t.setValue(pval);
                    expr.add(t);

                }
                // spracovanie funkcii
                else {
                    FunctionDef fdef = new FunctionDef();
                    int offset;
                    if ((offset = parseFunctDef(s, i, fdef))==-1) {
                        error = "Expr error - error in function calling!";
                        return;
                    }
                    Value pval = funct.eval(fdef.name, fdef.args);
                    if (pval == null) {
                        error = "Expr error - function error!";
                        return;
                    }
                    if ( pval.getType() == ValueType.VALUE_ERROR) {
                        error = pval.getError();
                        return;
                    }
                    if (negative) {
                        switch (pval.getType()) {
                            case NUMBER:
                                pval.setNumber(pval.getNumber().negate(funct.mc()));
                                break;
                            default:
                                break;
                        }
                        negative = false;
                    }
                    t.setValue(pval);
                    expr.add(t);
                    i = offset-1;
                }
                
            }
            //pridanie zatvsorky na koniec zoznamu
            t = new Entry();
            t.type = EntryType.BRACKET;
            t.sign = SignType.BRAC_CL;
            t.priority = -1;
            expr.add(t);
            //osetrenie chyb v infixnom vyraze - infixtopostfix niektore chyby vo vyraze ignorovalo a potom zle pocitalo
            int j;
            for (j = 1; j <= expr.size() - 2; ++j) {

                if (expr.get(j).type == EntryType.SIGN && (expr.get(j-1).type == EntryType.SIGN || expr.get(j + 1).type == EntryType.SIGN)) {
                    error = "Expr error! - 2 signs following each other ";
                    break;
                }
                if (expr.get(j).type == EntryType.VALUE && (expr.get(j-1).type == EntryType.VALUE || expr.get(j+1).type == EntryType.VALUE)) {
                    error = "Expr error! - sign missing";
                    break;
                }
                if (expr.get(j).sign == SignType.BRAC_OP && (expr.get(j-1).type == EntryType.VALUE ||
                        ((expr.get(j+1).type == EntryType.SIGN) && expr.get(j+1).sign != SignType.SIGN_NOT))) {
                    error = "Expr error! - opening bracket";
                    break;
                }
                if (expr.get(j).sign == SignType.BRAC_CL && (expr.get(j-1).type == EntryType.SIGN || expr.get(j+1).type == EntryType.VALUE)) {
                    error = "Expr error! - closing bracket";
                    break;
                }
            }
            if (( expr.get(1).type == EntryType.SIGN && expr.get(1).sign != SignType.SIGN_NOT) ||
                    expr.get(expr.size() - 2).type == EntryType.SIGN) {
                error = "Expr error! - sign not expected";
            }

        }

	/* Prevedie hodnotovy infixny zapis so znamienkami a zatvorkami na postfixny zapis. */
        private void infixtopostfix()  {
            //zasobnik na ukladanie znamienok
            Stack<Entry> zas = new Stack<Entry>();
            Vector<Entry> out = new Vector<Entry>();
            int k = 0;
            Entry t;
            for (int i = 0; i < expr.size(); ++i) {
                t = expr.get(i);
                switch (t.type) {
                    //cisla idu na vystup (v nasom pripade vysledny expr)
                    case VALUE:
                        out.add(t);
                    break;
                    //pri pridavani znamienka najskor vyprazdnit zo zasobnika znamienka s vyssou alebo rovnakou prioritou
                    case SIGN:
                        while ((zas.size() > 0) && (zas.peek().sign != SignType.BRAC_OP) &&
                                (zas.peek().priority <= t.priority)) {
                             out.add(zas.pop());
                        }
                        zas.push(t);
                    break;
                    // pridanie zatvorky '('
                    // uzatvaracia zatvorka vyhodi vsetky znamienka az po dalsiu otvaraciu zatvorku na vystup
                    case BRACKET:
                        if (t.sign == SignType.BRAC_OP) {
                            zas.push(t);
                        } else {
                            while ((zas.size() > 0) && (zas.peek().sign != SignType.BRAC_OP)) {
                                out.add(zas.pop());
                            }
                            if (zas.size() > 0) {
                                zas.pop();
                            }
                        }
                    break;
                    default:
                        break;
                }
            }
            expr = out;
        }

        /** metoda vyhodnoti vyraz ktory obsahuje len cisla a znamienka v postfixnej notacii. */
        private Value evaluatepostfix() throws EmptyStackException {
        Stack<Value> zas = new Stack<Value>();  //zasobnik pouzity pri vyhodnocovani
        Value a, b, t;                               //pomocne premenne
        
	for (int i = 0; i < expr.size(); ++i)
	{
		if (expr.get(i).type == EntryType.VALUE)
			zas.push(expr.get(i).value);
		else if (expr.get(i).type == EntryType.SIGN) {
			switch (expr.get(i).sign)
			{
                            case SIGN_PLUS:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case BOOL:
                                                error = "Can't execute operation! - wrong types!";
                                                return new Value();
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().add(b.getNumber(),funct.mc())));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(a.getString()+ b.getString()));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_MINUS:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().subtract(b.getNumber(),funct.mc())));
                                                    break;
                                            case STRING:
                                                    error = "Can't substract 2 strings!";
                                                    return new Value();
                                            case BOOL:
                                                    error = "Can't substract 2 booleans!";
                                                    return new Value();
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_MUL:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().multiply(b.getNumber(),funct.mc())));
                                                    break;
                                            case STRING:
                                                    error = "Can't multiply 2 strings!";
                                                    return new Value();
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_DIV:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    if (b.getNumber().equals(BigDecimal.ZERO)) {
                                                            error = "Division by zero!";
                                                            return new Value();
                                                    }
                                                    zas.push(new Value(a.getNumber().divide(b.getNumber(),funct.mc())));
                                                    break;
                                            case STRING:
                                                    error = "Can't divide 2 strings!";
                                                    return new Value();
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_MOD:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    if (b.getNumber().equals(BigDecimal.ZERO)) {
                                                            error = "Division by zero!";
                                                            return new Value();
                                                    }
                                                    zas.push(new Value(a.getNumber().remainder(b.getNumber(), funct.mc())));
                                                    break;
                                            case STRING:
                                                    error = "Can't mod 2 strings!";
                                                    return new Value();
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_AND:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case BOOL:
                                                    zas.push(new Value(a.getBool() && b.getBool()));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                    }
                                    break;
                            case SIGN_OR:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case BOOL:
                                                    zas.push(new Value(a.getBool() || b.getBool()));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_LT:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().compareTo(b.getNumber()) < 0));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(a.getString().compareTo(b.getString()) < 0));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_GT:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().compareTo(b.getNumber()) > 0));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(a.getString().compareTo(b.getString()) > 0));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_NOT:
                                    a = zas.pop();
                                    switch (a.getType()) {
                                            case BOOL:
                                                    zas.push(new Value(!a.getBool()));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_LE:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().compareTo(b.getNumber()) <= 0));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(a.getString().compareTo(b.getString()) <= 0));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_GE:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().compareTo(b.getNumber()) >= 0));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(a.getString().compareTo(b.getString()) >= 0));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_EQ:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case BOOL:
                                                    zas.push(new Value(a.getBool() == b.getBool()));
                                                    break;
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().compareTo(b.getNumber()) == 0));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(a.getString().equals(b.getString())));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                     }
                                    break;
                            case SIGN_NE:
                                    b = zas.pop();
                                    a = zas.pop();
                                    switch (Value.convert( a, b)) {
                                            case BOOL:
                                                    zas.push(new Value(a.getBool() != b.getBool()));
                                            case NUMBER:
                                                    zas.push(new Value(a.getNumber().compareTo(b.getNumber()) != 0));
                                                    break;
                                            case STRING:
                                                    zas.push(new Value(!a.getString().equals(b.getString())));
                                                    break;
                                            default:
                                                    error = "Can't execute operation! - mismatching types!";
                                                    return new Value();
                                    }
                                    break;
                            default: break;
                    }

		}
	}
	if (zas.isEmpty()) {
		error = "Expr error!";
		return new Value();
	}
	a = zas.pop();
	//toto je maly hack kedze pri pridani funkcie zaporneho cisla v niektorych specifickych situaciach nemame znamienko
	//cisla ktore toto znamienko zobrali su bud kladne alebo zaporne (tj. staci zostavajuce cisla scitat)
	//napr. 3-sin(0)-3 = 6 bez tejto upravy
	while (!zas.isEmpty())
	{
		b = zas.pop();
		switch (Value.convert( a, b)) {
			case NUMBER:
				a.setNumber(a.getNumber().add(b.getNumber()));
				break;
			case STRING:
                                a.setString(a.getString()+b.getString());
				break;
			default:
				error = "Can't execute operation! - mismatching types!";
				return new Value();
		}
	}
	return a;
        }

        /** Metoda na zistenie ci je vstup cislo
         *
         * @param c Vstupny znak
         * @return Boolean hodnota ci je alebo nie je cislo.
         */
        private static boolean isNumber(char c) {
            if (Character.isDigit(c) || c == '.' || c == 'e')
                return true;
            else return false;
        }

        /** Metoda na zistenie ci je vstup znamienko
         *
         * @param c Vstupny znak
         * @return Boolean hodnota ci je alebo nie je vstup znamienko
         */
        private static boolean isSign(char c) {
            if (c == '-' || c=='/' || c=='+' || c=='*' || c=='(' || c == '%' || c == '&' || c=='|' ||
                    c=='!' || c=='<' || c=='>' || c=='=')
                 return true;
            else return false;
        }

        /** Metoda na zistenie ci je vstupny znak cast premennej (tj [0-9a-zA-z_])
         *
         * @param c Vstupny znak
         * @return Boolean hodnota ci je znak povoleny pre premennu.
         */
        private static boolean isVariable(char c) {
            if (Character.isDigit(c) || Character.isLetter(c) || c=='_' || c=='.')
                return true;
            else return false;
        }

        /** Pomocna trieda sluziaca na ulozenie vyparsovaneho nazvu funkcie a vyparsovanych parametrov.*/
        public static class FunctionDef {
            String name;
            List<String> args;
        }
        
        /** Pomocna metoda na rozparsovanie nazvu funkcie a parametrov alebo premennych.
         *
         * @param s Vstupny retazec.
         * @param offset Offset na ktorom zacina volanie funkcie vo vstupnom retazci
         * @param funct objekt v ktorom, bude sa vrati vyparsovana definicia funkcie.
         * @return Offset prveho znaku za volanim funkcie, -1 ak nastala chyba.
         */
        public int parseFunctDef(String s, int offset, FunctionDef fdef)
        {
            // pomocne premenne
            int i = offset;
            StringBuilder temp = new StringBuilder();
            // udava pocet neuzavretych zatvoriek
            int z = 0;
            List<String> arguments = new LinkedList<String>();

            // vyparsovanie nazvu funkcie
            while ((i < s.length()) && isVariable(s.charAt(i)))
            {
                temp.append(s.charAt(i));
                ++i;
            }
            fdef.name = temp.toString();
            if (fdef.name.isEmpty()) {
               return -1;
            }

            // vyparsovanie argumentov
            if (s.charAt(i)=='(') {
                ++i;     //posun zo znaku '('
                temp = new StringBuilder();
                z = 1;   //udava pocet neuzavretych zatvoriek vovnutri argumentu
                while ((i < s.length()) && z != 0) {
                    if ( z == 1 && s.charAt(i) == ',') {
                        arguments.add(temp.toString());
                        temp = new StringBuilder();
                        ++i;
                        continue;
                    }
                    if (s.charAt(i) == '(')
                        ++z;
                    else if (s.charAt(i) == ')')
                        --z;

                   if (z != 0) {
                       temp.append(s.charAt(i));
                       ++i;
                   }
                }
                arguments.add(temp.toString());
            }
            fdef.args = arguments;
            if (z != 0) return -1;
            return i;
        }
}