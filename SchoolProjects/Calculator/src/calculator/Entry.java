package calculator;

/** Trieda ktora tvori zaklad pre ulozenie vyparsovaneho vyrazu. */
public class Entry {
    /** Typ polozky. */
    EntryType type;
    /** Typ znamienka ak je polozka typu SIGN alebo BRACKET inak nie je definovane. */
    SignType sign;
    /** Priorita znamienka - mensie cislo znaci vacsiu prioritu. */
    int priority;
    /** Hodnota ak je polozka typu VALUE */
    Value value;         

    /** Zisti typ hodnoty polozky.
     *
     * @return vracia typ hodnoty alebo VALUE_ERROR ak nema typ zmysel.
     */
    ValueType getValueType() {
        if (type == EntryType.VALUE && value != null) {
		return value.type;
	}
	return ValueType.VALUE_ERROR;
    }

    /** Nastavi hodnotu polozky.
     *
     * @param val Hodnota na ktoru nastavit.
     */
    void setValue(Value val) {
        type = EntryType.VALUE;
        value = val;
    }

    /** Inicializuje polozku. */
    void clear() {
        type = EntryType.NOTYPE;
	sign = SignType.SIGN_NO;
        value = null;
	priority = 0;
    }
}