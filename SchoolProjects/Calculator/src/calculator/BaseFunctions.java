package calculator;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import javax.swing.JOptionPane;

/** Definuje zakladne matematicke funkcie. Tieto budu volane
 * pomocou reflections API z vyhodnocovaca funkcii (FunctionEval).
 */
public class BaseFunctions {

    FunctionEval funct = null;

    public BaseFunctions(FunctionEval f) {
        funct = f;
    }
    
    public Value sin(BigDecimal val) {
        return new Value(new BigDecimal(Math.sin(val.doubleValue()),funct.mc()));
    }
    
    public Value asin(BigDecimal val) {
        return new Value(new BigDecimal(Math.asin(val.doubleValue()),funct.mc()));
    }

    public Value cos(BigDecimal val) {
        return new Value(new BigDecimal(Math.cos(val.doubleValue()),funct.mc()));
    }

    public Value acos(BigDecimal val) {
        return new Value(new BigDecimal(Math.acos(val.doubleValue()),funct.mc()));
    }

    public Value tan(BigDecimal val) {
        return new Value(new BigDecimal(Math.tan(val.doubleValue()),funct.mc()));
    }

    public Value atan(BigDecimal val) {
        return new Value(new BigDecimal(Math.atan(val.doubleValue()),funct.mc()));
    }

    /** Vseobecna mocnina.
     *
     * @param value hodnota
     * @param exponent exponent
     * @return hodnota^exponent (presne ak je exponent prirodzene cislo, nepresne inak)
     */
    public Value pow(BigDecimal value, BigDecimal exponent) {
        int exp = 0;
        try {
            exp = exponent.intValueExact();
        } catch( ArithmeticException ex) {
            double dexp = exponent.doubleValue();
            BigDecimal out = new BigDecimal(Math.pow(value.doubleValue(),dexp),funct.mc());
            return new Value(out);
        }
        BigDecimal out = value.pow(exp,funct.mc());
        return new Value(out);
    }

    public Value floor(BigDecimal val) {
        int scale = val.scale();
        int intdigits;
        if (scale >=0) {
            intdigits = val.precision() - scale;
            MathContext mc = new MathContext(intdigits,RoundingMode.FLOOR);
            return new Value(val.round(mc));
        }
        else
            return new Value(val);
    }

    public Value ceil(BigDecimal val) {
                int scale = val.scale();
        int intdigits;
        if (scale >=0) {
            intdigits = val.precision() - scale;
            MathContext mc = new MathContext(intdigits,RoundingMode.CEILING);
            return new Value(val.round(mc));
        }
        else
            return new Value(val);
    }

    public Value round(BigDecimal val) {
                int scale = val.scale();
        int intdigits;
        if (scale >=0) {
            intdigits = val.precision() - scale;
            MathContext mc = new MathContext(intdigits,RoundingMode.HALF_UP);
            return new Value(val.round(mc));
        }
        else
            return new Value(val);
    }

    public Value abs(BigDecimal val) {
        return new Value(val.abs());
    }

    /** Faktorial. */
    public Value fact(BigDecimal value) {
        int n = 0;
        try {
            n = value.intValueExact();
        } catch( ArithmeticException ex) {
            return FunctionEval.errorValue("Factorial needs integer argument");
        }
        BigDecimal out = new BigDecimal(BigInteger.ONE);
        for(int i=2;i<=n;++i) {
            out = out.multiply(new BigDecimal(i));
        }

        return new Value(out.round(funct.mc()));
    }

    /** Vypis na konzolu. */
    public Value write(String what) {
        Gui.getInstance().consoleWrite(what);
        return new Value(true);
    }

    /** Vypis na konzolu. */
    public Value writeln(String what) {
        Gui.getInstance().consoleWrite(what+"\n");
        return new Value(true);
    }

    /** Vypis na konzolu. */
    public Value writeln() {
        Gui.getInstance().consoleWrite("\n");
        return new Value(true);
    }

    /** Vypis na konzolu. */
    public Value write(BigDecimal what) {
        Gui.getInstance().consoleWrite(what.toString());
        return new Value(true);
    }

    /** Vypis na konzolu. */
    public Value writeln(BigDecimal what) {
        Gui.getInstance().consoleWrite(what.toString()+"\n");
        return new Value(true);
    }

    /** Vypis na konzolu. */
    public Value write(boolean what) {
        Gui.getInstance().consoleWrite(what + "");
        return new Value(true);
    }

    /** Vypis na konzolu. */
    public Value writeln(boolean what) {
        Gui.getInstance().consoleWrite(what+"\n");
        return new Value(true);
    }

    /** Vymaze obsah konzole. */
    public Value clear() {
        Gui.getInstance().consoleClear();
        return new Value(true);
    }

    /** Konverzia argumentu. */
    public Value toNumber(String what) {
        return new Value(new Value(what).toNumber());
    }

    /** Konverzia argumentu. */
    public Value toNumber(BigDecimal val) {
        return new Value(val);
    }

    /** Konverzia argumentu. */
    public Value toNumber(boolean b) {
        return new Value(new BigDecimal(b==true?1:0));
    }

    /** Konverzia argumentu. */
    public Value toString(BigDecimal val) {
        return new Value(val.toString());
    }

    /** Konverzia argumentu. */
    public Value toString(String what) {
        return new Value(what);
    }

    /** Konverzia argumentu. */
    public Value toString(boolean b) {
        return new Value(b==true?"true":"false");
    }

    /** Konverzia argumentu. */
    public Value toBool(BigDecimal val) {
        return new Value(!val.equals(BigDecimal.ZERO));
    }

    /** Konverzia argumentu. */
    public Value toBool(boolean b) {
        return new Value(b);
    }

    /** Konverzia argumentu. */
    public Value toBool(String what) {
        return new Value(Boolean.parseBoolean(what));
    }

    /** Vrati typ argumentu. */
    public Value type(String what) {
        return new Value("string");
    }

    /** Vrati typ argumentu. */
    public Value type(BigDecimal val) {
        return new Value("number");
    }

    /** Vrati typ argumentu. */
    public Value type(boolean val) {
        return new Value("boolean");
    }

    /**
     *
     * @param val vstupny retazec
     * @return vstupny retazec obsahujuci len lowercase znaky
     */
    public Value toLower(String val) {
        return new Value(val.toLowerCase());
    }

    /** Vyhodnoti script ktory dostane ako parameter - s lokalnou sadou parametrov. */
    public Value execute(String script) {
        ScriptEval innerScriptEval = new ScriptEval();
        innerScriptEval.addVariables(funct.getVariables());
        innerScriptEval.setScript(script);
        return innerScriptEval.execute();
    }

    /** Vyhodnoti vyraz. */
    public Value evaluate(String input) {
        Expression expr = new Expression(funct);
        expr.setExpression(input);
        return expr.eval();
    }

    /** Zobrazi message box. */
    public Value messageBox(String what) {
        JOptionPane.showMessageDialog(Gui.getInstance().getContentFrame(), what);
        return new Value(true);
    }

    /** Zobrazi input box. */
    public Value inputBox(String what) {
        String out = JOptionPane.showInputDialog(Gui.getInstance().getContentFrame(), what);
        if (out == null)
            return new Value("");
        return new Value(out);
    }
}