package calculator;

/**
 * Reprezentuje typ polozky vo vyparsovanom vyraze.
 */
public enum EntryType {
    /** Nedefinovany typ. */
    NOTYPE,
    /** Typ nesuci hodnotu. */
    VALUE,
    /** Znamienko. */
    SIGN,
    /** Zatvorka. */
    BRACKET;
}
