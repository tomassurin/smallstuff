package calculator;

/**
 * Reprezentuje typ hodnoty.
 */
public enum ValueType {
  /** Nedefinovana hodnota. */
  VALUE_ERROR,
  /** Logicka hodnota. */
  BOOL, 
  /** Cislo. */
  NUMBER,
  /** Retazec. */
  STRING,
  /** Bez typu. */
  VOID
}
