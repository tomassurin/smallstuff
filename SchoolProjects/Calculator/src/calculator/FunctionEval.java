package calculator;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *  Trieda sluziaca na spracovanie funkcii a premennych.
 */
public class FunctionEval {
    /** Navratova hodnota minuleho vypoctu. */
    private Value lastresult = new Value(BigInteger.ZERO);
    /** Asociativne pole na ukladanie premennych. */
    private Map<String,Value> variables = new HashMap<String,Value>();
    private MathContext context = new MathContext(20);
    /** Asociativne pole instancii kniznic funkcii. Klucom je prefix mena funkcie. */
    private Map<String,Object> instances = new HashMap<String, Object>();

    private ScriptEval scripteval = null;

    /** Konstruktor. */
    FunctionEval(ScriptEval scripteval)
    {
        this.scripteval = scripteval;
        // nacitanie zakladnych kniznic
        addFunctionLibrary("base","calculator.BaseFunctions");
        addFunctionLibrary("draw", "calculator.Draw");

        setVariable("pi", new Value(new BigDecimal(Math.PI)));
        setVariable("e", new Value(new BigDecimal(Math.E)));
    }

    /** Metoda prida novu kniznicu.
     *
     * @param id Identifikator kniznice.
     * @param mainclass Hlavna trieda kniznice (cela cesta aj s namespace).
     */
    private void addFunctionLibrary(String id, String mainclass)
    {
        Class cl = null;
        try {
            cl = Class.forName(mainclass);
        } catch (ClassNotFoundException ex) {
            Gui.getInstance().consoleWrite("Error loading library");
            return;
        }
        Object instance=null;
        try {
            Constructor constr = cl.getConstructor(FunctionEval.class);
            instance = constr.newInstance(this);
        } catch (Exception ex) {
            Gui.getInstance().consoleWrite("Error loading library");
            return;
        }
        instances.put(id, instance);
    }

    /** Ziskanie Math context objektu.
     *
     * @return Aktualny MathContext objekt vyhodnocovaca funkcii.
     */
    public MathContext mc() {
        return context;
    }

    /** Nastavenie Math context objektu. */
    public void setMc(MathContext mcont) {
        context = mcont;
    }

    /** Nastavi hodnotu premennej s nazvom name na hodnotu val.
     *
     * @param name Nazov premennej
     * @param val Hodnota premennej
     */
    public void setVariable(String name, Value val)
    {
        if (val == null) return;
        variables.put(name, val);
    }

    /** Ziska hodnotu premennej s nazvom name.
     *
     * @param name Nazov premennej
     * @return hodnotu premennej s nazvom name alebo null ak sa premenna s takymto nazvom nevyskytuje.
     */
    public Value getVariable(String name)
    {
        return variables.get(name);
    }

    /** Nastavenie celeho pola premennych
     *
     * @param vars Pole premennych
     */
    public void setVariables(Map<String,Value> vars)
    {
        variables = vars;
    }

    /** Ziskanie celeho pola premennych
     *
     * @return Pole premennych
     */
    public Map<String,Value> getVariables()
    {
        return variables;
    }

    /** Vytvorenie hodnoty typu error s chybovym hlasenim.
     *
     * @param str Chybove hlasenie
     * @return Value typu error s chybovym hlasenim
     */
    public static Value errorValue(String str)
    {
        Value val = new Value();
        val.setError("Error in Function Evaluation: " + str);
        return val;
    }

    /** Metoda nastavi vysledok minuleho vypoctu.
     *
     * @param last Hodnota minuleho vypoctu.
     */
    public void setLast(Value last) {
        this.lastresult = last;
    }

    /** Metoda vyhodnoti funkciu.
     *
     * @param name Nazov funkcie
     * @param args Zoznam parametrov funkcie
     * @return Vysledok vyhodnocovania.
     */
    public Value eval(String name, List<String> strargs)
    {
        //pomocne premenne
        List<Value> a = evaluateArguments(strargs);
        // pocet parametrov
        int n = a.size();
        
        // konstanty pre logicke hodnoty a vysledok minuleho vypoctu
        if (name.equals("true"))
            return new Value(true);
        else if (name.equals("false"))
            return new Value(false);
        else if (name.equals("ans"))
            return new Value(lastresult);
        else if (name.equals("setPrecision")) {
            if (a.size() != 1) {
                return errorValue("Bad number of parameters specified");
            }
            int prec = a.get(0).getNumber().intValue();
            try {
                context = new MathContext(prec);
            } catch(IllegalArgumentException ex ) {
                return new Value(false);
            }
            return new Value(true);
        }
        else if (name.equals("library")) {
            if (a.size() != 2) {
                return errorValue("Bad number of parameters specified");
            }
            String id = a.get(0).getString();
            String mainclass = a.get(1).getString();

            addFunctionLibrary(id, mainclass);
            return new Value(true);
        }

        // volanie funkcii pomocou reflections API
        Object instance;
        String prefix = "";
        String postfix = name;
        if (name.contains(".")) {
            int ind = name.lastIndexOf('.');
             prefix = name.substring(0,ind);
             name = name.substring(ind+1);
        }
        if (prefix.isEmpty())
            instance = instances.get("base");
        else 
            instance = instances.get(prefix);

        if (instance == null) {
            return errorValue("Function Error");
        }
        Method m = null;
        Object[] args = new Object[n];
        try {
            Class[] params = new Class[n];
            Value tmp;
            for(int i=0;i<n;++i) {
                tmp = a.get(i);
                switch (tmp.getType()) {
                    case BOOL:
                        params[i] = boolean.class;
                        args[i] = tmp.getBool();
                        break;
                    case NUMBER:
                        params[i] = BigDecimal.class;
                        args[i] = tmp.getNumber();
                        break;
                    case STRING:
                        params[i] = String.class;
                        args[i] = tmp.getString();
                        break;
                    default:
                        return errorValue("Bad arguments type");
                }
            }
            m = instance.getClass().getMethod(name, params);
        } catch (Exception ex) {
            return errorValue("Unknown function");
        }
        try {
            return (Value) m.invoke(instance, args);
        } catch (IllegalArgumentException ex) {
            return errorValue("Bad arguments type");
        } catch (Exception ex) {
            return errorValue("Unknown function");
        }
    }
    
    /** Spracovanie argumentov funkcii ulozenych vo vstupnom retazci, argumenty su oddelene ciarkami
     *  a mozu byt aj vnorene.
     *
     * @param args List vstupnych argumentov
     * @return List vypocitanych hodnot jednotlivych argumentov.
     */
    private List<Value> evaluateArguments(List<String> args)
    {
        List<Value> a = new LinkedList<Value>();

        for(String str:args)
        {
            if (!str.isEmpty()) {
                Expression texpr = new Expression(this);
                texpr.setExpression(str);
                a.add(texpr.eval());
            }
        }
        return a;
    }

}
