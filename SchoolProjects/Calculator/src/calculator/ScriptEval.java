package calculator;

import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;

/** Trieda sluzi na vyhodnocovanie skriptu ziskaneho z programu,  Skript moze obsahovat
 * priradovaci prikaz, prikaz if, while cyklus, goto, return, break, continue.
 */
public class ScriptEval {

    /** Ulozenie retazca skriptu. */
    String script = null;
    /** Zoznam riadkov skriptu. */
    ArrayList<String> lines = null;
    /** Pomocna premenna na ukladanie chyby. */
    String error = "";
    /** Objekt, ktory sa pouzije pri vyhodnocovani funkcii. */
    FunctionEval eval = null;
    /** Objekt, ktory sa pouzije pri vyhodnocovani vyrazov. */
    Expression expr = null;
    /** Definicia Patternov regularnych vyrazov. */
    private Pattern assign = Pattern.compile("^\\s*([0-9a-z_]+)\\s*:=\\s*(.*)\\s*$");
    /** Definicia Patternov regularnych vyrazov. */
    private Pattern returning = Pattern.compile("^\\s*return\\s+(.*)\\s*$");
    /** Definicia Patternov regularnych vyrazov. */
    private Pattern branch = Pattern.compile("^\\s*if\\s*\\((.*)\\)\\s*(.*)\\s*$");
    /** Definicia Patternov regularnych vyrazov. */
    private Pattern elseline = Pattern.compile("^\\s*else\\s+(.*)\\s*$");
    /** Definicia Patternov regularnych vyrazov. */
    private Pattern whilecycle = Pattern.compile("^\\s*while\\s*\\((.*)\\)\\s*(.*)\\s*$");
    /** Definicia Patternov regularnych vyrazov. */
    private Pattern gotoexpr = Pattern.compile("^\\s*goto\\s+(\\d*)\\s*$");

    ScriptEval()
    {
	error = "";
        eval = new FunctionEval(this);
        expr = new Expression(eval);
    }

    /** Odstrani pociatocne a koncove medzery z retazca a nahradi vovnutri retazca viac whitespaces za jednu medzeru.
     * @param inp Vstupny retazec.
     * @return Vysledny zjednoduseny retazec.
     */
    private String simplify(String inp)  {
        return inp.trim().replaceAll("\\s+", " ");
    }

    /** Nastavi premennu script na vstupny script a urobi zakladne predspracovanie scriptu.
     * Tj. rozdeli na riadky, odstrani zbytocne medzery, cykly nahradi s if a goto.
     * @param script Vstupny script.
     */
    public void setScript(String script)
    {
        this.script = script;
	error = "";

        lines = new ArrayList<String>();

	// predspracovanie skriptu
	String tmp;
	tmp = script;
        // chcem dostat { resp } na samostatnu polozku
	tmp = tmp.replace("{",";{;").replace("}",";};");
        lines = splitLines(tmp);

	for(int i = 0;i < lines.size(); ++i) {
		lines.set(i, simplify(lines.get(i)));

                String line = lines.get(i);
		if (line.isEmpty()) {
                    lines.remove(i);
                    --i;
                    continue;
                }
 
                Matcher branchmatch = branch.matcher(line);
		if (branchmatch.matches()) {
                        lines.set(i, simplify("if (" + branchmatch.group(1) + ")"));
			tmp = simplify(branchmatch.group(2));
			if (!tmp.isEmpty()) {
                            lines.add(++i, tmp);
                        }
                        continue;
		}
                Matcher elselinematch = elseline.matcher(line);
		if (elselinematch.matches()) {
			lines.set(i, "else");
			tmp = simplify(elselinematch.group(1));
			if (!tmp.isEmpty())
                            lines.add(++i,tmp);
                        continue;
		}
                Matcher whilematch = whilecycle.matcher(line);
		if (whilematch.matches()) {
                        lines.set(i, simplify("while (" + whilematch.group(1) + ")"));
			tmp = simplify(whilematch.group(2));
			if (!tmp.isEmpty()) {
                            lines.add(++i,tmp);
                        }
		}
	}

    	// nahradenie while, break, continue s goto <riadok>
	String whilecond;
	int jumpindex;
        int[] bound = new int[2];
	for(int i=0; i < lines.size(); ++i) {
            String line = lines.get(i);
            Matcher whilematch = whilecycle.matcher(line);
            if (whilematch.matches()) {
                whilecond = simplify(whilematch.group(1));
                lines.set(i, "if ("+whilecond + ")");
                i++;
                jumpindex = i;
                if (i < lines.size() && !lines.get(i).equals("{")) {
                    if (lines.get(i).equals("break") || lines.get(i).equals("continue")) {
                        lines.remove(i);
                        lines.remove(i-1);
                        i -=2;
                        continue;
                    }
                    lines.add(i+1, "}");
                    lines.add(i+1, "goto " + (jumpindex+2));
                    lines.add(i+1, "if ("+whilecond+")");
                    lines.add(i, "{");
                    i+=4;
                    continue;
                }
                if (i >= lines.size() || !lines.get(i).equals("{")) {
                    error = "Error in script: "+ lines.get(i);
                    return;
                }
                bound[0] = i;
                findBlock(bound);
                if (bound[0] == -1 || bound[1] ==  -1) {
                    error = "Error in script: "+ lines.get(i);
                    return;
                }
                lines.add(bound[1], "goto "+ (jumpindex+2));
                lines.add(bound[1], "if ("+whilecond+")");
                // break:   goto bound[1]+4		// continue: goto i
                for(int j=i; j < bound[1]; ++j) {
                    if (lines.get(j).equals("break"))
                        lines.set(j, "goto "+ (bound[1]+4));
                    else if (lines.get(j).equals("continue"))
                        lines.set(j, "goto "+ i);
                    else if (whilecycle.matcher(lines.get(j)).matches()) {
                            if (j+1 < lines.size() && !lines.get(j+1).equals("{")) {
                                ++j;
                                continue;
                            }
                            if (j+1 < lines.size() && lines.get(j+1).equals("{")) {
                                int[] bound2 = new int[2];
                                bound2[0] = j+1;
                                findBlock(bound2);
                                if (bound2[0] == -1 || bound2[1] ==  -1) {
                                    error = "Error in script: "+ lines.get(j);
                                    return;
                                }
                                j = bound2[1];
                                continue;
                            }
                    }
                }
            }
        }
    }

    /** Rozdeli vstupny script na riadky.
     *
     */
    private ArrayList<String> splitLines(String s) {
        ArrayList<String> lin = new ArrayList<String>();
        StringBuilder tmp = new StringBuilder();
        // udava ci sme vovnutri uvodzoviek
        boolean quotes = false;
        for(int i=0;i<s.length();++i) {
            if (!quotes && s.charAt(i) == ';') {
                lin.add(tmp.toString());
                tmp = new StringBuilder();
                continue;
            }
            if (s.charAt(i) == '\"')
                quotes = !quotes;

            tmp.append(s.charAt(i));
            
        }
        return lin;
    }

    /** Najde hranice bloku kodu.
     *
     * @param bound Interval, bloku kodu. bound[0] je zaciatok, bound[1] koniec.
     */
    private void findBlock(int[] bound)
    {
        int s = bound[0];
        int e = bound[0];
	if (!lines.get(s).equals("{")) {
                bound[0] = -1;
                bound[1] = -1;
		return;
	}
	++s;
	int i = s;
	int z = 1;   //udava pocet neuzavretych zatvoriek vovnutri argumentu
	while ((i < lines.size()) && z != 0)	{
		if (lines.get(i).equals("{")) ++z;
		else if (lines.get(i).equals("}")) --z;
		if (z != 0)
                    ++i;
	}
	if ( i < lines.size() && lines.get(i).equals("}")) {
		e = i;
	}
	else {
		s = -1;
		e = -1;
        }
        bound[0] = s;
        bound[1] = e;
    }

    /** Spusti vykonavanie skriptu. */
    public Value execute()
    {   
        Value out = evalBlock(0, lines.size());
	return out;
    }

    /** Vyhodnoti blok prikazov na poziciach lines[<start;end)]. */
    private Value evalBlock(int start, int end)
    {
	if (start >= lines.size() || end > lines.size() || start < 0 || end <0) 
            return new Value(false);

	Value val = null;
	int n = lines.size();
        String line;
	int tmpindex = 0;
        int bound[] = new int[2];
        
	for(int i=start; i < end; ++i)
	{
            line = lines.get(i);
            if (line.isEmpty())
                continue;

            if (line.equals("}") || line.equals("{") )
                continue;

            // priradenie hodnoty premennej z vyrazu
            Matcher assignmatch = assign.matcher(line);
            if (assignmatch.matches()) {
                if (assignmatch.group(1).isEmpty() || assignmatch.group(2).isEmpty()) {
                        error = "Error in script: "+ lines.get(i);
                        return Value.error(error);
                }
                expr.setExpression(simplify(assignmatch.group(2)));
                String name = simplify(assignmatch.group(1));
                val = expr.eval();
                if (val.getType() == ValueType.VALUE_ERROR) {
                    error = "Error in script: "+ lines.get(i)+" : "+val.getError();
                    return Value.error(error);
                }
                eval.setVariable(name, val);
                val = null;
                continue;
            }

            // goto
            Matcher gotomatch = gotoexpr.matcher(line);
            if (gotomatch.matches()) {
                try {
                    tmpindex = Integer.parseInt(gotomatch.group(1));
                } catch (NumberFormatException ex) {
                    error = "Error in script: "+ lines.get(i);
                    return Value.error(error);
                }
                i = tmpindex - 2;
                continue;
            }

            // podmienky
            Matcher branchmatch = branch.matcher(line);
            if (branchmatch.matches()) {
                expr.setExpression(simplify(branchmatch.group(1)));
                val = expr.eval();
                if (val.type == ValueType.VALUE_ERROR) {
                    error = "Error in script: "+ lines.get(i)+" : "+val.getError();
                    return Value.error(error);
                }
                if (val.getBool()) {
                    // vykonavame tuto vetvu ak podmienka vyhodnotena ako true
                    // najskor vykoname prikaz ktory bol v tele prikazu if (tj. ak za if nenasleduje zlozeny prikaz)
                    if (i+1 < n && !lines.get(i+1).equals("{")) {
                        continue;
                    }
                    // vykoname zlozeny prikaz
                    if (i+1 < n && lines.get(i+1).equals("{")) {
                        i++;
                        continue;
                    }
                    error = "Error in script: "+ lines.get(i);
                    return Value.error(error);
                }
                else {
                    // tuto vetvu vykonavame ak podmienka neplati
                    // najskor preskocime blok prikazov ktore by nastali ak by podmienka platila
                    if (i+1 < n && lines.get(i+1).equals("{")) {
                        bound[0] = i+1;
                        bound[1] = 0;
                        findBlock(bound);
                        if (bound[0] == -1 || bound[1] ==  -1) {
                            error = "Error in script: "+ lines.get(i);
                            return Value.error(error);
                        }
                        i = bound[1]+1;
                    }
                    else i+=2;
                    //vykoname prikaz v tele else (tj. ak za else nenasleduje zlozeny prikaz)
                    if (i < n && lines.get(i).equals("else")) {
                        if (i+1 < n && !lines.get(i+1).equals("{"))
                            continue;
                        else if (i+1 < n && lines.get(i+1).equals("{")) {
                            i++;
                            continue;
                        }
                        error = "Error in script: "+ lines.get(i);
                        return Value.error(error);

                    }
                    else {
                        --i;
                        continue;
                    }
                }
            }
            // preskocime nevykonane else
            else if (lines.get(i).equals("else")) {
                if (i+1 < n && !lines.get(i+1).equals("{")) {
                    i++;
                    continue;
                }
                if (i+1 < n && lines.get(i+1).equals("{")) {
                    bound[0] = i+1;
                    bound[1] = 0;
                    findBlock(bound);
                    if (bound[0] == -1 || bound[1] ==  -1) {
                        error = "Error in script: "+ lines.get(i);
                        return Value.error(error);
                    }
                    i = bound[1];
                    continue;
                }
                error = "Error in script: "+ lines.get(i);
                return Value.error(error);
            }
            
            // return
            Matcher returnmatch = returning.matcher(line);
            if (returnmatch.matches()) {
                expr.setExpression(simplify(returnmatch.group(1)));
                val = expr.eval();
                if (val.type == ValueType.VALUE_ERROR) {
                    error = "Error in script: "+ lines.get(i)+" : "+val.getError();
                    return Value.error(error);
                }
                return val;
            }

            //spracovanie vseobecnych vyrazov
            expr.setExpression(line);
            val = expr.eval();
            if (val.type == ValueType.VALUE_ERROR) {
                error = "Error in script: "+ lines.get(i)+" : "+val.getError();
                return Value.error(error);
            }
	}

	return Value.createVoid();
    }

    /** Nastavi premenne (tj. nastavi premenne v objekte vyhodnocujucom funkcie).
     * @param vars asociativne pole, s parami nazov - hodnota premennej.
     */
    public void setVariables(Map<String,Value> vars) {
        eval.setVariables(vars);
    }
    /** Ziska premenne.
     * @return asociativne pole, s parami nazov - hodnota premennej.
     */
    public Map<String,Value> getVariables() {
        return eval.getVariables();
    }

    /** Prida premenne.
     * @param vars asociativne pole, s parami nazov - hodnota premennej.
     */
    public void addVariables(Map<String,Value> vars) {
        for(String key:vars.keySet()) {
            eval.setVariable(key, vars.get(key));
        }
    }
}