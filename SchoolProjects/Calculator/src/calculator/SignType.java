/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package calculator;

/**
 * Reprezentuje typ znamienka vo vyrazoch.
 */
public enum SignType {
    /** Ziadne znamienko. */
    SIGN_NO,
    /** Otvaracia zatvorka. */
    BRAC_OP, 
    /** Uzatvaracia zatvorka. */
    BRAC_CL,
    /** Aritmeticke operacie. */
    SIGN_PLUS, SIGN_MINUS, SIGN_MUL, SIGN_DIV, SIGN_MOD,
    /** Logicke operacie. */
    SIGN_AND, SIGN_OR, SIGN_LT, SIGN_GT, SIGN_LE, SIGN_GE, SIGN_EQ, SIGN_NE, SIGN_NOT;
}
