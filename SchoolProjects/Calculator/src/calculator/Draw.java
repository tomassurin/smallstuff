package calculator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.math.BigDecimal;
import javax.swing.*;

/** Definuje funkcie pre vykreslovanie grafov. Tieto budu volane
 * pomocou reflections API z vyhodnocovaca funkcii (FunctionEval).
 */
public class Draw {
    /** Vyhodnocovac funkcii. */
    FunctionEval funct = null;

    /** Minimalna x-ova suradnica. */
    private double xmindef = -5;
    /** Maximalna x-ova suradnica. */
    private double xmaxdef = 5;
    /** Krok v ktorom sa vykreslia dieliky na x-ovej osi. */
    private double xstepdef = 1;
    /** Krok v ktorom sa vykreslia dieliky na y-ovej osi. */
    private double ystepdef = 1;
    /** Minimalna y-ova suradnica. */
    private double ymindef = -5;
    /** Maximalna y-ova suradnica. */
    private double ymaxdef = 5;
    /** Ktok v ktorm sa budu prepocitavat body grafu. */
    private double st = 0.1;

    /** Nastavi minimalnu x-ovu suradnicu. */
    public Value setXmin(BigDecimal val) {
        xmindef = val.doubleValue();
        return new Value(true);
    }
    /** Nastavi minimalnu y-ovu suradnicu. */
    public Value setYmin(BigDecimal val) {
        ymindef = val.doubleValue();
        return new Value(true);
    }

    /** Nastavi maximalnu x-ovu suradnicu. */
    public Value setXmax(BigDecimal val) {
        xmaxdef = val.doubleValue();
        return new Value(true);
    }
    /** Nastavi maximalnu y-ovu suradnicu. */
    public Value setYmax(BigDecimal val) {
        ymaxdef = val.doubleValue();
        return new Value(true);
    }
    /** Nastavi krok v ktorom sa budu prepocitavat body grafu. */
    public Value setStep(BigDecimal val) {
        st = val.doubleValue();
        return new Value(true);
    }
    /** Nastavi krok v ktorom sa vykreslia dieliky na x-ovej osi. */
    public Value setTickStepX(BigDecimal val) {
        xstepdef = val.doubleValue();
        return new Value(true);
    }
    /** Nastavi krok v ktorom sa vykreslia dieliky na y-ovej osi. */
    public Value setTickStepY(BigDecimal val) {
        ystepdef = val.doubleValue();
        return new Value(true);
    }

    /** Reprezentuje okno grafu. */
    private class Graph extends JPanel implements Runnable {
        /** Vstupna funkcia, ktoru treba vykreslit. */
        String input;
        /** Nazov parametra, ktory reprezentuje hodnoty na x-ovej osi. */
        String var;

        private double xmin = xmindef;
        private double xmax = xmaxdef;
        private double xstep = xstepdef;
        private double ystep = ystepdef;
        private double ymin = ymindef;
        private double ymax = ymaxdef;
        /** Hodnota priblizenia. */
        private double zoom = 1.0;
        /** Konstanta udavajuca kolko priblizenia pripada na jednu otocku kolecka mysi. */
        private final double WHEEL_ZOOM = 0.03;

        /** Metoda px prevedie suradnice z kartezkej sustavy na suradnicu pre kreslenie v okne
         *
         * @param x Kartezka suradnica.
         * @return Suradnica v okne.
         */
        private int px(double x)
        {
            return (int)((x - xmin) * this.getWidth() / (xmax - xmin));
        }

        /** Metoda py prevedie suradnice z kartezkej sustavy na suradnicu pre kreslenie v okne
         *
         * @param y Kartezska suradnica.
         * @return Suradnica v okne.
         */
        private int py(double y)
        {
            return (int)((y - ymax) * this.getHeight() / (ymin - ymax));
        }
        
        /** Metoda kartpx prevedie suradnice pre kreslenie v okne do kartezskych suradnic
         *
         * @param x suradnica v okne
         * @return Kartezska suradnica.
         */
        private double kartpx(int x)
        {
            return (double)(x * (xmax - xmin) / this.getWidth() + xmin);
        }

        /** Metoda kartpy prevedie suradnice pre kreslenie v okne do kartezskych suradnic
         *
         * @param y suradnica v okne
         * @return Kartezska suradnica.
         */
        private double kartpy(int y)
        {
            return (double)(y * (ymin - ymax) / this.getHeight() + ymax);
        }

        Graph() {
            setBackground(Color.WHITE);
            setPreferredSize(new Dimension(800,800));
        }

        Graph(String input, String var) {
            this();
            this.input = input;
            this.var = var;

            this.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    JOptionPane.showMessageDialog(Graph.this,
                        "x = "+kartpx(e.getX())+"\n y = "+kartpy(e.getY()));
                }
            });
            this.addMouseWheelListener(new MouseAdapter() {
                 @Override
                public void mouseWheelMoved(MouseWheelEvent e) {
                   int clicks = e.getWheelRotation();
                   if (clicks == 0)
                       return;

                   zoom = zoom * (1.0 - WHEEL_ZOOM*clicks);

                   if (zoom < 0.2 ) {
                       zoom = 0.2;
                       return;
                   }
                   if (zoom > 5 ) {
                        zoom = 5;
                        return;
                   }

                   xmin =zoom*xmindef;
                   xmax =zoom*xmaxdef;
                   xstep =zoom*xstepdef;
                   ystep =zoom*ystepdef;
                   ymin =zoom*ymindef;
                   ymax =zoom*ymaxdef;
                   repaint();
                }
            });
        }

        /** Prekreslenie grafu. */
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Expression expr = new Expression(funct);
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(Color.BLACK);
            Value yval;

            int x = 0;
            int lastx = Integer.MIN_VALUE;
            int y = 0;
            int lasty = Integer.MIN_VALUE;
            double tmp;

            // nakreslime suradnicove osi
            g.drawLine(px(xmin), py(0), px(xmax), py(0));
            g.drawLine(px(0),py(ymin), px(0), py(ymax));

            tmp = xmin+xstep;
            double tick  = (Math.abs(ymin) + Math.abs(ymax)) / 300;
            int tickbord = 2;
            int ptickn = py(-tick);
            int ptick = py(tick);
            int fontsize = g.getFont().getSize();
            while (tmp < xmax)
            {
                x = px(tmp);
                if (Math.round(tmp*100) != 0) {
                    g.drawString(String.format("%1$1.2f", tmp), x+tickbord, ptickn+fontsize);
                    g.drawLine(x, ptickn, x, ptick);
                }
                tmp += xstep;
            }
            tmp = ymin+ystep;
            tick  = (Math.abs(xmin) + Math.abs(xmax)) / 300;
            ptickn = px(-tick);
            ptick = px(tick);
            while (tmp < ymax)
            {
                y = py(tmp);
                if (Math.round(tmp*100) != 0) {
                    g.drawString(String.format("%1$1.2f", tmp),ptickn-fontsize, y-tickbord);
                    g.drawLine(ptickn, y, ptick, y);
                }
                tmp += ystep;
            }

            g.drawString("0", px(0)+tickbord, py(0)+fontsize);

            // vlastne kreslenie grafu
            for(double i=xmin;i<=xmax;i+=st) {
                String s = input.replace("$"+var, String.valueOf(i));
                expr.setExpression(s);
                yval = expr.eval();
                if (yval.getType() != ValueType.NUMBER ) {
                    return;
                }
                x = px(i);
                tmp = yval.getNumber().doubleValue();
                if (tmp > 5*ymax || tmp < 5*ymin) {
                    lasty = Integer.MIN_VALUE;
                    continue;
                }
                y = py(tmp);
                
                if (lasty == Integer.MIN_VALUE ) {
                    g.drawLine(x, y, x, y);
                }
                else
                    g.drawLine(lastx, lasty, x, y);
                                   
                lastx = x;
                lasty = y;
            }
        }
        
        public void run() {
            JFrame frame = new JFrame("Graph");
            frame.add(this);
            frame.pack();
            frame.setVisible(true);
            repaint();
            try {
                Thread.sleep(20);
            } catch(InterruptedException ex) { }
        }
    }

    public Draw(FunctionEval f)
    {
        funct = f;
    }

    /** Vykresli funkciu v 2D.
     *
     * @param input funkcia, ktoru chceme vykreslit
     * @param var premenna, ktoru pokladame vo funkcii za x
     * @return Value(true)
     */
    public Value draw2D(String input, String var)
    {
        Graph graph = new Graph(input,var);
        SwingUtilities.invokeLater(graph);
        return new Value(true);
    }

}
