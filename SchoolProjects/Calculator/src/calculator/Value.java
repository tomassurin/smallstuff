package calculator;

import java.math.BigDecimal;
import java.math.BigInteger;

/** Trieda <b>Value</b> reprezentuje hodnotu vo vyraze. */
public class Value {

        /** Typ hodnoty. */
        public ValueType type;
        /** Logicka hodnota, ak nie je type == BOOL tak nie je definovana. */
        private boolean bool;
        /** Ciselna hodnota, ak nie je type == NUMBER tak nie je definovana. */
        private BigDecimal number;
        /** Retazcova hodnota, ak nie je type == STRING alebo VALUE_ERROR tak nie je definovana. */
        private String str;

        /** Konstruktor triedy. */
        Value() {
            clear();
        }

        /** Vytvori objekt triedy Value s boolean hodnotou val. */
        Value(boolean val) {
            setBool(val);
        }

        /** Vytvori objekt triedy Value s ciselnou hodnotou val. */
        Value(BigInteger val) {
            setNumber(new BigDecimal(val));
        }

        /** Vytvori objekt triedy Value s ciselnou hodnotou val. */
        Value(BigDecimal val) {
            setNumber(val);
        }

        /** Vytvori objekt triedy Value s retazcovou hodnotou val. */
        Value(String val) {
            setString(val);
        }

        /** Kopirovaci konstruktor. */
        Value(Value val) {
            this.bool = val.bool;
            this.number = val.number;
            this.str = val.str;
            this.type = val.type;
        }

        /** Inicializuje hodnotu objektu na implicitnu. Typ hodnoty nastavi na ziadny (void) */
        public void clear() {
            type = ValueType.VALUE_ERROR;
            bool = false;
            number = null;
            str = null;
        }

        /** Nastavi objekt na logicky typ. */
        public void setBool(boolean val) {
            clear();
            this.bool = val;
            this.type = ValueType.BOOL;
        }
        /** Nastavi objekt na ciselny typ. */
        public void setNumber(BigInteger val) {
            clear();
            this.number = new BigDecimal(val);
            this.type = ValueType.NUMBER;
        }
        /** Nastavi objekt na numericky typ. */
        public void setNumber(BigDecimal val) {
            clear();
            this.number = val;
            this.type = ValueType.NUMBER;
        }
        /** Nastavi objekt na retazcovy typ. */
        public void setString(String val) {
            clear();
            this.str = val;
            this.type = ValueType.STRING;
        }
        /** Ak je objekt typu bool tak vrati jeho hodnotu.
         * @return hodnotu objektu alebo false ak nie je typu bool
         */
        public boolean getBool() {
            if (type == ValueType.BOOL)
		return bool;
            else return false;
        }
        /** Ak je objekt ciselneho typu tak vrati jeho hodnotu.
         * @return hodnotu objektu alebo 0 ak nie je ciselneho typu
         */
        public BigDecimal getNumber() {
            if (type == ValueType.NUMBER)
		return number;
            else return BigDecimal.ZERO;
        }
        /** Ak je objekt typu retazec tak vrati jeho hodnotu.
         * @return hodnotu objektu alebo prazdny retazec ak nie je typu retazec
         */
        public String getString() {
            if (type == ValueType.STRING)
		return str;
            else return new String();
        }
        /** Nastavi objekt na chybovu hodnotu
         * @param err Chybova hlaska, ktoru nesie tento objekt
         */
        public Value setError(String err) {
            clear();
            type = ValueType.VALUE_ERROR;
            str = err;
            return this;
        }

        /** Staticka metoda, ktora vytvori instanciu tejto triedy nastavenu na hodnotu error.
         * @param error Chybova hlaska
         * @return instancia vytvorenej triedy
         */
        public static Value error(String error) {
            Value val = new Value();
            val.setError(error);
            return val;
        }

        /** Staticka metoda, ktora vytvori instanciu tejto triedy nesucu hodnotu void
         * @return instancia vytvorenej triedy
         */
        public static Value createVoid() {
            Value val = new Value();
            val.type = ValueType.VOID;
            return val;
        }
        /** Ak je objekt typu VALUE_ERROR tak vrati jeho hodnotu.
         * @return hodnotu objektu alebo prazdny retazec ak nie je daneho typu
         */
        public String getError() {
            if (type == ValueType.VALUE_ERROR)
                return str;
            else
                return new String();
        }

        /** Metoda na zistenie typu objektu
         * @return Typ objektu
         */
        public ValueType getType() {
            return this.type;
        }

        /** Konverzia na bool. */
        public boolean toBool() {
            switch (type) {
                case BOOL:
                     return bool;
                case NUMBER:
                     return number.compareTo(BigDecimal.ZERO) == 0?false:true;
                default:
                     return false;
            }
        }

        /** Konverzia na cislo. */
        public BigDecimal toNumber() {
            switch (type) {
		case NUMBER:
			return number;
		case BOOL:
			return bool == false? BigDecimal.ZERO:BigDecimal.ONE;
		case STRING:
                        BigDecimal val;
                        try {
                            val =new BigDecimal(str);
                        } catch (NumberFormatException ex) {
                            return BigDecimal.ZERO;
                        }
			return val;
		default:
			return BigDecimal.ZERO;
            }
        }

        /** Konverzia na retazec. */
        public String toString() {
            switch (type) {
		case NUMBER:
			return number.toString();
		case BOOL:
			return String.valueOf(bool);
		case STRING:
			return str;
                case VALUE_ERROR:
                        return getError();
		default:
			return new String();
            }
        }

        /** Metoda convert konsoliduje typy svojich parametrov na odpovedajuce.
         *
         * @param a prva hodnota na spracovanie
         * @param b druha hodnota na spracovanie
         * @return spolocny typ alebo ValueType.VALUE_ERROR ak sa nedaju typy skonsolidovat.
         */
        public static ValueType convert(Value a, Value b) {
            switch (a.getType()) {
		case BOOL:
			if (b.getType() == ValueType.BOOL)
				return ValueType.BOOL;
			else if (b.getType() == ValueType.NUMBER) {
                                return ValueType.VALUE_ERROR;
			}
			else if (b.getType() == ValueType.STRING) {
				a.setString(a.toString());
				return ValueType.STRING;
			}
			break;
		case NUMBER:
			if (b.getType() == ValueType.BOOL) {
                                return ValueType.VALUE_ERROR;
			}
			else if (b.getType() == ValueType.NUMBER)
				return ValueType.NUMBER;
			else if (b.getType() == ValueType.STRING) {
                                a.setString(a.toString());
				return ValueType.STRING;
			}
			break;
		case STRING:
			if (b.getType() == ValueType.BOOL) {
                                b.setString(b.toString());
				return ValueType.STRING;
			}
			else if (b.getType() == ValueType.NUMBER) {
				b.setString(b.toString());
				return ValueType.STRING;
			}
			else if (b.getType() == ValueType.STRING)
				return ValueType.STRING;
			break;
		default:
			break;
            }
            return ValueType.VALUE_ERROR;
        }     
}
