package calculator;

import javax.swing.SwingUtilities;

/** Vstupny bod aplikacie.  
 * @author Tomáš Šurín
 */
public class Main {
    /** Vstupna bod aplikacie. Nastartuje graficke prostredie. 
     * @param args Parametre z prikazovej riadky
     */
    public static void main(String[] args){
        SwingUtilities.invokeLater(Gui.getInstance());
    }

}
