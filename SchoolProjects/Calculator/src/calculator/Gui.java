package calculator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.CharBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

/** Singleton trieda, ktora predstavuje graficke rozhranie programu. */
public class Gui implements Runnable {
    /** Referencia na instanciu gui triedy. */
    private static Gui thisgui = null;

    private ScriptEval eval = null;
    private JFrame frame = null;
    private JPanel panel = null;
    private JTextArea scriptEdit = null;
    private JTextArea console = null;

    /** Privatny konstruktor - zabranenie vytvarania instancii. */
    private Gui() {   }

    /** Singleton factory funkcia na ziskanie instancie gui. */
    public static Gui getInstance() {
        if (thisgui == null) {
            thisgui = new Gui();
        }
        return thisgui;
    }

    /** Ziska hlavny JFrame tohoto gui. */
    public JFrame getContentFrame() {
        return frame;
    }
    /** Vypise na konzolu programu retazec what. */
    synchronized public void consoleWrite(String what) {
        console.append(what);
    }

    /** Vymaze obsah konzole. */
    synchronized public void consoleClear() {
        console.setText("");
    }

    /** Ukonci aplikaciu. */
    public void exit() {
        System.exit(0);
    }

    private void runScript() {
        String lines = scriptEdit.getText();
        eval = new ScriptEval();
        eval.setScript(lines);
        Value out = eval.execute();
        if (out.getType() != ValueType.VOID)
            console.append(">"+out+"\n");
    }

    /** Vytvori graficke rozhranie. */
    public void createAndShowGui() {
        JFrame.setDefaultLookAndFeelDecorated(false);
        frame = new JFrame("Calculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        panel = new JPanel(new BorderLayout());
        scriptEdit = new JTextArea();
        scriptEdit.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        //scriptEdit.setBackground(Color.LIGHT_GRAY);
        //scriptEdit.setForeground(Color.BLACK);
        final JScrollPane scriptEditScroll = new JScrollPane(scriptEdit);

        console = new JTextArea();
        console.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        //console.setBackground(Color.LIGHT_GRAY);
        //console.setForeground(Color.BLACK);
        final JScrollPane consoleScroll = new JScrollPane(console);
        final JSplitPane splitpane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scriptEditScroll, consoleScroll);

        panel.add(splitpane,BorderLayout.CENTER);

        JMenuBar menubar = new JMenuBar();

            JMenu filemenu = new JMenu("File");
            JMenuItem item = new JMenuItem("Open Script...");
            item.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JFileChooser chooser = new JFileChooser();
                    chooser.setDialogType(JFileChooser.OPEN_DIALOG);
                    int retval = chooser.showOpenDialog(null);
                    if (retval == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        scriptEdit.setText("");
                        BufferedReader in;
                        try {
                            in = new BufferedReader(new FileReader(file));
                            String str;
                            while ((str = in.readLine()) != null)
                                scriptEdit.append(str+"\n");
                            in.close();
                        } catch (FileNotFoundException ex) { }
                          catch (IOException ex) { }

                    }
                }
            });
            item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F2,0));
            filemenu.add(item);

            item = new JMenuItem("Save Script...");
            item.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JFileChooser chooser = new JFileChooser();
                    chooser.setDialogType(JFileChooser.SAVE_DIALOG);
                    int retval = chooser.showSaveDialog(null);
                    if (retval == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();
                        FileWriter out;
                        try {
                            out = new FileWriter(file);
                            out.write(scriptEdit.getText());
                            out.close();
                        } catch (FileNotFoundException ex) { }
                          catch (IOException ex) { }
                    }
                }
            });
            item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F3,0));
            filemenu.add(item);
            
            filemenu.addSeparator();
            
            item = new JMenuItem("Exit");
            item.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    exit();
                }
            });
            filemenu.add(item);

            JMenu runmenu = new JMenu("Run");
            item = new JMenuItem("Run Script");
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    runScript();
                }
            });
            item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5,0));
            runmenu.add(item);

        menubar.add(filemenu);
        menubar.add(runmenu);

        frame.setJMenuBar(menubar);
        frame.add(panel);
        frame.pack();
        frame.setMinimumSize(new Dimension(800,600));
        frame.setVisible(true);
        splitpane.setDividerLocation(0.75);
    }

    public void run() {
        createAndShowGui();
    }
    

}
