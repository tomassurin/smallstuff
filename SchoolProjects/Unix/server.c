#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <limits.h>

#ifdef linux
#include <string.h>
#else
#include <strings.h>
#endif

#include <ctype.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <errno.h>

/*konstanty*/
#define SERV_PORT 80
#define NUM_CLIENTS 1024
#define CR 13
#define LF 10

#define BUF_LEN 255
#define HTTP_TIME_LEN 30
char *month[]={ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
char *week_day[]={ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

/*globalne premenne*/
char *host;		//host adresa serveru
char *root;		//cesta k root adresaru serveru
char *index_file;	//nazov suboru ktory hladat ak je resource adresar
char http_version[]="HTTP/1.0";
int http_version_major=1;
int http_version_minor=0;
char server_name[]="JustAnotherMinimalHTTPServer/0.1 (UNIX)";

pthread_mutex_t log_file_mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t tz_mutex=PTHREAD_MUTEX_INITIALIZER;

/*definicie datovych typov*/
typedef enum { OTHER=0, GET_SIMPLE=1, GET=2, HEAD=3, POST=4 } request_method_t;
	
typedef struct {
	char *proto;
	char *host;
	int port;
	char *abs_path;
	char *query;
} uri_t;

typedef struct {
	request_method_t method;
	uri_t request_uri;
	int http_version_major;
	int http_version_minor;
	time_t if_modified_since;
} http_message_t;

/*funkcia vytvori z UNIX time razitka (time_t) HTTP datum/cas retazec vo tvare*/
//Sun, 06 Nov 1994 08:49:37 GMT 	
void time_to_http(time_t absolut_time, char *http_time_str)
{
	struct tm time;
	gmtime_r(&absolut_time,&time);
	sprintf(http_time_str,"%s, %d %s %d %02d:%02d:%02d GMT",week_day[time.tm_wday], time.tm_mday,
		month[time.tm_mon], time.tm_year+1900, time.tm_hour, time.tm_min, time.tm_sec);
}

/*funkcia konvertuje datum/cas v nasledujucich tvaroch na time_t*/
//Sun, 06 Nov 1994 08:49:37 GMT    ; RFC 822, updated by RFC 1123
//Sunday, 06-Nov-94 08:49:37 GMT   ; RFC 850, obsoleted by RFC 1036
//Sun Nov 6 08:49:37 1994         ; ANSI C's asctime() format
time_t http_to_time(char *str, time_t *absolut_time) 
{
	struct tm time;
	size_t i=0;
	int k;
	char temp[strlen(str)+1];
	size_t j=0;
	char *tz;
	if (strlen(str)<HTTP_TIME_LEN-1) {
		return -1;
	}
	else {
		while (isalpha(str[i])) {
			i++;
		}

		i+=2;
		time.tm_mday=10*(str[i]-'0')+str[i+1]-'0';

		i+=3;
		while(isalpha(str[i])) {
			temp[j++]=str[i];
			i++;
		}
		temp[j]='\0';
		for(k=0;k<12;++k) 	
			if (strcmp(month[k],temp)==0) break;
		time.tm_mon=k+1;
		
		i++;
		k=0;
		while (isdigit(str[i])) {
			k=k*10+str[i]-'0';
			i++;	
		}	
		if (k<100) {
			time.tm_year=k<=50?k+100:k;
		}
		else time.tm_year=k-1900; 
		
		i++;
		time.tm_hour=10*(str[i]-'0')+str[i+1]-'0';
		i+=3;
		time.tm_min=10*(str[i]-'0')+str[i+1]-'0';
		i+=3;
		time.tm_sec=10*(str[i]-'0')+str[i+1]-'0';
	}

	//problem s casovymi pasmami lebo mktime pracuje s lokalnym casom takze nastavime cas na GMT
	pthread_mutex_lock(&tz_mutex);
		tz=getenv("TZ");
		setenv("TZ","",1);	
		tzset();
		*absolut_time=mktime(&time);
		if (tz) 
			setenv("TZ","",1);
		else 	
			unsetenv("TZ");
		tzset();
	pthread_mutex_unlock(&tz_mutex);
	
	return *absolut_time;
}

/*funkcie pre pracu s polom stringov*/
typedef struct {
	char **str;
	size_t size;
	size_t capacity;
} string_array_t;

void string_array_free(string_array_t *arr)
{
	size_t i;
	for(i=0;i<arr->size;++i)
		free(arr->str[i]);
	free(arr->str);
	arr->size=0;
	arr->capacity=0;
}

int string_array_init(string_array_t *arr)
{
	arr->size=0;
	arr->capacity=50;
	if ((arr->str=(char**)malloc(sizeof(char*)*arr->capacity))==NULL) {
		arr->capacity=0;
		return -1;
	}
	return 0;
}

int string_array_add_row(string_array_t *arr, char *temp)
{
	arr->size++;
	if (arr->size>=arr->capacity) {
		if ((arr->str=(char**)realloc(arr->str,sizeof(char*)*(arr->capacity+50)))==NULL) {
			arr->size--;
			return -1;
		}
		arr->capacity+=50;
	}
	if ((arr->str[arr->size-1]=(char*)malloc(strlen(temp)+1))==NULL) {
		arr->size--;
		return -1;
	}
	strcpy(arr->str[arr->size-1],temp);
	return 0;
}

void string_array_print(int fd, string_array_t *arr)
{
	size_t i;
	for(i=0;i<arr->size;++i)
		write(fd,arr->str[i],strlen(arr->str[i]));	
}
/**************************************************/

/*funkcie pre pracu so strukturou log-suboru*/
typedef struct {
	string_array_t log;
	char *temp;
	size_t temp_capacity;
	char *final;
	size_t final_capacity;
} log_file_t;

int log_file_add(log_file_t *log_file, const char *fmt, ...) 
{
	va_list arg;
	int n;
	log_file->final[0]='\0';
	if (strlen(fmt)==0) {
		if (string_array_add_row(&log_file->log,"\n")!=0) return -1;
	}
	else {
		time_to_http(time(NULL),log_file->final);
		while (1) {
			log_file->temp[0]='\0';
			va_start(arg,fmt);
			n=vsnprintf(log_file->temp, log_file->temp_capacity, fmt, arg);	
			va_end(arg);
			if (n>-1 && n<log_file->temp_capacity) {
				break;
			}
			
			if ((log_file->temp=(char*)realloc(log_file->temp,log_file->temp_capacity*2))==NULL) {
				return -1;
			}
			log_file->temp_capacity*=2;
		}
		if ((log_file->final=(char*)realloc(log_file->final,log_file->temp_capacity+HTTP_TIME_LEN+3))==NULL) {
			return -1;
		}
		log_file->final=strcat(log_file->final,": ");
		log_file->final=strcat(log_file->final,log_file->temp);
		if (string_array_add_row(&log_file->log,log_file->final)!=0) return -1;
	}
	return 0;
}

void log_file_print(int fd, log_file_t *log_file) 
{
	string_array_print(fd, &log_file->log);
}

void log_file_free(log_file_t *log_file) 
{
	string_array_free(&log_file->log);
	log_file->temp_capacity=0;
	log_file->final_capacity=0;
	free(log_file->temp);
	free(log_file->final);
}

int log_file_init(log_file_t *log_file) 
{
	string_array_init(&log_file->log);
	log_file->temp_capacity=BUF_LEN;
	if ((log_file->temp=(char*)malloc(log_file->temp_capacity))==NULL) {
		return -1;
	}
	log_file->final_capacity=BUF_LEN+HTTP_TIME_LEN;
	if ((log_file->final=(char*)malloc(log_file->final_capacity))==NULL) {
		return -1;
	} 
	return 0;
}
/**************************************************/

/*funkcia vyparsuje vstupny string raw na tokeny*/
int parse_tokens(char *raw, string_array_t *tok, char* delim, size_t tokens_count)
{
	size_t i,j;
	size_t raw_size=strlen(raw);
	size_t delim_size=strlen(delim);
	size_t temp_size=0;
	char temp[raw_size+1];
	int delim_found=0;
	
	string_array_free(tok);
	if (string_array_init(tok)!=0) return -1;
	
	for(i=0;i<=raw_size;++i) {
		if (tokens_count==0 || (tok->size<tokens_count-1)) {
			for(j=0;j<delim_size;++j) {
				if (raw[i]==delim[j]) {
					delim_found=1;
					break;		
				} 
			}
		}
		
		if (i==raw_size || delim_found) {
			delim_found=0;
			if (temp_size!=0) {
				temp[temp_size]='\0';
				if (string_array_add_row(tok,temp)!=0) return -1;
				temp_size=0;
			}
		}		
		else {
			temp[temp_size++]=raw[i];
		}
	}

	return 0;
}

/*funkcia vymaze zo vstupneho stringu str znaky ktore sa nachadzaju v retazci delim*/
void eradicate_delim(char *str,char *delim)
{
	size_t i,j;
	size_t delim_size=strlen(delim);
	size_t str_size=strlen(str);
	size_t temp_size=0;
	char temp[str_size+1];
	int delim_found=0;
	
	for(i=0;i<str_size;++i) {
		for(j=0;j<delim_size;++j) 
			if (str[i]==delim[j]) {	
				delim_found=1;
				break;
			}
		
		if (delim_found) {
			delim_found=0;
			continue;
		}
		temp[temp_size++]=str[i];
	}
	temp[temp_size]='\0';
	str=strcpy(str,temp);
}

/*funkcia dostane ako vstup url adresu z ktorej vyparsuje protokol, host adresu, port a abs cestu*/
int url_parse(char *url, uri_t *uri) 
{
	int i,j;	
	size_t url_size=strlen(url);
	char temp[url_size+1];
	j=0;
	uri->proto=NULL;
	uri->host=NULL;
	uri->port=SERV_PORT;
	uri->abs_path=NULL;
	uri->query=NULL;

	for(i=0;i<url_size;++i)
	{
		url[i]=tolower(url[i]);
		if (uri->proto==NULL && uri->host==NULL && (url[0]!='/' || (url[0]=='/' && url[1]=='/'))) {
			if (url[0]=='/' && url[1]=='/') {
				if ((uri->proto=(char*)malloc(3))==NULL) return -1;
				strcpy(uri->proto,"//");
				i=2;
			}
			while (url[i]!='/' && url[i]!=':' && i<strlen(url)) {
				temp[j++]=url[i];
				i++;	
			}
			if (url[i+1]=='/' && url[i+2]=='/' && uri->proto==NULL) {
				temp[j]='\0';
				if ((uri->proto=(char*)malloc(strlen(temp)+1))==NULL) return -1;
				strcpy(uri->proto,temp);
				j=0;
				i+=3;
				while (url[i]!=':' && url[i]!='/' && i<strlen(url)) {
					temp[j++]=url[i];
					i++;
				}
			}
			temp[j]='\0';
			if ((uri->host=(char*)malloc(strlen(temp)+1))==NULL) return -1;
			strcpy(uri->host,temp);
			j=0;
			if (url[i]==':') {
				i++;
				while (isdigit(url[i]) && i<strlen(url)) {
					temp[j++]=url[i];
					i++;
				}
				temp[j]='\0';
				uri->port=atol(temp);	
				if (uri->port==0) uri->port=SERV_PORT;
				j=0;
			}
			i--;
			continue;
			
		}

		if (url[i]=='%') {
			if (isxdigit(url[i+1]) && isxdigit(url[i+2])) {
				temp[j++]=16*(isdigit(url[i+1])?url[i+1]-'0':tolower(url[i+1])-'a'+10)+
		  			     (isdigit(url[i+2])?url[i+2]-'0':tolower(url[i+2])-'a'+10);	
			}	
			else return -1;
			i+=2;
			continue;
		}
 		if (url[i]=='+') { 
			temp[j++]=' ';
			continue;
		}

		if (url[i]=='/' && url[i+1]=='/') continue;
		//if (url[i]=='.' && url[i+1]=='.') continue;
		//if ((url[i]=='.' && url[i+1]=='.') || (url[i-1]=='.' && url[i]=='.')) continue;

		if (uri->abs_path==NULL && (i>=url_size-1 || url[i]=='?')) {
			if (i==url_size-1) temp[j++]=url[i];
			temp[j]='\0';
			if (j==0 || temp[0]!='/') {
				if ((uri->abs_path=(char*)malloc(j==0?1:strlen(temp)+1))==NULL) return -1;
				uri->abs_path[0]='/';
				uri->abs_path[1]='\0';
				strcat(uri->abs_path+1,temp);
			}
			else {
				if ((uri->abs_path=(char*)malloc(strlen(temp)+1))==NULL) return -1;
				strcpy(uri->abs_path,temp);
			}
			j=0;
			i++;
			if (i==url_size) break;
		}	
		
		temp[j++]=url[i];		 
	}

	if (uri->abs_path!=NULL && j!=0) {
		temp[j]='\0';
		if ((uri->query=(char*)malloc(strlen(temp)+1))==NULL) return -1;
		strcpy(uri->query,temp);
	}
	
	return 0;
}

/*funkcia ziska request od klienta s deskriptorom fd a vyparsuje ho*/
int get_raw_headers(int fd, string_array_t *request)
{
	size_t readsize;
	int i;
	char buf[BUF_LEN];
	string_array_t tok;
	char *temp;
	size_t temp_capacity=BUF_LEN;
	size_t temp_size=0;
	int whitespace=0;
	fd_set readfds;
	struct timeval tv;
	
	if (string_array_init(request)!=0) return -1;	
	if (string_array_init(&tok)!=0) return -1;
	
	if ((temp=(char*)malloc(BUF_LEN))==NULL) {
		string_array_free(&tok);
		return -1;
	}

	for(;;) {
		FD_ZERO(&readfds);
		FD_SET(fd,&readfds);
		tv.tv_sec=1;
		tv.tv_usec=0;

		select(fd+1,&readfds,NULL,NULL,&tv);
		if (FD_ISSET(fd,&readfds)) break;
	}
	// cyklus rozparsuje vstupny request do struktury request 
        // do kazdej polozky pola request->header ulozi 1 riadok (podla rfc je riadok zakonceny CRLF) zo vstupneho requestu
	while((readsize=read(fd,buf,BUF_LEN))>0) {
		if (readsize==-1) {
			break;
		}
		for(i=0;i<readsize-1;++i) {
			if (request->size==0 && (buf[0]==CR && buf[1]==LF && temp_size==0)) 
				break;			     			//orezania zaciatocnych prazdnych riadkov
			if (buf[i]==CR && buf[i+1]==LF && temp_size==0) goto request_end;  // koniec requestu
			if (buf[i]==CR && buf[i+1]==LF) { 			//koniec riadku
				temp[temp_size]='\0';
				whitespace=0;
				if (string_array_add_row(request,temp)!=0) {
					string_array_free(&tok);
					free(temp);
					return -1;
				}
			 	++i;
				temp_size=0;
				// simple request je v tvare GET /path CRLF (tj. v requeste nie je prazdny riadok s CRLF)
				if (request->size==1) {
					if (parse_tokens(temp, &tok, " \t", 3)!=0) {
						string_array_free(&tok);
						free(temp);
						return -1;
					}	
					if (tok.size<3) goto request_end;
				}
			}
			else {
				if (temp_size>=temp_capacity) {
					temp_capacity*=2;
					if ((temp=(char*)realloc(temp,1*temp_capacity))==NULL) {
						string_array_free(&tok);
						free(temp);
						return -1;
					}
				}
				//preskocime viac whitespaces
				if (buf[i]==' ' || buf[i]=='\t') {
					if (whitespace!=0) {
						continue;
					}
					whitespace=1;
				}
				else whitespace=0;
				temp[temp_size++]=buf[i]=='\t'?' ':buf[i];
			}
		}
	}
    request_end:
    	
	/*reallokacia pola headerov na potrebnu dlzku*/
	if ((request->str=(char**)realloc(request->str,sizeof(char*)*request->size))==NULL) {
		string_array_free(&tok);
		free(temp);
		return -1;
	}
	request->capacity=request->size;

	string_array_free(&tok);
	free(temp);
	return 0;
}

/*struktura vyparsuje z requestu header s nazvom name vo forme zoznamu tj. value,value,value*/
char *parse_header(string_array_t *raw, char *name, char *value, size_t value_length)
{
	size_t i,j;
	size_t temp_size=0;
	size_t temp_capacity=30;
	char *temp;
	string_array_t tok;

	if ((temp=(char*)malloc(temp_capacity+1))==NULL) return NULL;
	temp[0]='\0';
	value=NULL;
	if (string_array_init(&tok)!=0) {
		free(temp);
		return NULL;
	}

	for(i=0;i<strlen(name);++i) name[i]=tolower(name[i]);

	for(i=1;i<raw->size;++i) {
		if (tolower(raw->str[i][0])==name[0]) {
			parse_tokens(raw->str[i],&tok,":",2);
			for(j=0;j<strlen(tok.str[0]);++j) tok.str[0][j]=tolower(tok.str[0][j]);
			if (strcmp(tok.str[0],name)==0) {   //MATCH
				temp_size+=strlen(tok.str[1]);       //+1 for ','
				if (temp_size>=temp_capacity) {
					temp_capacity+=temp_size;
					if ((temp=(char*)realloc(temp,temp_capacity+1))==NULL) {
						free(temp);
						string_array_free(&tok);
						return NULL;
					}
				}
				temp=strcat(temp,tok.str[1]+1);
				temp[temp_size-1]=',';
				while (i+1<raw->size && raw->str[i+1][0]==' ') {
					i++;
					temp_size+=strlen(raw->str[i]);
					if (temp_size>=temp_capacity) {
						temp_capacity+=temp_size;
						if ((temp=(char*)realloc(temp,temp_capacity+1))==NULL) {
							free(temp);
							string_array_free(&tok);
							return NULL;
						}
					}
					temp=strcat(temp,raw->str[i]+1);
					temp[temp_size-1]=',';
				}
			}
		}
	}

	if (temp_size==0) return NULL;
	else {
		temp[temp_size-1]='\0';
		if (value_length==0) {
			if ((value=(char*)malloc(temp_size+1))==NULL) {
				free(temp);
				string_array_free(&tok);
				return NULL;
			}
			value[0]='\0';
			value=strcpy(value,temp);
		}
		if (value_length>=temp_size+1) {
			if ((value=(char*)realloc(temp,temp_size+1))==NULL) {
				free(temp);
				string_array_free(&tok);
				return NULL;
			}
			value[0]='\0';
			value=strcpy(value,temp);
		}
	}
	
	string_array_free(&tok);
	free(temp);
	return value;
}

/*funkcia sluzi na vytvorenie response spravy a jej odposlanie na socket fd*/
void response_general(int fd, int statuscode, int last, log_file_t *log)
{
	char reason_phrase[50];
	char temp[BUF_LEN];
	size_t temp_capacity=BUF_LEN;
	char http_time_str[HTTP_TIME_LEN];
	temp[0]='\0';
	string_array_t response;
	string_array_init(&response);

	switch (statuscode) {
		case 200: memcpy(reason_phrase,"OK",3);
		 break;
		case 304: memcpy(reason_phrase, "Not Modified",13);
		 break;
		case 400: memcpy(reason_phrase,"Bad Request",12);
		 break;
		case 401: memcpy(reason_phrase,"Unauthorized",13);
		 break;
		case 403: memcpy(reason_phrase,"Forbidden",10);
		 break;
		case 404: memcpy(reason_phrase,"Not Found",10);
		 break;
		case 500: memcpy(reason_phrase,"Internal Server Error",22);
		 break;
		case 501: memcpy(reason_phrase,"Not Implemented",16);
		 break;
		default: { memcpy(reason_phrase,"Other",6);}
	}
	//log file
	log_file_add(log,"Response: %d %s\n",statuscode,reason_phrase);

	//status line
	snprintf(temp,temp_capacity, "%s %d %s%c%c", http_version, statuscode, reason_phrase, CR, LF);
	string_array_add_row(&response,temp);
	temp[0]='\0';
	//date/time
	time_to_http(time(NULL),http_time_str);
	snprintf(temp,temp_capacity, "Date: %s%c%c", http_time_str, CR, LF);
	string_array_add_row(&response,temp);
	temp[0]='\0'; 
	//server header
	snprintf(temp,temp_capacity, "Server: %s%c%c", server_name, CR, LF);
	string_array_add_row(&response,temp);
	temp[0]='\0';
	//CRLF
	if (last) {
		snprintf(temp,temp_capacity, "%c%c", CR, LF);
		string_array_add_row(&response,temp);
	}
			
	//vypis na socket	
	string_array_print(fd,&response);
			
	//upratovanie
	string_array_free(&response);
}

/*funkcia ziska ziska request od klienta a vyparsuje ho do struktury http_message_t*/
int get_http_request(int fd, http_message_t *request, log_file_t *log)
{
	size_t i;
	int tmp;
	string_array_t raw_headers;
	string_array_t tok;
	size_t val_size=0;
	char *val=NULL;
	char temp[BUF_LEN];
	
	if (string_array_init(&tok)!=0) {
		log_file_add(log,"Internal server Error\n");
		response_general(fd,500,1,log);
		return -1;
	}

	if (get_raw_headers(fd,&raw_headers)!=0) {
		log_file_add(log,"Internal server Error\n");
		response_general(fd,500,1,log);
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return -1;
	}

	if (raw_headers.size==0) {
		log_file_add(log,"Request: Empty\n");
		response_general(fd,400,1,log);
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return -1;
	}

	log_file_add(log,"Request: %s\n",raw_headers.str[0]);

	//request-line
	if (parse_tokens(raw_headers.str[0],&tok," \t",3)!=0) {
		log_file_add(log,"Internal server Error\n");
		response_general(fd,500,1,log);
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return -1;
	}

	if (tok.size<2) {
		response_general(fd,400,1,log);	
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return -1;
	}	
	
	//zistenie metody
	if (strcmp(tok.str[0],"GET")==0) request->method=GET;
	else if (strcmp(tok.str[0],"HEAD")==0) request->method=HEAD;
		else  if (strcmp(tok.str[0],"POST")==0) request->method=POST;
			else request->method=OTHER;

	//ziskanie request-uri
	if (url_parse(tok.str[1],&(request->request_uri))!=0) {
		log_file_add(log,"Internal server Error\n");
		response_general(fd,500,1,log);
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return -1;	
	}
	
	if (tok.size==2) request->method=GET_SIMPLE;

	if (request->request_uri.abs_path==NULL) {
		if (request->method!=GET_SIMPLE) response_general(fd,400,1,log);
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return -1;
	}
	
	if (request->method==GET_SIMPLE) {
		string_array_free(&raw_headers);
		string_array_free(&tok);
		return 0;
	}
	else {
		//ziskanie http_verzie
		eradicate_delim(tok.str[2]," \t");
		if (strlen(tok.str[2])<6) {
			request->method=GET_SIMPLE;
			return 0;
		}
		i=5;tmp=0;
		while ((i<strlen(tok.str[2])) && (isdigit(tok.str[2][i]))) {
			tmp=tmp*10+tok.str[2][i]-'0';
			i++;
		}
		request->http_version_major=tmp;
		tmp=0;
		i++;
		while ((i<strlen(tok.str[2])) && (isdigit(tok.str[2][i])) ) {
			tmp=tmp*10+tok.str[2][i]-'0';
			i++;
		}
		request->http_version_minor=tmp;
	}
	
	temp[0]='\0';
	memcpy(temp,"If-Modified-Since",18);
	val=parse_header(&raw_headers, temp, val, val_size);
	if (val!=NULL) 	{
		if (http_to_time(val, &request->if_modified_since)==-1) request->if_modified_since=0;
		free(val);
	}
	temp[0]='\0';

	string_array_free(&raw_headers);
	string_array_free(&tok);
	return 0;
}

/*funkcia na obsluhu metody GET*/
int response_GET(int fd,http_message_t *request, log_file_t *log)
{
	int f;
	char buf[BUF_LEN];
	size_t readsize;
	char *abs_path;
	char *rel_path;
	size_t sz,root_sz,uri_sz,index_sz;
	struct stat stat_buf;
	size_t temp_capacity=BUF_LEN;
	char *temp;
	char http_time_str[HTTP_TIME_LEN];
	string_array_t response;

	if ((temp=(char*)malloc(temp_capacity))==NULL) {
		log_file_add(log,"Can't malloc memory\n");
		response_general(fd,500,1,log);
		return -1;
	}
	
	if (string_array_init(&response)!=0) {
		response_general(fd,500,1,log);
		free(temp);
		return -1;
	}
	
	root_sz=strlen(root);
	uri_sz=strlen(request->request_uri.abs_path);
	sz=uri_sz+root_sz+1;
	if ((rel_path=(char*)malloc(sz))==NULL) {
		log_file_add(log,"Can't malloc memory\n");
		free(temp);
		string_array_free(&response);
		response_general(fd,500,1,log);
		return -1;
	}
	memcpy(rel_path,root,root_sz);
	memcpy(rel_path+root_sz,request->request_uri.abs_path,uri_sz+1);

	if ((abs_path=realpath(rel_path,NULL))==NULL) {
		log_file_add(log,"Can't resolve real path\n");
		response_general(fd,404,1,log);
		free(temp);
		free(rel_path);
		string_array_free(&response);
		return -1;
	}

	if (stat(abs_path,&stat_buf)!=0) {
		if (errno==EACCES) response_general(fd,403,1,log);
			else response_general(fd,500,1,log);
		goto response_GET_maintenance;
	}

	if (S_ISDIR(stat_buf.st_mode)) {
		index_sz=strlen(index_file);
		sz+=index_sz;
		if ((rel_path=(char*)realloc(rel_path,sz))==NULL) {
			log_file_add(log,"Can't realloc memory\n");
			response_general(fd,500,1,log);
			goto response_GET_maintenance;		
		}
		memcpy(rel_path+root_sz+uri_sz,index_file,index_sz+1);
		if ((abs_path=realpath(rel_path,NULL))==NULL) {
			log_file_add(log,"Can't resolve real path\n");
			response_general(fd,404,1,log);
			goto response_GET_maintenance;
		}	
		if (stat(abs_path,&stat_buf)!=0) {
			if (errno==EACCES) response_general(fd,403,1,log);
				else response_general(fd,500,1,log);
			goto response_GET_maintenance;
		}
	}

	if (strstr(abs_path,root)==NULL) {
		response_general(fd,403,1,log);
		goto response_GET_maintenance;
	}

	if ((f=open(abs_path,O_RDONLY))==-1) {
		if (request->method!=GET_SIMPLE) {
			if (errno==EACCES) {
				response_general(fd,403,1,log);
			}
			else response_general(fd,404,1,log);
		}
		goto response_GET_maintenance;
	}

	if (request->method!=GET_SIMPLE) { 
		if (stat_buf.st_mtime<=request->if_modified_since) {
			response_general(fd,304,1,log);
			
			close(f);
			goto response_GET_maintenance;
		}

		response_general(fd,200,0,log);

		if (abs_path[0]=='/') snprintf(temp,temp_capacity, "Location: http://%s/%s%c%c",host, rel_path, CR, LF);
		else snprintf(temp,temp_capacity, "Location: http://%s/%s%c%c",host,rel_path, CR, LF);
		if (string_array_add_row(&response,temp)!=0) {
			response_general(fd,500,1,log);
			goto response_GET_maintenance;
		}
		temp[0]='\0';

		time_to_http(stat_buf.st_mtime,http_time_str);
		snprintf(temp,temp_capacity, "Last-Modified: %s%c%c",http_time_str, CR, LF);
		if (string_array_add_row(&response,temp)!=0) {
			response_general(fd,500,1,log);
			goto response_GET_maintenance;
		}
		temp[0]='\0';

		snprintf(temp,temp_capacity, "Content-Length: %d%c%c", (unsigned)stat_buf.st_size, CR, LF);
		if (string_array_add_row(&response,temp)!=0) {
			response_general(fd,500,1,log);
			goto response_GET_maintenance;
		}
		temp[0]='\0';

		snprintf(temp,temp_capacity, "Connection: close%c%c", CR, LF);
		if (string_array_add_row(&response,temp)!=0) {
			response_general(fd,500,1,log);
			goto response_GET_maintenance;
		}
		temp[0]='\0';

		snprintf(temp,temp_capacity, "%c%c", CR, LF);
		if (string_array_add_row(&response,temp)!=0) {
			response_general(fd,500,1,log);
			goto response_GET_maintenance;
		}

		string_array_print(fd,&response);
	}

	if (request->method!=HEAD) {
		log_file_add(log,"Transfer of file \"%s\" started\n",abs_path);
		log_file_add(log,"...\n");
		while((readsize=read(f,buf,255))>0) {
			write(fd,buf,readsize);
		}
		log_file_add(log,"Transfer finished\n");
	}
	close(f);

    response_GET_maintenance:

	
	free(abs_path);
	free(rel_path);
	string_array_free(&response);
	free(temp);
	printf("790\n");
	return 0;
}

/*vstupny bod vlakien klientov*/
int client_handler(int serv_fd)
{
	int newsock;
	struct sockaddr_in client_addr;
	size_t sz=sizeof(client_addr);
	http_message_t request;
	log_file_t log;
	char netaddr[16];
	
	if ((newsock=accept(serv_fd,(struct sockaddr*)&client_addr,&sz))==-1) {
		return -1;
	}
	
	log_file_init(&log);
	log_file_add(&log, "");
	log_file_add(&log, "Client %s:%d connected...\n", inet_ntop(AF_INET, (const void*)&client_addr.sin_addr, netaddr, 16), ntohs(client_addr.sin_port));

	if (get_http_request(newsock,&request,&log)!=-1) {	
		switch (request.method) {
			case GET_SIMPLE: 
			case HEAD:
			case GET: response_GET(newsock, &request, &log);
			break;
			case POST:
			case OTHER:
			default: response_general(newsock,501,1,&log);
		}
	}

	//ukoncenia spojenia s klientom
	log_file_add(&log, "Client quits ...\n");
	
	pthread_mutex_lock(&log_file_mutex);
		log_file_print(0, &log);
	pthread_mutex_unlock(&log_file_mutex);

	log_file_free(&log);
	close(newsock);		
	return 0;
}

int main(int argc, char ** argv)
{
	int serv_fd;
	unsigned int serv_port=SERV_PORT;		//defaultny port
	struct sockaddr_in serv_addr;
	extern char *optarg;
	int opt;	
	size_t sz;
	pthread_t client_thread;
	pthread_attr_t attr;
	sigset_t sigset;
	fd_set readfds;
	char timestamp[HTTP_TIME_LEN];
	struct timeval tv;
	tv.tv_sec=1;
	tv.tv_usec=0;
		
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);

	
	if ((host=(char*)malloc(10))==NULL) {
		time_to_http(time(NULL),timestamp);
		fprintf(stderr,"%s: Can't malloc memory\n",timestamp);
		exit(1);
	}
	host=memcpy(host,"localhost",10);

	if ((root=(char*)malloc(7))==NULL) {
		time_to_http(time(NULL),timestamp);
		fprintf(stderr,"%s: Can't malloc memory\n",timestamp);
		exit(1);
	}
	root=memcpy(root,"htdocs",7);	

	if ((index_file=(char*)malloc(11))==NULL) {
		time_to_http(time(NULL),timestamp);
		fprintf(stderr,"%s: Can't malloc memory\n",timestamp);
		exit(1);
	}
	index_file=memcpy(index_file,"default.htm",11);

	/*ziskanie argumentov programu*/
	/*
		-r path       Cesta k root adresaru serveru
		-i filename   Nazov defaultneho suboru z adresara
		-p port	      Server port
		-h hostname   hostname serveru
	*/
	while((opt=getopt(argc,argv,"r:p:h:"))!=-1) {
		switch(opt) {
		case 'r': 
			sz=strlen(optarg);
			if ((root=(char*)realloc(root,sz))==NULL) {
				time_to_http(time(NULL),timestamp);
				fprintf(stderr,"%s: Can't realloc memory\n",timestamp);
				exit(1);
			}				
			root=memcpy(root,optarg,sz);
		  break;
		case 'p':
			serv_port=atoi(optarg);
		  break;
		case 'h':
			sz=strlen(optarg);
			if ((host=(char*)realloc(host,sz))==NULL) {
				time_to_http(time(NULL),timestamp);
				fprintf(stderr,"%s: Can't realloc memory\n",timestamp);
				exit(1);
			}
			host=memcpy(host,optarg,sz);
		  break;
		}	
	}

	/*nastavenie blokovania signalov*/
        sigemptyset(&sigset);
        sigaddset(&sigset, SIGABRT);
        sigaddset(&sigset, SIGTERM);
        sigaddset(&sigset, SIGINT);
        sigaddset(&sigset, SIGQUIT);
	sigaddset(&sigset, SIGSEGV);
	sigaddset(&sigset, SIGPIPE);
	sigprocmask(SIG_BLOCK,&sigset,NULL);

        /*initialize connection*/
	time_to_http(time(NULL),timestamp);
	fprintf(stderr,"%s: Server is starting ...\n", timestamp);
	/*vytvori socket a nastavit jeho file descriptor na non-blocking*/
	if ((serv_fd=socket(AF_INET, SOCK_STREAM, 0)) ==-1) {
		time_to_http(time(NULL),timestamp);
		fprintf(stderr,"%s: Error opening socket\n",timestamp);
		exit(1);
	} 

	opt=fcntl(serv_fd, F_GETFL) | O_NONBLOCK;
	fcntl(serv_fd, F_SETFL,opt);

	/*povoli znovu pouzitie adriesy serveru po skonceni programu*/
	opt=1;
	setsockopt(serv_fd,SOL_SOCKET,SO_REUSEADDR,&opt,sizeof(opt));

	/*bind socket*/
	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_port=htons(serv_port);
	serv_addr.sin_addr.s_addr=INADDR_ANY;
	if (bind(serv_fd,(struct sockaddr*)&serv_addr,sizeof(serv_addr))==-1) {
		time_to_http(time(NULL),timestamp);
		fprintf(stderr,"%s: Error binding socket to address\n",timestamp);
		exit(1);
	}

	/*listening for new connections*/
	if (listen(serv_fd,NUM_CLIENTS)==-1) {
		time_to_http(time(NULL),timestamp);
		fprintf(stderr,"%s: Error in listen\n",timestamp);
		exit(1);
	}
	time_to_http(time(NULL),timestamp);
	fprintf(stderr,"%s: Server is running on port %d\n",timestamp, serv_port);
	
	/*hlavny cyklus*/
	for(;;) {
		FD_ZERO(&readfds);
		FD_SET(serv_fd,&readfds);
		tv.tv_sec=1;
		tv.tv_usec=0;
		
		select(serv_fd+1,&readfds,NULL,NULL,&tv);
		
		//zistime ci sa da zo serv_fd citat (=> cakajuce spojenia)
		if (FD_ISSET(serv_fd,&readfds))
			//zalozime vlakno pre obsluhu klienta
			
			if (pthread_create(&client_thread, &attr,
				(void*(*)(void*))client_handler, (void*)(uintptr_t)serv_fd)!=0) 
				{
					time_to_http(time(NULL),timestamp);
					fprintf(stderr,"%s: Error Creating Client Thread\n",timestamp);	
				}
		
		/*signals handling*/
		sigpending(&sigset);
		if(sigismember(&sigset, SIGABRT) || 
                   sigismember(&sigset, SIGTERM) ||
		   sigismember(&sigset, SIGINT)  ||
		   sigismember(&sigset, SIGQUIT) ||
		   sigismember(&sigset, SIGSEGV) ||
		   sigismember(&sigset, SIGUSR1)) {
			break;
		}
	}

	/*ukoncenie serveru*/
	fprintf(stderr,"\n");
	time_to_http(time(NULL),timestamp);
	fprintf(stderr,"%s: Server is shuting down ...\n",timestamp);
	close(serv_fd);
	serv_fd=-1;

	return 0;
}
