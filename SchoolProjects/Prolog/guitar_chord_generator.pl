%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Zapoctovy program -- Neproceduralne programovanie                          %
% Letny semester 2009                                                        %
%                                                                            %
% Autor: Tomas Surin                                                         %
%                                                                            %
% Tema: Generator gitarovych akordov                                         %
%                                                                            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Fakt tony([...]). reprezentuje jednu oktavu poltonov
% V celom programe budem pouzivat len tieto mena tonov - enharmonicke zameny nebudem uvazovat (napr tony Des a Cis sa rovnaju)
% Kedze vystupom programu bude diagram popisujuci kde na hmatniku gitary zahrad dany akord tak to nevadi - tieto nazvy nepotrebujem.
tony([c,cis,d,dis,e,f,fis,g,gis,a,ais,h]).

% je_zakl_ton - fakty, vyjadrujuce zakladne tony
je_zakl_ton('c'). je_zakl_ton('d'). je_zakl_ton('e'). je_zakl_ton('f').
je_zakl_ton('g'). je_zakl_ton('a'). je_zakl_ton('h').

% intervaly  interval(Oznacenie,Maly(0)/Velky(1),Poradie_poltonu)
% ciste/zmensene   velke
interval(1,_,0).
interval(2,0,1).   interval(2,1,2).
interval(3,0,3).   interval(3,1,4).
interval(4,_,5).
interval(5,_,7).
interval(6,0,8).   interval(6,1,9).
interval(7,0,10).  interval(7,1,11).
interval(8,_,12).
interval(9,0,13).  interval(9,1,14).
interval(10,0,15). interval(10,1,16).
interval(11,_,17).
interval(12,_,19).
interval(13,0,20). interval(13,1,21).
interval(14,0,22). interval(14,1,23).
interval(15,_,24).

% interval_*(+X,-O)
interval_zmenseny(X,O):-interval(X,0,O1), O is O1-1.
interval_zvacseny(X,O):-interval(X,1,O1), O is O1+1.

% struny
struny([e,a,d,g,h,e]).
 struna(e,[e,f,fis,g,gis,a]).
 struna(a,[a,ais,h,c,cis,d]).
 struna(d,[d,dis,e,f,fis,g]).
 struna(g,[g,gis,a,ais,h,c]).
 struna(h,[h,c,cis,d,dis,e]).
%struna(E,[e,f,fis,g,gis,a]).

% 0, 1,   2,  3,   4,  5, 6,   7, 8,   9,  10,  11, 12, 13,  14, 15,  16,  17, 20,  21,
% 1, 2m,  2v, 3m,  3v, 4,      5, 6m,  6v, 7m,  7v, 8,  9m,  9v, 10m, 10v, 11, 13m, 13v,
% c, cis, d,  dis, e,  f, fis, g, gis, a,  ais, h,  c,  cis, d,  dis, e,   f,

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
demo:- go('c'),
       go('gmi'),
       go('Fmi7/5-'),
       go('ami add6 7maj').
%%%

go:- read(Popis), go(Popis).

go(Popis):- vytvor_zoznam_tonov(Popis,Akord),
     write('Akord "'), write(Popis), write('" tvoria tieto tony: '), write(Akord), nl,
     write('Na gitare ho je mozne zahrat takto: '), nl,
     write('-----------'), nl, write('e-a-d-g-h-e'), nl, write('-----------'),nl,
     setof(X, vytvor_akord(Akord,X), Out), vypis_zoznam_akordov(Out), nl.

% vypise zoznam akordov [[akord],[akord],...]
vypis_zoznam_akordov([]).
vypis_zoznam_akordov([H|T]):-vypis_akord(H), nl, vypis_zoznam_akordov(T).

% vypise 1 riadok zoznamu akordov
vypis_akord([H]):- write(H).
vypis_akord([H|T]):- write(H), write('-'), vypis_akord(T).
     
% vytvor_2oktavy_tonov(+ZakladnyTon,-Tony) - vytvori zoznam pozostavajuci z 2 oktav tonov od zakladneho tonu akordu
vytvor_2oktavy_tonov(Zakl,Tony):-tony(Oktava),rozdel(Zakl,Oktava,L,P),
                                conc(P,L,Temp),conc(Temp,Oktava,Tony).
                                
% vytvor_zoznam_tonov(+Popis,-Akord) - Zo popisu akordu vygeneruje zoznam tonov tvoriacich akord (body postupu 1-3)
vytvor_zoznam_tonov(Popis,Akord):-vytvor_popis(Popis,[ZaklTon|PopisList]),
                                  vytvor_2oktavy_tonov(ZaklTon,Tony), zoznam_tonov(0,Tony,PopisList,Akord).

zoznam_tonov(_,_,[],[]).
zoznam_tonov(N,[Ton|TTon],[HPopis|TPopis],[Ton|TAkord]):-HPopis==N, !, N1 is N+1,
                       zoznam_tonov(N1,TTon,TPopis,TAkord).
zoznam_tonov(N,[_|TTon],[HPopis|TPopis],Akord):-HPopis\==N, !, N1 is N+1,
                       zoznam_tonov(N1,TTon,[HPopis|TPopis],Akord).


% akord_akord(+Akord, -Zahranie) - vytvori zahranie akordu na hmatniku - kazdy ton akordu iba 1x
vytvor_akord(Akord, Zahranie):- struny(Struny), akord_hmatnik(Akord, Struny, Zahranie).

akord_hmatnik([], [], []).
akord_hmatnik(Akord, [Struna|ST], [Fret|Out]):- ton_struna(Akord, Struna, Fret, Zb),
                                                akord_hmatnik(Zb, ST, Out).

% ton_struna(+Akord, +Struna, -Fret, -Zbytok) - na zadanej strune najde nejaky ton z akordu vrati zbytok akordu
ton_struna([], _, x, []).
ton_struna([H|T], Struna, Fret, T):- najdi_na_strune(H, Struna, Fret).
ton_struna([H|T], Struna, Fret, [H|Zb]):- ton_struna(T, Struna, Fret, Zb).

% najdi_na_strune(+Ton, +Struna, -Fret) - najde Ton na Strune a Fret bude na ktorom mieste sa nachadza
najdi_na_strune(H, Struna, Fret):- struna(Struna,L), najdi(H, L, 0, Fret).

najdi(H,[H|_], N, Fret):- Fret is N.
najdi(H,[_|T], N, Fret):- N1 is N+1, najdi(H, T, N1, Fret).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% vytvor_popis(+Popis,-PopisList) - vytvori zoznam logickych casti popisu akordu
vytvor_popis(Popis,PopisList):- downcase_atom(Popis,PopisD), atom_chars(PopisD,[Zakl|L]),
                               !, je_zakl_ton(Zakl), parse_popis(L,[7,4,0,Zakl],Temp),
                               concrev(Temp,[],PopisList).

% parse_popis(+Seznam,+Acc,-Out) vyparsuje popis zo zonamu seznam (bez zakl, tonu akordu)

  % ukoncenie rekurzie
parse_popis([],Acc,Acc):-!.
  % preskocenie whitespaces
parse_popis([H|S],Acc,Out):- (char_type(H,white); H=='/'), !, parse_popis(S,Acc,Out).
  % molove akordy
parse_popis(S,Acc,Out):- get_skratka([m,i],S,T), zmen_ton(4,3,Acc,OAcc), !, parse_popis(T,OAcc,Out).
  % akordy ktore maju pridany interval
parse_popis(S,Acc,Out):- get_skratka([a,d,d],S,T), ziskaj_cislo(T,Temp,Znam,Z), !,
                         ((Znam=='+', interval_zvacseny(Temp,X));
                          (Znam=='-', interval_zmenseny(Temp,X));
                          interval(Temp,1,X)), !, pridaj_ton(X,Acc,OAcc), parse_popis(Z,OAcc,Out).
  % zmensene akordy
parse_popis(S,Acc,Out):- get_skratka([d,i,m],S,Z), !,
                         (( najdi(10,Acc), zmen_tony([10,7,4],[9,6,3],Acc,OAcc), !);
                          (zmen_tony([11,7,4],[9,6,3],Acc,OAcc))
                          ),
                         parse_popis(Z,OAcc,Out).
% velka septima
parse_popis(S,Acc,Out):- ( get_skratka([m,a,j,'7'],S,Z); get_skratka(['7',m,a,j],S,Z)), !,
                         zmen_ton(10,11,Acc,OAcc), parse_popis(Z,OAcc,Out).
  % zvacsena kvinta akordu
parse_popis([A|S],Acc,Out):-( A=='#'; A=='+'), !, zmen_ton(7,8,Acc,OAcc), parse_popis(S, OAcc, Out).
parse_popis(S,Acc,Out):- get_skratka([a,u,g],S,Z) , !, zmen_ton(7,8, Acc, OAcc), parse_popis(Z,OAcc,Out).

   % akordy vzniknute nahradenim
parse_popis(S,Acc,Out):- ((get_skratka([s,u,s],S,Z), ziskaj_cislo(Z,Temp,Znam,Zvys));
                         (ziskaj_cislo(S,Temp,Znam,Z), get_skratka([s,u,s],Z,Zvys))), not(Temp==7), !,                            !,
                         ((Znam=='+', interval_zvacseny(Temp,X));
                          (Znam=='-', interval_zmenseny(Temp,X));
                          interval(Temp,1,X)), !,
                          (( Temp==2, (( najdi(3,Acc), zmen_ton(3,X,Acc,OAcc));
                                      (  najdi(4,Acc), zmen_ton(4,X,Acc,OAcc)))
                           );
                           ( Temp==4, (( najdi(3,Acc), zmen_ton(3,X,Acc,OAcc));
                                      (  najdi(4,Acc), zmen_ton(4,X,Acc,OAcc)))
                           );
                           ( Temp==6, najdi(7,Acc), zmen_ton(7,X,Acc,OAcc))
                          ), parse_popis(Zvys,OAcc,Out).

  % akordy s cislami intervalov
parse_popis(S,Acc,Out):- ziskaj_cislo(S,Temp,Znam,Z), !,
                         (( Znam=='+', interval_zvacseny(Temp,X));
                          ( Znam=='-', interval_zmenseny(Temp,X));
                          interval(Temp,1,X)), !,
                          (( Temp==2, (( najdi(3,Acc), zmen_ton(3,X,Acc,OAcc));
                                      (  najdi(4,Acc), zmen_ton(4,X,Acc,OAcc)))
                           );
                           ( Temp==4, (( najdi(3,Acc), zmen_ton(3,X,Acc,OAcc));
                                      (  najdi(4,Acc), zmen_ton(4,X,Acc,OAcc)))
                           );
                           ( Temp==5, zmen_ton(7,X,Acc,OAcc));
                           ( Temp==6, zmen_ton(9,X,Acc,OAcc));
                           ( Temp==7, zmen_ton(10,10,Acc,OAcc));
                           ( Temp==9, zmen_tony([14,10],[X,10],Acc,OAcc));
                           ( Temp==11, zmen_tony([17,14,10],[X,14,10],Acc,OAcc));
                           ( Temp==13, zmen_tony([21,17,14,10],[X,17,14,10],Acc,OAcc))
                          ), parse_popis(Z,OAcc,Out).
   
% zmen_ton(+X,+Y,+S,-O)
zmen_ton(X,Y,S,O):- not(najdi(X,S)), !, pridaj_ton(Y,S,O).
zmen_ton(_,_,[],[]).
zmen_ton(X,Y,[X|T],[Y|T]):-not(najdi(Y,T)),!.
zmen_ton(X,Y,[H|T],[H|O]):-X\=H,zmen_ton(X,Y,T,O).

zmen_tony([],[],S,S).
zmen_tony([X|TX],[Y|TY],S,O):-zmen_ton(X,Y,S,NewO), zmen_tony(TX,TY,NewO,O).

% pridaj_ton(+X,+S,-O) - prida ak neexistuje, navyse zachova vzostupne usporiadanie
pridaj_ton(X,S,S):-najdi(X,S), !.
pridaj_ton(X,[],[X]).
pridaj_ton(X,[A|S],[X,A|S]):-X>A, !.
pridaj_ton(X,[A,B|S],[A,X,B|S]):-X<A,X>B, !.
pridaj_ton(X,[H|S],[H|O]):-pridaj_ton(X,S,O).

pridaj_tony([],S,S).
pridaj_tony([X|TX],S,O):-pridaj_ton(X,S,NewO), pridaj_tony(TX,NewO,O).

% najdi(+X,+S)
najdi(X,[X|_]).
najdi(X,[_|T]):-najdi(X,T).

% get_skratka(+Skratka,+Vstup,-Zvysok) - ziskanie skratky (mi, add, maj... zo vstupu)
get_skratka([],Z,Z).
get_skratka([H|S],[H|I],Zvysok):-get_skratka(S,I,Zvysok).

ziskaj_cislo(S,O,Znam,Zv):- get_cislo(S,Out,Znam,Zv), notempty(Out), concat_atom(Out,Num), atom_number(Num,O).
get_cislo([],[],_,[]):-!.
get_cislo([H|T],[H|O],Znam,Zv):- char_type(H,digit),!,get_cislo(T,O,Znam,Zv).
get_cislo([H|T],O,Znam,Zv):- (H=='+';H=='#'), Znam='+', !, get_cislo(T,O,Znam,Zv).
get_cislo([H|T],O,Znam,Zv):- (H=='-';H=='b'), Znam='-',!,get_cislo(T,O,Znam,Zv).
get_cislo(T,[],_,T):-!.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% rozdel(+Nazov,+Seznam,-Lavy,-Pravy) - rozdeli zoznam na 2 zoznamy pred polozkou Nazov a za nou
rozdel(_,[],[],[]).
rozdel(X,[X|S],[],[X|S]).
rozdel(X,[H|S],[H|L],P):-H\=X,rozdel(X,S,L,P).

% conc(+F,+S,-Out) - spoji zoznamy F a S
conc([],S,S).
conc([H|T],S,[H|O]):-conc(T,S,O).

% concrev(+F,+S,-Out) - spoji prvy obrateny s druhym
concrev([],S,S).
concrev([H|T],S,Out):-concrev(T,[H|S],Out).

% odstran_white(+S,-O) - odstranenie whitespaces zo zoznamu znakov
odstran_white([],[]).
odstran_white([H|T],O):-char_type(H,white),!,odstran_white(T,O).
odstran_white([H|T],[H|O]):-odstran_white(T,O).

% notempty(+S)
notempty([_]).
notempty([_|_]).

/*******************************************************************************************************************
******************************************* Popis riesenia: ********************************************************

Program dostane na vstupe zadany retazec popisujuci akord.
        1.) Pozriem sa na prve pismeno urcujuce akord = zakladny ton.
        2.) Vytvorim si zoznam pozostavajuci z 2 oktav tonov od zakladneho tonu akordu.
        3.) Linearne prejdem vstupny retazec a z neho vytvorim zoznam tonov tvoriacich akord.
        4.) Vyskusam vsetky moznosti ako sa da dany akord zahrat na gitare (kazdy ton len 1x)

Akord je popisany v tvare:
   - Prve pismeno urcuje zakladny ton akordu (C, G, F ...), je povinne.
   - Dalej moze nasledovat skratka 'mi', ktora hovori ze sa jedna o molovy akord (Cmi, Gmi...)
   - Cisla sa pisu za pismena oznacujuce akord. Urcuju akordovu triedu (tj. 7 znamena septakord, 9 nonovy akord ...)
       - Akord oznaceny vyssou akordovou triedou (napr C13) obsahuje automaticky aj nizsie triedy (v tomto pripade 11, 9 a 7)
       - Pre oznacenie sexty pouzivame cislo 6, ak je ku kvintakordu pridana. Ak nahradzujeme kvintu akordu tak pouzijeme skratku sus.
   - Dalsie skratky:
       - 'add<cislo>' -> jedna o pridany interval (napr 'Cadd9' prida velku nonu).
       - '#' -> jedna sa o zvacseny kvintakord (napr 'C#')
       - 'b' -> jedna sa o zmenseny kvintakord (napr 'Cb')
       - 'maj7' -> jedna sa o zvacsenu septimu (napr 'Cmaj7').
       - 'dim' -> oznacuje zmensene zmenseny septakord (obsahuje 3 male tercie po sebe)
       - '<cislo>sus' -> oznacuje nahradeny interval s cislom <cislo> (C4sus oznacuje nahradenu terciu kvartou)
                      -> 6sus nahradzuje 5 za 6, 4sus nahradzuje 3 za 4, 2sus nahradzuje 3 za 2
   - Intervaly mozme kombinovat za pomoci '/' (napr C11/9/7) - oznacuje to znejdnotenie jednotlivych akordov (C11, C9, C7)

*******************************************************************************************************************/