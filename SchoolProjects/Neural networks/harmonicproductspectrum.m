% metoda harmonic product spectrum
% zdroj: http://cnx.org/content/m11714/latest/

% predpokladam ze v sig je signal (nacitava sa napr prikazom
% sig=wavread('nazovsuboru.wav');

% sample rate
Fs = 44100;
% velkost fft -> cim vyssie tym vyssie rozlisenie frekvencii
fft_size = Fs;
% velkost okienka -> velkost fft
window_size = fft_size;

% vytvorenie okienka -> v praci by bolo vhodne porovnat ine okienka 
% zoznam okienok -> http://en.wikipedia.org/wiki/Window_function
% blackman window
% alpha = 0.16;
% window = (1-alpha)/2-0.5.*cos(2.*pi.*(0:(window_size-1))./(window_size-1))+alpha./2.*cos(4.*pi.*(0:(window_size-1))./(window_size-1));

% pouzijem hanning window
window = 0.5.*(1-cos(2.*pi.*(1:window_size)./(window_size-1)));

i = 1;
overlap = 0;
cycle = 0;

out = zeros(1,fft_size);
n = length(sig);

% freq -> vytvorime si vektor frekvencii -> ideme len do fft_size/2 lebo
% potom je graf spektra sumerny
freq = Fs./fft_size.*(1:floor(fft_size/2));

% prejdeme signal po jednotlivych okienkach a vyhodim F0 kazdeho okienka
while (i+window_size <= n) 
    cycle = cycle+1;
    y = sig(i:i+window_size-1);
    i = i+window_size-overlap;
    y = y.*window;
    % fft
    Y = fft(y,fft_size);
    % ziskame signal v polovicnej sample rate (kazdy druhy index z pola Y)
    Y2 = Y(1:2:length(Y));
    % ziskame signal v 1/3 sample rate
    Y3 = Y(1:3:length(Y));
    yn = length(Y);
    % doplnime Y2 a Y3 nulami
    Y2 = [Y2, zeros(1,yn-length(Y2))];
    Y3 = [Y3, zeros(1,yn-length(Y3))];
    % vynasobime vytvorene signaly (povodny, polovicny a 1/3)
    Y = Y.*Y2.*Y3;
    out = out + Y;
        
    %magnituda komplexnych cisel v out
    power = abs(Y).^2;
    
    % pokus o urcenie frekvencie
    % najdeme maximalnu hodnotu v poli power
    freqInd = find(freq >= 50 & freq <= 1000);
    [c,j] = max(power(freqInd));
    % index i odpoveda frekvencii maximalnej vychylky 
    % output je trojica (i-1,i,i+1) -> v tomto intervale sa nachadza nasa
    % hladana F0 (fundamentalna frekvencia)
    freq(freqInd(j))
end

%magnituda komplexnych cisel v Y
power = abs(out./cycle).^2;

% spektrum
plot(freq,power(1:(fft_size/2)),'.-');
% zobrazime len po cca 3000Hz
xlim([0,1000]);

% pokus o urcenie frekvencie
% najdeme maximalnu hodnotu v poli power
freqInd = find(freq >= 50 & freq <= 1000);
[c,i] = max(power(freqInd));
% index i odpoveda frekvencii maximalnej vychylky 
% output je trojica (i-1,i,i+1) -> v tomto intervale sa nachadza nasa
% hladana F0 (fundamentalna frekvencia)
freq(freqInd(i))