function [out,base] = midi2notes(song,conf)
% konverzia midi reprezentacie (maticou s casmi zaciatku a konca noty) do
% reprezentacie s dlzkami not a pomlckami
% @param song:
%        song.midi - originalny midi subor (struktura z midiread())
%        song.data - matica midi suboru (struktura z midiInfo())
% @param conf:
%        conf.relative_notes - 1 vytvori relativne vysky tonov (0 je 1. ton dalsie
%                              tony su udane vzdialenostou od tohto 1. v poltonoch)
%        conf.normalize_tempo - 1 ak mame normalizovat tempo
%        conf.target_tempo - hodnota normalizovaneho tempa v BPM
%        conf.quantize_time - casovy interval na ktoreho velkost kvantizujeme
% @return out - notova reprezentacia skladby
% @return base - prvy ton skladby (absolutna vyska)

if (nargin < 2)
    error('Bad arguments specified');
end

pauseConst = 128;
base = 0;

if (conf.pitch_representation == 1) 
    % vypocet vzdialenosti od prveho tonu
    base = song.data(1,3);
    song.data(:,3) = song.data(:,3)-song.data(1,3);
elseif (conf.pitch_representation == 2)
    lastpitch = song.data(1,3);
    base = lastpitch;
    for i=2:size(song.data,1)
        newval = song.data(i,3)-lastpitch;
        lastpitch = song.data(i,3);
        song.data(i,3) = newval;
    end
    song.data(1,3) = 0;
elseif (conf.pitch_representation == 3)
    song.data(:,3) = song.data(:,3)-song.data(1,3);
    song.data(:,3) = song.data(:,3)+conf.target_base;
end

sortrows(song.data,5);

% vypocet dlziek tonov
out = [];
lasttime = 0;
for (i=1:size(song.data,1))
    note = song.data(i,3);
    curstart = song.data(i,5);
    curend = song.data(i,6);
    
    if ((curstart-lasttime)>conf.quantize_time)
        % vlozime pauzu
        duration = curstart-lasttime;
        if (duration>0)
            out(end+1,1) = pauseConst;
            out(end,2) = duration;
        end
    end
    
    duration = curend-curstart;
    if (duration>0)
        out(end+1,1) = note;
        out(end,2) = duration;
    end
    lasttime = curend;
end

% if (conf.normalize_tempo)
%     out(:,2) = out(:,2).*(out.tempo./conf.target_tempo);
% end

out(:,2) = quantizeNote(out(:,2),conf.quantize_time);

% nahradenie tonov s nulovou dlzkou kvantizacnym tonom
out(:,2) = replace(out(:,2),0,1);

end