function y = quantizeNote(x,qr)
% kvantizuje notu o dlzke x (sec) na casove intervaly dlzky qr (sec)

if (nargin < 2)
    error('Bad arguments specified');
end

y = round(x/qr);