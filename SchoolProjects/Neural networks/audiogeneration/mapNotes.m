function y = mapNotes(action, x, conf)
% namapuje hodnoty matice not a ich dlzok do intervalu [conf.a;conf.b]

pauseConst = 128;

pitchMin = conf.pitch_min;
pitchMax = conf.pitch_max;
%pitchMin = 60;
%pitchMax = 90;
a = conf.a;
b = conf.b;
dur = conf.allowed_durations;

if (nargin < 2)
    error('Bad arguments specified');
end

if (strcmp(action,'apply'))
    tmp = x(x(:,1)==pauseConst,1);

    if (~isempty(tmp))
        x(x(:,1)==pauseConst,1) = ones(size(tmp,1),1).*(pitchMax);
    end
    
    % namapovanie na interval [-1,1]
    % vyska tonu
    y(:,1) = (x(:,1)-pitchMin)./(pitchMax-pitchMin).*(b-a)+a;
    % dlzka tonu
    %y(:,2) = (x(:,2)-durationMin)./(durationMax-durationMin).*(b-a)+a;
    for i=1:size(x,1)
        val = x(i,2);
        diffs = abs(dur-val);
        m = min(diffs);
        tmp = find(diffs==m);
        y(i,2) = tmp(1);
    end
    y(:,2) = (y(:,2)-1)./(length(dur)-1).*(b-a)+a;
        
    % odstranenie hodnot (<a) OR (>b)
    for i=1:2
        tmp = size(y(y(:,i)<a,i),1);
        y(y(:,i)<a,i) = ones(tmp,1).*a;
        tmp = size(y(y(:,i)>b,i),1);
        y(y(:,i)>b,i) = ones(tmp,1).*b;
    end
elseif (strcmp(action,'reverse'))
    % namapovanie z intervalu [-a,b]
    
    % odstranenie hodnot (<a) OR (>b)
    for i=1:2
        tmp = size(x(x(:,i)<a,i),1);
        x(x(:,i)<a,i) = ones(tmp,1).*a;
        tmp = size(x(x(:,i)>b,i),1);
        x(x(:,i)>b,i) = ones(tmp,1).*b;
    end
    
    % vyska tonu
    y(:,1) = (x(:,1)-a)./(b-a).*(pitchMax-pitchMin)+pitchMin;
    y(:,1) = quant(y(:,1),1);
    % dlzka tonu
    y(:,2) = (x(:,2)-a)./(b-a).*(length(dur)-1)+1;
    y(:,2) = quant(y(:,2),1);
    for i=1:size(x,1)
        y(i,2) = dur(y(i,2));
    end    

    % base tone
    tmp = y(y(:,1)==pitchMax,1);
    
    if (~isempty(tmp))
        y(y(:,1)==pitchMax,1) = ones(size(tmp,1),1).*pauseConst;
    end
end