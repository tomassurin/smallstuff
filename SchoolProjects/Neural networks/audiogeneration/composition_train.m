function conf = composition_train(conf)
% Train composition network
% @param conf configuration structure created with composition_init()

song = conf.data;
song_sz = size(song,1);

%% Ziskanie trenovacich vzorov
% Ziskame nahodne vzory zo vstupu alebo postupne vsetky vzory
% (ak nastavene conf.train_seq).
if (conf.train_seq)
    sz = conf.window_sz+conf.out_sz;
    tmp_sz = song_sz-sz+1;%ceil(song_sz./(sz));
    p = zeros(sz*2,tmp_sz);
    last_pos = 1;
    for i=1:tmp_sz
        p(:,i) = fvector(song(last_pos:last_pos+sz-1,:));
        last_pos = last_pos + 1;
    end
    
%     tmp = fvector(song(last_pos:song_sz,:));
%     
%     p(:,i+1) = [tmp;zeros((sz-(song_sz-last_pos+1))*2,1)];
    conf.train_samples = tmp_sz;
    
    p = p(:,1:i);
    
    % randomizujeme vzory
    tmp_perm = randperm(i);
    p(:,1:i) = p(:,tmp_perm);
    
    trainIn = p(1:conf.window_sz*2,:);
    trainOut = p(conf.window_sz*2+1:end,:);
else
    % random vzory
    trainIn = zeros(conf.window_sz*2,conf.train_samples);
    trainOut = zeros(conf.out_sz*2,conf.train_samples);
    for i=1:conf.train_samples
        r = round(rand(1).*(song_sz-conf.window_sz-conf.out_sz-1)+1);
        last = r+conf.window_sz-1;
        trainIn(:,i) = fvector(song(r:last,:));
        trainOut(:,i) = fvector(song(last+1:last+conf.out_sz,:));
    end
end


%% Vytvorenie siete.
% if (conf.morelayers_enabled)
%    net = newff(trainIn,trainOut,[conf.hidden_sz_1,conf.hidden_sz,conf.hidden_sz_2],{conf.hidden_train_1,transferFcn,conf.hidden_train_2,conf.outTransferFcn},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
% else
%    net = newff(trainIn,trainOut,[conf.hidden_sz],{conf.hiddenTransferFcn,conf.outTransferFcn},conf.trainParam.algo,'learngdm');
% end

net = newff(trainIn,trainOut,[conf.hidden_sz],{conf.hiddenTransferFcn,conf.outTransferFcn},conf.trainParam.algo,'learngdm');
net = init(net);

% trenovacie parametre
net.trainParam.goal = conf.trainParam.goal;
net.trainParam.epochs = conf.trainParam.epochs;
net.trainParam.max_fail = conf.trainParam.max_fail;
net.divideParam.trainRatio = conf.divideParam.trainRatio;
net.divideParam.valRatio = conf.divideParam.valRatio;
net.divideParam.testRatio = conf.divideParam.testRatio;

conf.net = net;
conf.tr = [];

%% Trenovanie siete 
[net,tr] = train(net,trainIn,trainOut);
conf.tr = [conf.tr, tr];
conf.net = net;

end 

function y = fvector(x)
    y = reshape(x',prod(size(x)),1);
end
