function comp = composition_compose_fftd(conf, len, init)
% Pouzitie natrenovanej siete v conf.net na vytvorenie novej skladby.
% @param conf - konfiguracna struktura s natrenovanou sietou
% @param len - pozadovana dlzka vyslednej skladby (~priblizne)
% @param init - zaciatok piesne

if (nargin < 3)
    error('Bad arguments specified');
end

startpos = 1;
endpos = conf.window_sz;

startpos = 1;
endpos = conf.window_sz;
init = [zeros(conf.window_sz-size(init,1),2);init];

inp = con2seq(init');

p = con2seq(ones(2,conf.out_sz));

y = inp;

while (endpos < len)
    %inp = fvector(comp(startpos:endpos,:));
    out = sim(conf.net,p,inp);
    y = [y, out];
    startpos = startpos+conf.out_sz;
    endpos = endpos+conf.out_sz;
    inp = y(startpos:endpos);
end

comp = cell2mat(y)';

end

function y = fvector(x)
    y = reshape(x',size(x,1)*2,1);
end

function y = fvector_inv(x)
    y = reshape(x,length(x)/2,2);
end