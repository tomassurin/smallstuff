function config = composition_train_fftd(conf)

song = conf.data;
song_sz = size(song,1);

in = conf.window_sz;
out = conf.out_sz;
%tmp = con2seq(fvector(song)');
tmp = con2seq(song');
 
% vytvorenie siete
net = newfftd(tmp,tmp,[out:in],conf.hidden_sz,{conf.hiddenTransferFcn,conf.outTransferFcn},conf.trainParam.algo,'learngdm');
net = init(net);

% trenovacie parametre
net.trainParam.goal = conf.trainParam.goal;
net.trainParam.epochs = conf.trainParam.epochs;
net.trainParam.max_fail = conf.trainParam.max_fail;
net.divideParam.trainRatio = conf.divideParam.trainRatio;
net.divideParam.valRatio = conf.divideParam.valRatio;
net.divideParam.testRatio = conf.divideParam.testRatio;


Pi = tmp(1:in);
p = tmp(in+1:end);
t = tmp(in+1:end);

[net,tr] = train(net,p,t,Pi);

config = conf;
config.net = net;
config.tr = tr;

end

function y = fvector(x)
    y = reshape(x',prod(size(x)),1);
end