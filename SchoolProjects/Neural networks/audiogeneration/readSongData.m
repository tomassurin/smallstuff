function out = readSongData(filename,track)
% Reads MIDI file
% @param filename - input filename
% @param track - track number to treat as melody
% @return out - representation of midi file

if (nargin < 1)
    error('Bad number of arguments');
elseif (nargin < 2)
    track = [];
else
    track = [track];
end

midi = readmidi(filename);
if (isempty(track))
    midd = midiInfo(midi,0);
else
    midd = midiInfo(midi,0,track);
end

out.midi = midi;
out.data = midd;

% calculate duration of quarter notes and BPM
tempo = getTempoChanges(midi);
out.tempo = 60000000/tempo(1); %BPM
out.quarter_duration = tempo(1)/1000000;

end