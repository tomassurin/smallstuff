function conf = composition_init()
%

% notes mapping preferences
conf.pitch_min = -20;
conf.pitch_max = 21;
conf.a = -1;
conf.b = 1;
conf.allowed_durations = [1,2,4,6,8,16];
conf.pause_const = 128;

% nota na kt. velkost kvantizujeme 16 - sestnastinova, 8 - osminova, 4 -
% stvrtova, 2 - polova, 1 - cela ...
conf.quantize_note = 16;
conf.target_sz = 500;
conf.compositions = 10;
% 0 - original; 1 - relative to 1. note; 2 - interval between adjacent
% notes; 3 - absolute with pitch normalization to C4
conf.pitch_representation = 3;
conf.target_base = 60;

% pouzitie Feed forward time delay siete
conf.fftd_net = 0;
% povelenie ucenia
conf.train_enabled = 1;

% pocet trenovacich vzorov
conf.train_samples = 10000;
conf.train_seq = 0;

% velkost okienka historie elementov
conf.window_sz = 20;
% pocet vystupnych elementov (# predikovanych elementov)
conf.out_sz = 1;
% velkost vstupnej vrstvy
conf.input_sz = conf.window_sz*2;
% velkost vystupnej vrstvy
conf.output_sz = conf.out_sz*2;
% velkost skrytej vrstvy
conf.hidden_sz = 50;

% prenosova funkcia
conf.hiddenTransferFcn = 'tansig';
conf.outTransferFcn = 'tansig';

% parametre ucenia
conf.trainParam.goal = 1e-5;
conf.trainParam.epochs = 1000;
conf.trainParam.algo = 'trainscg';
%conf.trainParam.algo = 'trainlm';
%conf.trainParam.algo = 'trainr';
conf.trainParam.lr = 0.01;
conf.trainParam.max_fail = 50;
conf.divideParam.trainRatio = 0.6; 
conf.divideParam.valRatio = 0.2;
conf.divideParam.testRatio = 0.2;

% dalsie vrstvy
% conf.morelayers_enabled = 0;
% conf.hidden_sz_1 = 32;
% conf.hidden_sz_2 = 32;
% conf.hidden_train_1 = 'logsig';
% conf.hidden_train_2 = 'logsig';

conf.data = [];
end