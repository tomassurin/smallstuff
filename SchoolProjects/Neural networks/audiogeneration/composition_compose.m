function comp = composition_compose(conf, len, init)
% Pouzitie natrenovanej siete v conf.net na vytvorenie novej skladby.
% @param conf - konfiguracna struktura s natrenovanou sietou
% @param len - pozadovana dlzka vyslednej skladby (~priblizne)
% @param init - zaciatok piesne

if (nargin < 3)
    error('Bad arguments specified');
end

startpos = 1;
endpos = conf.window_sz;
comp = [zeros(conf.window_sz-size(init,1),2);init];

while (endpos < len)
    inp = fvector(comp(startpos:endpos,:));
    out = sim(conf.net,inp);
    out = fvector_inv(out);
    comp = [comp; out];
    startpos = startpos+conf.out_sz;
    endpos = endpos+conf.out_sz;
end

config = conf;

end

function y = fvector(x)
    y = reshape(x',size(x,1)*2,1);
end

function y = fvector_inv(x)
    y = reshape(x,length(x)/2,2);
end