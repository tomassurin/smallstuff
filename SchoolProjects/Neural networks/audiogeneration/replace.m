function y = replace(x, a, b)
% replace all values a to b in matrix x
for i=1:size(x,2)
    tmp = size(x(x(:,i)==a,i),1);
    x(x(:,i)==a,i) = ones(tmp,1).*b;
end

y = x;