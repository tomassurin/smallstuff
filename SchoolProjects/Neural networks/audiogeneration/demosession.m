%% Nastavenia
%filename = 'demo.mid';
%filename = 'AHardDaysNight.mid';
outfile = 'outtt';
%files = {'coll1.mid','coll2.mid','coll3.mid','coll4.mid','coll5.mid','coll6.mid',...
         %'coll7.mid','coll8.mid','coll9.mid','coll10.mid','coll11.mid','coll12.mid',...
         %'coll13.mid','coll14.mid','coll15.mid'};
%files = {'demo.mid'}
%files = {'Astleys Ride.mid'};
%files = {'scales.mid'};
%files = {'Halloween.mid'};

%% inicializacia konfiguracie
conf = composition_init();

if (conf.train_enabled)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% priprava dat
    tic;
    disp('Preparing data...');

    % nacitanie skladieb z disku
    for i=1:length(files)
        song = readSongData(files{i});
        conf.tempo = song.tempo;
        conf.quarter_duration = song.quarter_duration;
        conf.quantize_time = song.quarter_duration/(conf.quantize_note/4);

        % ziskanie internej reprezentacie skladby
        [data,conf.basetone] = midi2notes(song,conf);
        conf.data = [conf.data;data];
    end

    conf.pitch_min = min(conf.data(:,1));
    conf.pitch_max = max(conf.data(conf.data(:,1)~=conf.pause_const))+1;
    
    % namapovanie hodnot na interval [conf.a;conf.b]
    conf.data = mapNotes('apply', conf.data, conf);

    disp('DONE: Preparing data');
    conf.timestats.prepare = toc;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% trenovanie siete
    tic;
    disp('Training network...');
    if (conf.fftd_net)
        conf = composition_train_fftd(conf);
    else
        conf = composition_train(conf);
    end
    disp('DONE: Training network');
    conf.timestats.train = toc;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% komponovanie novej skladby
conf.timestats.composition = [];
conf.timestats.writemidi = [];
for (i=1:conf.compositions)
    tic;
    disp('Composing...');
    if (i == 1)
        t = 1;
    else
        t = round(rand(1)*(size(conf.data,1)-conf.window_sz)+1);
    end
    if (conf.fftd_net)
        compos = composition_compose_fftd(conf,conf.target_sz,conf.data(t:t+conf.window_sz-1,:));
    else
        compos = composition_compose(conf,conf.target_sz,conf.data(t:t+conf.window_sz-1,:));
    end
    disp('DONE: Composing');
    conf.timestats.composition(end+1) = toc;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% ulozenie skladby na disk
    tic;
    disp('Saving...');
    compos = mapNotes('reverse',compos,conf);
    mid = notes2midi(compos,conf);
    writemidi(matrix2midi(mid),[outfile,48+i,'.mid']);
    disp('DONE: Saving');
    conf.timestats.writemidi(end+1) = toc;
end
%     disp('Saving...');
    compos = mapNotes('reverse',conf.data,conf);
    mid = notes2midi(compos,conf);
    writemidi(matrix2midi(mid),'original.mid');
    disp('DONE: Saving');