function mid = notes2midi(data,conf)
% konverzia reprezentacie not do midi matice
% @param data - data formatu midi2notes()
% @param conf.basetone - prvy ton skladby (MIDI vyska)
% @param conf.quantize_time - casovy interval na ktoreho velkost kvantizujeme
% @return mid - matica pouzitelna v matrix2midi() a nasledne writemidi()

if (nargin < 2)
    error('Bad arguments specified');
end

pauseConst = 128;

data(:,2) = data(:,2).*conf.quantize_time;

lasttime = 0;
mid = [];

if (conf.pitch_representation == 2)
    lastpitch = conf.basetone;
end

for i=1:size(data,1)
    if (data(i,2)==0)
        continue;
    end
    if (data(i,1) ~= pauseConst)
        mid(end+1,1) = 1;
        mid(end,2) = 1;
        if (conf.pitch_representation == 2)
            mid(end,3) = lastpitch + data(i,1);
            lastpitch = mid(end,3);
        else
            mid(end,3) = conf.basetone+data(i,1);
        end
        mid(end,4) = 127;
        mid(end,5) = lasttime;
        mid(end,6) = lasttime+data(i,2);
    end
    lasttime = lasttime+data(i,2);
end
