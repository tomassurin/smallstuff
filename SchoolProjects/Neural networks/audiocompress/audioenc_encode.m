function [y,config] = audioenc_encode(conf,sig)

tic;
disp('Compressing...');

i = 1;
en = 1;
k = 0;
disp_step = 1000;
sig_sz = size(sig,1);
enc_sz = ceil(sig_sz./conf.input_sz).*conf.hidden_sz;
y = zeros(enc_sz,1);

while (i+conf.input_sz-1<=sig_sz) 
     sg = sig(i:i+conf.input_sz-1);
     if (conf.mapminmax_enabled)
        sg = mapminmax('apply',sg,conf.mapminmax_param);
     end  
     
     y(en:en+conf.hidden_sz-1,1) = sim(conf.enc_net,sg);
     i = i+conf.input_sz;
     en = en+conf.hidden_sz;
     
     k = k+1;
     if (mod(k,disp_step) == 0)
         str = sprintf('%f %%\n',en*100/enc_sz);
         disp(str);
         k = 0;
     end
end

if (i <= sig_sz)
    sg = [sig(i:sig_sz);zeros(conf.input_sz-(sig_sz-i+1),1)];
    if (conf.mapminmax_enabled)
        sg = mapminmax('apply',sg,conf.mapminmax_param);
    end
    
    y(en:en+conf.hidden_sz-1,1) = sim(conf.enc_net,sg);
end

if (conf.quantize_enabled)
    y = quantize(y,conf.quantize_bits);
end

disp('DONE: Compressing...');
toc;
conf.timestats.encode = toc;
config = conf;