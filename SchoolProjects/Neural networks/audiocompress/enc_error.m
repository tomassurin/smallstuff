function y = enc_error(config, a, b)
% Vrati strednu kvadraticku odchylku dvoch signalov.

a_sz = size(a,1);
b_sz = size(b,1);

if (a_sz <= b_sz)
    sz = a_sz;
else
    sz = b_sz;
end

y = 0;
i = 1;

while (i <= sz)
    if (i+config.train_sz-1 <= sz)
        last = i+config.train_sz-1;
    else
        last = sz;
    end
    y =  y + sum((a(i:last)-b(i:last)).^2,1);
    i = i+config.train_sz;
end

y = y/sz;