function conf = audioenc_init()
% Inicializacia konfiguracie audiokompresie.
   
% samplovacia frekvencia
conf.fs = 44100;
% velkost vstupu kompresnej siete
conf.input_sz = 64;
% velkost skrytej vrstvy pre kompresiu
conf.hidden_sz = 16;
% dalsie vrstvy
conf.morelayers_enabled = 0;
conf.hidden_sz_1 = 32;
conf.hidden_sz_2 = 32;
conf.hidden_train_1 = 'logsig';
conf.hidden_train_2 = 'logsig';
% pocet bitov na quantizaciu
conf.quantize_enabled = 1;
conf.quantize_fp = struct;
conf.quantize_fp.bits = 0;
% pocet trenovacich vzorov
conf.train_sz = conf.fs*120;
conf.train_samples = 5000;
conf.train_step = 5000;
% ziskat trenovacie vzory sekvencne
conf.train_seq = 0;

conf.mapminmax_enabled = 1;
conf.first_train = 1;

conf.trainParam.goal = 1e-5;
conf.trainParam.epochs = 1000;
conf.trainParam.algo = 'trainscg';
%conf.trainParam.algo = 'trainr';
conf.trainParam.lr = 0.01;
conf.trainParam.max_fail = 6;

conf.divideParam.trainRatio = 0.8; 
conf.divideParam.valRatio = 0.1;
conf.divideParam.testRatio = 0.1;