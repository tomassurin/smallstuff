function y = dequantize(qy,bits,ymin,ymax)
% Dekvantizacia vstupu.

if (nargin < 2)
    error('Zly pocet argumentov');
end

if (nargin < 4)
    ymin = 0;
    ymax = 1;
end

y = qy.*(ymax-ymin)./(2.^bits-1) + ymin;