function qy = quantize(y,bits,ymin,ymax)
% Kvantizujeme vstup z [ymin,ymax] do bits bitoveho prirodzeneho cisla.

if (nargin < 2)
    error('Zly pocet argumentov');
end

if (nargin < 4)
    ymin = 0;
    ymax = 1;
end

qy = round((y-ymin)./(ymax-ymin).*(2^bits-1));