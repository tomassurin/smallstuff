function [y,config] = audioenc_decode(conf,sig)

tic;
disp('Decompressing...');

i = 1;
de = 1;
k = 0;
disp_step = 1000;
sig_sz = size(sig,1);
dec_sz = ceil(sig_sz./conf.hidden_sz).*conf.input_sz;
y = zeros(dec_sz,1);

if (conf.quantize_enabled)
    % kvantizovane dekodovanie
    while (i+conf.hidden_sz-1<=sig_sz)
         data = sim(conf.dec_net,dequantize(sig(i:i+conf.hidden_sz-1),conf.quantize_bits));
         if (conf.mapminmax_enabled)
            data = mapminmax('reverse',data,conf.mapminmax_param);
         end
         
         y(de:de+conf.input_sz-1,1) = data;
         i = i+conf.hidden_sz;
         de = de+conf.input_sz;
         
         k = k+1;
         if (mod(k,disp_step) == 0)
             str = sprintf('%f %%\n',de*100/dec_sz);
             disp(str);
             k = 0;
         end
         
    end

    if (i <= sig_sz)
        data = sim(conf.dec_net,[dequantize(sig(i:sig_sz),conf.quantize_bits);zeros(conf.hidden_sz-(sig_sz-i+1),1)]);
        if (conf.mapminmax_enabled)
           data = mapminmax('reverse',data,conf.mapminmax_param);
        end
                 
        y(de:de+conf.input_sz-1,1) = data;
    end
else
    % nekvantizovane dekodovanie
    while (i+conf.hidden_sz-1<=sig_sz)
         data = sim(conf.dec_net,sig(i:i+conf.hidden_sz-1));
         if (conf.mapminmax_enabled)
            data = mapminmax('reverse',data,conf.mapminmax_param);
         end
                          
         y(de:de+conf.input_sz-1,1) = data;
         i = i+conf.hidden_sz;
         de = de+conf.input_sz;
    end

    if (i <= sig_sz)
        data = sim(conf.dec_net,[sig(i:sig_sz);zeros(conf.hidden_sz-(sig_sz-i+1),1)]);
        if (conf.mapminmax_enabled)
           data = mapminmax('reverse',data,conf.mapminmax_param);
        end
                
        y(de:de+conf.input_sz-1,1) = data;
    end
end

disp('DONE: Decompressing...');
toc;
conf.timestats.decode = toc;
config = conf;