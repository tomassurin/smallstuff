%sig = wavread('classic.wav',[1*44100,60*44100]);
% 
config = audioenc_init();

i = 1;

while (i <= length(sig))
    if (i+config.train_sz-1 <= length(sig))
        last = i+config.train_sz-1;
    else
        last = length(sig);
    end
    config = audioenc_train(config,sig(i:last));
    i = i+config.train_sz;
end

[compressed,config] = audioenc_encode(config,sig);
[decompressed,config] = audioenc_decode(config,compressed);

config.enc_error = enc_error(config, sig,decompressed);
plottrainstats(config);

wavwrite(decompressed,config.fs,'out.wav');