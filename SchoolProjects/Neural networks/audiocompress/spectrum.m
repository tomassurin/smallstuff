function spectrum(sig,Fs,fft_size)
% sig -> signal
% Fs -> sample rate
% fft_size -> velkost fft, cim vyssie tym vyssie rozlisenie frekvencii

if (nargin < 1)
    error('Zly pocet argumentov');
elseif (nargin < 2)
    Fs = 44100;
    fft_size = Fs;
elseif (nargin < 3)
    fft_size = Fs;
end

% vytvorenie okienka 
% zoznam okienok -> http://en.wikipedia.org/wiki/Window_function
% blackman window
alpha = 0.16;
window_size = fft_size;
window = (1-alpha)/2-0.5.*cos(2.*pi.*(0:(window_size-1))./(window_size-1))+alpha./2.*cos(4.*pi.*(0:(window_size-1))./(window_size-1));

y = sig(1:fft_size);
Y = fft(y,fft_size);

%magnituda
power = abs(log(Y)).^2;
% freq
freq = Fs./fft_size.*(1:floor(fft_size/2));

% spektrum
%from = 20;
%z = 50;
%plot(freq(from:z),power(from:z));
plot(freq,power(1:(fft_size/2)),'.-');
%ylim([0,0.005]);
xlim([0,1000]);
%ylim([0,200]);