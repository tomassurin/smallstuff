function plottrainstats(conf)

trr = conf.tr;
tr = trr(1);

timestats_train = conf.timestats.train(1);

for i=2:length(conf.timestats.train)
    timestats_train = timestats_train + conf.timestats.train(i);
end

str = sprintf('\n%s %15.15f','Encoding Error', conf.enc_error);
disp(str);
str = sprintf('%s %g','Training length', timestats_train);
disp(str);
str = sprintf('%s %g','Compression length', conf.timestats.encode);
disp(str);
str = sprintf('%s %g','Decompression length', conf.timestats.decode);
disp(str);

str = sprintf('\nNetwork: [%d:%d:%d]',conf.input_sz,conf.hidden_sz,conf.input_sz);
disp(str);
str = sprintf('Quantize: %d/%d bits',conf.quantize_enabled, conf.quantize_fp.bits);
disp(str);
str = sprintf('MapMinMax: %d',conf.mapminmax_enabled);
disp(str);
str = sprintf('Train samples: %d',conf.train_samples);
disp(str);
str = sprintf('Train epochs: %d',conf.trainParam.epochs);
disp(str);
str = sprintf('Divide param train/val/test: %g/%g/%g',conf.divideParam.trainRatio,conf.divideParam.valRatio,conf.divideParam.testRatio);
disp(str);

for i=2:length(trr)
    tmp = trr(i);
    tr.epoch = [tr.epoch, tmp.epoch+length(tr.epoch)];
    tr.perf = [tr.perf, tmp.perf];
    tr.vperf = [tr.vperf, tmp.vperf];
    tr.time = [tr.time, tmp.time];
    tr.tperf = [tr.tperf, tmp.tperf];
    tr.gradient = [tr.gradient, tmp.gradient];
    tr.val_fail = [tr.val_fail, tmp.val_fail];
end

tr.num_epochs = length(tr.epoch)-1;

plotperform(tr);
saveas(gcf,'plotperform.png');
plottrainstate(tr);
saveas(gcf,'plottrainstate.png');

y = zeros(conf.input_sz,size(conf.p,2));

i = 1;
last = 1;

sz = size(conf.p,2);

while (i<=sz)
    last = i+conf.train_step-1;
    if (last>=sz)
        last = sz;
    end
    y(:,i:last) = sim(conf.net,conf.p(:,i:last));
    i = i+conf.train_step;
end

plotregression(conf.p,y);
saveas(gcf,'plotregression.png');