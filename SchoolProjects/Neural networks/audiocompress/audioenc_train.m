function conf = audioenc_train(conf, sig)
% Natrenovanie kompresnej siete.
%   conf -> konfiguracia kompresie
%   sig -> signal pre kompresiu (stlpcovy vektor)
% % 
tic;
disp('Training network...');

sig_sz = size(sig,1);

%% Vytvorenie neuronovej siete.
% Ziskame nahodne vzory zo vstupneho signalu alebo postupne vsetky vzory
% (ak nastavene conf.train_seq).
if (conf.train_seq)
    tmp_sz = ceil(sig_sz./conf.input_sz)
    p = zeros(conf.input_sz,tmp_sz);
    last_pos = 1;
    for i=1:tmp_sz-1
        p(:,i) = sig(last_pos:conf.input_sz.*i);
        last_pos = conf.input_sz.*i+1;
    end
    
    p(:,i+1) = [sig(last_pos:sig_sz);zeros(conf.input_sz-(sig_sz-last_pos+1),1)];
    conf.train_samples = tmp_sz;
    conf.train_samples = tmp_sz;
    
    % randomizujeme vzory
    tmp_perm = randperm(tmp_sz);
    p(:,1:tmp_sz) = p(:,tmp_perm);
else
    p = zeros(conf.input_sz,conf.train_samples);
    for i=1:conf.train_samples
        r = round(rand(1).*(sig_sz-conf.input_sz-1)+1);
        p(:,i) = sig(r:r+conf.input_sz-1,1);
    end
end

if (conf.first_train)
    % Vytvorenie siete.
    transferFcn = 'tansig';
    if (conf.quantize_enabled)
        transferFcn = 'quantizetransfer_tansig';
    end
    
    if (conf.morelayers_enabled)
       net = newff(p,p,[conf.hidden_sz_1,conf.hidden_sz,conf.hidden_sz_2],{conf.hidden_train_1,transferFcn,conf.hidden_train_2,'purelin'},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
    else
       net = newff(p,p,[conf.hidden_sz],{transferFcn,'purelin'},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
    end
    
    net = init(net);

    % trenovacie parametre
    net.trainParam.goal = conf.trainParam.goal;
    net.trainParam.epochs = conf.trainParam.epochs;
    net.trainParam.max_fail = conf.trainParam.max_fail;
    net.divideParam.trainRatio = conf.divideParam.trainRatio;
    net.divideParam.valRatio = conf.divideParam.valRatio;
    net.divideParam.testRatio = conf.divideParam.testRatio;

    % nastavenie parametrov konfiguracie
    if (conf.quantize_enabled)
        if (conf.morelayers_enabled)
            conf.quantize_bits = net.layers{2,1}.transferParam.bits;
            conf.quantize_fp = net.layers{2,1}.transferParam;
        else
            conf.quantize_bits = net.layers{1,1}.transferParam.bits;
            conf.quantize_fp = net.layers{1,1}.transferParam;
        end
    end

    conf.net = net;
    
    conf.first_train = 0;
    % minmax normalizacia na [-1,1];
    if (conf.mapminmax_enabled)
        [p,ps] = mapminmax(p);
        conf.mapminmax_param = ps;
    end
    
  
    conf.p = [];
    conf.timestats.train = [];
    conf.tr = [];
    
else 
    % minmax normalizacia na [-1,1];
    if (conf.mapminmax_enabled)
        p = mapminmax('apply', p, conf.mapminmax_param);
    end   
end

net = conf.net;



%% Trenovanie siete (musime po kuskoch ;-( -> chyba malo pamati).
i = 1;
while (i <= conf.train_samples-conf.train_step+1)
     [net,tr] = train(net,p(:,i:i+conf.train_step-1),p(:,i:i+conf.train_step-1));
     i = i+conf.train_step
     conf.tr = [conf.tr, tr];
end

if (i <= conf.train_samples)
    [net,tr] = train(net,p(:,i:conf.train_samples),p(:,i:conf.train_samples));
    conf.tr = [conf.tr, tr];
end

i

conf.net = net;

%% Vytvorenie kompresnej siete (tvori ju vstupna a skryta vrstva povodnej siete).
if (conf.morelayers_enabled)
    enc_net = newff(p,p(1:conf.hidden_sz,:),[conf.hidden_sz_1],{conf.hidden_train_1,'tansig'},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
else
    enc_net = newff(p,p(1:conf.hidden_sz,:),[],{'tansig'},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
end

enc_net = init(enc_net);
enc_net.IW{1,1} = net.IW{1,1};
enc_net.b{1,1} = net.b{1,1};

if (conf.morelayers_enabled)
    enc_net.LW{2,1} = net.LW{2,1};
    enc_net.b{2,1} = net.b{2,1};
end

conf.enc_net = enc_net;

%% Vytvorenie dekompresnej siete (tvori ju vystupna vrstva povodnej siete).
if (conf.morelayers_enabled)
    dec_net = newff(p(1:conf.hidden_sz,:),p,[conf.hidden_sz_2],{conf.hidden_train_2,'purelin'},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
    dec_net = init(dec_net);
    dec_net.IW{1,1} = net.LW{3,2};
    dec_net.b{1,1} = net.b{3,1};

    dec_net.LW{2,1} = net.LW{4,3};
    dec_net.b{2,1} = net.b{4,1};
else
    dec_net = newff(p(1:conf.hidden_sz,:),p,[],{'purelin'},conf.trainParam.algo,'learngdm','mse',{'fixunknowns','removeconstantrows'},{'removeconstantrows'});
    dec_net = init(dec_net);
    dec_net.IW{1,1} = net.LW{2,1};
    dec_net.b{1,1} = net.b{2,1};
end
    
conf.dec_net = dec_net;

conf.p = [conf.p, p];

disp('DONE: training network...');
toc;
conf.timestats.train = [conf.timestats.train, toc];

